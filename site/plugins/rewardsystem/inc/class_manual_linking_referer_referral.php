<?php
/*
 * Manual Linking Tab Settings
 *
 */

class FPRewardSystemManualLinking {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_manual_linking'] = __('Manual Referral Linking', 'rewardsystem');
        return $settings_tabs;
    }

    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        return apply_filters('woocommerce_rewardsystem_point_encash_settings', array(
            array(
                'type' => 'rs_user_role_dynamics_manual',
            ),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemManualLinking::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemManualLinking::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemManualLinking::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

    public static function reward_system_add_manual_table_to_action() {
        global $woocommerce;
        wp_nonce_field(plugin_basename(__FILE__), 'rsdynamicrulecreation_manual');
        global $woocommerce;
//        $user_list_manual_linking = get_users();
//        // Array of stdClass objects.
//        foreach ($user_list_manual_linking as $user) {
//            $separate_user_manual_name[] = $user->display_name;
//            $seperate_userid_manual_id[] = $user->ID;
//        }
//
//        $newcombineddatas_manual_linking = array_combine((array) $seperate_userid_manual_id, (array) $separate_user_manual_name);
//        //var_dump($newcombineddatas_manual_linking);
        ?>
        <style type="text/css">
            .rs_manual_linking_referral{
                width:60%;
            }
            .rs_manual_linking_referer{
                width:60%;
            }
            .chosen-container-single {
                position:absolute;
            }

        </style>
        <script type="text/javascript">
        <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                jQuery(function () {
                    // Ajax Chosen Product Selectors
                    jQuery("select.rs_manual_linking_referer").ajaxChosen({
                        method: 'GET',
                        url: '<?php echo admin_url('admin-ajax.php'); ?>',
                        dataType: 'json',
                        afterTypeDelay: 100,
                        data: {
                            action: 'woocommerce_json_search_customers',
                            security: '<?php echo wp_create_nonce("search-customers"); ?>'
                        }
                    }, function (data) {
                        var terms = {};

                        jQuery.each(data, function (i, val) {
                            terms[i] = val;
                        });
                        return terms;
                    });
                });
                jQuery(function () {
                    // Ajax Chosen Product Selectors
                    jQuery("select.rs_manual_linking_referral").ajaxChosen({
                        method: 'GET',
                        url: '<?php echo admin_url('admin-ajax.php'); ?>',
                        dataType: 'json',
                        afterTypeDelay: 100,
                        data: {
                            action: 'woocommerce_json_search_customers',
                            security: '<?php echo wp_create_nonce("search-customers"); ?>'
                        }
                    }, function (data) {
                        var terms = {};

                        jQuery.each(data, function (i, val) {
                            terms[i] = val;
                        });
                        return terms;
                    });
                });
                jQuery(document).ready(function () {
                    //jQuery(".rs_manual_linking_referer").chosen();
                });
        <?php } ?>
        </script>
        <table class="widefat fixed rsdynamicrulecreation_manual" cellspacing="0">
            <thead>
                <tr>

                    <th class="manage-column column-columnname" scope="col"><?php _e('Referer Username', 'rewardsystem'); ?></th>
                    <th class="manage-column column-columnname" scope="col"><?php _e('Buyer Username', 'rewardsystem'); ?></th>
                    <th class="manage-column column-columnname num" scope="col"><?php _e('Remove Linking', 'rewardsystem'); ?></th>
                </tr>
            </thead>

            <tfoot>
                <tr>
                    <td></td>
                    <td></td>

                    <td class="manage-column column-columnname num" scope="col"> <span class="add button-primary"><?php _e('Add Linking', 'rewardsystem'); ?></span></td>
                </tr>
                <tr>

                    <th class="manage-column column-columnname" scope="col"><?php _e('Referer Username', 'rewardsystem'); ?></th>
                    <th class="manage-column column-columnname" scope="col"><?php _e('Buyer Username', 'rewardsystem'); ?></th>
                    <th class="manage-column column-columnname num" scope="col"><?php _e('Add Linking', 'rewardsystem'); ?></th>

                </tr>
            </tfoot>

            <tbody id="here">
                <?php
                $rewards_dynamic_rulerule_manual = get_option('rewards_dynamic_rule_manual');
                echo "<pre>";
                //var_dump($rewards_dynamic_rulerule_manual);
                echo "</pre>";
                $i = 0;
                if (is_array($rewards_dynamic_rulerule_manual)) {
                    foreach ($rewards_dynamic_rulerule_manual as $rewards_dynamic_rule) {
                        ?>
                        <tr>
                            <td class="column-columnname">
                                <p class="form-field">
                                    <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                                        <select name="rewards_dynamic_rule_manual[<?php echo $i; ?>][referer]" class="short rs_manual_linking_referer">
                                            <?php
                                            if ($rewards_dynamic_rule['referer'] != '') {
                                                $user = get_user_by('id', absint($rewards_dynamic_rule['referer']));
                                                echo '<option value="' . absint($user->ID) . '" ';
                                                selected(1, 1);
                                                echo '>' . esc_html($user->display_name) . ' (#' . absint($user->ID) . ' &ndash; ' . esc_html($user->user_email) . ')</option>';
                                            } else {
                                                ?>
                                                <option value=""></option>
                                                <?php
                                            }
                                            ?>

                                        </select>
                                    <?php } else { ?>
                                        <?php
                                        if ($rewards_dynamic_rule['referer'] != '') {
                                            $user_id = absint($rewards_dynamic_rule['referer']);
                                            $user = get_user_by('id', $user_id);
                                            $user_string = esc_html($user->display_name) . ' (#' . absint($user->ID) . ' &ndash; ' . esc_html($user->user_email);
                                        }
                                        ?>
                                        <input type="hidden" class="wc-customer-search" name="rewards_dynamic_rule_manual[<?php echo $i; ?>][referer]" data-placeholder="<?php _e('Search for a customer&hellip;', 'rewardsystem'); ?>" data-selected="<?php echo esc_attr($user_string); ?>" value="<?php echo $user_id; ?>" data-allow_clear="true" />

                                    <?php } ?>
                                                                                                                                                                                                                                          <!--                                    <input type="text" name="rewards_dynamic_rule_manual[<?php echo $i; ?>][referer]" class="short" value="<?php echo $rewards_dynamic_rule['referer']; ?>"/>-->
                                </p>
                            </td>
                            <td class="column-columnname">
                                <p class="form-field">
                                    <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                                        <select name="rewards_dynamic_rule_manual[<?php echo $i; ?>][refferal]" class="short rs_manual_linking_referral">
                                            <?php
                                            if ($rewards_dynamic_rule['refferal'] != '') {
                                                $user = get_user_by('id', absint($rewards_dynamic_rule['refferal']));
                                                echo '<option value="' . absint($user->ID) . '" ';
                                                selected(1, 1);
                                                echo '>' . esc_html($user->display_name) . ' (#' . absint($user->ID) . ' &ndash; ' . esc_html($user->user_email) . ')</option>';
                                            } else {
                                                ?>
                                                <option value=""></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    <?php } else { ?>
                                        <?php
                                        if ($rewards_dynamic_rule['refferal'] != '') {
                                            $user_id = absint($rewards_dynamic_rule['refferal']);
                                            $user = get_user_by('id', $user_id);
                                            $user_string = esc_html($user->display_name) . ' (#' . absint($user->ID) . ' &ndash; ' . esc_html($user->user_email);
                                        }
                                        ?>
                                        <input type="hidden" class="wc-customer-search" name="rewards_dynamic_rule_manual[<?php echo $i; ?>][refferal]" data-placeholder="<?php _e('Search for a customer&hellip;', 'rewardsystem'); ?>" data-selected="<?php echo esc_attr($user_string); ?>" value="<?php echo $user_id; ?>" data-allow_clear="true" />
                                    <?php } ?>
                <!--                                    <input type="text" name="rewards_dynamic_rule_manual[<?php echo $i; ?>][refferal]" id="rewards_dynamic_ruleamount_manual<?php echo $i; ?>" class="short" value="<?php echo $rewards_dynamic_rule['refferal']; ?>"/>-->
                                </p>
                            </td>
                            <td class="column-columnname num">
                                <span class="remove button-secondary"><?php _e('Remove Linking', 'rewardsystem'); ?></span>
                            </td>
                        </tr>
                        <?php
                        $i = $i + 1;
                    }
                }
                ?>
            </tbody>
        </table>
        <script>
            jQuery(document).ready(function () {
                jQuery('#afterclick').hide();
                var countrewards_dynamic_rule = <?php echo $i; ?>;
                jQuery(".add").click(function () {
                    jQuery('#afterclick').show();
                    countrewards_dynamic_rule = countrewards_dynamic_rule + 1;
        <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>

                        jQuery('#here').append('<tr><td><p class="form-field"><select name="rewards_dynamic_rule_manual[' + countrewards_dynamic_rule + '][referer]" class="short rs_manual_linking_referer"><option value=""></option></select></p></td>\n\
                                                        \n\<td><p class="form-field"><select name="rewards_dynamic_rule_manual[' + countrewards_dynamic_rule + '][refferal]" class="short rs_manual_linking_referral"><option value=""></option></select></p></td>\n\
                                                        \n\\n\
                                                    \n\
                                                    <td class="num"><span class="remove button-secondary">Remove Linking</span></td></tr><hr>');
                        jQuery(function () {
                            // Ajax Chosen Product Selectors
                            jQuery("select.rs_manual_linking_referer").ajaxChosen({
                                method: 'GET',
                                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                                dataType: 'json',
                                afterTypeDelay: 100,
                                data: {
                                    action: 'woocommerce_json_search_customers',
                                    security: '<?php echo wp_create_nonce("search-customers"); ?>'
                                }
                            }, function (data) {
                                var terms = {};

                                jQuery.each(data, function (i, val) {
                                    terms[i] = val;
                                });
                                return terms;
                            });
                        });
                        jQuery(function () {
                            // Ajax Chosen Product Selectors
                            jQuery("select.rs_manual_linking_referral").ajaxChosen({
                                method: 'GET',
                                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                                dataType: 'json',
                                afterTypeDelay: 100,
                                data: {
                                    action: 'woocommerce_json_search_customers',
                                    security: '<?php echo wp_create_nonce("search-customers"); ?>'
                                }
                            }, function (data) {
                                var terms = {};

                                jQuery.each(data, function (i, val) {
                                    terms[i] = val;
                                });
                                return terms;
                            });
                        });
        <?php } else { ?>

                        jQuery('#here').append('<tr><td><p class="form-field"><input type="hidden" class="wc-customer-search" name="rewards_dynamic_rule_manual[' + countrewards_dynamic_rule + '][referer]" data-placeholder="<?php _e("Search for a customer&hellip;", "rewardsystem"); ?>" data-selected="" value="" data-allow_clear="true"/> </p></td>\n\
                                                        \n\<td><p class="form-field"><input type="hidden" class="wc-customer-search" name="rewards_dynamic_rule_manual[' + countrewards_dynamic_rule + '][refferal]" data-placeholder="<?php _e("Search for a customer&hellip;", "rewardsystem"); ?>" data-selected="" value="" data-allow_clear="true"/></p></td>\n\
                                                        \n\\n\
                                                    \n\
                                                    <td class="num"><span class="remove button-secondary">Remove Linking</span></td></tr><hr>');
                        jQuery('body').trigger('wc-enhanced-select-init');
        <?php } ?>
                    return false;
                });
                jQuery(document).on('click', '.remove', function () {
                    jQuery(this).parent().parent().remove();
                });
            });</script>

        <?php
        echo "<pre>";
        //var_dump(get_option('rewards_dynamic_rule_manual'));
        echo "</pre>";
        //var_dump(get_option('rewards_dynamic_rule_manual'));
    }

    public static function save_data_for_dynamic_rule_manual() {

        $rewards_dynamic_rulerule_manual = array_values($_POST['rewards_dynamic_rule_manual']);

        update_option('rewards_dynamic_rule_manual', $rewards_dynamic_rulerule_manual);
        return false;
    }

    public static function rs_get_referer_id_linking_rule($linkarray, $field, $value) {
        if (is_array($linkarray)) {
            foreach ($linkarray as $key => $eachreferer) {
                // echo $eachreferer[$field];
                if ($eachreferer[$field] == $value)
                    return $eachreferer['referer'];
            }
        }
        return FALSE;
    }

//        public static function testingbuyerchecker() {
//                $data = get_option('rewards_dynamic_rule_manual');
////            echo "<pre>";
////            var_dump($data);
////            echo "</pre>";
////            foreach($data as $key => $value) {
////                if($value['refferal']=='4')
////                    var_dump ($value['referer']);
////            }
//
//           //var_dump(self::rs_get_referer_id_linking_rule($data,'refferal','4'));
//            //var_dump(self::rs_perform_manual_link_referer('4'));
//        }

    public static function rs_perform_manual_link_referer($buyer_id) {
        $data = get_option('rewards_dynamic_rule_manual');
        return self::rs_get_referer_id_linking_rule($data, "refferal", $buyer_id);
    }

}

new FPRewardSystemManualLinking();


/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemManualLinking', 'reward_system_tab_settings'), 999);


// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_manual_linking', array('FPRewardSystemManualLinking', 'reward_system_update_settings'));


// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemManualLinking', 'reward_system_default_settings'));

add_action('admin_head', array('FPRewardSystemManualLinking', 'rs_perform_manual_link_referer'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_manual_linking', array('FPRewardSystemManualLinking', 'reward_system_register_admin_settings'));

add_action('woocommerce_admin_field_rs_user_role_dynamics_manual', array('FPRewardSystemManualLinking', 'reward_system_add_manual_table_to_action'));

//add_action('admin_head',array('FPRewardSystemManualLinking','save_data_for_dynamic_rule_manual'));
add_action('woocommerce_update_options_rewardsystem_manual_linking', array('FPRewardSystemManualLinking', 'save_data_for_dynamic_rule_manual'));


//For testing
//add_action('wp_head',array('FPRewardSystemManualLinking','testingbuyerchecker'));
?>