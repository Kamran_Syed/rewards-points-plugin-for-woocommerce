<?php
if (!class_exists('WP_List_Table')) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class FPRewardSystemEncashTabList extends WP_List_Table {

    function __construct() {
        global $status, $page;
        parent::__construct(array(
            'singular' => 'encashing_application',
            'plural' => 'encashing_applications',
            'ajax' => true
        ));
    }

    function column_default($item, $column_name) {
        return $item[$column_name];
    }

    function column_userloginname($item) {

        //Build row actions
        $actions = array(
          'edit' => sprintf('<a href="?page=rewardsystem_callback&tab=encash_applications_edit&encash_application_id=%s">Edit</a>', $item['id']),
            'delete' => sprintf('<a href="?page=%s&tab=%s&action=%s&id=%s">Delete</a>', $_REQUEST['page'], $_REQUEST['tab'], 'encash_application_delete', $item['id']),
        );

        //Return the title contents
        return sprintf('%1$s %3$s',
                /* $1%s */ $item['userloginname'],
                /* $2%s */ $item['id'],
                /* $3%s */ $this->row_actions($actions)
        );
    }

    function column_cb($item) {
        return sprintf(
                '<input type="checkbox" name="id[]" value="%s" />', $item['id']
        );
    }

    function get_columns() {
        $columns = array(
            'cb' => '<input type="checkbox" />', //Render a checkbox instead of text
            //'userid' => __('User ID', 'rewardsystem'),            
            'userloginname' => __('User Name', 'rewardsystem'),
            'encashercurrentpoints' => __('Current Points', 'rewardsystem'),
            'pointstoencash' => __('Points for Cash Back', 'rewardsystem'),
            'pointsconvertedvalue' => __('Amount for Cash Back '.get_woocommerce_currency_symbol(), 'rewardsystem'),
            'reasonforencash' => __('Reason for Cash Back', 'rewardsystem'),
            'paypalemailid' => __('Paypal Address ', 'rewardsystem'),
            'otherpaymentdetails'=> __('Other Payment Details','rewardsystem'),
            'status' => __('Application Status', 'rewardsystem'),
            'date' => __('Cash Back Requested Date', 'rewardsystem')
        );
        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            'userloginname' => array('userloginname', false), //true means it's already sorted
            'encashercurrentpoints'=> array('encashercurrentpoints', false),
            'pointstoencash' => array('pointstoencash', false),
            'pointsconvertedvalue' => array('pointsconvertedvalue', false),
            'reasonforencash' => array('reasonforencash', false),
            'paypalemailid' => array('paypalemailid', false),
            'otherpaymentdetails' => array('otherpaymentdetails', false),
            'status' => array('status', false),
            'date' => array('date', false)
        );
        return $sortable_columns;
    }

    function get_bulk_actions() {
        $actions = array(
            'encash_application_delete' => __('Delete', 'rewardsystem'),
            'rspaid' => __('Mark as Paid', 'rewardsystem'),
            'rsdue' => __('Mark as Due', 'rewardsystem'),
        );
        return $actions;
    }

    function process_bulk_action() {
        global $wpdb;
        $table_name = $wpdb->prefix . 'sumo_reward_encashing_submitted_data'; // do not forget about tables prefix

        if ('encash_application_delete' === $this->current_action()) {
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if (is_array($ids))
                $ids = implode(',', $ids);

            $mainids = explode(',', $ids);

            if (!empty($ids)) {
                $wpdb->query("DELETE FROM $table_name WHERE id IN($ids)");
            }
        } elseif ('rspaid' === $this->current_action()) {
            // var_dump($_REQUEST);
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if (is_array($ids))
                $ids = implode(',', $ids);

            if (!empty($ids)) {
                $ids = explode(',', $ids);
                $countids = count($ids);
                foreach ($ids as $eachid) {
                    $wpdb->update($table_name, array('status' => 'Paid'), array('id' => $eachid));
                    $message = __($countids . ' Status Changed to Paid', 'rewardsystem');
                }
                if (!empty($message)):
                    ?>
                    <div id="message" class="updated"><p><?php echo $message ?></p></div>
                    <?php
                endif;
            }
        } else {
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if (is_array($ids))
                $ids = implode(',', $ids);
            if (!empty($ids)) {
                $ids = explode(',', $ids);
                $countids = count($ids);
                foreach ($ids as $eachid) {
                    $wpdb->update($table_name, array('status' => 'Due'), array('id' => $eachid));

                    $message = __($countids . ' Status Changed to Due', 'multivendor');
                }
                if (!empty($message)):
                    ?>
                    <div id="message" class="updated"><p><?php echo $message ?></p></div>
                    <?php
                endif;
            }
        }
    }

    function extra_tablenav($which) {
        global $wpdb;
        $mainlistarray = array();
        $tablename = $wpdb->prefix . 'sumo_reward_encashing_submitted_data';
        if ($which == 'top') {
            ?>
            <input type="submit" class="button-primary" name="fprs_encash_export_csv_paypal" id="fprs_encash_export_csv_paypal" value="<?php _e('Export Due Points as CSV for Paypal Mass Payment', 'rewardsystem'); ?>"/>
            <input type="submit" class="button-primary" name="fprs_encash_export_csv_alldata" id="fprs_encash_export_csv_alldata" value="<?php _e('Export All Cash Back Requests', 'rewardsystem'); ?>"/>
            <?php
            $getallresults = $wpdb->get_results("SELECT * FROM $tablename WHERE status='Due'", ARRAY_A);

            if (isset($getallresults)) {
                foreach ($getallresults as $value) {
                    // var_dump($value['id']);
                    if ($value['pointstoencash'] != '' && $value['paypalemailid'] != '') {
                        $mainlistarray_paypal[] = array($value['paypalemailid'], $value['pointsconvertedvalue'], get_woocommerce_currency(), $value['userid'], get_option('rs_encashing_paypal_custom_notes'));
                    }
                }
                if (isset($_POST['fprs_encash_export_csv_paypal'])) {
                    ob_end_clean();
                    header("Content-type: text/csv");
                    header("Content-Disposition: attachment; filename=sumoreward_cashback_paypal" . date("Y-m-d H:i:s") . ".csv");
                    header("Pragma: no-cache");
                    header("Expires: 0");
                    $output = fopen("php://output", "w");
                    foreach ($mainlistarray_paypal as $row) {
                        fputcsv($output, $row); // here you can change delimiter/enclosure
                    }
                    fclose($output);
                    exit();
                }
            }
                        
            if (isset($getallresults)) {
                foreach ($getallresults as $allvalue) {
                    // var_dump($allvalue['id']);
                    if ($allvalue['pointstoencash'] != '' ) {
                        $mainlistarray_alldata_heading = "User Name,Current Points,Points for Cash Back, Currency Code, Amount for Cash Back,Reason for Encashing,Paypal Address, Other Payment Details,Application Status,Cash Back Requested Date" ."\n";
                        $mainlistarray_alldata[] = array($allvalue['userloginname'], $allvalue['encashercurrentpoints'], $allvalue['pointstoencash'], get_woocommerce_currency(), $allvalue['pointsconvertedvalue'], $allvalue['reasonforencash'], $allvalue['paypalemailid'], $allvalue['otherpaymentdetails'], $allvalue['status'], $allvalue['date']);
                    }
                }                
                if (isset($_POST['fprs_encash_export_csv_alldata'])) {
                    ob_end_clean();
                    echo $mainlistarray_alldata_heading;
                    header("Content-type: text/csv");
                    header("Content-Disposition: attachment; filename=sumoreward_cashback_alldata" . date("Y-m-d H:i:s") . ".csv");
                    header("Pragma: no-cache");
                    header("Expires: 0");
                    $output = fopen("php://output", "w");
                    foreach ($mainlistarray_alldata as $row) {
                        fputcsv($output, $row); // here you can change delimiter/enclosure
                    }
                    fclose($output);
                    exit();
                }                
            }                        
        }
    }
          
    function prepare_items() {
        global $wpdb;
        $table_name = $wpdb->prefix . 'sumo_reward_encashing_submitted_data'; // do not forget about tables prefix

        $per_page = 10; // constant, how much records will be shown per page

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        // here we configure table headers, defined in our methods
        $this->_column_headers = array($columns, $hidden, $sortable);

        // [OPTIONAL] process bulk action if any
        $this->process_bulk_action();

        // will be used in pagination settings
        $total_items = $wpdb->get_var("SELECT COUNT(id) FROM $table_name");

        // prepare query params, as usual current page, order by and order direction
        $paged = isset($_REQUEST['paged']) ? max(0, intval($_REQUEST['paged']) - 1) : 0;
        $orderby = (isset($_REQUEST['orderby']) && in_array($_REQUEST['orderby'], array_keys($this->get_sortable_columns()))) ? $_REQUEST['orderby'] : 'id';
        $order = (isset($_REQUEST['order']) && in_array($_REQUEST['order'], array('asc', 'desc'))) ? $_REQUEST['order'] : 'asc';

        // [REQUIRED] define $items array
        // notice that last argument is ARRAY_A, so we will retrieve array
        $this->items = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name ORDER BY $orderby $order LIMIT %d OFFSET %d", $per_page, $paged), ARRAY_A);

        // [REQUIRED] configure pagination
        $this->set_pagination_args(array(
            'total_items' => $total_items, // total items defined above
            'per_page' => $per_page, // per page constant defined at top of method
            'total_pages' => ceil($total_items / $per_page) // calculate pages count
        ));
    }

}
