<?php

class FPRewardSystemCouponPoints {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_couponpointstab'] = __('Coupon Reward Points', 'rewardsystem');
        return $settings_tabs;
    }

    // Add Admin Fields in the Array Format
    /**
     * Rewardsystem Add Custom Field to the Rewardsystem Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        return apply_filters('woocommerce_rewardsystem_couponpoints_settings', array(
            array(
                'name' => __('Coupon Reward Points Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Here you can Reward your users with Reward Points for using Coupon Codes', 'rewardsystem'),
                'id' => '_rs_reward_point_coupon_reward_settings'
            ),
            array(
                'name' => __('Priority Level Selection', 'rewardsystem'),
                'desc' => __('If more than one type(level) is enabled then use the highest/lowest points for the Coupons ', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_choose_priority_level_selection_coupon_points',
                'class' => 'rs_choose_priority_level_selection_coupon_points',
                'std' => '1',
                'type' => 'radio',
                'newids' => 'rs_choose_priority_level_selection_coupon_points',
                'options' => array(
                    '1' => __('Use Highest Reward Points for the Coupon Codes', 'rewardsystem'),
                    '2' => __('Use Lowest Reward Points  for the Coupon Codes', 'rewardsystem'),
                ),
                'desc_tip' => true,
            ),
            array(
                'name' => __('Coupon Reward Points Message Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('', 'rewardsystem'),
                'id' => '_rs_reward_point_coupon_message_settings'
            ),
            array(
                'name' => __('Coupon Applied Success Message', 'rewardsystem'),
                'desc' => __('This messgae will be displayed when The User applies the Selected coupons for Reward Points ', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_coupon_applied_reward_success',
                'css' => 'min-width:550px;',
                'std' => 'You have recieved [coupon_rewardpoints] Points for using the coupon [coupon_name]',
                'type' => 'textarea',
                'newids' => 'rs_coupon_applied_reward_success',
                'desc_tip' => true,
            ),
            array(
                'type' => 'rs_coupon_usage_points_dynamics',
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_point_coupon_reward_settings'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Rewardsystem in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemCouponPoints::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in Rewardsystem
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemCouponPoints::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemCouponPoints::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

    public static function reward_add_coupon_usage_points_to_action() {
        wp_nonce_field(plugin_basename(__FILE__), 'rsdynamicrulecreation_coupon_usage');
        global $woocommerce;
        ?>
        <style type="text/css">
            .coupon_code_points_selected{
                width: 60%!important;
            }
            .coupon_code_points{
                width: 60%!important;
            }
            .chosen-container-multi {
                position:absolute!important;
            }


        </style>
        <table class="widefat fixed rsdynamicrulecreation_coupon_usage" cellspacing="0">
            <thead>
                <tr>

                    <th class="manage-column column-columnname" scope="col"><?php _e('Coupon Codes', 'rewardsystem'); ?></th>
                    <th class="manage-column column-columnname" scope="col"><?php _e('Reward Points', 'rewardsystem'); ?></th>
                    <th class="manage-column column-columnname num" scope="col"><?php _e('Remove Linking', 'rewardsystem'); ?></th>
                </tr>
            </thead>
            <tbody id="here">
                <?php
                $rewards_dynamic_rulerule_coupon_points = get_option('rewards_dynamic_rule_couponpoints');

                $i = 0;
                if (is_array($rewards_dynamic_rulerule_coupon_points)) {
                    foreach ($rewards_dynamic_rulerule_coupon_points as $rewards_dynamic_rule) {
                        //var_dump($rewards_dynamic_rule["coupon_codes"]);
                        ?>
                        <tr>
                            <td class="column-columnname">
                                <p class="form-field">
                                    <select multiple="multiple" name="rewards_dynamic_rule_coupon_usage[<?php echo $i; ?>][coupon_codes][]" class="short coupon_code_points_selected">
                                        <?php
                                        if ($rewards_dynamic_rule["coupon_codes"] != "") {
                                            $coupons_list = $rewards_dynamic_rule["coupon_codes"];
                                            foreach ($coupons_list as $separate_coupons) {
                                                ?>
                                                <option value="<?php echo $separate_coupons; ?>" selected><?php echo $separate_coupons; ?></option>
                                                <?php
                                            }
                                            foreach (get_posts('post_type=shop_coupon') as $value) {
                                                $coupon_title = $value->post_title;
                                                $coupon_object = new WC_Coupon($coupon_title);
                                                if ($coupon_object->is_valid()) {
                                                    if (!in_array($coupon_title, $coupons_list)) {
                                                        ?>
                                                        <option value="<?php echo $coupon_title; ?>"><?php echo $coupon_title; ?></option>
                                                        <?php
                                                    }
                                                }
                                            }
                                        }
                                        ?>
                                    </select>
                                </p>
                            </td>


                            <td class="column-columnname">
                                <p class="form-field">
                                    <input type="text" name="rewards_dynamic_rule_coupon_usage[<?php echo $i; ?>][reward_points]" value="<?php echo $rewards_dynamic_rule["reward_points"]; ?>" />
                                </p>
                            </td>
                            <td class="column-columnname num">
                                <span class="remove button-secondary"><?php _e('Remove Linking', 'rewardsystem'); ?></span>
                            </td>
                        </tr>


                        <?php
                        $i = $i + 1;
                    }
                }
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <td></td>
                    <td></td>

                    <td class="manage-column column-columnname num" scope="col"> <span class="add button-primary"><?php _e('Add Linking', 'rewardsystem'); ?></span></td>
                </tr>
                <tr>

                    <th class="manage-column column-columnname" scope="col"><?php _e('Coupon Codes', 'rewardsystem'); ?></th>
                    <th class="manage-column column-columnname" scope="col"><?php _e('Reward Points', 'rewardsystem'); ?></th>
                    <th class="manage-column column-columnname num" scope="col"><?php _e('Add Linking', 'rewardsystem'); ?></th>

                </tr>
            </tfoot>
        </table>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery('#afterclick').hide();
                var countrewards_dynamic_rule = <?php echo $i; ?>;
                jQuery(".add").click(function () {
                    jQuery('#afterclick').show();
                    countrewards_dynamic_rule = countrewards_dynamic_rule + 1;
                    jQuery('#here').append('<tr><td><p class="form-field"><select multiple="multiple" name="rewards_dynamic_rule_coupon_usage[' + countrewards_dynamic_rule + '][coupon_codes][]" class="short coupon_points coupon_code_points"><?php
        foreach (get_posts('post_type=shop_coupon') as $value) {
            $coupon_title = $value->post_title;
            $coupon_object = new WC_Coupon($coupon_title);

            if ($coupon_object->is_valid()) {
                ?><option value="<?php echo $coupon_title; ?>"><?php echo $coupon_title; ?><?php
            }
        }
        ?></option></select></p></td>\n\
        \n\<td><p class="form-field"><input type = "text" name="rewards_dynamic_rule_coupon_usage[' + countrewards_dynamic_rule + '][reward_points]" class="short " /></p></td>\n\
        \n\\n\
        \n\
        <td class="num"><span class="remove button-secondary">Remove Linking</span></td></tr><hr>');
        <?php if ((float) $woocommerce->version > (float) ('2.2.0')) { ?>
                        jQuery('.coupon_code_points').select2();
        <?php } else { ?>
                        jQuery('.coupon_code_points').chosen();
        <?php } ?>
                });

                jQuery(document).on('click', '.remove', function () {
                    jQuery(this).parent().parent().remove();
                });
        <?php if ((float) $woocommerce->version > (float) ('2.2.0')) { ?>
                    jQuery('.coupon_code_points_selected').select2();
        <?php } else { ?>
                    jQuery('.coupon_code_points_selected').chosen();
        <?php } ?>
            });



        </script>

        <?php
        //var_dump(get_option('rewards_dynamic_rule_couponpoints'));
    }

    public static function multi_dimensional_descending_sort_coupon_points($arr, $index) {
        $b = array();
        $c = array();
        if (is_array($arr)) {
            foreach ($arr as $key => $value) {
                $b[$key] = $value[$index];
            }
            arsort($b);
            // var_dump($b);
            foreach ($b as $key => $value) {
                //echo $key;
                $c[] = $arr[$key];
            }
//var_dump($c);

            return $c;
        }
    }

    public static function find_coupon_values($couponcode, $applied_coupons_cart) {

        if (is_array($applied_coupons_cart)) {
            if (in_array($couponcode, $applied_coupons_cart)) {
                return "1";
            }
        }
    }

    public static function display_message_coupon_reward_points() {
        global $woocommerce;
        //$coupons_for_points_rule_list = RSUserRoleRewardPoints::multi_dimensional_sort(get_option('rewards_dynamic_rule_couponpoints'), 'reward_points');
        if (get_option('rs_choose_priority_level_selection_coupon_points') == '1') {
            $coupons_for_points_rule_list = self::multi_dimensional_descending_sort_coupon_points(get_option('rewards_dynamic_rule_couponpoints'), 'reward_points');
        } else {
            $coupons_for_points_rule_list = RSUserRoleRewardPoints::multi_dimensional_sort(get_option('rewards_dynamic_rule_couponpoints'), 'reward_points');
        }
//$coupons_for_points_rule_list = get_option('rewards_dynamic_rule_couponpoints');
        // $coupons_for_points_rule_list = array_map("unserialize", array_unique(array_map("serialize", $coupons_for_points_rule_list)));
//
//
//
//        echo "<pre>";
//        var_dump($coupons_for_points_rule_list);
//        echo "</pre>";
        $getthedatas = array();
        if (is_array($coupons_for_points_rule_list)) {
            foreach ($coupons_for_points_rule_list as $key => $value) {
                // var_dump($value);
                if (!in_array($value['coupon_codes'], $getthedatas)) {
                    $getthedatas[$key] = $value['coupon_codes'];
                }
            }
        }
        $c = array();
        foreach ($getthedatas as $key => $mainvalue) {
            $c[] = $coupons_for_points_rule_list[$key];
        }



        foreach ($c as $coupons_for_points_each_rule) {
            $rule_created_coupons_list = $coupons_for_points_each_rule["coupon_codes"];

            $rule_created_coupons_points_list = $coupons_for_points_each_rule["reward_points"];

            //$rule_created_coupons_points_list = $coupons_for_points_each_rule["reward_points"];
            //var_dump(array_intersect($rule_created_coupons_list,$woocommerce->cart->applied_coupons));

            foreach ($rule_created_coupons_list as $separate_rule_coupons) {
                //   if (in_array($separate_rule_coupons, $woocommerce->cart->applied_coupons)) {
                $newfunctionchecker = self::find_coupon_values($separate_rule_coupons, $woocommerce->cart->applied_coupons);
                if ($newfunctionchecker == '1') {
                    $coupon_name_shortcode_to_find = "[coupon_name]";
                    $coupon_name_shortcode_to_replace = $separate_rule_coupons;
                    $coupon_name_shortcode_replaced = str_replace($coupon_name_shortcode_to_find, $coupon_name_shortcode_to_replace, get_option('rs_coupon_applied_reward_success'));
                    $coupon_reward_points_shortcode_to_find = "[coupon_rewardpoints]";
                    $coupon_reward_points_shortcode_to_replace = $rule_created_coupons_points_list;
                    $coupon_reward_points_shortcode_replaced = str_replace($coupon_reward_points_shortcode_to_find, $coupon_reward_points_shortcode_to_replace, $coupon_name_shortcode_replaced);
                    ?>
                    <div class="woocommerce-message">
                        <?php echo $coupon_reward_points_shortcode_replaced; ?>
                    </div>
                    <?php
                }
            }
        }
    }

    public static function apply_coupon_code_reward_points_user($order_id) {
        //$order_id = '337';
        $order = new WC_order($order_id);
        $coupons_applied_in_cart = '';
        if (get_option('rs_choose_priority_level_selection_coupon_points') == '1') {
            $coupons_for_points_rule_list = self::multi_dimensional_descending_sort_coupon_points(get_option('rewards_dynamic_rule_couponpoints'), 'reward_points');
        } else {
            $coupons_for_points_rule_list = RSUserRoleRewardPoints::multi_dimensional_sort(get_option('rewards_dynamic_rule_couponpoints'), 'reward_points');
        }

        $getthedatas = array();

        foreach ($coupons_for_points_rule_list as $key => $value) {
            // var_dump($value);
            if (!in_array($value['coupon_codes'], $getthedatas)) {
                $getthedatas[$key] = $value['coupon_codes'];
            }
        }
        $c = array();
        foreach ($getthedatas as $key => $mainvalue) {
            $c[] = $coupons_for_points_rule_list[$key];
        }

        $rewardpointscoupons = $order->get_items(array('coupon'));
        //var_dump($rewardpointscoupons);
        foreach ($rewardpointscoupons as $applied_coupons) {
            $coupons_applied_in_cart[] = $applied_coupons['name'];
            //var_dump($coupons_applied_in_cart);
        }

        foreach ($c as $coupons_for_points_each_rule) {
            global $woocommerce;
            $rule_created_coupons_list = $coupons_for_points_each_rule["coupon_codes"];

            $rule_created_coupons_points_list = $coupons_for_points_each_rule["reward_points"];

            //$rule_created_coupons_points_list = $coupons_for_points_each_rule["reward_points"];
            //var_dump(array_intersect($rule_created_coupons_list,$woocommerce->cart->applied_coupons));

            foreach ($rule_created_coupons_list as $separate_rule_coupons) {
                //   if (in_array($separate_rule_coupons, $woocommerce->cart->applied_coupons)) {
                //$newfunctionchecker = self::find_coupon_values($separate_rule_coupons, $woocommerce->cart->applied_coupons);
                //if ($newfunctionchecker == '1') {
                $coupon_name_shortcode_to_find = "[coupon_name]";
                $coupon_name_shortcode_to_replace = $separate_rule_coupons;
                //var_dump($coupon_name_shortcode_to_replace);
                //var_dump($coupon_name_shortcode_to_replace);
//                    $coupon_name_shortcode_replaced = str_replace($coupon_name_shortcode_to_find, $coupon_name_shortcode_to_replace, get_option('rs_coupon_applied_reward_success'));
//                    $coupon_reward_points_shortcode_to_find = "[coupon_rewardpoints]";
                $coupon_reward_points_to_update = $rule_created_coupons_points_list;
//                    $coupon_reward_points_shortcode_replaced = str_replace($coupon_reward_points_shortcode_to_find, $coupon_reward_points_shortcode_to_replace, $coupon_name_shortcode_replaced);

                $newfunctionchecker = self::find_coupon_values($separate_rule_coupons, $coupons_applied_in_cart);
                //  var_dump($newfunctionchecker);
                $boolvalue = false;
                if ((int) $newfunctionchecker == 1) {

//                           echo "googleer";
                    //var_dump($coupon_name_shortcode_to_replace.$coupon_reward_points_shortcode_to_replace);
                    //echo $coupon_name_shortcode_to_replace. " ". $coupon_reward_points_shortcode_to_replace."<br/>";
                    //update_user_meta($order->user_id, '_my_reward_points', $totalpoints_payment);
                    //$user_id = get_current_user_id();
                    //var_dump($coupon_reward_points_shortcode_to_replace);
                    $current_user_points = get_user_meta($order->user_id, '_my_reward_points', true);
                    //var_dump($current_user_points);
                    $updated_points = $current_user_points + $coupon_reward_points_to_update;
//                        var_dump($updated_points);
                    update_user_meta($order->user_id, '_my_reward_points', $updated_points);

                    $coupon_code_points_message = get_option('_rs_localize_coupon_reward_points_log');
                    $coupon_points_message_to_find = "{coupon_codes}";
                    $coupon_points_message_to_replace = '"' . $coupon_name_shortcode_to_replace . '"';
                    $post_url = admin_url('post.php?post=' . $order_id) . '&action=edit';
                    $coupon_points_order_id_find = "{order_id}";
                    $coupon_points_order_id_replace = '<a href="' . $post_url . '">#' . $order_id . '</a>';
                    $coupon_points_message_to_replaced1 = str_replace($coupon_points_message_to_find, $coupon_points_message_to_replace, $coupon_code_points_message);
                    $coupon_points_order_id_replaced2 = str_replace($coupon_points_order_id_find, $coupon_points_order_id_replace, $coupon_points_message_to_replaced1);
                    $pointslogs[] = array('orderid' => $order_id, 'userid' => $order->user_id, 'points_earned_order' => $coupon_reward_points_to_update, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $updated_points, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => $coupon_points_order_id_replaced2, 'rewarder_for_frontend' => $coupon_points_order_id_replaced2);
                    $overalllogs[] = array('userid' => $order->user_id, 'totalvalue' => $updated_points, 'eventname' => $coupon_points_order_id_replaced2, 'date' => date('Y-m-d H:i:s'));
//            $pointslogs[] = array('orderid' => '', 'userid' => '', 'points_earned_order' => '23', 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => '10034', 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => "HI this is for testing", 'rewarder_for_frontend' => "Main Testing");
//            $overalllogs[] = array('userid' => '', 'totalvalue' => '23', 'eventname' => "testing", 'date' => date('Y-m-d H:i:s'));

                    FPRewardSystem::save_total_earned_points($order->user_id, $coupon_reward_points_to_update);
                    $boolvalue = true;
                }


                //}
            }
        }
        if ($boolvalue == true) {
            $getoveralllogs = get_option('rsoveralllog');
            $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
            update_option('rsoveralllog', $logmerges);
            $getmypointss = get_user_meta($order->user_id, '_my_points_log', true);
            $mergeds = array_merge((array) $getmypointss, $pointslogs);
            update_user_meta($order->user_id, '_my_points_log', $mergeds);
        }


        //  $totalpoints = $getearnedpoints + $getpoints;
        //    update_user_meta($_POST['currentuserid'], '_my_reward_points', $totalpoints);
        //$mycurrentpoints = get_user_meta($_POST['currentuserid'], '_my_reward_points', true);
//            $getoveralllogs_coupon_points = get_option('rsoveralllog');
//            $logmerges_coupon_points = array_merge((array) $getoveralllogs_coupon_points, $overalllogs);
//            update_option('rsoveralllog', $logmerges_coupon_points);
//            $getmypointss_coupon_points = get_user_meta($order->user_id, '_my_points_log', true);
//            $mergeds_payment = array_merge((array) $getmypointss_coupon_points, $pointslogs_coupons);
//           // var_dump($mergeds_payment);
//            //var_dump($order->user_id);
//            update_user_meta($order->user_id, '_my_points_log', $mergeds_payment);
    }

    public static function save_data_for_dynamic_rule_coupon_points() {
        $rewards_dynamic_rulerule_couponpoints = array_values($_POST['rewards_dynamic_rule_coupon_usage']);
        update_option('rewards_dynamic_rule_couponpoints', $rewards_dynamic_rulerule_couponpoints);
        return false;
    }

    public static function add_chosen_to_coupon_points_tab() {
        global $woocommerce;

        if (isset($_GET['page'])) {
            if ($_GET['page'] == 'rewardsystem_callback') {
                if ((float) $woocommerce->version > (float) ('2.2.0')) {
                    ?>
                    <script type="text/javascript">
                        jQuery("document").ready(function () {
                            jQuery('.coupon_points').select2();
                        });
                    </script>
                <?php } else { ?>
                    <script type="text/javascript">
                        jQuery("document").ready(function () {
                            jQuery('.coupon_points').chosen();
                        });
                    </script>
                    <?php
                }
            }
        }
    }

}

new FPRewardSystemCouponPoints();


/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemCouponPoints', 'reward_system_tab_settings'), 999);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_couponpointstab', array('FPRewardSystemCouponPoints', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemCouponPoints', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_couponpointstab', array('FPRewardSystemCouponPoints', 'reward_system_register_admin_settings'));


add_action('woocommerce_admin_field_rs_coupon_usage_points_dynamics', array('FPRewardSystemCouponPoints', 'reward_add_coupon_usage_points_to_action'));

add_action('admin_head', array('FPRewardSystemCouponPoints', 'add_chosen_to_coupon_points_tab'));


add_action('woocommerce_update_options_rewardsystem_couponpointstab', array('FPRewardSystemCouponPoints', 'save_data_for_dynamic_rule_coupon_points'));

//add_action('woocommerce_applied_coupon',  array('FPRewardSystemCouponPoints','display_message_coupon_reward_points'));
add_action('woocommerce_before_cart_table', array('FPRewardSystemCouponPoints', 'display_message_coupon_reward_points'), 999);
add_action('woocommerce_before_checkout_form', array('FPRewardSystemCouponPoints', 'display_message_coupon_reward_points'), 999);

//add_action('wp_head',  array('FPRewardSystemCouponPoints','apply_coupon_code_reward_points_user'));

function delete_log() {
    delete_option('rsoveralllog');
}

//add_action('admin_head','delete_log');