<?php 

$rewardpointscoupons = $order->get_items(array('coupon'));
                    $getuserdatabyid = get_user_by('id', $order->user_id);
                    $getusernickname = $getuserdatabyid->user_login;
                    $maincouponchecker = 'sumo_' . strtolower($getusernickname);
                    foreach ($rewardpointscoupons as $couponcodeser => $value) {
                        if ($maincouponchecker == $value['name']) {
                            update_user_meta($order->user_id, 'rsfirsttime_redeemed', 1);
                            $getuserdatabyid = get_user_by('id', $order->user_id);
                            $getusernickname = $getuserdatabyid->user_login;
                            $getcouponid = get_user_meta($order->user_id, 'redeemcouponids', true);
                            $currentamount = get_post_meta($getcouponid, 'coupon_amount', true);
                            //update_option('testings', $order->get_total_discount());
                            if ($currentamount >= $value['discount_amount']) {
                                $current_conversion = get_option('rs_redeem_point');
                                $point_amount = get_option('rs_redeem_point_value');
                                $redeemedamount = $value['discount_amount'] * $current_conversion;
                                $redeemedpoints = $redeemedamount / $point_amount;

                                $redeemloginformation = get_option('_rs_localize_points_redeemed_towards_purchase');
                                $startarray = array('{currentorderid}');
                                $endarray = array($order_id);

                                $mainupdatedvaluesinarray = str_replace($startarray, $endarray, $redeemloginformation);
                                $overalllog[] = array('userid' => $order->user_id, 'totalvalue' => $redeemedpoints, 'eventname' => $mainupdatedvaluesinarray, 'date' => $order->order_date);
                                $getoveralllog = get_option('rsoveralllog');
                                $logmerge = array_merge((array) $getoveralllog, $overalllog);
                                update_option('rsoveralllog', $logmerge);
                                update_user_meta($order->user_id, '_redeemed_points', $redeemedpoints);
                                update_user_meta($order->user_id, '_redeemed_amount', $value['discount_amount']);
                                $yourpoints = get_user_meta($order->user_id, '_my_reward_points', true);
                                //update_user_meta($order->user_id, '_my_reward_points', $yourpoints - $redeemedpoints);
                            }
                        }
                    }