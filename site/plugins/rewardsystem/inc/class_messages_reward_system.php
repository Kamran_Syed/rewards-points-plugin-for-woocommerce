<?php

class FPRewardSystemMessages {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_messages'] = __('Messages', 'rewardsystem');
        return $settings_tabs;
    }

    public static function list_shortcodes() {
        ?>
        <strong>
            <pre>
                       For Single Product Page
                       [rewardpoints] --> Single Product Reward Points
                       [equalamount] --> Value of Earning Points

                       For Variable Product Page
                       [variationrewardpoints] --> Variable Product Reward Points
                       [variationpointsvalue] --> Value of Earning Points

                       For Cart/Checkout Page
                       [loginlink] --> Login Link for Non-Members
                       [rspoint] --> Points for Each Product in Cart
                       [carteachvalue] --> Points Value of Each Product in Cart
                       [totalrewards] --> Total Reward Points in Cart
                       [totalrewardsvalue] --> Total Reward Points Value in Cart
                       [userpoints]  --> Show User Points
                       [userpoints_value]  --> Show Value of User Points
                       [redeempoints]  --> Redeem Points
                       [redeemeduserpoints] --> Remaining User Points after Redeeming
                       {rssitelinkwithid} --> Link to Unsubscribe from Emails

            </pre>
        </strong>
        <?php

    }

    // Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        return apply_filters('woocommerce_rewardsystem_messages_settings', array(
            array(
                'name' => __('Reward System Message Customization ', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Customize your Message with Reward System', 'rewardsystem'),
                'id' => '_rs_reward_messages'
            ),
            array(
                'name' => __('List of Shortcodes', 'rewardsystem'),
                'type' => 'list_rs_shortcodes',
                'id' => '_rs_list_shortcodes',
            ),
            array(
                'name' => __('Success Message for Redeeming Reward Points on Cart', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed when Reward Points are redeemed in cart', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_success_coupon_message',
                'css' => 'min-width:550px;',
                'std' => 'Reward Points Successfully Added',
                'type' => 'text',
                'newids' => 'rs_success_coupon_message',
                'desc_tip' => true,
            ),            
            array(
                'name' => __('Show/Hide Message for Single Product Page as Points', 'rewardsystem'),
                'desc' => __('This helps to Show/Hide Message in Single Product Page', 'rewardsystem'),
                'id' => 'rs_show_hide_message_for_single_product',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_show_hide_message_for_single_product',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Message for Single Product Page as Points in Bar', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed on top of Single Product Page', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_message_for_single_product_point_rule',
                'css' => 'min-width:550px;',
                'std' => 'Purchase this Product and Earn [rewardpoints] Reward Points ([equalamount])',
                'type' => 'textarea',
                'newids' => 'rs_message_for_single_product_point_rule',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Show/Hide Earn Points Message for Shop Page ', 'rewardsystem'),
                'desc' => __('This helps to Show/Hide Message in Shop Page', 'rewardsystem'),
                'id' => 'rs_show_hide_message_for_shop_archive',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_show_hide_message_for_shop_archive',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Show/Hide Earn Points Message for Single Product Page', 'rewardsystem'),
                'desc' => __('This helps to Show/Hide Message in Single Product Page', 'rewardsystem'),
                'id' => 'rs_show_hide_message_for_shop_archive_single',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_show_hide_message_for_shop_archive_single',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Show/Hide Earn Points Message for Variations in Single Product Page', 'rewardsystem'),
                'desc' => __('This helps to Show/Hide Message for Variations in Single Product Page', 'rewardsystem'),
                'id' => 'rs_show_hide_message_for_variable_gift_message',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_show_hide_message_for_variable_gift_message',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Message Position for Shop Page and Single Product Page for Simple Product', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed on Shop Page and Single Product Page', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_message_position_for_single_simple_products',
                'css' => '',
                'std' => '1',
                'type' => 'select',
                'newids' => 'rs_message_position_for_single_simple_products',
                'options' => array(
                    '1' => __('Before Product Price', 'rewardsystem'),
                    '2' => __('After Product Price', 'rewardsystem'),
                ),
                'desc_tip' => true,
            ),
            array(
                'name' => __('Message for Shop Page and Single Product Page for Simple Product', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed on Shop Page and Single Product Page', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_message_for_single_products',
                'css' => 'min-width:550px;',
                'std' => 'Earn [rewardpoints] Reward Points',
                'type' => 'textarea',
                'newids' => 'rs_message_for_single_products',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Message for Variation Page of Variable Product', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed on Variation Page of Variable Product', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_message_for_single_product_variation',
                'css' => 'min-width:550px;',
                'std' => 'Earn [variationrewardpoints] Reward Points',
                'type' => 'textarea',
                'newids' => 'rs_message_for_single_product_variation',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Show/Hide Message for each Variant in Variable Product', 'rewardsystem'),
                'desc' => __('This helps to Show/Hide Message in Variable Product Page', 'rewardsystem'),
                'id' => 'rs_show_hide_message_for_variable_product',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_show_hide_message_for_variable_product',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Message for each Variant in Variable Product', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed on top of Variable Product', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_message_for_variation_products',
                'css' => 'min-width:550px;',
                'std' => 'Purchase this Product and Earn [variationrewardpoints] Reward Points ([variationpointsvalue])',
                'type' => 'textarea',
                'newids' => 'rs_message_for_variation_products',
                'desc_tip' => true,
            ),
//            array(
//                'name' => __('Message for Single Product Page as Product Percentage Rule', 'rewardsystem'),
//                'desc' => __('Change the Message that you want to customize', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_message_for_single_product_percent_rule',
//                'css' => 'min-width:550px;',
//                'std' => 'Reward Point for this Order with Percent [rewardpercent] as [rewardpoints]',
//                'type' => 'textarea',
//                'newids' => 'rs_message_for_single_product_percent_rule',
//                'desc_tip' => true,
//            ),
            array(
                'name' => __('Show/Hide Message for Guest in Cart Page', 'rewardsystem'),
                'desc' => __('This helps to Show/Hide Message for Guest in Cart/Checkout Page', 'rewardsystem'),
                'id' => 'rs_show_hide_message_for_guest',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_show_hide_message_for_guest',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Show/Hide Message for Guest in Checkout Page', 'rewardsystem'),
                'desc' => __('This helps to Show/Hide Message for Guest in Checkout Page', 'rewardsystem'),
                'id' => 'rs_show_hide_message_for_guest_checkout_page',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_show_hide_message_for_guest_checkout_page',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Message for Guest in Cart and Checkout Page', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed on top of Cart Page and Checkout Page for Guest', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_message_for_guest_in_cart',
                'css' => 'min-width:550px;',
                'std' => 'Earn Reward Points for Product Purchase, Product Review and Signup [loginlink]',
                'type' => 'textarea',
                'newids' => 'rs_message_for_guest_in_cart',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Show/Hide Reward Points Message for each Products in Cart Page', 'rewardsystem'),
                'desc' => __('This helps to Show/Hide Message for each Products in Cart Page', 'rewardsystem'),
                'id' => 'rs_show_hide_message_for_each_products',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_show_hide_message_for_each_products',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Show/Hide Reward Points Message for each Products in Checkout Page', 'rewardsystem'),
                'desc' => __('This helps to Show/Hide Message for each Products in Checkout Page', 'rewardsystem'),
                'id' => 'rs_show_hide_message_for_each_products_checkout_page',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_show_hide_message_for_each_products_checkout_page',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Message in Cart Page for each Products', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed in each Products added in the Cart', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_message_product_in_cart',
                'css' => 'min-width:550px;',
                'std' => 'Purchase [titleofproduct] and Earn <strong>[rspoint]</strong> Reward Points ([carteachvalue])',
                'type' => 'textarea',
                'newids' => 'rs_message_product_in_cart',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Show/Hide Total Reward Points Message in Cart Page', 'rewardsystem'),
                'desc' => __('This helps to Show/Hide Message for Total Reward Points in Cart Page', 'rewardsystem'),
                'id' => 'rs_show_hide_message_for_total_points',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_show_hide_message_for_total_points',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Show/Hide Total Reward Points Message in Checkout Page', 'rewardsystem'),
                'desc' => __('This helps to Show/Hide Message for Total Reward Points in Checkout Page', 'rewardsystem'),
                'id' => 'rs_show_hide_message_for_total_points_checkout_page',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_show_hide_message_for_total_points_checkout_page',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Message in Cart/Checkout Page for Completing the Total Purchase', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed on top of Cart Page and Checkout Page', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_message_total_price_in_cart',
                'css' => 'min-width:550px;',
                'std' => 'Complete the Purchase and Earn <strong>[totalrewards]</strong> Reward Points ([totalrewardsvalue])',
                'type' => 'textarea',
                'newids' => 'rs_message_total_price_in_cart',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Show/Hide Your Reward Points Message in Cart Page', 'rewardsystem'),
                'desc' => __('This helps to Show/Hide Message for My Rewards in Cart Page', 'rewardsystem'),
                'id' => 'rs_show_hide_message_for_my_rewards',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_show_hide_message_for_my_rewards',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Show/Hide Your Reward Points Message in Checkout Page', 'rewardsystem'),
                'desc' => __('This helps to Show/Hide Message for My Rewards in Checkout Page', 'rewardsystem'),
                'id' => 'rs_show_hide_message_for_my_rewards_checkout_page',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_show_hide_message_for_my_rewards_checkout_page',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Message in Cart/Checkout Page that display Your Reward Points', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed on top of Cart Page and Checkout Page with Your Reward Points ', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_message_user_points_in_cart',
                'css' => 'min-width:550px;',
                'std' => 'My Reward Points [userpoints] ([userpoints_value])',
                'type' => 'textarea',
                'newids' => 'rs_message_user_points_in_cart',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Show/Hide Redeemed Points Message in Cart Page', 'rewardsystem'),
                'desc' => __('This helps to Show/Hide Message for Redeemed Points Message in Cart Page', 'rewardsystem'),
                'id' => 'rs_show_hide_message_for_redeem_points',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_show_hide_message_for_redeem_points',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Show/Hide Redeemed Points Message in Checkout Page', 'rewardsystem'),
                'desc' => __('This helps to Show/Hide Message for Redeemed Points Message in Checkout Page', 'rewardsystem'),
                'id' => 'rs_show_hide_message_for_redeem_points_checkout_page',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_show_hide_message_for_redeem_points_checkout_page',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Message in Cart/Checkout Page that Display Redeeming Your Points', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed on top of Cart Page and Checkout Page', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_message_user_points_redeemed',
                'css' => 'min-width:550px;',
                'std' => '[redeempoints] Reward Points Redeemed. Balance [redeemeduserpoints] Reward Points',
                'type' => 'textarea',
                'newids' => 'rs_message_user_points_redeemed',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Show/Hide Payment Gateway Reward Points Message in Checkout Page', 'rewardsystem'),
                'desc' => __('This helps to Show/Hide Message for Payment Gateway Reward Points in Checkout Page', 'rewardsystem'),
                'id' => 'rs_show_hide_message_payment_gateway_reward_points',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_show_hide_message_payment_gateway_reward_points',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name'=>__('Error Message for Maximum Discount Type','rewardsystem'),
                'desc'=>__('Error Message for Maximum Discount Type','rewardsystem'),
                'id'=>'rs_errmsg_for_max_discount_type',
                'css' => 'min-width:550px;',
                'std'=>'Maximum Discount has been Limited to [percentage] %',
                'type'=>'textarea',
                'newids'=>'rs_errmsg_for_max_discount_type',
                'class'=>'rs_errmsg_for_max_discount_type',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Message for Payment Gateway Reward Points', 'rewardsystem'),
                'desc' => __('Enter the Message for Payment Gateway Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_message_payment_gateway_reward_points',
                'css' => 'min-width:550px;',
                'std' => 'Use this [paymentgatewaytitle] and Earn [paymentgatewaypoints] Reward Points',
                'type' => 'textarea',
                'newids' => 'rs_message_payment_gateway_reward_points',
                'desc_tip' => true,
            ),
            array(
                'name'=>__('Unsubscribe Link Message for Email','rewardsystem'),
                'desc'=>__('This link is to unsubscribe your email','rewardsystem'),
                'id'=>'rs_unsubscribe_link_for_email',
                'css' => 'min-width:550px;',
                'std'=>'If you want to unsubscribe your mail,click here...{rssitelinkwithid}',
                'type'=>'textarea',
                'newids'=>'rs_unsubscribe_link_for_email',
                'class'=>'rs_unsubscribe_link_for_email',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_messages'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemMessages::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemMessages::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemMessages::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

}

new FPRewardSystemMessages();

add_action('woocommerce_admin_field_list_rs_shortcodes', array('FPRewardSystemMessages', 'list_shortcodes'));
/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemMessages', 'reward_system_tab_settings'), 106);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_messages', array('FPRewardSystemMessages', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemMessages', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_messages', array('FPRewardSystemMessages', 'reward_system_register_admin_settings'));
?>
