<?php
class FPRewardSystemSms{
    
    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_pointssmstab'] = __('SMS', 'rewardsystem');
        return $settings_tabs;
    }
    
    /**
     * Registering Custom Field Admin Settings of Rewardsystem in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemSms::rewardsystem_admin_fields());
    }
    
    /**
     * Update the Settings on Save Changes may happen in Rewardsystem
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemSms::rewardsystem_admin_fields());
    }
    
    
    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemSms::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }
    
    // Add Admin Fields in the Array Format
    /**
     * Rewardsystem Add Custom Field to the Rewardsystem Admin Settings
     */
    public static function rewardsystem_admin_fields() {
     return apply_filters('woocommerce_rewardsystem_pointssmstab_settings', array(
         array(
                'name' => __('SMS Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Here you can choose the SMS Sending Api and Enter the Credentials to Send SMS', 'rewardsystem'),
                'id' => '_rs_reward_point_sms_settings'
            ),
         array(
                    'title' => __('Enable/Disable', 'woocommerce'),
                    'type' => 'checkbox',                    
                    'std' => 'no',
                    'id' => 'rs_enable_send_sms_to_user',
                    'desc' => __('Enable this checkbox to send SMS to your Users', 'rewardsystem'),
                    'newids' => 'rs_enable_send_sms_to_user',
                ),
         array(
                'name' => __('Select SMS API', 'rewardsystem'),
                'desc' => __('Here you can choose the sms sending APi', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_sms_sending_api_option',
                'css' => '',
                'std' => '1',
                'type' => 'radio',
                'options' => array('1' => 'Twilio', '2' => 'Nexmo'),
                'newids' => 'rs_sms_sending_api_option',
                'desc_tip' => true,
            ),
         array(
                    'title' => __('Send SMS for Earning Points', 'woocommerce'),
                    'type' => 'checkbox',                    
                    'std' => 'no',
                    'id' => 'rs_send_sms_earning_points',
                    'desc' => __('Enable this checkbox to send SMS When your User Earn Points', 'rewardsystem'),
                    'newids' => 'rs_send_sms_earning_points',
		  
                ),
         array(
                    'title' => __('Send SMS for Redeeming Points', 'woocommerce'),
                    'type' => 'checkbox',                    
                    'std' => 'no',
                    'id' => 'rs_send_sms_redeeming_points',
                    'desc' => __('Enable this checkbox to send SMS When your User Redeems Points', 'rewardsystem'),
                    'newids' => 'rs_send_sms_redeeming_points',
		  
                ),
         array(
                'name' => __('Twilio Account SID', 'rewardsystem'),
                'desc' => __('Enter Twilio Account Id', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_twilio_secret_account_id',
                'css' => 'min-width:550px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_twilio_secret_account_id',
                'desc_tip' => true,
            ),
         array(
                'name' => __('Twilio Account Auth Token', 'rewardsystem'),
                'desc' => __('Enter Twilio Auth Token', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_twilio_auth_token_id',
                'css' => 'min-width:550px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_twilio_auth_token_id',
                'desc_tip' => true,
            ),
         array(
                'name' => __('Twilio From Number', 'rewardsystem'),
                'desc' => __('Enter Twilio From Number', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_twilio_from_number',
                'css' => 'min-width:550px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_twilio_from_number',
                'desc_tip' => true,
            ),
         array(
                'name' => __('Nexmo Key', 'rewardsystem'),
                'desc' => __('Enter Nexmo Key', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_nexmo_key',
                'css' => 'min-width:550px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_nexmo_key',
                'desc_tip' => true,
            ),
         array(
                'name' => __('Nexmo  Secret', 'rewardsystem'),
                'desc' => __('Enter Nexmo Secret', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_nexmo_secret',
                'css' => 'min-width:550px;',
                'std' => '',
                'type' => 'text',
                'newids' => 'rs_nexmo_secret',
                'desc_tip' => true,
            ),
         array(
                'name' => __('SMS Content', 'rewardsystem'),
                'desc' => __('Enter the SMS Content Here', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_points_sms_content',
                'css' => 'min-width:550px;',
                'std' => 'Hi {username}, {rewardpoints} points is in your account use it to make discount {sitelink}',
                'type' => 'textarea',
                'newids' => 'rs_points_sms_content',
                'desc_tip' => true,
            ),
         array('type' => 'sectionend', 'id' => '_rs_reward_point_sms_settings'),
     ));
    }
    
    public static function display_credentials_for_smsapi(){
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                if ((jQuery('input[name=rs_sms_sending_api_option]:checked').val()) === '1') {
                    jQuery('#rs_nexmo_key').parent().parent().hide();
                    jQuery('#rs_nexmo_secret').parent().parent().hide();                
                    jQuery('#rs_twilio_secret_account_id').parent().parent().show();                
                    jQuery('#rs_twilio_auth_token_id').parent().parent().show();                
                    jQuery('#rs_twilio_from_number').parent().parent().show();                
                }else{
                    jQuery('#rs_nexmo_key').parent().parent().show();
                    jQuery('#rs_nexmo_secret').parent().parent().show();                
                    jQuery('#rs_twilio_secret_account_id').parent().parent().hide();                
                    jQuery('#rs_twilio_auth_token_id').parent().parent().hide();                
                    jQuery('#rs_twilio_from_number').parent().parent().hide();                
                } 
                jQuery('input[name=rs_sms_sending_api_option]:radio').change(function () {
                    if ((jQuery('input[name=rs_sms_sending_api_option]:checked').val()) === '1') {
                    jQuery('#rs_nexmo_key').parent().parent().hide();
                    jQuery('#rs_nexmo_secret').parent().parent().hide(); 
                    jQuery('#rs_twilio_secret_account_id').parent().parent().show();                
                    jQuery('#rs_twilio_auth_token_id').parent().parent().show();                
                    jQuery('#rs_twilio_from_number').parent().parent().show();
                }else{
                    jQuery('#rs_nexmo_key').parent().parent().show();
                    jQuery('#rs_nexmo_secret').parent().parent().show();                
                    jQuery('#rs_twilio_secret_account_id').parent().parent().toggle();                
                    jQuery('#rs_twilio_auth_token_id').parent().parent().toggle();                
                    jQuery('#rs_twilio_from_number').parent().parent().toggle();                
                } 
                });
            });
        </script>
        <?php
    }


    
    
    public static function send_sms_nexmo_api($order_id) {        
        global $woocommerce;
        include ( "NexmoMessage.php" );
        $order_id = new WC_order($order_id);
        $phone_number = get_user_meta($order_id->user_id, 'billing_phone', true);
        if (strpos($phone_number, '+') !== false) {
            echo "valid Phone number";
            $user_id = $order_id->user_id;
            $user_details = get_user_by('id', $user_id);
            $user_login = $user_details->user_login;
            $user_points = get_user_meta($user_id, '_my_reward_points', true);
            $url_to_click = site_url();

//            $banned_user_list = get_option('rs_banned-users_list');
//            if (!in_array($order_id->user_id, (array) $banned_user_list)) {
//                $getarrayofuserdata = get_userdata($order_id->user_id);
//                $banninguserrole = get_option('rs_banning_user_role');
//                if (!in_array(isset($getarrayofuserdata->roles[0]) ? $getarrayofuserdata->roles[0] : '0', (array) $banninguserrole)) {
                    $banning_type = FPRewardSystem::check_banning_type($order_id->user_id);        
                    if($banning_type!='earningonly'&&$banning_type!='both') {
                    $nexmo_sms = new NexmoMessage(get_option('rs_nexmo_key'), get_option('rs_nexmo_secret'));
                    $message_content = get_option('rs_points_sms_content');
                    $message_content_name_to_find = "{username}";
                    $message_content_name_to_replace = $user_login;
                    $message_content_points_to_find = "{rewardpoints}";
                    $message_content_points_to_replace = round($user_points);
                    $message_content_link_to_find = "{sitelink}";
                    $message_content_link_to_replace = $url_to_click;
                    $message_replaced_name = str_replace($message_content_name_to_find,$message_content_name_to_replace,$message_content);
                    $message_replaced_points = str_replace($message_content_points_to_find,$message_content_points_to_replace,$message_replaced_name);
                    $message_replaced_link = str_replace($message_content_link_to_find,$message_content_link_to_replace,$message_replaced_points);
                    //$custommessage = "Hi" . " " . $user_login . "," . round($user_points) . " " . "points is in your account use it to make discount" . " " . $url_to_click;
                    $info = $nexmo_sms->sendText($phone_number, 'Sumo Rewards', $message_replaced_link);
//                }
//            }
        }         
        }
    }
    
    public static function send_sms_twilio_api($order_id) {        
        global $woocommerce;
        require_once  "Twilio.php";
        $order_id = new WC_order($order_id);
        $phone_number = get_user_meta($order_id->user_id, 'billing_phone', true);
        if (strpos($phone_number, '+') !== false) {
            $user_id = $order_id->user_id;
            $user_details = get_user_by('id', $user_id);
            $user_login = $user_details->user_login;
            $user_points = get_user_meta($user_id, '_my_reward_points', true);
            $url_to_click = site_url();

//            $banned_user_list = get_option('rs_banned-users_list');
//            if (!in_array($order_id->user_id, (array) $banned_user_list)) {
//                $getarrayofuserdata = get_userdata($order_id->user_id);
//                $banninguserrole = get_option('rs_banning_user_role');
//                if (!in_array(isset($getarrayofuserdata->roles[0]) ? $getarrayofuserdata->roles[0] : '0', (array) $banninguserrole)) {
                    $banning_type = FPRewardSystem::check_banning_type($order_id->user_id);        
                    if($banning_type!='earningonly'&&$banning_type!='both') {
                    //echo "Test";
            $AccountSid = get_option('rs_twilio_secret_account_id');
            $AuthToken = get_option('rs_twilio_auth_token_id');
            $client = new Services_Twilio($AccountSid, $AuthToken);
            $people = array(
                $phone_number => $user_login,
            );
                    $message_content = get_option('rs_points_sms_content');
                    $message_content_name_to_find = "{username}";
                    $message_content_name_to_replace = $user_login;
                    $message_content_points_to_find = "{rewardpoints}";
                    $message_content_points_to_replace = round($user_points);
                    $message_content_link_to_find = "{sitelink}";
                    $message_content_link_to_replace = $url_to_click;
                    $message_replaced_name = str_replace($message_content_name_to_find,$message_content_name_to_replace,$message_content);
                    $message_replaced_points = str_replace($message_content_points_to_find,$message_content_points_to_replace,$message_replaced_name);
                    $message_replaced_link = str_replace($message_content_link_to_find,$message_content_link_to_replace,$message_replaced_points);
            //$custommessage =  "Hi" . " " . $user_login . "," . round($user_points) . " " . "points is in your account use it to make discount" . " " . $url_to_click;          
            foreach ($people as $number => $name) {
                $sms = $client->account->messages->sendMessage(
                        get_option('rs_twilio_from_number'),
    // the number we are sending to - Any phone number
                        $phone_number,
    // the sms body
                        $message_replaced_link
                );
                //echo "Sent message to $name";
            }
//                }
//            }
        }}
    }

}
new FPRewardSystemSms();
//add_action('wp_head', array('FPRewardSystemSms','send_sms_nexmo_api'));
/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemSms', 'reward_system_tab_settings'), 999);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_pointssmstab', array('FPRewardSystemSms', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemSms', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_pointssmstab', array('FPRewardSystemSms', 'reward_system_register_admin_settings'));

add_action('admin_head',array('FPRewardSystemSms','display_credentials_for_smsapi'));



//add_action('wp_head', array('FPRewardSystemSms','send_sms_twilio_api'));
//add_action('wp_head', array('FPRewardSystemSms','send_sms_nexmo_api'));

?>