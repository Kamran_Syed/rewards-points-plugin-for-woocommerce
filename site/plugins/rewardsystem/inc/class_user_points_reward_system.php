<?php

class FPRewardSystemUserPoints {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_userpoints'] = __('Master Log', 'rewardsystem');
        return $settings_tabs;
    }

    // Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        $user_list = get_users();
        foreach ($user_list as $user) {
            $separate_user_exportmasterlog[] = $user->display_name;
            $seperate_userid_exportmasterlog[] = $user->ID;
        }
        $newcombineddatas_export_masterlog = array_combine((array) $seperate_userid_exportmasterlog, (array) $separate_user_exportmasterlog);
        return apply_filters('woocommerce_rewardsystem_userpoints_settings', array(
            array(
                'name' => __('Export Master Log Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => '',
                'id' => '_rs_reward_system_export_masterlog_csv'
            ),
            array(
                'name' => __('Export Master Log for', 'rewardsystem'),
                'desc' => __('Here you can set whether to Export Master Log for All Users or Selected Users', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_export_import_masterlog_option',
                'css' => '',
                'std' => '1',
                'type' => 'radio',
                'options' => array('1' => 'All Users', '2' => 'Selected Users'),
                'newids' => 'rs_export_import_masterlog_option',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Select the users that you wish to Export Master Log', 'rewardsystem'),
                'desc' => __('Here you select the users to whom you wish to Export the Master Log', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_export_masterlog_users_list',
                'css' => 'min-width:400px;',
                'std' => '',
                'type' => 'multiselect',
                'options' => $newcombineddatas_export_masterlog,
                'newids' => 'rs_export_masterlog_users_list',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_system_export_masterlog_csv'),
            array(
                'type' => 'rs_masterlog',
            ),
            //array('type'=>'title'),
            array('type' => 'sectionend'),
        ));
    }

    public static function outputCSV($data) {
        $output = fopen("php://output", "w");
        // var_dump($data);
        foreach ($data as $row) {
            fputcsv($output, $row); // here you can change delimiter/enclosure
        }
        fclose($output);
    }

    public static function add_chosen_to_masterlog_tab() {
        global $woocommerce;
        if (isset($_GET['page'])) {
            if ($_GET['page'] == 'rewardsystem_callback') {
                ?>
                <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                    <script type="text/javascript">
                        jQuery(document).ready(function () {
                            jQuery('#rs_export_masterlog_users_list').chosen();
                        });
                    </script>
                    <?php
                } else {
                    ?>
                    <script type="text/javascript">
                        jQuery(document).ready(function () {
                            jQuery('#rs_export_masterlog_users_list').select2();
                        });
                    </script>
                    <?php
                }
            }
        }
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                if ((jQuery('input[name=rs_export_import_masterlog_option]:checked').val()) === '2') {
                    jQuery('#rs_export_masterlog_users_list').parent().parent().show();
                } else {
                    jQuery('#rs_export_masterlog_users_list').parent().parent().hide();
                }
                jQuery('input[name=rs_export_import_masterlog_option]:radio').change(function () {
                    jQuery('#rs_export_masterlog_users_list').parent().parent().toggle();
                });
                jQuery(document).ready(function () {
                    var selected_masterlog_option = jQuery('input[name="rs_export_import_masterlog_option"]').val();
                    var masterlog_data = {
                        action: "rs_export_masterlog_option",
                        export_masterlog_type: selected_masterlog_option,
                    };
                    jQuery.post('<?php echo admin_url('admin-ajax.php') ?>', masterlog_data, function (response) {
                        console.log('Got this from the server: ' + response);
                    });
                    jQuery('input[name="rs_export_import_masterlog_option"]').change(function () {
                        var selected_masterlog_option = jQuery(this).val();
                        var masterlog_data = {
                            action: "rs_export_masterlog_option",
                            export_masterlog_type: selected_masterlog_option,
                        };
                        jQuery.post('<?php echo admin_url('admin-ajax.php') ?>', masterlog_data, function (response) {
                            console.log('Got this from the server: ' + response);
                        });
                    });
                });
                jQuery(document).ready(function () {
                    jQuery('#rs_export_masterlog_users_list').change(function () {
                        var selected_users_mastelog = jQuery(this).val();
                        var selected_users_masterlog_param = {
                            action: "rs_list_of_users_masterlog_export",
                            selected_users_masterlog_export: selected_users_mastelog
                        };
                        jQuery.post('<?php echo admin_url('admin-ajax.php') ?>', selected_users_masterlog_param, function (response) {
                            console.log('Got this from the server: ' + response);
                        });
                    });
                });

            });
        </script>

        <?php
        $i = 1;
        $masterlog_export_selected_option = get_option('selected_user_type_masterlog');
        $list_users_masterlog_export = get_option('rs_selected_userlist_masterlog_export');
        $getusernickname = '';
        //var_dump($list_users_masterlog_export);
        if (is_array(get_option('rsoveralllog'))) {
            if ($masterlog_export_selected_option == '1') {
                foreach (get_option('rsoveralllog') as $exportmastervalue) {
                    if ($i % 2 != 0) {
                        $name = 'alternate';
                    } else {
                        $name = '';
                    }
                    if ($exportmastervalue != '') {
                        if (!empty($exportmastervalue['totalvalue'])) {
                            if (is_float($exportmastervalue['totalvalue'])) {
                                $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';                                
                                $total = round(number_format($exportmastervalue['totalvalue'], 2),$roundofftype);
                            } else {
                                $total = number_format($exportmastervalue['totalvalue']);
                            }
                        } else {
                            $total = $exportmastervalue['totalvalue'];
                        }

                        $getusernickname_masterlog_exp = get_user_meta($exportmastervalue['userid'], 'nickname', true);
                        if ($getusernickname == '') {
                            $getusernickname = $exportmastervalue['userid'];
                        }
                        $i++;
                        $export_masterlog_heading = "User Name,Points,Event,Date" . "\n";
                        $masterloglist[] = array($getusernickname_masterlog_exp, $total, $exportmastervalue['eventname'], $exportmastervalue['date']);
                    }
                }
            } else {
                //masterlog selected users
                if ($list_users_masterlog_export != NULL) {
                    foreach (get_option('rsoveralllog') as $exportmastervalue) {
                        if (in_array($exportmastervalue["userid"], $list_users_masterlog_export)) {
                            if ($i % 2 != 0) {
                                $name = 'alternate';
                            } else {
                                $name = '';
                            }
                            if ($exportmastervalue != '') {
                                if (!empty($exportmastervalue['totalvalue'])) {
                                    if (is_float($exportmastervalue['totalvalue'])) {
                                        $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';                                
                                        $total = round(number_format($exportmastervalue['totalvalue'], 2),$roundofftype);
                                    } else {
                                        $total = number_format($exportmastervalue['totalvalue']);
                                    }
                                } else {
                                    $total = $exportmastervalue['totalvalue'];
                                }

                                $getusernickname_masterlog_exp = get_user_meta($exportmastervalue['userid'], 'nickname', true);
                                if ($getusernickname == '') {
                                    $getusernickname = $exportmastervalue['userid'];
                                }
                                $i++;
                                $export_masterlog_heading = "User Name,Points,Event,Date" . "\n";
                                $masterloglist[] = array($getusernickname_masterlog_exp, $total, $exportmastervalue['eventname'], $exportmastervalue['date']);
                            }
                        }
                    }
                }
            }
        }

        if (isset($_POST['rs_export_master_log_csv'])) {
//var_dump($_POST['rs_export_user_points_csv']);
            ob_end_clean();
            echo $export_masterlog_heading;
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=reward_points_masterlog" . date("Y-m-d") . ".csv");
            header("Pragma: no-cache");
            header("Expires: 0");
            FPRewardSystemUserPoints::outputCSV($masterloglist);
            exit();
        }
    }

    public static function selected_users_for_export_masterlog_callback() {
        global $wpdb; // this is how you get access to the database
        $rs_selected_users_export_masterlog = $_POST['selected_users_masterlog_export'];
        update_option('rs_selected_userlist_masterlog_export', $rs_selected_users_export_masterlog);
    }

    public static function selected_option_masterlog_export_callback() {
        global $wpdb; // this is how you get access to the database
        if (isset($_POST['export_masterlog_type'])) {
            $export_masterloguser_type_value = $_POST['export_masterlog_type'];
            update_option('selected_user_type_masterlog', $export_masterloguser_type_value);
        }
        exit();
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemUserPoints::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemUserPoints::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemUserPoints::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

    public static function list_user_log() {
        echo "Testing";
    }

}

new FPRewardSystemUserPoints();


/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemUserPoints', 'reward_system_tab_settings'), 150);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_userpoints', array('FPRewardSystemUserPoints', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemUserPoints', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_userpoints', array('FPRewardSystemUserPoints', 'reward_system_register_admin_settings'));

add_action('woocommerce_admin_field_rs_masterlog', array('FPRewardSystem', 'list_all_points_log'));
if (isset($_GET['tab'])) {
    if ($_GET['tab'] == 'rewardsystem_userpoints') {
        add_action('admin_head', array('FPRewardSystemUserPoints', 'add_chosen_to_masterlog_tab'));
    }
}
add_action('wp_ajax_rs_export_masterlog_option', array('FPRewardSystemUserPoints', 'selected_option_masterlog_export_callback'));
add_action('wp_ajax_rs_list_of_users_masterlog_export', array('FPRewardSystemUserPoints', 'selected_users_for_export_masterlog_callback'));
?>
