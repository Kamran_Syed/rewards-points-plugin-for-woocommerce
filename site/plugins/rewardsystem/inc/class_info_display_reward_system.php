<?php

class FPRewardSystemInfoDisplay {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_infodisplay'] = __('Info Display', 'rewardsystem');
        return $settings_tabs;
    }

    // Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        return apply_filters('woocommerce_rewardsystem_info_display_settings', array(
//            array(
//                'name' => __('Add to Cart Button Settings', 'rewardsystem'),
//                'type' => 'title',
////                'desc' => 'Shortcode Available (Supported for Simple Products)<br> <pre> [cf_min_price] => Minimum Contribution </pre><pre> [cf_max_price] => Maximum Contribution </pre>',
//                'id' => '_cf_add_to_cart_button'
//            ),
//            array(
//                'name' => __('Add to Cart Button Label', 'rewardsystem'),
//                'desc' => __('Please Enter Add to cart Button Label to show', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'cf_add_to_cart_label',
//                'css' => 'min-width:550px;',
//                'std' => 'Contribute',
//                'type' => 'text',
//                'newids' => 'cf_add_to_cart_label',
//                'desc_tip' => true,
//            ),
//            array(
//                'name' => __('Redirect after Contribution', 'rewardsystem'),
//                'desc' => __('Please Select the place you want redirect after Contribution', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'cf_add_to_cart_redirection',
//                'css' => '',
//                'std' => '1',
//                'type' => 'radio',
//                'options' => array('1' => 'Checkout Page', '2' => 'Cart Page'),
//                'newids' => 'cf_add_to_cart_redirection',
//                'desc_tip' => true,
//            ),
//            array('type' => 'sectionend', 'id' => '_cf_add_to_cart_button'),
//            array(
//                'name' => __('Campaign Out of Stock Settings', 'rewardsystem'),
//                'type' => 'title',
//                'id' => '_cf_campaign_out_of_stock'
//            ),
//            array(
//                'name' => __('Out of Stock Label', 'rewardsystem'),
//                'desc' => __('Please Enter Out of Stock Label', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'cf_outofstock_label',
//                'css' => 'min-width:550px;',
//                'std' => 'Campaign Closed',
//                'type' => 'text',
//                'newids' => 'cf_outofstock_label',
//                'desc_tip' => true,
//            ),
//            array('type' => 'sectionend', 'id' => '_cf_campaign_out_of_stock'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemInfoDisplay::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemInfoDisplay::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemInfoDisplay::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

}

new FPRewardSystemInfoDisplay();


/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemInfoDisplay', 'reward_system_tab_settings'), 105);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_infodisplay', array('FPRewardSystemInfoDisplay', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemInfoDisplay', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_infodisplay', array('FPRewardSystemInfoDisplay', 'reward_system_register_admin_settings'));
?>