<?php
/*
 * User Role Reward Points Calculation
 */

/**
 * Initialize the Class Name
 */
class RSUserRoleRewardPoints {
    /* Construct the Object */

    public function __construct() {
        // add_action('wp_head', array($this, 'user_role_based_reward_points'));

        add_filter('woocommerce_rs_settings_tabs_array', array($this, 'reward_system_tab_settings'), 101);

// call the woocommerce_update_options_{slugname} to update the reward system
        add_action('woocommerce_update_options_rewardsystem_memberlevel', array($this, 'reward_system_update_settings'));

// call the init function to update the default settings on page load
        add_action('init', array($this, 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
        add_action('woocommerce_rs_settings_tabs_rewardsystem_memberlevel', array($this, 'reward_system_register_admin_settings'));

        add_filter("woocommerce_rewardsystem_memberlevel_settings", array($this, 'reward_system_add_settings_to_action'));

        add_action('woocommerce_admin_field_rs_user_role_dynamics', array($this, 'reward_system_add_table_to_action'));

        add_action('woocommerce_update_options_rewardsystem_memberlevel', array($this, 'save_data_for_dynamic_rule'));


        // add_action('init', array($this, 'call_add_free_product_to_cart'), 10);
        //add_action('init', array($this, 'add_product'), 1);
    }

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_memberlevel'] = __('Member Level', 'rewardsystem');
        return $settings_tabs;
    }

    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        return apply_filters('woocommerce_rewardsystem_memberlevel_settings', array(
            array(
                'name' => __('', 'rewardsystem'),
                'type' => 'title',
                'class' => '_rs_priority_level',
                'id' => '_rs_priority_level',
            ),
            array(
                'name' => __('Priority Level Selection', 'rewardsystem'),
                'desc' => __('If more than one type(level) is enabled then use the highest/lowest percentage', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_choose_priority_level_selection',
                'class' => 'rs_choose_priority_level_selection',
                'std' => '1',
                'type' => 'radio',
                'newids' => 'rs_choose_priority_level_selection',
                'options' => array(
                    '1' => __('Use the Level that gives Highest Percentage', 'rewardsystem'),
                    '2' => __('Use the Level that gives Lowest Percentage', 'rewardsystem'),
                ),
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_priority_level', 'class' => '_rs_priority_level'),
            array(
                'name' => __('Reward Points Earning Percentage By User Roles', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_user_role_reward_points',
            ),
            array(
                'name' => __('Member Level', 'rewardsystem'),
                'desc' => __('Choose User Role Based Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_enable_user_role_based_reward_points',
                'css' => 'min-width:150px;',
                'std' => 'yes',
                'type' => 'checkbox',
                'newids' => 'rs_enable_user_role_based_reward_points',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_user_role_reward_points'),
            array(
                'name' => __('Reward Points Eaning Percentage By Total Earned Points', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_member_level_earning_points',
            ),
            array(
                'name' => __('Earning Level', 'rewardsystem'),
                'desc' => __('Choose Earned Level based Total Earned Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_enable_earned_level_based_reward_points',
                'css' => 'min-width:150px;',
                'std' => 'no',
                'type' => 'checkbox',
                'newids' => 'rs_enable_earned_level_based_reward_points',
                'desc_tip' => true,
            ),
            array(
                'type' => 'rs_user_role_dynamics',
            ),
            array('type' => 'sectionend', 'id' => '_rs_member_level_earning_points'),
            array(
                'name' => __('Member Level Message Settings', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_member_level_message_settings',
            ),
            array(
                'name' => __('Message Displayed for Free Productss', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed for the Free Products in Cart', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_free_product_message_info',
                'css' => 'min-width:550px;',
                'std' => 'You have got this product for Reaching [current_level_points] Points',
                'type' => 'textarea',
                'newids' => 'rs_free_product_message_info',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Caption for Free Product', 'rewardsystem'),
                'desc' => __('Enter the Caption which will be displayed when after Free Product is removed from cart', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_free_product_msg_caption',
                'css' => 'min-width:550px;',
                'std' => 'Free Product',
                'type' => 'textarea',
                'newids' => 'rs_free_product_msg_caption',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Display Free Product Message in Cart and Order', 'rewardsystem'),
                'desc' => __('Enable displaying free product message in cart/order', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_remove_msg_from_cart_order',
                'css' => 'min-width:150px;',
                'std' => 'yes',
                'type' => 'checkbox',
                'newids' => 'rs_remove_msg_from_cart_order',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_member_level_message_settings'),
        ));
    }

    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(self::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(self::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (self::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

    public static function rs_get_percentage_in_dynamic_rule($products, $field, $value) {
        if (is_array($products)) {
            foreach ($products as $key => $product) {
                if ($product[$field] >= $value)
                    return $product['percentage'];
            }
        }else {
            return '100';
        }
        return false;
    }

    public static function rs_get_products_in_dynamic_rule($products, $field, $value) {
        if (is_user_logged_in()) {
            if (is_array($products)) {
                foreach ($products as $key => $product) {
                    if ($product[$field] >= $value)
                        return @$product['product_list'];
                }
            }

            return false;
        }
    }

    public static function multi_dimensional_sort($arr, $index) {
        $b = array();
        $c = array();
        if (is_array($arr)) {
            foreach ($arr as $key => $value) {
                $b[$key] = $value[$index];
            }

            asort($b);

            foreach ($b as $key => $value) {
                $c[$key] = $arr[$key];
            }


            return $c;
        }
    }

    public static function reward_system_add_table_to_action() {
        global $woocommerce;
        wp_nonce_field(plugin_basename(__FILE__), 'rsdynamicrulecreation');
        ?>
        <style type="text/css">
            .rs_add_free_product_user_levels{
                width:100%;
            }
            .chosen-container-active{
                position: absolute;
            }
        </style>
        <script type="text/javascript">
        <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                jQuery(function () {
                    jQuery("select.rs_add_free_product_user_levels").ajaxChosen({
                        method: 'GET',
                        url: '<?php echo admin_url('admin-ajax.php'); ?>',
                        dataType: 'json',
                        afterTypeDelay: 100,
                        data: {
                            action: 'woocommerce_json_search_products_and_variations',
                            security: '<?php echo wp_create_nonce("search-products"); ?>'
                        }
                    }, function (data) {
                        var terms = {};

                        jQuery.each(data, function (i, val) {
                            terms[i] = val;
                        });
                        return terms;
                    });
                });
        <?php } ?>
        </script>
        <table class="widefat fixed rsdynamicrulecreation" cellspacing="0">
            <thead>
                <tr>

                    <th class="manage-column column-columnname" scope="col"><?php _e('Name', 'rewardsystem'); ?></th>
                    <th class="manage-column column-columnname" scope="col"><?php _e('Total Earned Points', 'rewardsystem'); ?></th>
                    <th class="manage-column column-columnname" scope="col"><?php _e('Reward Percentage', 'rewardsystem'); ?></th>
                    <th class="manage-column column-columnname" scope="col"><?php _e('Free Products', 'rewardsystem'); ?></th>
                    <th class="manage-column column-columnname num" scope="col"><?php _e('Remove Level', 'rewardsystem'); ?></th>
                </tr>
            </thead>

            <tfoot>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="manage-column column-columnname num" scope="col"> <span class="add button-primary"><?php _e('Add Level', 'rewardsystem'); ?></span></td>
                </tr>
                <tr>

                    <th class="manage-column column-columnname" scope="col"><?php _e('Name', 'rewardsystem'); ?></th>
                    <th class="manage-column column-columnname" scope="col"><?php _e('Total Earned Points', 'rewardsystem'); ?></th>
                    <th class="manage-column column-columnname" scope="col"><?php _e('Reward Percentage', 'rewardsystem'); ?></th>
                    <th class="manage-column column-columnname" scope="col"><?php _e('Free Products', 'rewardsystem'); ?></th>
                    <th class="manage-column column-columnname num" scope="col"><?php _e('Add Level', 'rewardsystem'); ?></th>

                </tr>
            </tfoot>

            <tbody id="here">
                <?php
                $rewards_dynamic_rulerule = get_option('rewards_dynamic_rule');
                //var_dump($rewards_dynamic_rulerule['1423814420734']);
                //$i = 0;
                if (is_array($rewards_dynamic_rulerule)) {
                    foreach ($rewards_dynamic_rulerule as $i => $rewards_dynamic_rule) {
                        ?>
                        <tr>
                            <td class="column-columnname">
                                <p class="form-field">
                                    <input type="text" name="rewards_dynamic_rule[<?php echo $i; ?>][name]" class="short" value="<?php echo $rewards_dynamic_rule['name']; ?>"/>
                                </p>
                            </td>
                            <td class="column-columnname">
                                <p class="form-field">
                                    <input type="number" name="rewards_dynamic_rule[<?php echo $i; ?>][rewardpoints]" id="rewards_dynamic_rewardpoints<?php echo $i; ?>" class="short" value="<?php echo $rewards_dynamic_rule['rewardpoints']; ?>"/>
                                </p>
                            </td>
                            <td class="column-columnname">
                                <p class="form-field">
                                    <input type ="number" name="rewards_dynamic_rule[<?php echo $i; ?>][percentage]" id="rewards_dynamic_rule_percentage<?php echo $i; ?>" class="short test" value="<?php echo $rewards_dynamic_rule['percentage']; ?>"/>
                                </p>
                            </td>
                            <td class="column-columnname">
                                <p class="form-field">

                                    <?php
                                    if ((float) $woocommerce->version > (float) ('2.2.0')) {
                                        ?>
                                        <!-- For Latest -->
                                        <?php //var_dump($rewards_dynamic_rule['product_list']); ?>
                                        <input type="hidden" class="wc-product-search" style="width: 100%;" id="rewards_dynamic_rule[<?php echo $i; ?>][product_list][]" name="rewards_dynamic_rule[<?php echo $i; ?>][product_list][]" data-placeholder="<?php _e('Search for a product&hellip;', 'woocommerce'); ?>" data-action="woocommerce_json_search_products_and_variations" data-multiple="true" data-selected="<?php
                                       $json_ids = array();
                                        if ($rewards_dynamic_rule['product_list'] != "") {
                                            $list_of_produts = $rewards_dynamic_rule['product_list']['0'];

                                            $product_ids = array_filter(array_map('absint', (array) explode(',', $list_of_produts)));
                                            

                                            foreach ($product_ids as $product_id) {
                                                $product = wc_get_product($product_id);
                                                $json_ids[$product_id] = wp_kses_post($product->get_formatted_name());
                                            } echo esc_attr(json_encode($json_ids));
                                        }
                                        ?>" value="<?php echo implode(',', array_keys($json_ids)); ?>" />
                                               <?php
                                           } else {
                                               ?>
                                        <!-- For Old Version -->
                                        <select multiple name="rewards_dynamic_rule[<?php echo $i; ?>][product_list][]" class="rs_add_free_product_user_levels">
                                            <?php
                                            if ($rewards_dynamic_rule['product_list'] != "") {
                                                $list_of_produts = $rewards_dynamic_rule['product_list'];
                                                foreach ($list_of_produts as $rs_free_id) {
                                                    echo '<option value="' . $rs_free_id . '" ';
                                                    selected(1, 1);
                                                    echo '>' . ' #' . $rs_free_id . ' &ndash; ' . get_the_title($rs_free_id);
                                                    ?>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <option value=""></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                        <?php
                                    }
                                    ?>



                                </p>
                            </td>
                            <td class="column-columnname num">
                                <span class="remove button-secondary"><?php _e('Remove Level', 'rewardsystem'); ?></span>
                            </td>
                        </tr>
                        <?php
                        //$i = $i + 1;
                    }
                }
                ?>
            </tbody>
        </table>
        <script>
            jQuery(document).ready(function () {
                jQuery('#afterclick').hide();
                //var countrewards_dynamic_rule =


                jQuery(".add").on('click', function () {

                    jQuery('#afterclick').show();
                    var countrewards_dynamic_rule = Math.round(new Date().getTime() + (Math.random() * 100));

        <?php if ((float) $woocommerce->version > (float) ('2.2.0')) { ?>

                        jQuery('#here').append('<tr><td><p class="form-field"><input type="text" name="rewards_dynamic_rule[' + countrewards_dynamic_rule + '][name]" class="short" value=""/></p></td>\n\
                                               \n\<td><p class="form-field"><input type="number" id="rewards_dynamic_ruleamount' + countrewards_dynamic_rule + '" name="rewards_dynamic_rule[' + countrewards_dynamic_rule + '][rewardpoints]" class="short" value=""/></p></td>\n\
                                      \n\\n\
                                    <td><p class="form-field"><input type ="number" id="rewards_dynamic_rule_claimcount' + countrewards_dynamic_rule + '" name="rewards_dynamic_rule[' + countrewards_dynamic_rule + '][percentage]" class="short test"  value=""/></p></td>\n\\n\
                                    \n\<td><p class="form-field">\n\
                                    \n\
                                    <input type=hidden style="width:100%;" name="rewards_dynamic_rule[' + countrewards_dynamic_rule + '][product_list][]" class="wc-product-search" data-placeholder="Search for a product" data-action="woocommerce_json_search_products_and_variations" data-multiple="true"/></p></td>n\
                                    <td class="num"><span class="remove button-secondary">Remove Rule</span></td></tr><hr>');

                        jQuery('body').trigger('wc-enhanced-select-init');
        <?php } else { ?>
                        jQuery('#here').append('<tr><td><p class="form-field"><input type="text" name="rewards_dynamic_rule[' + countrewards_dynamic_rule + '][name]" class="short" value=""/></p></td>\n\
                                    \n\<td><p class="form-field"><input type="number" id="rewards_dynamic_ruleamount' + countrewards_dynamic_rule + '" name="rewards_dynamic_rule[' + countrewards_dynamic_rule + '][rewardpoints]" class="short" value=""/></p></td>\n\
                                    \n\\n\
                                    <td><p class="form-field"><input type ="number" id="rewards_dynamic_rule_claimcount' + countrewards_dynamic_rule + '" name="rewards_dynamic_rule[' + countrewards_dynamic_rule + '][percentage]" class="short test"  value=""/></p></td>\n\\n\
                                    \n\<td><p class="form-field">\n\
                                    \n\
                                    <select multiple name="rewards_dynamic_rule[' + countrewards_dynamic_rule + '][product_list][]" class="rs_add_free_product_user_levels"><option value=""></option></select></p></td>n\
                                    <td class="num"><span class="remove button-secondary">Remove Rule</span></td></tr><hr>');

        <?php } ?>
        <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                        jQuery(function () {
                            jQuery("select.rs_add_free_product_user_levels").ajaxChosen({
                                method: 'GET',
                                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                                dataType: 'json',
                                afterTypeDelay: 100,
                                data: {
                                    action: 'woocommerce_json_search_products_and_variations',
                                    security: '<?php echo wp_create_nonce("search-products"); ?>'
                                }
                            }, function (data) {
                                var terms = {};

                                jQuery.each(data, function (i, val) {
                                    terms[i] = val;
                                });
                                return terms;
                            });
                        });
        <?php } ?>
                    return false;
                });
                jQuery(document).on('click', '.remove', function () {
                    jQuery(this).parent().parent().remove();
                });


                jQuery('#rs_enable_user_role_based_reward_points').addClass('rs_enable_user_role_based_reward_points');
                jQuery('#rs_enable_earned_level_based_reward_points').addClass('rs_enable_user_role_based_reward_points');


                if (jQuery('#rs_enable_user_role_based_reward_points').is(':checked')) {
                    jQuery('.rewardpoints_userrole').parent().parent().show();
                } else {
                    jQuery('.rewardpoints_userrole').parent().parent().hide();
                }


                if (jQuery('#rs_enable_earned_level_based_reward_points').is(':checked')) {
                    jQuery('.rsdynamicrulecreation').show();
                } else {
                    jQuery('.rsdynamicrulecreation').hide();
                }
                jQuery(document).on('click', '#rs_enable_user_role_based_reward_points', function () {
                    if (jQuery(this).is(":checked")) {
                        jQuery('.rewardpoints_userrole').parent().parent().show();
                    } else {
                        jQuery('.rewardpoints_userrole').parent().parent().hide();
                    }

                });

                jQuery(document).on('click', '#rs_enable_earned_level_based_reward_points', function () {
                    if (jQuery(this).is(":checked")) {
                        jQuery('.rsdynamicrulecreation').show();
                    } else {
                        jQuery('.rsdynamicrulecreation').hide();
                    }
                });
            });</script>

        <?php
    }

    public static function user_role_based_reward_points($getuserid, $userpoints) {

        //Set Bool Value for User ID
        $userrole = get_option('rs_enable_user_role_based_reward_points');
        $earnuserrole = get_option('rs_enable_earned_level_based_reward_points');

        if (($userrole == 'yes') && ($earnuserrole != 'yes')) {
            $user = new WP_User($getuserid);
            $user_roles = $user->roles;
            $currentuserrole = $user_roles[0];
            $getcurrentrolepercentage = get_option('rs_reward_user_role_' . $currentuserrole) != '' ? get_option('rs_reward_user_role_' . $currentuserrole) : '100';
            $currentpoints = $userpoints;
            $calculation = $currentpoints * $getcurrentrolepercentage;
            $calculation = $calculation / 100;
            return $calculation;
        }
        if (($earnuserrole == 'yes') && ($userrole != 'yes')) {


            //var_dump(get_option('rewards_dynamic_rule') != '' ? get_option('rewards_dynamic_rule') : '');
            $arrayvalue = self::multi_dimensional_sort(get_option('rewards_dynamic_rule'), 'rewardpoints');
            $rs_total_earned_points_user = get_user_meta(get_current_user_id(), 'rs_user_total_earned_points', true);
            $getpercentage = self::rs_get_percentage_in_dynamic_rule($arrayvalue, 'rewardpoints', $rs_total_earned_points_user);
            $getpercentage = $getpercentage != false ? $getpercentage : '100';
            $calculation = $userpoints * $getpercentage;
            $calculation = $calculation / 100;
            return $calculation;
        }

        if (($userrole == 'yes') && ($earnuserrole == 'yes')) {
            $user = new WP_User($getuserid);
            $user_roles = $user->roles;
            $currentuserrole = $user_roles[0];
            $getcurrentrolepercentage = get_option('rs_reward_user_role_' . $currentuserrole) != '' ? get_option('rs_reward_user_role_' . $currentuserrole) : '100';

            $arrayvalue = self::multi_dimensional_sort(get_option('rewards_dynamic_rule'), 'rewardpoints');
            $rs_total_earned_points_user = get_user_meta(get_current_user_id(), 'rs_user_total_earned_points', true);
            $getpercentage = self::rs_get_percentage_in_dynamic_rule($arrayvalue, 'rewardpoints', $rs_total_earned_points_user);
            $getpercentage = $getpercentage != false ? $getpercentage : '100';

            if (get_option('rs_choose_priority_level_selection') == '1') {
                if ($getcurrentrolepercentage >= $getpercentage) {
                    $getcurrentrolepercentage = $getcurrentrolepercentage;
                } else {
                    $getcurrentrolepercentage = $getpercentage;
                }
            } else {
                if (get_option('rs_choose_priority_level_selection') == '2') {
                    if ($getcurrentrolepercentage <= $getpercentage) {
                        $getcurrentrolepercentage = $getcurrentrolepercentage;
                    } else {
                        $getcurrentrolepercentage = $getpercentage;
                    }
                }
            }

            $currentpoints = $userpoints;
            $calculation = $currentpoints * $getcurrentrolepercentage;
            $calculation = $calculation / 100;
            return $calculation;
        }

        if (($userrole != 'yes') && ($earnuserrole != 'yes')) {
            $getcurrentrolepercentage = 100;
            $currentpoints = $userpoints;
            $calculation = $currentpoints * $getcurrentrolepercentage;
            $calculation = $calculation / 100;
            return $calculation;
        }
    }

    public static function get_available_variation_attributes($item_id) {
        $productobject = new WC_Product_Variation($item_id);
        $variation_data = $productobject->get_variation_attributes();
        $variation_detail = woocommerce_get_formatted_variation($variation_data, true);  // this will give all variation detail in one line
        return $variation_data;
    }

    public static function add_free_product_to_user_cart() {

        if (!is_admin()) {
            if (is_user_logged_in()) {
                global $woocommerce;
                $each_member_level = self::multi_dimensional_sort(get_option('rewards_dynamic_rule'), 'rewardpoints');
                if ($each_member_level != "") {
                    foreach ($each_member_level as $value) {
                        $rs_total_earned_points_user = get_user_meta(get_current_user_id(), 'rs_user_total_earned_points', true);
                        $free_products_list = self::rs_get_products_in_dynamic_rule($each_member_level, 'rewardpoints', $rs_total_earned_points_user);
                        $current_user_total_earned_points = get_user_meta(get_current_user_id(), 'rs_user_total_earned_points', true);
                        $current_level_earning_points_limit = $value["rewardpoints"];
                        if ($current_level_earning_points_limit >= $current_user_total_earned_points) {
                            if (is_array($free_products_list)) {
                                foreach ($free_products_list as $separate_free_product_id) {
                                    $found = false;
                                    $current_cart_contents = $woocommerce->cart->get_cart();
                                    //if (sizeof($woocommerce->cart->get_cart()) > 0) {
                                    foreach ($woocommerce->cart->get_cart() as $cart_items_key => $values) {
                                        //var_dump($cart_items_key);
                                        $_product = $values['data'];
                                        if (($_product->id == $separate_free_product_id) || ($_product->variation_id == $separate_free_product_id)) {
                                            $found = true;
                                        }
                                    }
                                    //}

                                    if (!$found) {
                                        if (sizeof($woocommerce->cart->get_cart()) > 0) {
                                            $free_product_key = get_user_meta(get_current_user_id(), 'sumo_rs_cart_item_key', true);
                                            $anotherkey = get_user_meta(get_current_user_id(), 'rs_cart_item_key', true);
                                            //var_dump(array_unique($free_product_key));
                                            $unique = array_unique((array) $free_product_key);
                                            $newunique = array_unique((array) $anotherkey);

                                            $updatedarray = array_values(array_filter(array_intersect((array) $unique, (array) $newunique)));
                                            $cart_key_id = $woocommerce->cart->generate_cart_id($separate_free_product_id);
                                            if (in_array($cart_key_id, $updatedarray)) {
                                                WC()->cart->set_quantity($cart_key_id, 0);
                                            } else {
                                                $checktype = get_product($_product->id);

                                                $woocommerce->cart->add_to_cart($separate_free_product_id);


                                                $getolddata = get_user_meta(get_current_user_id(), 'rs_cart_item_key', true);
                                                $arraymerge = array_merge((array) $getolddata, (array) $cart_key_id);
                                                update_user_meta(get_current_user_id(), 'rs_cart_item_key', $arraymerge);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static function get_list_from_cart_item_key() {
        if (is_user_logged_in()) {
            $getlistkey = get_user_meta(get_current_user_id(), 'sumo_rs_cart_item_key', true);


            $free_product_key = get_user_meta(get_current_user_id(), 'sumo_rs_cart_item_key', true);
            $anotherkey = get_user_meta(get_current_user_id(), 'rs_cart_item_key', true);
            //var_dump(array_unique($free_product_key));
            $unique = array_filter(array_unique((array) $free_product_key));
            $newunique = array_filter(array_unique((array) $anotherkey));
//                                   echo"<pre>";
//                                    var_dump($newunique);
//                                    echo "</pre>";
            $updatedarray = array_values(array_filter(array_intersect((array) $unique, (array) $newunique)));
//var_dump($updatedarray);
            //$current_user_earning_points_level = get_option('rewards_dynamic_rule');
            $each_member_level = self::multi_dimensional_sort(get_option('rewards_dynamic_rule'), 'rewardpoints');
            if ($each_member_level != "") {
                foreach ($each_member_level as $value) {
                    $rs_total_earned_points_user = get_user_meta(get_current_user_id(), 'rs_user_total_earned_points', true);
                    $free_products_list = self::rs_get_products_in_dynamic_rule($each_member_level, 'rewardpoints', $rs_total_earned_points_user);
                    if ($free_products_list) {
                        foreach ($free_products_list as $eachproduct) {
                            $cart_id = WC()->cart->generate_cart_id($eachproduct);
                            $findproductincart = WC()->cart->find_product_in_cart($cart_id);
                            //var_dump($findproductincart);
                            if ($findproductincart == '') {
                                //var_dump($updatedarray);
                                if (in_array($cart_id, $updatedarray)) {
                                    $product_info = get_product($eachproduct);
                                    if ($product_info->product_type == 'simple') {
                                        $producttitle = get_the_title($eachproduct);
                                        //if(has_post_thumbnail($eachproduct)){
                                        ?>
                                        <a href="javascript:void(0)" class="add_removed_free_product_to_cart" data-cartkey="<?php echo $cart_id; ?>"><?php echo get_the_post_thumbnail($eachproduct, array(24, 24)); ?><?php echo $producttitle; ?></a><br/>
                                        <?php
                                        //}
                                    }
                                    if ($product_info->product_type == 'variation') {
                                        $variation_removed_product = new WC_Product_Variation($eachproduct);
                                        foreach ($variation_removed_product as $value) {
                                            if (isset($value->post_title)) {
                                                $producttitle = $value->post_title;
                                                //if(has_post_thumbnail($eachproduct)){
                                                ?>
                                                <a href="javascript:void(0)" class="add_removed_free_product_to_cart" data-cartkey="<?php echo $cart_id; ?>"><?php echo get_the_post_thumbnail($eachproduct, array(24, 24)); ?><?php echo $producttitle; ?></a><br/>
                                                <?php
                                                //}
                                            }
                                        }
                                    }
                                    ?>
                                    <!--                        <a href="javascript:void(0)" class="add_removed_free_product_to_cart" data-cartkey="<?php echo $cart_id; ?>"><?php echo $producttitle; ?></a><br/>-->
                                    <?php
                                }
                                ?>
                                <?php
                            }
                        }
                    }
                }
            }
        }
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery('.add_removed_free_product_to_cart').click(function () {
                    var removed_key = jQuery(this).attr('data-cartkey');
                    var current_user_id = '<?php echo get_current_user_id(); ?>';
                    var removed_key_param = {
                        action: "delete_meta_current_key",
                        key_to_remove: removed_key,
                        current_user_id: current_user_id,
                    };
                    jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", removed_key_param, function (response) {
                        if (response === '1') {
                            location.reload();
                        }
                        console.log('Got this from the server: ' + response);
                    });
                    return false;
                });
                return false;
            });
        </script>
        <?php
    }

    public static function delete_saved_product_key_callback() {
        global $wpdb;
        if (isset($_POST['key_to_remove']) && $_POST['current_user_id']) {
            $selected_key_to_delete = $_POST['key_to_remove'];
            $user_id_to_remove = $_POST['current_user_id'];



            $after_unset = self::unset_saved_keys($selected_key_to_delete, array_filter(array_unique(get_user_meta($user_id_to_remove, 'listsetofids', true))));
            // var_dump(array_unique($after_unset));
            update_user_meta($user_id_to_remove, 'listsetofids', array_unique($after_unset));


            echo "1";
        }
        exit();
    }

    public static function unset_saved_keys($del_val, $messages) {
        if (($key = array_search($del_val, $messages)) !== false) {
            unset($messages[$key]);
        }
        return $messages;
    }

    public static function update_price_for_free_product($cart_object) {
        if (is_user_logged_in()) {
            $custom_price = 0;
            $custom_price_free_product = get_option('rewards_dynamic_rule');
            $current_earning_points_level = get_option('rewards_dynamic_rule');
            if ($current_earning_points_level != "") {
                foreach ($current_earning_points_level as $value) {
                    $current_user_points = get_user_meta(get_current_user_id(), '_my_reward_points', true);
                    $current_user_total_earned_points = get_user_meta(get_current_user_id(), 'rs_user_total_earned_points', true);
                    $total_earned_current_level = $value["rewardpoints"];
                    if ($total_earned_current_level >= $current_user_total_earned_points) {
                        //foreach ($custom_price_free_product as $value) {
                        $rs_total_earned_points_user = get_user_meta(get_current_user_id(), 'rs_user_total_earned_points', true);
                        $free_products_list = self::rs_get_products_in_dynamic_rule($current_earning_points_level, 'rewardpoints', $rs_total_earned_points_user);
                        if (is_array($free_products_list)) {
                            foreach ($free_products_list as $separate_free_product) {
                                $target_product_id = $separate_free_product;
                                foreach ($cart_object->cart_contents as $key => $value) {
                                    //var_dump($key);
                                    if ($value['product_id'] == $target_product_id) {
                                        $value['data']->price = $custom_price;
                                    }
                                    if ($value['variation_id'] == $target_product_id) {
                                        $value['data']->price = $custom_price;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static function alter_product_price_in_cart($productprice, $values, $cart_item_key) {
        $free_product_original_price = get_post_meta($values['product_id'], '_price', true);
        $custom_price = 0;
        if ($values['data']->price == 0) {
            echo "<del>" . get_woocommerce_currency_symbol() . $free_product_original_price . "</del>" . "<span>" . " " . get_woocommerce_currency_symbol() . $custom_price . "</span>";
            return;
        }
        return $productprice;
    }

    public static function alter_quantity_in_cart($product_quantity, $values) {
        global $woocommerce;
        $current_cart_contents = $woocommerce->cart->get_cart();
        //var_dump($current_cart_contents[$values]['data']->price);
//        foreach ($current_cart_contents as $key => $value){
//            var_dump($key);
//            var_dump($value['data']->price);
//
//        }

        if ($current_cart_contents[$values]['data']->price == '0') {
            echo sprintf('1 <input type="hidden" name="cart[%s][qty]" value="1" />', $values);
            return;
        }



        return $product_quantity;
    }

    public static function save_data_for_dynamic_rule() {
        $rewards_dynamic_rulerule = $_POST['rewards_dynamic_rule'];
        update_option('rewards_dynamic_rule', $rewards_dynamic_rulerule);
    }

    public static function reward_system_add_settings_to_action($settings) {
        global $wp_roles;
        $updated_settings = array();
        $mainvariable = array();
        global $woocommerce;
        foreach ($settings as $section) {
            if (isset($section['id']) && '_rs_user_role_reward_points' == $section['id'] &&
                    isset($section['type']) && 'sectionend' == $section['type']) {

                foreach ($wp_roles->role_names as $value => $key) {
                    $updated_settings[] = array(
                        'name' => __('Reward Points for ' . $value . ' User Role in Percentage %', 'rewardsystem'),
                        'desc' => __('Please Enter Percentage of Reward Points for ' . $value, 'rewardsystem'),
                        'tip' => '',
                        'class' => 'rewardpoints_userrole',
                        'id' => 'rs_reward_user_role_' . $value,
                        'css' => 'min-width:150px;',
                        'std' => '100',
                        'type' => 'text',
                        'newids' => 'rs_reward_user_role_' . $value,
                        'desc_tip' => true,
                    );
                }

                $updated_settings[] = array(
                    'type' => 'sectionend', 'id' => '_rs_reward_system_payment_gateway',
                );
            }


            $updated_settings[] = $section;
        }

        return $updated_settings;
    }

    public static function call_add_free_product_to_cart() {
        $rs_total_earned_points_user = get_user_meta(get_current_user_id(), 'rs_user_total_earned_points', true);
        if (!is_admin()) {
            if (is_user_logged_in()) {
                global $woocommerce;
                $each_member_level = self::multi_dimensional_sort(get_option('rewards_dynamic_rule'), 'rewardpoints');
                if ($each_member_level != "") {
                    foreach ($each_member_level as $value) {
                        $rs_total_earned_points_user = get_user_meta(get_current_user_id(), 'rs_user_total_earned_points', true);
                        $free_products_list = self::rs_get_products_in_dynamic_rule($each_member_level, 'rewardpoints', $rs_total_earned_points_user);
                        $current_user_total_earned_points = get_user_meta(get_current_user_id(), 'rs_user_total_earned_points', true);
                        $current_level_earning_points_limit = $value["rewardpoints"];
                        if ($current_level_earning_points_limit >= $current_user_total_earned_points) {
                            RSUserRoleRewardPoints::add_free_product_to_user_cart();
                        }
                        //add_action('wp', array('RSUserRoleRewardPoints','add_free_product_to_user_cart'));
                    }
                }
            }
        }
    }

    public static function add_product() {
        if (!empty($_GET['remove_item']) && isset($_GET['_wpnonce'])) {
            $getvalue = $_GET['remove_item'];
            // delete_user_meta(get_current_user_id(),'sumo_rs_cart_item_key');
            $olddataifany = (array) get_user_meta(get_current_user_id(), 'listsetofids', true);
            $arraymergedata = array_unique(array_filter(array_merge($olddataifany, (array) $getvalue)));
            update_user_meta(get_current_user_id(), 'listsetofids', $arraymergedata);
        }
    }

}

$obj = new RSUserRoleRewardPoints();




add_filter('woocommerce_before_cart_item_quantity_zero', 'fp_remove_cart_item_key', 10, 1);

function fp_remove_cart_item_key($cart_item_key) {
    //update_option('mainlog', $cart_item_key);
    $olddataifany = (array) get_user_meta(get_current_user_id(), 'listsetofids', true);
    $arraymergedata = array_unique(array_filter(array_merge($olddataifany, (array) $cart_item_key)));
    update_user_meta(get_current_user_id(), 'listsetofids', $arraymergedata);
}

function wowtesting() {
    var_dump(get_user_meta(get_current_user_id(), 'listsetofids', true));
}

//add_action('wp_head', 'wowtesting');
//add_action('init', array('RSUserRoleRewardPoints', 'call_add_free_product_to_cart'),10);




if (!function_exists('rs_free_product_message')) {

    function rs_free_product_message($cart_item_data, $product_id) {
        global $woocommerce;
        session_start();
        $option = 'Custom Data';
        $new_value = array('rs_free_product_message_value' => $option);
        if (empty($option))
            return $cart_item_data;
        else {
            if (empty($cart_item_data))
                return $new_value;
            else
                return array_merge($cart_item_data, $new_value);
        }
        unset($_SESSION['rs_free_product_message']);
        //Unset our custom session variable, as it is no longer needed.
    }

}

if (!function_exists('rs_get_cart_items_from_session')) {

    function rs_get_cart_items_from_session($item, $values, $key) {
        if (array_key_exists('rs_free_product_message', $values)) {
            $item['rs_free_product_message'] = $values['rs_free_product_message'];
        }
        return $item;
    }

}

if (!function_exists('add_user_custom_message_from_session_into_order')) {

    function add_user_custom_message_from_session_into_order($product_name, $values) {
        /* code to add custom data on Cart & checkout Page */
        //var_dump($values);
        $current_earning_points_level = RSUserRoleRewardPoints::multi_dimensional_sort(get_option('rewards_dynamic_rule'), 'rewardpoints');
        foreach ($current_earning_points_level as $value) {
            $current_user_total_earned_points = get_user_meta(get_current_user_id(), 'rs_user_total_earned_points', true);
            $total_earned_current_level = $value["rewardpoints"];
            if (count(isset($values['rs_free_product_message'])) > 0) {
                $return_string = $product_name;

                if ($total_earned_current_level >= $current_user_total_earned_points) {
                    //var_dump($values['data']->product_type);
                    //var_dump($cart_item_key);

                    if ($values['line_total'] == 0) {
                        $getproducttype = get_product($values['product_id']);
                        if ($getproducttype->is_type('simple')) {
                            $free_product_message_to_find = "[current_level_points]";
                            $free_product_message_to_replace = $total_earned_current_level;
                            $free_product_message_replaced = str_replace($free_product_message_to_find, $free_product_message_to_replace, get_option('rs_free_product_message_info'));
                            $return_string .= "<br><span>" . $free_product_message_replaced . "</span>";
                        } else {
                            //var_dump($values['item_meta']);
                            // $return_string .= $product_name;
                            $item_meta = new WC_Order_Item_Meta($values['item_meta']);
                            $productobject = new WC_Product_Variation($values['variation_id']);
                            $variation_data = $productobject->get_variation_attributes();
                            if (is_array($variation_data)) {
                                $return_string .= '<dl class="variation">';
                                foreach ($variation_data as $key => $eachvariation) {

                                    $return_string .='<dt class="variation">';
                                    $return_string .=ucwords(str_replace('attribute_', '', $key));
                                    $return_string .=":";
                                    $return_string .= '</dt>';

                                    $return_string .='<dd class="variation">';
                                    $return_string .="<p>" . ucwords($eachvariation) . "</p>";
                                    $return_string .='</dd>';
                                }
                                $return_string .='</dl>';
                            }

                            $free_product_message_to_find = "[current_level_points]";
                            $free_product_message_to_replace = $total_earned_current_level;
                            $free_product_message_replaced = str_replace($free_product_message_to_find, $free_product_message_to_replace, get_option('rs_free_product_message_info'));
                            $return_string .= "<span class='variation'>" . $free_product_message_replaced . "</span>";
                        }
                    }
                }

                return $return_string;
            } else {
                return $product_name;
            }
        }
    }

}


if (!function_exists('add_user_custom_message_from_session_into_cart')) {

    function add_user_custom_message_from_session_into_cart($product_name, $values, $cart_item_key) {
        /* code to add custom data on Cart & checkout Page */
        //var_dump($values);
        $current_earning_points_level = RSUserRoleRewardPoints::multi_dimensional_sort(get_option('rewards_dynamic_rule'), 'rewardpoints');
        foreach ($current_earning_points_level as $value) {
            $current_user_total_earned_points = get_user_meta(get_current_user_id(), 'rs_user_total_earned_points', true);
            $total_earned_current_level = $value["rewardpoints"];
            if (count(isset($values['rs_free_product_message'])) > 0) {
                $return_string = $product_name;
                //$return_string .= "<table id='" . $values['product_id'] . "'>";
                if ($total_earned_current_level >= $current_user_total_earned_points) {
                    //var_dump($values['data']->product_type);
                    //var_dump($cart_item_key);
                    if ($values['data']->price == 0) {
                        if ($values['data']->product_type == 'simple') {
                            $free_product_message_to_find = "[current_level_points]";
                            $free_product_message_to_replace = $total_earned_current_level;
                            $free_product_message_replaced = str_replace($free_product_message_to_find, $free_product_message_to_replace, get_option('rs_free_product_message_info'));
                            $return_string .= "<span>" . $free_product_message_replaced . "</span>";
                        } else {
//                            var_dump($values['data']);
                            $variation_details = $values['data']->variation_data;
                            foreach ($variation_details as $key => $value) {
                                $variation_name = explode("_", $key);
                                //var_dump($variation_name[1]);
                                $return_string .= "<dl class='variation'>";
                                $return_string .= "<dt class='variation_$variation_name[1]'>";
                                $return_string .= ucwords($variation_name[1]) . ":";
                                $return_string .= "<dd class='variation_$variation_name[1]'>";
                                $return_string .= ucwords($value);
                                $return_string .= "</dd >";
                                $return_string .= "</dt >";
                                $return_string .= "</dl>";
                            }
                            $free_product_message_to_find = "[current_level_points]";
                            $free_product_message_to_replace = $total_earned_current_level;
                            $free_product_message_replaced = str_replace($free_product_message_to_find, $free_product_message_to_replace, get_option('rs_free_product_message_info'));
                            $return_string .= "<span>" . $free_product_message_replaced . "</span>";
                        }
                    }
                }
                // $return_string .= "</table>";

                return $return_string;
            } else {
                return $product_name;
            }
        }
    }

}

if (!function_exists('add_custom_message_to_order_item_meta_cart')) {

    function add_custom_message_to_order_item_meta_cart($item_id) {
        global $woocommerce, $wpdb;
        $free_product_message_to_find = "[current_level_points]";
        $current_user_total_earned_points = get_user_meta(get_current_user_id(), 'rs_user_total_earned_points', true);
        $free_product_message_replaced = str_replace($free_product_message_to_find, $current_user_total_earned_points, get_option('rs_free_product_message_info'));
        $user_custom_values = "<span>" . $free_product_message_replaced . "</span>";
        $getdatas_order = wc_get_order_item_meta($item_id, '', false);
        // wc_add_order_item_meta($item_id,'testing',$getdatas_order);
        update_option('testingids', $item_id);
        update_option('testing', $getdatas_order);

        $order_product_list_id = $getdatas_order['_product_id'];
        $order_product_list_id_variation = $getdatas_order['_variation_id'];
        if (is_array($order_product_list_id)) {
            if ($getdatas_order['_variation_id'][0] == '0') {
                foreach ($order_product_list_id as $each_product_order) {

                    if ($each_product_order == display_free_product_id($each_product_order)) {


                        if (!empty($user_custom_values)) {

                            wc_add_order_item_meta($item_id, 'Free Product', $user_custom_values);
                            //wc_add_order_item_meta($item_id, 'itemid', $item_id);
                        }
                    }
                }
            } else {
                foreach ($order_product_list_id_variation as $each_product_order) {
                    if ($each_product_order == display_free_product_id($each_product_order)) {

                        if (!empty($user_custom_values)) {

                            wc_add_order_item_meta($item_id, 'Free Product', $user_custom_values);
                            //wc_add_order_item_meta($item_id, 'itemid', $item_id);
                        }
                    }
                }
            }
        }
    }

}

if (!function_exists('add_custom_message_to_order_item_meta')) {

    function add_custom_message_to_order_item_meta($item_id) {
        global $woocommerce, $wpdb;
        $free_product_message_to_find = "[current_level_points]";
        $current_user_total_earned_points = get_user_meta(get_current_user_id(), 'rs_user_total_earned_points', true);
        $free_product_message_replaced = str_replace($free_product_message_to_find, $current_user_total_earned_points, get_option('rs_free_product_message_info'));
        $user_custom_values = "<span>" . $free_product_message_replaced . "</span>";
        $getdatas_order = wc_get_order_item_meta($item_id, '', false);
        $order_product_list_id = $getdatas_order['_product_id'];
        $order_product_list_id_variation = $getdatas_order['_variation_id'];
        if (is_array($order_product_list_id)) {
            if ($getdatas_order['_variation_id'][0] == '0') {
                foreach ($order_product_list_id as $each_product_order) {

                    if ($each_product_order == display_free_product_id($each_product_order)) {


                        if (!empty($user_custom_values)) {

                            wc_add_order_item_meta($item_id, 'Free Product', $user_custom_values);
                            //wc_add_order_item_meta($item_id, 'itemid', $item_id);
                        }
                    }
                }
            } else {
                foreach ($order_product_list_id_variation as $each_product_order) {
                    if ($each_product_order == display_free_product_id($each_product_order)) {

                        if (!empty($user_custom_values)) {

                            wc_add_order_item_meta($item_id, 'Free Product', $user_custom_values);
                            //wc_add_order_item_meta($item_id, 'itemid', $item_id);
                        }
                    }
                }
            }
        }
    }

}

//function fp_get_free_product_level_id($total_earned_points) {
//
//    if (!is_admin()) {
//        if (is_user_logged_in()) {
//            global $woocommerce;
//            $each_member_level = RSUserRoleRewardPoints::multi_dimensional_sort(get_option('rewards_dynamic_rule'), 'rewardpoints');
//            if ($each_member_level != "") {
//                foreach ($each_member_level as $key => $value) {
//                    // $free_products_list = self::rs_get_products_in_dynamic_rule($each_member_level, 'rewardpoints', $total_earned_points);
//                    $current_user_total_earned_points = $total_earned_points;
//                    $current_level_earning_points_limit = $value["rewardpoints"];
//                    if ($current_level_earning_points_limit >= $current_user_total_earned_points) {
//                        //RSUserRoleRewardPoints::add_free_product_to_user_cart();
//                        //var_dump($key);
//                        return $key;
//                    }
//                }
//            }
//        }
//    }
//}
//function fp_get_corresponding_product($dynamicruleid, $total_earned_points) {
//    $get_datas = get_option('rewards_dynamic_rule');
//    if (isset($dynamicruleid)) {
//        $free_products_list = $get_datas[$dynamicruleid]['product_list'];
//
//        return $free_products_list;
//    }
//}
//function fp_check_if_free_product_is_in_cart($productid, $dynamicruleid, $total_earned_points) {
//    global $woocommerce;
//    $productids = array();
//    $soloproductids = array();
//    foreach ($woocommerce->cart->cart_contents as $key => $values) {
//        $getids = $values['variation_id'] != '' ? $values['variation_id'] : $values['product_id'];
//
//        $productids[] = $getids;
//        $getfreeproductids = fp_get_corresponding_product($dynamicruleid, $total_earned_points);
//        if (!in_array($getids, $getfreeproductids)) {
//            $soloproductids[] = $getids;
//        }
//    }
//
//    if (in_array($productid, $productids)) {
//        if (count($soloproductids) > 1) {
//            return "true"; //Found
//        } else {
//            // $woocommerce->cart->empty_cart();
//            return "true";
//        }
//    } else {
//        return "false"; //Not Found
//    }
//}
//function fp_add_free_product_to_cart($productid) {
//    global $woocommerce;
//
//    $woocommerce->cart->add_to_cart($productid);
//}
//add_action('wp_head', 'fp_main_function_add_to_cart');
//
//function fp_main_function_add_to_cart() {
//    global $woocommerce;
//    $total_earned_points = get_user_meta(get_current_user_id(), 'rs_user_total_earned_points', true);
//    //var_dump($total_earned_points);
//    $dynamicruleid = fp_get_free_product_level_id($total_earned_points);
//    //var_dump($dynamicruleid);
//    $getcorrespondingproducts = fp_get_corresponding_product($dynamicruleid, $total_earned_points);
//
//    if (!empty($getcorrespondingproducts)) {
//        if (is_array($getcorrespondingproducts)) {
//            foreach ($getcorrespondingproducts as $eachproduct) {
//                $listofcartitemkeys[] = $woocommerce->cart->generate_cart_id($eachproduct);
//                $combinedproductids[] = $eachproduct;
//                $getcurrentcartids = $woocommerce->cart->generate_cart_id($eachproduct);
//
//                $cartremovedlist = get_user_meta(get_current_user_id(), 'listsetofids', true);
//                var_dump($getcurrentcartids);
//                if (!in_array($getcurrentcartids, (array) $cartremovedlist)) {
//                    echo "This is true";
//                }
//
//                $found_or_not = fp_check_if_free_product_is_in_cart($eachproduct, $dynamicruleid, $total_earned_points);
//                var_dump($found_or_not);
//                if (!in_array($getcurrentcartids, (array) $cartremovedlist)) {
//                    if ($found_or_not == 'false') {
//                        $getcurrentcartids = $woocommerce->cart->generate_cart_id($eachproduct);
//
//                        $cartremovedlist = get_user_meta(get_current_user_id(), 'listsetofids', true);
//                        fp_add_free_product_to_cart($eachproduct);
//                        WC()->session->set('setruleids', $dynamicruleid);
//                    }
//                }
//            }
//        }
//    }
//    // $combinetwodatas = array_combine((array)$listofcartitemkeys,(array)$combinedproductids);
//    //  WC()->session->set('listofcartitemkeys',$listofcartitemkeys);
//    //  WC()->session->set('cartkeysnids',$combinetwodatas);
//}
//add_action('woocommerce_checkout_update_order_meta', 'save_data_to_order');
//
//function save_data_to_order($orderid) {
//    $getsavedsession = WC()->session->get('setruleids');
//    update_post_meta($orderid, 'listruleids', $getsavedsession);
//    $getalldatas = get_option('rewards_dynamic_rule');
//    update_post_meta($orderid, 'ruleidsdata', $getalldatas[$getsavedsession]);
//}
//function americanfunction() {
//    var_dump(get_post_meta('224', 'ruleidsdata', true));
//}
//add_action('wp_head', 'americanfunction');
//add_action('init', 'fp_remove_free_product_from_cart', 1);
//function fp_remove_free_product_from_cart() {
//    global $woocommerce;
//    if (!empty($_GET['remove_item']) && isset($_GET['_wpnonce'])) {
//        (array) $getvalue = $_GET['remove_item'];
//        (array) $getpreviousvalue = get_user_meta(get_current_user_id(), 'removedlistids', true);
//        (array) $mergecurrentvalue = array_merge($getpreviousvalue, $getvalue);
//        update_user_meta(get_current_user_id(), 'removedlistids', $mergecurrentvalue);
//        // WC()->session->set('removedlistids',$mergecurrentvalue);
//    }
//}
//
////function
//function display_free_product_id($free_product_id) {
//    if (is_user_logged_in()) {
//        $each_member_level = RSUserRoleRewardPoints::multi_dimensional_sort(get_option('rewards_dynamic_rule'), 'rewardpoints');
//        if ($each_member_level != "") {
//            foreach ($each_member_level as $value) {
//                $rs_total_earned_points_user = get_user_meta(get_current_user_id(), 'rs_user_total_earned_points', true);
//
//                $free_products_list = RSUserRoleRewardPoints::rs_get_products_in_dynamic_rule($each_member_level, 'rewardpoints', $rs_total_earned_points_user);
//
//                if ($free_products_list) {
//                    if (in_array($free_product_id, $free_products_list)) {
//                        return $free_product_id;
//                    }
//                }
//            }
//        }
//    }
//}
//add_filter('woocommerce_checkout_cart_item_quantity','add_user_custom_message_from_session_into_cart',1,3);
$current_earning_points_level = get_option('rewards_dynamic_rule');
$enable_member_level = get_option('rs_enable_earned_level_based_reward_points');
$current_points = get_user_meta(get_current_user_id(), '_my_reward_points', true);



//var_dump($enable_member_level);
if ($enable_member_level != 'no') {
    if ($current_earning_points_level != '') {
        //if($current_points > '0'){
//            add_action('wp_ajax_delete_meta_current_key',array('RSUserRoleRewardPoints','delete_saved_product_key_callback'));
//            add_filter('woocommerce_cart_item_name', 'add_user_custom_message_from_session_into_cart', 999, 3);
//            add_filter('woocommerce_order_item_name','add_user_custom_message_from_session_into_order',999,2);
//            add_filter('woocommerce_cart_item_quantity', array('RSUserRoleRewardPoints', 'alter_quantity_in_cart'), 1, 2);
//            //add_filter('woocommerce_add_cart_item_data', 'add_custom_message_to_order_item_meta_cart', 1, 2);
//            //add_action('woocommerce_add_order_item_meta', 'add_custom_message_to_order_item_meta', 1, 2);
//            add_filter('woocommerce_get_cart_item_from_session', 'rs_get_cart_items_from_session', 1, 3);
//            add_filter('woocommerce_cart_item_price', array('RSUserRoleRewardPoints', 'alter_product_price_in_cart'), 1, 3);
        // add_action('woocommerce_after_cart_table', array('RSUserRoleRewardPoints', 'get_list_from_cart_item_key'), 999);
//            add_action('woocommerce_before_calculate_totals', array('RSUserRoleRewardPoints', 'update_price_for_free_product'));
        //}
    }
}

//add_action('wp_head','get_free_product_level_id');