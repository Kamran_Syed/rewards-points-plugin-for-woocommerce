<?php

class FPRewardSystemUpdate {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_updates'] = __('Update', 'rewardsystem');
        return $settings_tabs;
    }

    // Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {

        $getproductlist = array();
        $categorylist = array();
        $socialproductlists = array();
        $socialproductids = array();
        $socialproducttitles = array();
        $getproductids = array();
        $getproducttitles = array();
        $categoryname = array();
        $categoryid = array();
        $ajaxproductsearch = array();
        $rsproductids = array();
        $rsproduct_name = array();

        $ajaxproductsearchsocial = array();
        $rsproductidssocial = array();
        $rsproduct_namesocial = array();

        if (isset($_GET['tab'])) {
            if ($_GET['tab'] == 'rewardsystem_updates') {
                global $woocommerce;
                $product_field_type_ids = get_option('rs_select_particular_products');
                $product_ids = !empty($product_field_type_ids) ? array_map('absint', (array) $product_field_type_ids) : null;
                if ($product_ids) {
                    foreach ($product_ids as $product_id) {
                        $rsproductids[] = $product_id;
                        $productobject = new WC_Product($product_id);
                        $rsproduct_name[] = $productobject->get_formatted_name($rsproductids);
                    }
                }
                @$ajaxproductsearch = array_combine((array) $rsproductids, (array) $rsproduct_name);


                $product_field_type_ids_social = get_option('rs_select_particular_social_products');
                $product_ids_social = !empty($product_field_type_ids_social) ? array_map('absint', (array) $product_field_type_ids_social) : null;
                if ($product_ids_social) {
                    foreach ($product_ids_social as $each_product_id) {
                        $rsproductidssocial[] = $each_product_id;
                        $rsproductobject = new WC_Product($each_product_id);
                        $rsproduct_namesocial[] = $rsproductobject->get_formatted_name($rsproductidssocial);
                    }
                }
                @$ajaxproductsearchsocial = array_combine((array) $rsproductidssocial, (array) $rsproduct_namesocial);



                $listcategories = get_terms('product_cat');
                // var_dump($listcategories);
                if (is_array($listcategories)) {
                    foreach ($listcategories as $category) {
                        $categoryname[] = $category->name;
                        $categoryid[] = $category->term_id;
                    }
                }
                //  if (is_array(($categoryid) && ($categoryname))) {
                @$categorylist = array_combine((array) $categoryid, (array) $categoryname);
                //  }
                //var_dump($categoryid);
                // if (is_array(($getproductids) && ($getproducttitles))) {
            }
        }
        return apply_filters('woocommerce_rewardsystem_my_account_settings', array(
            array(
                'name' => __('Update Settings for Existing Products/Existing Categories', 'rewardsystem'),
                'type' => 'title',
                'desc' => 'Here you can update the following options for the Existing Products/Existing Categories',
                'id' => '_rs_update_redeem_settings'
            ),
            array(
                'name' => __('Select Products/Categories', 'rewardsystem'),
                'desc' => __('Enable/Disable Product Selection in Reward System', 'rewardsystem'),
                'id' => 'rs_which_product_selection',
                'css' => 'min-width:150px;',
                'std' => '1',
                'class' => 'rs_which_product_selection',
                'default' => '1',
                'newids' => 'rs_which_product_selection',
                'type' => 'select',
                'options' => array(
                    '1' => __('All Products', 'rewardsystem'),
                    '2' => __('Selected Products', 'rewardsystem'),
                    '3' => __('All Categories', 'rewardsystem'),
                    '4' => __('Selected Categories', 'rewardsystem'),
                ),
                'desc_tip' => true,
            ),
//            array(
//                'name' => __('Select Particular Products', 'rewardsystem'),
//                'desc' => __('Select Particular Product in Reward System', 'rewardsystem'),
//                'id' => 'rs_select_particular_products',
//                'css' => 'min-width:150px;',
//                'std' => '1',
//                'class' => 'rs_select_particular_products ajax_chosen_select_products',
//                'default' => '1',
//                'newids' => 'rs_select_particular_products',
//                'type' => 'multiselect',
//                'options' => $ajaxproductsearch,
//            ),
            array(
                'name' => __('Selected Particular Products', 'rewardsystem'),
                'type' => 'selected_products',
                'desc' => __('Select Particular Categories in Reward System', 'rewardsystem'),
                'id' => 'rs_select_particular_products',
                'css' => 'min-width:150px;',
                'class' => 'rs_select_particular_products',
                'newids' => 'rs_select_particular_products',
            ),
            array(
                'name' => __('Select Particular Categories', 'rewardsystem'),
                'desc' => __('Select Particular Categories in Reward System', 'rewardsystem'),
                'id' => 'rs_select_particular_categories',
                'css' => 'min-width:150px;',
                'std' => '1',
                'class' => 'rs_select_particular_categories',
                'default' => '1',
                'newids' => 'rs_select_particular_categories',
                'type' => 'multiselect',
                'options' => $categorylist,
            ),
            array(
                'name' => __('Enable SUMO Reward Points', 'rewardsystem'),
                'desc' => __('This helps to Enable Reward Points in Global Level', 'rewardsystem'),
                'id' => 'rs_local_enable_disable_reward',
                'css' => 'min-width:150px;',
                'std' => '2',
                'default' => '2',
                'desc_tip' => true,
                'newids' => 'rs_local_enable_disable_reward',
                'type' => 'select',
                'options' => array(
                    '1' => __('Enable', 'rewardsystem'),
                    '2' => __('Disable', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Reward Type', 'rewardsystem'),
                'desc' => __('Select Reward Type by Points/Percentage', 'rewardsystem'),
                'id' => 'rs_local_reward_type',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_local_reward_type',
                'type' => 'select',
                'options' => array(
                    '1' => __('By Fixed Reward Points', 'rewardsystem'),
                    '2' => __('By Percentage of Product Price', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Reward Points', 'rewardsystem'),
                'desc' => __('Please Enter Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_local_reward_points',
                'css' => 'min-width:150px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_local_reward_points',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Reward Points in Percent %', 'rewardsystem'),
                'desc' => __('Please Enter Percentage value of Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_local_reward_percent',
                'css' => 'min-width:150px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_local_reward_percent',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Referral Reward Type', 'rewardsystem'),
                'desc' => __('Select Reward Type by Points/Percentage', 'rewardsystem'),
                'id' => 'rs_local_referral_reward_type',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'newids' => 'rs_local_referral_reward_type',
                'type' => 'select',
                'desc_tip' => true,
                'options' => array(
                    '1' => __('By Fixed Reward Points', 'rewardsystem'),
                    '2' => __('By Percentage of Product Price', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Referral Reward Points', 'rewardsystem'),
                'desc' => __('Please Enter Referral Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_local_referral_reward_point',
                'css' => 'min-width:150px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_local_referral_reward_point',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Referral Reward Points in Percent %', 'rewardsystem'),
                'desc' => __('Please Enter Percentage value of Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_local_referral_reward_percent',
                'css' => 'min-width:150px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_local_referral_reward_percent',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Test Button', 'rewardsystem'),
                'desc' => __('This is for testing button', 'rewardsystem'),
                'id' => 'rs_sumo_reward_button',
                'css' => '',
                'std' => '',
                'type' => 'button',
                'desc_tip' => true,
                'newids' => 'rs_sumo_reward_button',
            ),
            array('type' => 'sectionend', 'id' => '_rs_update_redeem_settings'),
            array(
                'name' => __('Update Social Settings for Existing Products/Existing Categories', 'rewardsystem'),
                'type' => 'title',
                'desc' => 'Here you can update the following Social options for the Existing Products/Existing Categories',
                'id' => '_rs_update_social_settings'
            ),
            array(
                'name' => __('Select Products/Categories', 'rewardsystem'),
                'desc' => __('Enable/Disable Product Selection in Reward System', 'rewardsystem'),
                'id' => 'rs_which_social_product_selection',
                'css' => 'min-width:150px;',
                'std' => '1',
                'class' => 'rs_which_social_product_selection',
                'default' => '1',
                'newids' => 'rs_which_social_product_selection',
                'type' => 'select',
                'options' => array(
                    '1' => __('All Products', 'rewardsystem'),
                    '2' => __('Selected Products', 'rewardsystem'),
                    '3' => __('All Categories', 'rewardsystem'),
                    '4' => __('Selected Categories', 'rewardsystem'),
                ),
                'desc_tip' => true,
            ),
//            array(
//                'name' => __('Select Particular Products', 'rewardsystem'),
//                'desc' => __('Select Particular Product in Reward System', 'rewardsystem'),
//                'id' => 'rs_select_particular_social_products',
//                'css' => 'min-width:150px;',
//                'std' => '1',
//                'class' => 'rs_select_particular_social_products',
//                'default' => '1',
//                'newids' => 'rs_select_particular_social_products',
//                'type' => 'multiselect',
//                'options' => $ajaxproductsearchsocial,
//            ),
            array(
                'name' => __('Selected Particular Products', 'rewardsystem'),
                'type' => 'selected_social_products',
                'desc' => __('Select Particular Categories in Reward System', 'rewardsystem'),
                'id' => 'rs_select_particular_social_products',
                'css' => 'min-width:150px;',
                'class' => 'rs_select_particular_social_categories',
                'newids' => 'rs_select_particular_social_products',
            ),
            array(
                'name' => __('Select Particular Categories', 'rewardsystem'),
                'desc' => __('Select Particular Categories in Reward System', 'rewardsystem'),
                'id' => 'rs_select_particular_social_categories',
                'css' => 'min-width:150px;',
                'std' => '1',
                'class' => 'rs_select_particular_social_categories',
                'default' => '1',
                'newids' => 'rs_select_particular_social_categories',
                'type' => 'multiselect',
                'options' => $categorylist,
            ),
            array(
                'name' => __('Enable SUMO Reward Points for Social Promotion', 'rewardsystem'),
                'desc' => __('This helps to Enable Reward Points in Global Level', 'rewardsystem'),
                'id' => 'rs_local_enable_disable_social_reward',
                'css' => 'min-width:150px;',
                'std' => '2',
                'default' => '2',
                'desc_tip' => true,
                'newids' => 'rs_local_enable_disable_social_reward',
                'type' => 'select',
                'options' => array(
                    '1' => __('Enable', 'rewardsystem'),
                    '2' => __('Disable', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Facebook Reward Type', 'rewardsystem'),
                'desc' => __('Select Reward Type for Facebook by Points/Percentage', 'rewardsystem'),
                'id' => 'rs_local_reward_type_for_facebook',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_local_reward_type_for_facebook',
                'type' => 'select',
                'options' => array(
                    '1' => __('By Fixed Reward Points', 'rewardsystem'),
                    '2' => __('By Percentage of Product Price', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Facebook Reward Points', 'rewardsystem'),
                'desc' => __('Please Enter Reward Points for Facebook', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_local_reward_points_facebook',
                'css' => 'min-width:150px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_local_reward_points_facebook',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Facebook Reward Points in Percent %', 'rewardsystem'),
                'desc' => __('Please Enter Percentage value of Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_local_reward_percent_facebook',
                'css' => 'min-width:150px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_local_reward_percent_facebook',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Twitter Reward Type', 'rewardsystem'),
                'desc' => __('Select Reward Type for Twitter by Points/Percentage', 'rewardsystem'),
                'id' => 'rs_local_reward_type_for_twitter',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_local_reward_type_for_twitter',
                'type' => 'select',
                'options' => array(
                    '1' => __('By Fixed Reward Points', 'rewardsystem'),
                    '2' => __('By Percentage of Product Price', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Twitter Reward Points', 'rewardsystem'),
                'desc' => __('Please Enter Reward Points for Twitter', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_local_reward_points_twitter',
                'css' => 'min-width:150px;',
                'std' => '',
                'type' => 'text',
                'newids' => 'rs_local_reward_points_twitter',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Twitter Reward Points in Percent %', 'rewardsystem'),
                'desc' => __('Please Enter Percentage value of Reward Points for Twitter', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_local_reward_percent_twitter',
                'css' => 'min-width:150px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_local_reward_percent_twitter',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Google Reward Type', 'rewardsystem'),
                'desc' => __('Select Reward Type for Google by Points/Percentage', 'rewardsystem'),
                'id' => 'rs_local_reward_type_for_google',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_local_reward_type_for_google',
                'type' => 'select',
                'options' => array(
                    '1' => __('By Fixed Reward Points', 'rewardsystem'),
                    '2' => __('By Percentage of Product Price', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Google+ Reward Points', 'rewardsystem'),
                'desc' => __('Please Enter Reward Points for Google+', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_local_reward_points_google',
                'css' => 'min-width:150px;',
                'std' => '',
                'type' => 'text',
                'newids' => 'rs_local_reward_points_google',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Google+ Reward Points in Percent %', 'rewardsystem'),
                'desc' => __('Please Enter Percentage value of Reward Points for Google+', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_local_reward_percent_google',
                'css' => 'min-width:150px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_local_reward_percent_google',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Test Button', 'rewardsystem'),
                'desc' => __('This is for testing button', 'rewardsystem'),
                'id' => 'rs_sumo_reward_button',
                'css' => '',
                'std' => '',
                'type' => 'button_social',
                'desc_tip' => true,
                'newids' => 'rs_sumo_reward_button',
            ),
            array('type' => 'sectionend', 'id' => '_rs_update_redeem_settings'),
            array(
                'name' => __('Apply Reward Points for Previous Orders', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Here you can Apply Reward Points for Previous Orders', 'rewardsystem'),
                'id' => '_rs_apply_reward_points',
            ),
            array(
                'name' => __('Apply Points for Previous Orders', 'rewardsystem'),
                'desc' => __('This is for Previous Order Reward Point Selection', 'rewardsystem'),
                'id' => 'rs_sumo_select_order_range',
                'tip' => '',
                'css' => 'min-width:150px;',
                'std' => '1',
                'type' => 'select',
                'options' => array('1' => 'All Time', '2' => 'Specific Date'),
                'newids' => 'rs_sumo_select_order_range',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Test Button', 'rewardsystem'),
                'desc' => __("This is for Previous Order Reward Points", 'rewardsystem'),
                'id' => 'rs_sumo_reward_previous_order_points',
                'css' => '',
                'std' => '',
                'type' => 'previous_order_button_range',
                'desc_tip' => true,
                'newids' => 'rs_sumo_reward_previous_order_points',
            ),
            array(
                'name' => __('Test Button', 'rewardsystem'),
                'desc' => __("This is for Previous Order Reward Points", 'rewardsystem'),
                'id' => 'rs_sumo_reward_previous_order_points',
                'css' => '',
                'std' => '',
                'type' => 'previous_order_button',
                'desc_tip' => true,
                'newids' => 'rs_sumo_reward_previous_order_points',
            ),
            array('type' => 'sectionend', 'id' => '_rs_update_social_settings'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemUpdate::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemUpdate::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemUpdate::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && isset($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

}

new FPRewardSystemUpdate();


/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemUpdate', 'reward_system_tab_settings'), 200);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_updates', array('FPRewardSystemUpdate', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemUpdate', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_updates', array('FPRewardSystemUpdate', 'reward_system_register_admin_settings'));

function fp_rs_add_product_selection_normal() {
    global $woocommerce;    
    if ((float) $woocommerce->version > (float) ('2.2.0')) {
        ?>
        <tr valign="top">
            <th class="titledesc" scope="row">
                <label for="rs_select_particular_products"><?php _e('Select Particular Products', 'rewardsystem'); ?></label>
            </th>
            <td class="forminp forminp-select">

                <input type="hidden" class="wc-product-search" style="width: 100%;" id="rs_select_particular_products" name="rs_select_particular_products" data-placeholder="<?php _e('Search for a product&hellip;', 'rewardsystem'); ?>" data-action="woocommerce_json_search_products_and_variations" data-multiple="true" data-selected="<?php
                 $json_ids = array();
                if (get_option('rs_select_particular_products') != "") {
                    $list_of_produts = get_option('rs_select_particular_products');
                    if(!is_array($list_of_produts)){
                        $list_of_produts = explode(',', $list_of_produts);
                        $product_ids = array_filter(array_map('absint', (array) explode(',', get_option('rs_select_particular_products'))));                    
                    }else{
                        $product_ids = $list_of_produts;
                    }
                    if($product_ids != NULL){
                    foreach ($product_ids as $product_id) {
                        if(isset($product_id)){
                            $product = wc_get_product($product_id);
                            if(is_object($product)){
                                $json_ids[$product_id] = wp_kses_post($product->get_formatted_name());
                            }
                        }
                    } echo esc_attr(json_encode($json_ids));
                    }
                }
                ?>" value="<?php echo implode(',', array_keys($json_ids)); ?>" />                
            </td>            
        </tr>
    <?php } else { ?>
        <tr valign="top">
            <th class="titledesc" scope="row">
                <label for="rs_select_particular_products"><?php _e('Select Particular Products', 'rewardsystem'); ?></label>
            </th>
            <td class="forminp forminp-select">
                <select multiple name="rs_select_particular_products" style='width:550px;' id='rs_select_particular_products' class="rs_select_particular_products">
                    <?php
                    if ((array) get_option('rs_select_particular_products') != "") {
                        $list_of_produts = (array) get_option('rs_select_particular_products');
                        foreach ($list_of_produts as $rs_free_id) {
                            echo '<option value="' . $rs_free_id . '" ';
                            selected(1, 1);
                            echo '>' . ' #' . $rs_free_id . ' &ndash; ' . get_the_title($rs_free_id);
                        }
                    } else {
                        ?>
                        <option value=""></option>
                        <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <?php
    }
}

function fp_rs_add_product_selection_social($value) {

    global $woocommerce;

    if ((float) $woocommerce->version > (float) ('2.2.0')) {
        ?>

        <tr valign="top">
            <th class="titledesc" scope="row">
                <label for="rs_select_particular_social_products"><?php _e('Select Particular Products', 'rewardsystem'); ?></label>
            </th>
            <?php
            //var_dump(get_option('rs_select_particular_social_products'));
            //var_dump(array_filter(array_map('absint', (array) explode(',', get_option('rs_select_particular_social_products')))));
            ?>
            <td class="forminp forminp-select">
                <input type="hidden" class="wc-product-search" style="width: 100%;" id="rs_select_particular_social_products" name="rs_select_particular_social_products" data-placeholder="<?php _e('Search for a product&hellip;', 'rewardsystem'); ?>" data-action="woocommerce_json_search_products_and_variations" data-multiple="true" data-selected="<?php
          $json_idss = array();
           if (get_option('rs_select_particular_social_products') != "") {
            //echo "onetwothress";
            $list_of_produtss = get_option('rs_select_particular_social_products');
            //  var_dump($list_of_produts);
            $list_of_productss = explode(',', get_option('rs_select_particular_social_products'));
            //var_dump($list_of_productss);
            $product_idss = array_filter(array_map('absint', (array) explode(',', get_option('rs_select_particular_social_products'))));
            //var_dump($product_idss);
            //$product_ids = $list_of_produts;
          
            foreach ($product_idss as $product_ide) {
                $product = wc_get_product($product_ide);
                $json_idss[$product_ide] = wp_kses_post($product->get_formatted_name());
            } echo esc_attr(json_encode($json_idss));
        }
            ?>" value="<?php echo implode(',', array_keys($json_idss)); ?>" />
            </td>
        </tr>
    <?php } else { ?>

        <tr valign="top">
            <th class="titledesc" scope="row">
                <label for="rs_select_particular_social_products"><?php _e('Select Particular Products', 'rewardsystem'); ?></label>
            </th>
            <td class="forminp forminp-select">
                <select multiple name="rs_select_particular_social_products" style='width:550px;' id='rs_select_particular_social_products' class="rs_select_particular_social_products">
                    <?php
                    if ((array) get_option('rs_select_particular_social_products') != "") {
                        $list_of_produts = (array) get_option('rs_select_particular_social_products');
                        foreach ($list_of_produts as $rs_free_id) {
                            echo '<option value="' . $rs_free_id . '" ';
                            selected(1, 1);
                            echo '>' . ' #' . $rs_free_id . ' &ndash; ' . get_the_title($rs_free_id);
                        }
                    } else {
                        ?>
                        <option value=""></option>
                        <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <?php
    }
}

add_action('woocommerce_admin_field_selected_social_products', 'fp_rs_add_product_selection_social');

add_action('woocommerce_update_option_selected_social_products', 'fp_save_rs_product_selection_social', 999);

function fp_save_rs_product_selection_social($value) {
    //  update_option('social_products', (array) explode(',', $_POST['rs_select_particular_social_products']));

    update_option('rs_select_particular_social_products', $_POST['rs_select_particular_social_products']);
}

add_action('woocommerce_update_option_selected_products', 'fp_save_rs_product_selection', 999);

function fp_save_rs_product_selection($value) {
    //update_option('social_ssproducts', (array) explode(',', $_POST['rs_select_particular_products']));
    update_option('rs_select_particular_products', $_POST['rs_select_particular_products']);
}

add_action('woocommerce_admin_field_selected_products', 'fp_rs_add_product_selection_normal');

function add_update_chosen_reward_system() {
    global $woocommerce;
    if (isset($_GET['page'])) {
        if ($_GET['page'] == 'rewardsystem_callback') {
            ?>
            <script type="text/javascript">
                jQuery(function () {
                    jQuery('.gif_rs_sumo_reward_button').css('display', 'none');
                    jQuery('.gif_rs_sumo_reward_button_social').css('display', 'none');

                });

            <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                    jQuery(function () {

                        // Ajax Chosen Product Selectors

                        jQuery("select.rs_select_particular_products").ajaxChosen({
                            method: 'GET',
                            url: '<?php echo admin_url('admin-ajax.php'); ?>',
                            dataType: 'json',
                            afterTypeDelay: 100,
                            data: {
                                action: 'woocommerce_json_search_products_and_variations',
                                security: '<?php echo wp_create_nonce("search-products"); ?>'
                            }
                        }, function (data) {
                            var terms = {};

                            jQuery.each(data, function (i, val) {
                                terms[i] = val;
                            });
                            return terms;
                        });
                    });


                    jQuery(function () {
                        // Ajax Chosen Product Selectors
                        jQuery("select.rs_select_particular_social_products").ajaxChosen({
                            method: 'GET',
                            url: '<?php echo admin_url('admin-ajax.php'); ?>',
                            dataType: 'json',
                            afterTypeDelay: 100,
                            data: {
                                action: 'woocommerce_json_search_products_and_variations',
                                security: '<?php echo wp_create_nonce("search-products"); ?>'
                            }
                        }, function (data) {
                            var terms = {};

                            jQuery.each(data, function (i, val) {
                                terms[i] = val;
                            });
                            return terms;
                        });
                    });
            <?php } ?>
                jQuery(document).ready(function () {

            <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                        jQuery('.rs_select_particular_products').chosen();
                        jQuery('#rs_select_particular_social_products').chosen();
                        jQuery('#rs_select_particular_categories').chosen();
                        jQuery('#rs_select_particular_social_categories').chosen();
            <?php } else { ?>
                        jQuery('#rs_select_particular_categories').select2();
                        jQuery('#rs_select_particular_social_categories').select2();
            <?php } ?>

                    if ((jQuery('.rs_which_product_selection').val() === '1')) {
                        jQuery('#rs_select_particular_products').parent().parent().hide();
                        jQuery('#rs_select_particular_categories').parent().parent().hide();
                    } else if (jQuery('.rs_which_product_selection').val() === '2') {
                        jQuery('#rs_select_particular_products').parent().parent().show();
                        jQuery('#rs_select_particular_categories').parent().parent().hide();
                    } else if (jQuery('.rs_which_product_selection').val() === '3') {
                        jQuery('#rs_select_particular_products').parent().parent().hide();
                        jQuery('#rs_select_particular_categories').parent().parent().hide();
                    } else {
                        jQuery('#rs_select_particular_categories').parent().parent().show();
                        jQuery('#rs_select_particular_products').parent().parent().hide();
                    }

                    if ((jQuery('.rs_which_social_product_selection').val() === '1')) {
                        jQuery('#rs_select_particular_social_products').parent().parent().hide();
                        jQuery('#rs_select_particular_social_categories').parent().parent().hide();
                    } else if (jQuery('.rs_which_social_product_selection').val() === '2') {
                        jQuery('#rs_select_particular_social_products').parent().parent().show();
                        jQuery('#rs_select_particular_social_categories').parent().parent().hide();
                    } else if (jQuery('.rs_which_social_product_selection').val() === '3') {
                        jQuery('#rs_select_particular_social_products').parent().parent().hide();
                        jQuery('#rs_select_particular_social_categories').parent().parent().hide();
                    } else {
                        jQuery('#rs_select_particular_social_categories').parent().parent().show();
                        jQuery('#rs_select_particular_social_products').parent().parent().hide();
                    }

                    jQuery('.rs_which_product_selection').change(function () {
                        if ((jQuery(this).val() === '1') || (jQuery(this).val() === '3')) {
                            jQuery('#rs_select_particular_products').parent().parent().hide();
                            jQuery('#rs_select_particular_categories').parent().parent().hide();
                        } else if (jQuery(this).val() === '2') {
                            jQuery('#rs_select_particular_products').parent().parent().show();
                            jQuery('#rs_select_particular_categories').parent().parent().hide();
                        } else {
                            jQuery('#rs_select_particular_categories').parent().parent().show();
                            jQuery('#rs_select_particular_products').parent().parent().hide();
                        }
                    });

                    jQuery('.rs_which_social_product_selection').change(function () {
                        if ((jQuery(this).val() === '1') || (jQuery(this).val() === '3')) {
                            jQuery('#rs_select_particular_social_products').parent().parent().hide();
                            jQuery('#rs_select_particular_social_categories').parent().parent().hide();
                        } else if (jQuery(this).val() === '2') {
                            jQuery('#rs_select_particular_social_products').parent().parent().show();
                            jQuery('#rs_select_particular_social_categories').parent().parent().hide();
                        } else {
                            jQuery('#rs_select_particular_social_categories').parent().parent().show();
                            jQuery('#rs_select_particular_social_products').parent().parent().hide();
                        }
                    });

                    var selectrangevalue = jQuery('#rs_sumo_select_order_range').val();
                    if (selectrangevalue === '1') {
                        jQuery('#rs_from_date').parent().parent().hide();
                    } else {
                        jQuery('#rs_from_date').parent().parent().show();
                    }
                    jQuery('#rs_sumo_select_order_range').change(function () {
                        if (jQuery(this).val() === '1') {
                            jQuery('#rs_from_date').parent().parent().hide();
                        } else {
                            jQuery('#rs_from_date').parent().parent().show();
                        }
                    });
                });

            </script>
            <?php
        }
    }
}

add_action('admin_head', 'add_update_chosen_reward_system');
