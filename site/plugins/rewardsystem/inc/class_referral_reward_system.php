<?php

class FPReferralSystem {

    public static function generate_referral_key() {
        if (is_user_logged_in()) {
            ?>

            <div class="referral_field" style="margin-top:10px;">
                <input type="text" size="50" name="generate_referral_field" id="generate_referral_field" required="required" value="<?php echo get_option('rs_prefill_generate_link'); ?>"><input type="submit" style="margin-left:10px;" class="button <?php echo get_option('rs_extra_class_name_generate_referral_link'); ?>" name="refgeneratenow" id="refgeneratenow" value="<?php _e('Generate Referral Link', 'rewardsystem'); ?>"/>
            </div>

            <script type="text/javascript">
                jQuery(document).ready(function () {
                    jQuery('#refgeneratenow').click(function () {
                        var referral_generate = jQuery('#generate_referral_field').val();
                        if (referral_generate === '') {
                            jQuery('#generate_referral_field').css('outline', 'red solid');
                            return false;
                        } else {
                            jQuery('#generate_referral_field').css('outline', '');
                            var urlstring = jQuery('#generate_referral_field').val();
                            var dataparam = ({
                                action: 'ajaxify_referral',
                                url: urlstring,
                                userid: '<?php echo get_current_user_id(); ?>',
                            });
                            jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                                    function (response) {

                                        jQuery(".my_account_referral_link").load(window.location + " .my_account_referral_link");
                                        jQuery(document).ajaxComplete(function () {
                                            try {
                                                twttr.widgets.load();
                                                FB.XFBML.parse();
                                                gapi.plusone.go();
                                            } catch (ex) {
                                            }


                                            jQuery('.referralclick').click(function () {
                                                var getarraykey = jQuery(this).attr('data-array');
                                                //alert(jQuery(this).attr('data-array'));
                                                jQuery(this).parent().parent().hide();
                                                var dataparam = ({
                                                    action: 'unset_referral',
                                                    unsetarray: getarraykey,
                                                    userid: '<?php echo get_current_user_id(); ?>',
                                                });
                                                jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                                                        function (response) {
                                                            // alert(response);
                                                            var newresponse = response.replace(/\s/g, '');
                                                            if (newresponse === "success") {

                                                            }
                                                        });
                                                return false;
                                            });
                                        });
                                    });
                            return false;
                        }
                    });
                });
            </script>
            <?php
        } else {
            $myaccountlink = get_permalink(get_option('woocommerce_myaccount_page_id'));
            $myaccounttitle = get_the_title(get_option('woocommerce_myaccount_page_id'));
            echo 'Please Login to View this Page <a href=' . $myaccountlink . '> Login </a>';
        }
    }

    public static function ajaxify_referral_key() {
        $currentuserid = $_POST['userid'];
        $objectcurrentuser = get_userdata($currentuserid);
        if (get_option('rs_generate_referral_link_based_on_user') == '1') {
            $referralperson = $objectcurrentuser->user_login;
        } else {
            $referralperson = $currentuserid;
        }

        if (isset($_POST['url'])) {
            $refurl = add_query_arg('ref', $referralperson, $_POST['url']);
            $previousref = get_option('arrayref' . $currentuserid);

            $arrayref[] = $refurl . ',' . date("Y/m/d");
            if (is_array($previousref)) {
                $arrayref = array_unique(array_merge($previousref, $arrayref), SORT_REGULAR);
            }
            update_option('arrayref' . $currentuserid, $arrayref);
            //echo $refurl;
            echo "success";
        }
        exit();
    }

    public static function unset_array_referral_key() {
        $currentuserid = $_POST['userid'];
        if (isset($_POST['unsetarray'])) {
            $listarray = get_option('arrayref' . $currentuserid);
            unset($listarray[$_POST['unsetarray']]);
            update_option('arrayref' . $currentuserid, $listarray);
            echo "success";
        }
        exit();
    }

    public static function list_table_array() {
        $currentuserid = get_current_user_id();
        ?>
        <style type="text/css">
            .referralclick {
                border: 2px solid #a1a1a1;
                padding: 3px 9px;
                background: #dddddd;
                width: 5px;
                border-radius: 25px;
            }
            .referralclick:hover {
                cursor: pointer;
                background:red;
                color:#fff;
                border: 2px solid #fff;
            }
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery('.referralclick').click(function () {
                    var getarraykey = jQuery(this).attr('data-array');
                    //alert(jQuery(this).attr('data-array'));
                    console.log(jQuery(this).parent().parent().hide());
                    var dataparam = ({
                        action: 'unset_referral',
                        unsetarray: getarraykey,
                        userid: '<?php echo get_current_user_id(); ?>'
                    });
                    jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                            function (response) {
                                // alert(response);
                                var newresponse = response.replace(/\s/g, '');
                                if (newresponse === "success") {

                                }
                            });
                    return false;
                });
            });
        </script>
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>

        <script>!function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                if (!d.getElementById(id)) {
                    js = d.createElement(s);
                    js.id = id;
                    js.src = p + '://platform.twitter.com/widgets.js';
                    fjs.parentNode.insertBefore(js, fjs);
                }
            }(document, 'script', 'twitter-wjs');</script>

        <!-- Place this tag where you want the share button to render. -->


        <!-- Place this tag after the last share tag. -->
        <script type="text/javascript">
            (function () {
                var po = document.createElement('script');
                po.type = 'text/javascript';
                po.async = true;

                po.src = 'https://apis.google.com/js/platform.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(po, s);
            })();
        </script>
        <h3><?php _e('Generated Referral List', 'rewardsystem'); ?></h3>
        <table class="shop_table my_account_referral_link">
            <thead>
                <tr>
                    <th class="referral-number"><span class="nobr"><?php _e('S.No', 'rewardsystem'); ?></span></th>
                    <th class="referral-date"><span class="nobr"><?php _e('Date', 'rewardsystem'); ?></span></th>
                    <th class="referral-link"><span class="nobr"><?php _e('Referral Link', 'rewardsystem'); ?></span></th>
                    <th class="referral-social"><span class="nobr"><?php _e('Social', 'rewardsystem'); ?></span></th>
                    <th class="referral-actions"><span class="nobr"><?php _e('Actions', 'rewardsystem'); ?></span></th>
                </tr>
            </thead>
            <tbody>

                <?php
                if (is_array(get_option('arrayref' . $currentuserid))) {
                    $i = 1;
                    $j = 0;
                    foreach (get_option('arrayref' . $currentuserid) as $array => $key) {
                        $mainkey = explode(',', $key);
                        ?>
                        <tr class="referrals">
                            <td><?php echo $i; ?></td>
                            <td><?php echo $mainkey[1]; ?></td>
                            <td><?php
                                echo $mainkey[0];
                                ?></td>
                            <td><div class="fb-share-button" data-href="<?php echo $mainkey[0]; ?>" data-type="button"></div><br><a href="https://twitter.com/share" class="twitter-share-button" data-count="none" data-url="<?php echo $mainkey[0]; ?>">Tweet</a><br>
                                <div class="g-plusone" data-action="share" data-annotation="none" data-href="<?php echo $mainkey[0]; ?>"></div>
                            </td>
                            <td><span data-array="<?php echo $array; ?>" class="referralclick">x</span></td>
                        </tr>
                        <?php
                        $i++;
                        $j++;
                    }
                }
                ?>

            </tbody>
        </table>
        <?php
    }

    public static function count_statistics_referral() {
        if (isset($_GET['ref'])) {
            if (get_option('rs_referral_cookies_expiry') == '1') {
                setcookie('rsreferredusername', $_GET['ref'], time() + get_option('rs_referral_cookies_expiry_in_min'), '/');
            } elseif (get_option('rs_referral_cookies_expiry') == '2') {
                $hours = 60 * get_option('rs_referral_cookies_expiry_in_hours');
                setcookie('rsreferredusername', $_GET['ref'], time() + 60 * $hours, '/');
            } else {
                $days = 24 * get_option('rs_referral_cookies_expiry_in_days');
                setcookie('rsreferredusername', $_GET['ref'], time() + 60 * 60 * $days, '/');
            }

            $user = get_user_by('login', $_GET['ref']);
            if ($user != false) {
                $currentuserid = $user->ID;
            } else {
                $currentuserid = $_GET['ref'];
            }
            if (isset($_COOKIE['rsreferredusername'])) {
                $mycookies = $_COOKIE['rsreferredusername'];
            } else {
                $mycookies = '';
            }
            if ($mycookies == '') {
                $previouscount = get_user_meta($currentuserid, 'rsreferredusernameclickthrough', true);
                $updatedcount = $previouscount + 1;
                update_user_meta($currentuserid, 'rsreferredusernameclickthrough', $updatedcount);
            }
        }
    }

    public static function show_statistics_referral() {
        ?>
        <h2><?php _e('My Referrals', 'rewardsystem'); ?></h2>
        <table class="form-table">
            <tr>
                <th>Click Through Counts</th><td><?php echo get_user_meta(get_current_user_id(), 'rsreferredusername', true); ?></td>
            </tr>
            <tr>
                <th>Registered</th><td><?php echo get_user_meta(get_current_user_id(), 'rsreferreduserregisteredcount', true); ?></td>
            </tr>
        </table>
        <?php
    }

    public static function checkout_cookies_referral_meta($order_id, $order_posted) {

        if (isset($_COOKIE['rsreferredusername'])) {
            $refuser = get_user_by('login', $_COOKIE['rsreferredusername']);
            $myid = $refuser->ID;
            if (get_current_user_id() != $myid) {
                update_post_meta($order_id, '_referrer_name', $_COOKIE['rsreferredusername']);
                $getmetafromuser = get_user_meta($getcurrentuserid, '_update_user_order', true);
                $getorderlist[] = $order_id;
                if (is_array($getmetafromuser)) {
                    $mainmerge = array_merge($getmetafromuser, $getorderlist);
                } else {
                    $mainmerge = $getorderlist;
                }
                update_user_meta($getcurrentuserid, '_update_user_order', $mainmerge);
            }
        }
    }

}

new FPReferralSystem();
add_action('woocommerce_checkout_update_order_meta', array('FPReferralSystem', 'checkout_cookies_referral_meta'),10,2);
if (get_option('rs_show_hide_generate_referral') == '1') {
    add_action('woocommerce_before_my_account', array('FPReferralSystem', 'generate_referral_key'));
    add_action('woocommerce_before_my_account', array('FPReferralSystem', 'list_table_array'));
}
add_action('wp_ajax_nopriv_ajaxify_referral', array('FPReferralSystem', 'ajaxify_referral_key'));
add_action('wp_ajax_ajaxify_referral', array('FPReferralSystem', 'ajaxify_referral_key'));

add_action('wp_ajax_nopriv_unset_referral', array('FPReferralSystem', 'unset_array_referral_key'));
add_action('wp_ajax_unset_referral', array('FPReferralSystem', 'unset_array_referral_key'));

add_action('wp_head', array('FPReferralSystem', 'count_statistics_referral'));
?>
