<?php

class FPRewardSystemResetTab {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_resettab'] = __('Reset', 'rewardsystem');
        return $settings_tabs;
    }

    // Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        return apply_filters('woocommerce_rewardsystem_reset_settings', array(
            array(
                'name' => __('Reset Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Here you can select the Reset Options', 'rewardsystem'),
                'id' => '_rs_reward_point_troubleshoot_settings'
            ),
            array(
                'type' => 'reset_field',
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_point_troubleshoot_settings'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemResetTab::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemResetTab::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemResetTab::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

    public static function add_admin_field_to_reward_system() {
        global $woocommerce;
        ?>
        <style type="text/css">
            p.submit {
                display:none;
            }
            #mainforms {
                display:none;
            }
        </style>
        <tr valign="top">
            <th class="titledesc" scope="row">
                <label for="rs_reset_data_for"><?php _e('Reset Data for', 'rewardsystem'); ?></label>
            </th>
            <td class="forminp forminp-select">
                <input type="radio" name="rs_reset_data_all_users" id="rs_reset_data_all_users" class="rs_reset_data_for_users" value="1" checked="checked"/>All Users<br>
                <input type="radio" name="rs_reset_data_all_users" id="rs_reset_data_selected_users" class="rs_reset_data_for_users" value="2"/>Selected Users<br>
            </td>
        </tr>
        <tr class="rs_reset_selected_users">
            <th><label for="rs_reset_selected_user_data"><?php _e('Select Users to Reset Data', 'rewardsystem'); ?></label></th>
            <td><select style="width:550px;" id="rs_reset_selected_user_data" name="rs_reset_selected_user_data" multiple="multiple">
                    <?php
                    foreach (get_users() as $eachuser) {
                        ?>
                        <option value="<?php echo $eachuser->ID; ?>"><?php echo $eachuser->nickname; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr valign="top">
            <th class="titledesc" scope="row">
                <label for="rs_reset_user_reward_points">
                    <?php _e('Reset User Reward Points', 'rewardsystem'); ?>
                </label>
            </th>
            <td>
                <input type="checkbox" name="rs_reset_user_reward_points" id="rs_reset_user_reward_points" value="1" checked="checked"/>
            </td>
        </tr>
        <tr valign="top">
            <th class="titledesc" scope="row">
                <label for="rs_reset_user_log">
                    <?php _e('Reset User Logs', 'rewardsystem'); ?>
                </label>
            </th>
            <td>
                <input type="checkbox" name="rs_reset_user_log" id="rs_reset_user_log" value="1" checked="checked"/>
            </td>
        </tr>
        <tr valign="top">
            <th class="titledesc" scope="row">
                <label for="rs_reset_master_log">
                    <?php _e('Reset Master Logs', 'rewardsystem'); ?>
                </label>
            </th>
            <td>
                <input type="checkbox" name="rs_reset_master_log" id="rs_reset_master_log" value="1" checked="checked"/>
            </td>
        </tr>
        <tr valign="top">
            <td>
            </td>
            <td>
                <input type="submit" class="button-primary" name="rs_reset_data_submit" id="rs_reset_data_submit" value="Reset Data" /><br>
                <div class="rs_reset_success_data">

                </div>
            </td>
        </tr>
        <?php if (isset($_GET['page']) == 'rewardsystem_callback') { ?>
            <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                <script type="text/javascript">
                    jQuery(document).ready(function () {
                        jQuery('#rs_reset_selected_user_data').chosen();
                    });
                </script>
            <?php } else { ?>
                <script type="text/javascript">
                    jQuery(document).ready(function () {
                        jQuery('#rs_reset_selected_user_data').select2();
                    });
                </script>
                <?php
            }
        }
        ?>
        <script type="text/javascript">
            jQuery(function () {
        <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                    jQuery('.rs_reset_selected_user_data').chosen();
        <?php } else { ?>
                    jQuery('.rs_reset_selected_user_data').select2();
        <?php } ?>
                //alert(jQuery('.rs_reset_data_for_users').filter(":checked").val());
                var initialdata = jQuery('.rs_reset_data_for_users').filter(":checked").val();
                if (initialdata === '1') {
                    jQuery('.rs_reset_selected_users').css('display', 'none');
                } else {
                    jQuery('.rs_reset_selected_users').css('display', 'table-form-group');
                }
                //Get a Value on Change of Radio Button
                jQuery('.rs_reset_data_for_users').change(function () {
                    var presentdata = jQuery(this).filter(":checked").val();
                    if (presentdata === '1') {

                        jQuery('.rs_reset_selected_users').css('display', 'none');
                        jQuery('#rs_reset_master_log').parent().parent().css('display', 'block');
                    } else {
                        jQuery('.rs_reset_selected_users').css('display', 'table-row');
                        jQuery('#rs_reset_master_log').parent().parent().css('display', 'none');
                    }
                });

                jQuery('#rs_reset_data_submit').click(function () {
                    // alert("You Clicked Data Submit");
                    //return false;
                    var resetoptions = jQuery('.rs_reset_data_for_users').filter(":checked").val();
                    var selectedusers = jQuery('#rs_reset_selected_user_data').val();
                    var resetuserpoints = jQuery('#rs_reset_user_reward_points').filter(":checked").val();
                    var resetuserlogs = jQuery('#rs_reset_user_log').filter(":checked").val();
                    var resetmasterlogs = jQuery('#rs_reset_master_log').filter(":checked").val();
                    var dataparam = ({
                        action: 'rsresetuserdata',
                        resetdatafor: resetoptions,
                        rsselectedusers: selectedusers,
                        rsresetuserpoints: resetuserpoints,
                        rsresetuserlogs: resetuserlogs,
                        rsresetmasterlogs: resetmasterlogs,
                    });
                    jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                            function (response) {
                                // alert(response);
                                var newresponse = response.replace(/\s/g, '');
                                if (newresponse === 'success') {
                                    //jQuery('.submit .button-primary').trigger('click');
                                    // alert("Success");
                                    jQuery('.rs_reset_success_data').html("Data's Resetted Successfully");
                                    return false;
                                }
                            });
                    return false;
                });
            });
        </script>
        <?php
    }

    public static function process_reset_data_users() {
        if (isset($_POST['resetdatafor'])) {
            //delete_user_meta(1, '_my_reward_points');
            //If select option is selectedusers
            if (isset($_POST['rsresetuserpoints'])) {
                $proceeduserrewardpoints = $_POST['rsresetuserpoints'];
            } else {
                $proceeduserrewardpoints = '';
            }
            if (isset($_POST['rsresetuserlogs'])) {
                $proceeduserlogs = $_POST['rsresetuserlogs'];
            } else {
                $proceeduserlogs = '';
            }
            if (isset($_POST['rsresetmasterlogs'])) {
                $proceedmasterlogs = $_POST['rsresetmasterlogs'];
            } else {
                $proceedmasterlogs = "";
            }
            if ($_POST['resetdatafor'] == '2') {
                if (isset($_POST['rsselectedusers'])) {
                    foreach ($_POST['rsselectedusers']as $userlists) {

                        if ($proceeduserrewardpoints == '1') {
                            delete_user_meta($userlists, '_my_reward_points');
                        }
                        if ($proceeduserlogs == '1') {
                            delete_user_meta($userlists, '_my_points_log');
                        }
                        if ($proceedmasterlogs == '1') {
                            delete_user_meta($userlists, 'rsoveralllog');
                        }
                    }
                }
            } else {
                //If not then All Users
                foreach (get_users() as $eachuser) {
                    //echo $eachuser->ID;
                    if ($proceeduserrewardpoints == '1') {
                        delete_user_meta($eachuser->ID, '_my_reward_points');
                    }
                    if ($proceeduserlogs == '1') {
                        delete_user_meta($eachuser->ID, '_my_points_log');
                    }
                    if ($proceedmasterlogs == '1') {
                        delete_option('rsoveralllog');
                    }
                }
            }
            echo "success";
        }
        exit();
    }

}

new FPRewardSystemResetTab();


add_action('woocommerce_admin_field_reset_field', array('FPRewardSystemResetTab', 'add_admin_field_to_reward_system'));


add_action('wp_ajax_nopriv_rsresetuserdata', array('FPRewardSystemResetTab', 'process_reset_data_users'));
add_action('wp_ajax_rsresetuserdata', array('FPRewardSystemResetTab', 'process_reset_data_users'));

/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemResetTab', 'reward_system_tab_settings'), 999);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_resettab', array('FPRewardSystemResetTab', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemResetTab', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_resettab', array('FPRewardSystemResetTab', 'reward_system_register_admin_settings'));
?>