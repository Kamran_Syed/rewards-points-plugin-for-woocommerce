<?php

class FPRewardSystemAddRemovePoints {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_pointsrule'] = __('Add/Remove Reward Points', 'rewardsystem');
        return $settings_tabs;
    }

    // Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        return apply_filters('woocommerce_rewardsystem_addremove_settings', array(
            array(
                'name' => __('', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Add/Remove Reward Points for Users', 'rewardsystem'),
                'id' => '_rs_add_remove_reward_points'
            ),
            array(
                'name' => __('Add/Remove Reward Points', 'rewardsystem'),
                'desc' => __('Select', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_reward_signup',
                'css' => 'min-width:150px;',
                'std' => '1000',
                'type' => 'text',
                'newids' => 'rs_reward_signup',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Reward Points for Product Review', 'rewardsystem'),
                'desc' => __('Please Enter the Reward Points that will be earned for Reviewing a Product', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_reward_product_review',
                'css' => 'min-width:150px;',
                'std' => '200',
                'type' => 'text',
                'newids' => 'rs_reward_product_review',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_add_remove_reward_points'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemPointsRule::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemPointsRule::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemPointsRule::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

}

new FPRewardSystemAddRemovePoints();


/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemAddRemovePoints', 'reward_system_tab_settings'), 101);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_pointsrule', array('FPRewardSystemAddRemovePoints', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemAddRemovePoints', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_pointsrule', array('FPRewardSystemAddRemovePoints', 'reward_system_register_admin_settings'));
?>