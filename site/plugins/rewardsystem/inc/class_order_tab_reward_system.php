<?php


class FPRewardSystemOrderTab {
    
    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_ordertab'] = __('Order', 'rewardsystem');
        return $settings_tabs;
    }
    
     public static function rewardsystem_admin_fields() {
        global $woocommerce;
        return apply_filters('woocommerce_rewardsystem_order_settings', array(
            array(
                'name' => __('Order Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Here you can select the Order Options', 'rewardsystem'),
                'id' => '_rs_reward_order'
            ),
            array(
                'name'=>__('Enable Message for Earned Points','rewardsystem'),
                'desc'=>__('Enable Message for Earned Points','rewardsystem'),
                'id'=>'rs_enable_msg_for_earned_points',
                'newids'=>'rs_enable_msg_for_earned_points',
                'class'=>'rs_enable_msg_for_earned_points',
                'type'=>'checkbox',
                'desc_tip'=>true,
            ),
            array(
                'name'=>__('Message For Earned Points','rewardsystem'),
                'desc'=>__('Message For Earned Points','rewardsystem'),
                'id'=>'rs_msg_for_earned_points',
                'newids'=>'rs_msg_for_earned_points',
                'class'=>'rs_msg_for_earned_points',
                'css' => 'min-width:550px;',
                'std'=>'Points Earned For this Order [earnedpoints]',
                'type'=>'textarea',
                'desc_tip'=>true,
            ),
            array(
                'name'=>__('Enable Message for Redeem Points','rewardsystem'),
                'desc'=>__('Enable Message for Redeem Points','rewardsystem'),
                'id'=>'rs_enable_msg_for_redeem_points',
                'newids'=>'rs_enable_msg_for_redeem_points',
                'class'=>'rs_enable_msg_for_redeem_points',
                'type'=>'checkbox',
                'desc_tip'=>true,
            ),
            array(
                'name'=>__('Message For Redeem Points','rewardsystem'),
                'desc'=>__('Message For Redeem Points','rewardsystem'),
                'id'=>'rs_msg_for_redeem_points',
                'newids'=>'rs_msg_for_redeem_points',
                'class'=>'rs_msg_for_redeem_points',
                'css' => 'min-width:550px;',
                'std'=>'Points Redeem For this Order [redeempoints]',
                'type'=>'textarea',
                'desc_tip'=>true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_order'),
        ));
    }
    
       /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemOrderTab::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemOrderTab::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemOrderTab::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && isset($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }
    
   
}
new FPRewardSystemOrderTab();


//add_action('woocommerce_order_status_complete', array('FPRewardSystemOrderTab', 'get_earned_points'));
// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemOrderTab', 'reward_system_tab_settings'), 999);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_ordertab', array('FPRewardSystemOrderTab', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemOrderTab', 'reward_system_default_settings'));


// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_ordertab', array('FPRewardSystemOrderTab', 'reward_system_register_admin_settings'));
?>
