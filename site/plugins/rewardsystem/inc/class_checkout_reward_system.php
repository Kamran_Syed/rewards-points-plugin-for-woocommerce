<?php
/*
 * Checkout Tab Settings
 */

class FPRewardSystemCheckoutTab {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_checkouttab'] = __('Checkout', 'rewardsystem');
        return $settings_tabs;
    }

    // Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        $rsproductlist = array();
        $rsproductids = array();
        $rsproduct_name = array();
        $particular_product_id = get_option('rs_select_product_for_purchase_using_points');
        $particular_ids = !empty($particular_product_id) ? array_map('absint', (array) $particular_product_id) : NULL;
        //var_dump($particular_ids);
        if(function_exists('get_product')){
        if (!is_wp_error($particular_ids)) {
            if(!empty($particular_ids)) {
        if ($particular_ids != NULL) {
            foreach ($particular_ids as $particular_id) {
                $rsproductids[] = $particular_id;
                
                $productobject = get_product($particular_id);                
                 $rsproduct_name[] = $productobject->get_formatted_name($rsproductids);                 
                }
            }
                
        $rsproductlist = array_combine((array) $rsproductids, (array) $rsproduct_name);
        }
        }
        }
        return apply_filters('woocommerce_rewardsystem_checkout_settings', array(
            array(
                'name' => __('Checkout Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Here you can select the Checkout Customization Options', 'rewardsystem'),
                'id' => '_rs_reward_point_checkout_settings'
            ),
            array(
                'name' => __('Show/Hide Redeeming Field in Checkout Page', 'rewardsystem'),
                'desc' => __('Show/Hide Redeeming Field in Checkout Page of WooCommerce', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_show_hide_redeem_field_checkout',
                'css' => '',
                'std' => '2',
                'type' => 'select',
                'newids' => 'rs_show_hide_redeem_field_checkout',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
                'desc_tip' => true,
            ),
            array(
                'name' => __('Redeeming Field Type', 'rewardsystem'),
                'desc' => __('Select the type of Redeeming to used in Cart Page of WooCommerce', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_redeem_field_type_option_checkout',
                'css' => '',
                'std' => '1',
                'type' => 'select',
                'newids' => 'rs_redeem_field_type_option_checkout',
                'options' => array(
                    '1' => __('Default', 'rewardsystem'),
                    '2' => __('Button', 'rewardsystem'),
                ),
                'desc_tip' => true,
            ),
            array(
                'name' => __('Redeeming Field label', 'rewardsystem'),
                'desc' => __('This Text will be displayed as redeeming field label in checkout page', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_reedming_field_label_checkout',
                'css' => 'min-width:550px;',
                'std' => 'Have Reward Points ?',
                'type' => 'text',
                'newids' => 'rs_reedming_field_label_checkout',
                'class' => 'rs_reedming_field_label_checkout',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Redeeming Field link label', 'rewardsystem'),
                'desc' => __('This Text will be displayed as redeeming field link label in checkout page', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_reedming_field_link_label_checkout',
                'css' => 'min-width:550px;',
                'std' => 'Redeem it',
                'type' => 'text',
                'newids' => 'rs_reedming_field_link_label_checkout',
                'class' => 'rs_reedming_field_link_label_checkout',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Show/Hide Redeem It Link Call To Action', 'rewardsystem'),
                'desc' => __('Show/Hide Redeem It Link Call To Action in WooCommerce', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_show_hide_redeem_it_field_checkout',
                'css' => '',
                'std' => '1',
                'type' => 'select',
                'newids' => 'rs_show_hide_redeem_it_field_checkout',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
                'desc_tip' => true,
            ),
            array(
                'name' => __('Enable Selected Products for Purchase using reward points', 'rewardsystem'),
                'desc' => __('Enable Products Purchase for Selceted Products Using Earned Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_enable_selected_product_for_purchase_using_points',
                'class' => 'rs_enable_selected_product_for_purchase_using_points',
                'newids' => 'rs_enable_selected_product_for_purchase_using_points',
                'type' => 'checkbox',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Select Products for Purchase Using Points', 'rewardsystem'),
                'desc' => __('Select Products for Purchase Using Earned Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_select_product_for_purchase_using_points',
                'css' => 'min-width:350px;',
                'type' => 'rs_product_for_purchase',
                'newids' => 'rs_select_product_for_purchase_using_points',
                'class' => 'rs_select_product_for_purchase_using_points rs_ajax_chosen_select_products_for_purchase_using_points',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Error Message when other products added to Cart Page', 'rewardsystem'),
                'desc' => __('Error Message when other products added to Cart Page', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_errmsg_when_other_products_added_to_cart_page',
                'css' => 'min-width:550px;',
                'std' => '[productname] is removed from the Cart.Because it can be purchased only through Reward points',
                'type' => 'textarea',
                'newids' => 'rs_errmsg_when_other_products_added_to_cart_page',
                'class' => 'rs_errmsg_when_other_products_added_to_cart_page',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Percentage of Cart Total to be Redeemed', 'rewardsystem'),
                'desc' => __('Enter the Percentage of the cart total that has to be Redeemed', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_percentage_cart_total_redeem_checkout',
                'css' => 'min-width:550px;',
                'std' => '100 ',
                'type' => 'text',
                'newids' => 'rs_percentage_cart_total_redeem_checkout',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Redeeming Button Message ', 'rewardsystem'),
                'desc' => __('Enter the Message for the Redeeming Button', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_redeeming_button_option_message_checkout',
                'css' => 'min-width:550px;',
                'std' => '[cartredeempoints] points worth of [currencysymbol] [pointsvalue] will be Redeemed',
                'type' => 'textarea',
                'newids' => 'rs_redeeming_button_option_message_checkout',
                'desc_tip' => true,
            ),
//            array(
//                'name' => __('Redeeming Button Message when User Points Value is less than cart Total', 'rewardsystem'),
//                'desc' => __('Enter the Message for the Redeeming Button', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_redeeming_button_option_message_less_checkout',
//                'css' => 'min-width:550px;',
//                'std' => '[cartredeempoints] points will be Redeemed',
//                'type' => 'textarea',
//                'newids' => 'rs_redeeming_button_option_message_less_checkout',
//                'desc_tip' => true,
//            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_point_checkout_settings'),
            array(
                'name' => __('Custom CSS Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => 'Try !important if styles doesn\'t apply ',
                'id' => '_rs_checkout_custom_css_settings',
            ),
            array(
                'name' => __('Custom CSS', 'rewardsystem'),
                'desc' => __('Enter the Custom CSS for the Cart Page ', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_checkout_page_custom_css',
                'css' => 'min-width:350px; min-height:350px;',
                'std' => '#rs_apply_coupon_code_field { } #mainsubmi { } .fp_apply_reward{ }',
                'type' => 'textarea',
                'newids' => 'rs_checkout_page_custom_css',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_checkout_custom_css_settings'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
       woocommerce_admin_fields(FPRewardSystemCheckoutTab::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
       woocommerce_update_options(FPRewardSystemCheckoutTab::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemCheckoutTab::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && isset($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

    public static function add_product_selection_normal_compatibility() {

        global $woocommerce;
        if ((float) $woocommerce->version > (float) ('2.2.0')) {
            ?>
            <tr valign="top">
                <th class="titledesc" scope="row">
                    <label for="rs_select_product_for_purchase_using_points"><?php _e('Select Products for Purchase using Points', 'rewardsystem'); ?></label>
                </th>
                <td class="forminp forminp-select">
                    <input type="hidden" class="wc-product-search" style="width: 100%;" id="rs_select_product_for_purchase_using_points"  name="rs_select_product_for_purchase_using_points" data-placeholder="<?php _e('Search for a product&hellip;', 'rewardsystem'); ?>" data-action="woocommerce_json_search_products_and_variations" data-multiple="true" data-selected="<?php
                   $json_ids = array();
                    if (get_option('rs_select_product_for_purchase_using_points') != "") {
                        $list_of_produts = get_option('rs_select_product_for_purchase_using_points');
                        $product_ids = array_filter(array_map('absint', (array) explode(',', get_option('rs_select_product_for_purchase_using_points'))));
                        
                        foreach ($product_ids as $product_id) {
                            $product = wc_get_product($product_id);
                            $json_ids[$product_id] = wp_kses_post($product->get_formatted_name());
                        } echo esc_attr(json_encode($json_ids));
                    }
                    ?>" value="<?php echo implode(',', array_keys($json_ids)); ?>" />
                </td>
            </tr>
        <?php } else { ?>
            <tr valign="top">
                <th class="titledesc" scope="row">
                    <label for="rs_select_product_for_purchase_using_points"><?php _e('Select Products for Purchase using Points', 'rewardsystem'); ?></label>
                </th>
                <td class="forminp forminp-select">
                    <select multiple name="rs_select_product_for_purchase_using_points" style='width:550px;' id='rs_select_product_for_purchase_using_points' class="rs_select_product_for_purchase_using_points">
                        <?php
                        if ((array) get_option('rs_select_product_for_purchase_using_points') != "") {
                            $list_of_produts = (array) get_option('rs_select_product_for_purchase_using_points');
                            foreach ($list_of_produts as $rs_free_id) {
                                echo '<option value="' . $rs_free_id . '" ';
                                selected(1, 1);
                                echo '>' . ' #' . $rs_free_id . ' &ndash; ' . get_the_title($rs_free_id);
                            }
                        } else {
                            ?>
                            <option value=""></option>
                            <?php
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <?php
        }
    }

    public static function save_product_selection_normal_compatibility() {
        global $woocommerce;
        update_option('rs_select_product_for_purchase_using_points', $_POST['rs_select_product_for_purchase_using_points']);
    }

    public static function rs_purchase_product_using_point() {
        global $woocommerce;
        if (isset($_GET['tab'])) {
            if ($_GET['tab'] == 'rewardsystem_checkouttab') {
                ?>
                <script type="text/javascript">
                <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                        jQuery(function () {

                            // Ajax Chosen Product Selectors
                            jQuery("select.rs_select_product_for_purchase_using_points").ajaxChosen({
                                method: 'GET',
                                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                                dataType: 'json',
                                afterTypeDelay: 100,
                                data: {
                                    action: 'woocommerce_json_search_products_and_variations',
                                    security: '<?php echo wp_create_nonce("search-products"); ?>'
                                }
                            }, function (data) {
                                var terms = {};

                                jQuery.each(data, function (i, val) {
                                    terms[i] = val;
                                });
                                return terms;
                            });
                        });
                <?php } ?>
                    jQuery(document).ready(function () {
                        var enable_selected_product_for_purchase = jQuery('#rs_enable_selected_product_for_purchase_using_points').is(':checked') ? 'yes' : 'no';
                        if (enable_selected_product_for_purchase == 'yes') {
                            jQuery('#rs_select_product_for_purchase_using_points').parent().parent().show();
                        } else {
                            jQuery('#rs_select_product_for_purchase_using_points').parent().parent().hide();
                        }

                        jQuery('#rs_enable_selected_product_for_purchase_using_points').change(function () {
                            jQuery('#rs_select_product_for_purchase_using_points').parent().parent().toggle();
                        });
                    });
                </script>
                <?php
            }
        }
    }

    public static function apply_redeem_points_checkout_button_type_settings() {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                if (jQuery("#rs_redeem_field_type_option_checkout").val() == '1') {
                    jQuery("#rs_percentage_cart_total_redeem_checkout").parent().parent().hide();
                    jQuery("#rs_redeeming_button_option_message_checkout").parent().parent().hide();
                    //jQuery("#rs_redeeming_button_option_message_less_checkout").parent().parent().hide();
                    jQuery("#rs_show_hide_redeem_it_field_checkout").parent().parent().show();
                } else {
                    jQuery("#rs_percentage_cart_total_redeem_checkout").parent().parent().show();
                    jQuery("#rs_redeeming_button_option_message_checkout").parent().parent().show();
                    //jQuery("#rs_redeeming_button_option_message_less_checkout").parent().parent().show();
                    jQuery("#rs_show_hide_redeem_it_field_checkout").parent().parent().hide();
                }
                jQuery("#rs_redeem_field_type_option_checkout").change(function () {
                    jQuery("#rs_percentage_cart_total_redeem_checkout").parent().parent().toggle();
                    jQuery("#rs_redeeming_button_option_message_checkout").parent().parent().toggle();
                    //jQuery("#rs_redeeming_button_option_message_less_checkout").parent().parent().toggle();
                    jQuery("#rs_show_hide_redeem_it_field_checkout").parent().parent().toggle();
                });
            });
        </script>
        <?php
    }

}

new FPRewardSystemCheckoutTab();


/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemCheckoutTab', 'reward_system_tab_settings'), 106);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_checkouttab', array('FPRewardSystemCheckoutTab', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemCheckoutTab', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_checkouttab', array('FPRewardSystemCheckoutTab', 'reward_system_register_admin_settings'));

add_action('admin_head', array('FPRewardSystemCheckoutTab', 'apply_redeem_points_checkout_button_type_settings'));

add_action('admin_head', array('FPRewardSystemCheckoutTab', 'rs_purchase_product_using_point'));

add_action('woocommerce_update_option_rs_product_for_purchase', array('FPRewardSystemCheckoutTab', 'save_product_selection_normal_compatibility'));


add_action('woocommerce_admin_field_rs_product_for_purchase', array('FPRewardSystemCheckoutTab', 'add_product_selection_normal_compatibility'));
