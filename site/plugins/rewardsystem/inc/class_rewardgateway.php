<?php

add_action('plugins_loaded', 'init_reward_gateway_class');

function init_reward_gateway_class() {
    if (!class_exists('WC_Payment_Gateway'))
        return;

    class WC_Reward_Gateway extends WC_Payment_Gateway {

        public function __construct() {
            global $woocommerce;

            $this->id = 'reward_gateway';
            $this->method_title = __('SUMO Reward Points Gateway', 'woocommerce');
            $this->has_fields = false; //Load Form Fields
            $this->init_form_fields();
            $this->init_settings();
            $this->title = $this->get_option('gateway_titles');
            $this->description = $this->get_option('gateway_descriptions');
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
        }

        function init_form_fields() {
            $this->form_fields = array(
                'enabled' => array(
                    'title' => __('Enable/Disable', 'woocommerce'),
                    'type' => 'checkbox',
                    'label' => __('Enable Rewards Point Gateway', 'woowcommerce'),
                    'default' => 'no'
                ),
                'gateway_titles' => array(
                    'title' => __('Title', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('This Controls the Title which the user sees during checkout', 'woocommerce'),
                    'default' => __('SUMO Reward Points', 'woocommerce'),
                    'desc_tip' => true,
                ),
                'gateway_descriptions' => array(
                    'title' => __('Description', 'woocommerce'),
                    'type' => 'textarea',
                    'description' => __('This controls the description which the user sees during checkout.', 'woocommerce'),
                    'default' => __('Pay with your SUMO Reward Points', 'woocommerce'),
                    'desc_tip' => true,
                ),
                'error_payment_gateway' => array(
                    'title' => 'Error Message',
                    'type' => 'textarea',
                    'description' => __('This Controls the errror message which is displayed during Checkout', 'woocommerce'),
                    'desc_tip' => true,
                    'default' => __('You need [needpoints] Points in your Account .But You have only [userpoints] Points.', 'rewardsystem'),
                ),
            );
        }

        function process_payment($order_id) {
            global $woocommerce;
            $order = new WC_Order($order_id);
            
            $ordertotal = $order->get_total();
            $getuserid = $order->user_id;            
            $couponcodeuserid = get_userdata($getuserid);
            $couponcodeuserlogin=$couponcodeuserid->user_login;
            //var_dump($couponcodeuserlogin);
            $usernickname = 'sumo_' . strtolower("$couponcodeuserlogin");
            $current_conversion = get_option('rs_redeem_point');
            $point_amount = get_option('rs_redeem_point_value'); 
            $getmyrewardpoints = get_user_meta($getuserid, '_my_reward_points', true);                                           
            if (isset($woocommerce->cart->coupon_discount_amounts["$usernickname"])) {
            $total1=$woocommerce->cart->coupon_discount_amounts[$usernickname];                                     
            $total2 = $total1 * $current_conversion;
            $total3 = $total2 / $point_amount;
            $userpoints=$getmyrewardpoints-$total3;
            var_dump($userpoints);
            }else{
                $userpoints=$getmyrewardpoints;
            }
            $redeemedamount = $ordertotal * $current_conversion;            
            $redeemedpoints = $redeemedamount / $point_amount;            
            if ($redeemedpoints > $getmyrewardpoints) {
                $error_msg = $this->get_option('error_payment_gateway');
                $find = array('[userpoints]', '[needpoints]');
                $replace = array($userpoints, $redeemedpoints);
                $finalreplace = str_replace($find, $replace, $error_msg);
                wc_add_notice(__($finalreplace, 'woocommerce'), 'error');
                return;
            } else {
                $totalrewardpoints = $getmyrewardpoints - $redeemedpoints;
                update_user_meta($order->user_id, '_my_reward_points', $totalrewardpoints);
            }
            
            //Status of the product purchased
            //$order->update_status('on-hold', __( 'Awaiting cheque payment', 'woocommerce' ));
            $order->payment_complete();
            $order->update_status('completed');
            //Reduce Stock Levels
            $order->reduce_order_stock();

            //Remove Cart
            $woocommerce->cart->empty_cart();

            //Redirect the User
            return array(
                'result' => 'success',
                'redirect' => $this->get_return_url($order)
            );
            wc_add_notice(__('Payment error:', 'woothemes') . $error_message, 'error');
            return;
        }

    }

    add_filter('woocommerce_available_payment_gateways', 'filter_gateway',10,1);

    function filter_gateway($gateways) {

        global $woocommerce;
        $enableproductpurchase = get_option('rs_enable_selected_product_for_purchase_using_points');
//            //var_dump($enableproductpurchase);
        if (($enableproductpurchase == 'yes')) {
            foreach ($woocommerce->cart->cart_contents as $key => $values) {
                $productid = $values['product_id'];
                //var_dump($productid);
                
                if (!is_array(get_option('rs_select_product_for_purchase_using_points'))) {
                    if((get_option('rs_select_product_for_purchase_using_points')!=''&&(get_option('rs_select_product_for_purchase_using_points')!=NULL))) {
                        $product_id = explode(',', get_option('rs_select_product_for_purchase_using_points'));
                    }
                } else {
                    $product_id = get_option('rs_select_product_for_purchase_using_points');
                }

                //$product_id = get_option('rs_select_product_for_purchase_using_points');
                //var_dump($product_id);

                if (in_array($productid, (array) $product_id)) {
                    foreach (WC()->payment_gateways->payment_gateways() as $gateway) {
                        if ($gateway->id != 'reward_gateway') {
                            unset($gateways[$gateway->id]);
                        }
                    }
                }
            }
           
        }
         return $gateways!='NULL'?$gateways:array();
    }

    add_filter('woocommerce_add_to_cart_validation', 'mainfunction_testing', 10, 6);

    function mainfunction_testing($passed, $product_id, $product_quantity, $variation_id = '', $variatins = array(), $cart_item_data = array()) {
        global $woocommerce;
        $productnametodisplay = '';
        $sellindividuallyproducts = array();
        $current_strtofind = "[productname]";
        $getstrtodisplay = get_option('rs_errmsg_when_other_products_added_to_cart_page');
        if (!is_array(get_option('rs_select_product_for_purchase_using_points'))) {
            $strtodisplay = explode(',', get_option('rs_select_product_for_purchase_using_points'));
        } else {
            $strtodisplay = get_option('rs_select_product_for_purchase_using_points');
        }
        //$strtodisplay = get_option('rs_select_product_for_purchase_using_points');

        if (is_array($strtodisplay)) {
            foreach ($strtodisplay as $values) {
                //var_dump($values);

                $productnametodisplay = get_the_title($values);
            }
        }
        $msgtoreplace = str_replace($current_strtofind, $productnametodisplay, $getstrtodisplay);
        $sellindividuallyproducts[] = $strtodisplay;
        $sellindividuallyproductsids = array();
        $productid = array();
        $getcartcount = $woocommerce->cart->cart_contents_count;
        //var_dump($getcartcount);
        //wc_add_notice(__($errormessage), 'error');
        foreach ($woocommerce->cart->cart_contents as $key => $values) {
            $productid[] = $values['product_id'];
            if (in_array($values['product_id'], $sellindividuallyproducts)) {
                $sellindividuallyproductsids[] = $values['product_id'];
            }
        }
        if (in_array($product_id, $sellindividuallyproducts)) {
            if (!in_array($product_id, $productid)) {
                $woocommerce->cart->empty_cart();
                $passed = true;
            } else {
                $woocommerce->cart->empty_cart();
                $passed = true;
            }
        } else {
            if (is_array($sellindividuallyproductsids)) {
                if (!empty($sellindividuallyproductsids)) {
                    $woocommerce->cart->empty_cart();
                    wc_add_notice(__($msgtoreplace), 'error');
                }
            }
            $passed = true;
        }

        return $passed;
    }

    function add_your_gateway_class($methods) {
        if (is_user_logged_in()) {
            //$banned_user_list = get_option('rs_banned-users_list');
            //if(!in_array(get_current_user_id(), (array)$banned_user_list)){
//               $getarrayofuserdata = get_userdata(get_current_user_id());
//                $banninguserrole = get_option('rs_banning_user_role');
//
//                if (!in_array(isset($getarrayofuserdata->roles[0])?$getarrayofuserdata->roles[0]:'0', (array) $banninguserrole)) {
            $userid = get_current_user_id();
            $banning_type = FPRewardSystem::check_banning_type($userid);
            if ($banning_type != 'redeemingonly' && $banning_type != 'both') {
                $methods[] = 'WC_Reward_Gateway';
//                 }
//        }
            }
        }
        //}
        return $methods;
    }

    add_filter('woocommerce_payment_gateways', 'add_your_gateway_class');
}
