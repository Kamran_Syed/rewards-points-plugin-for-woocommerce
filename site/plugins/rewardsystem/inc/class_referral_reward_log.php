<?php 
class FPRewardSystemReferralRewardLog{
    
    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_referral_log'] = __('Referral Rewards Table', 'rewardsystem');
        return $settings_tabs;
    }
    
    // Add Admin Fields in the Array Format
    
    /**
     * RewardSystem Add Custom Field to the RewardSystem Admin Settings
     */
    
    public static function rewardsystem_admin_fields() {
        return apply_filters('woocommerce_rewardsystem_userpoints_settings', array( 
        
            array(
                'type' => 'display_referral_reward_log',
            ),
            
        ));
    }
    
    
    public static function list_referral_rewards_log(){
        ?>
        <style type="text/css">
            p.submit {
                display:none;
            }
            #mainforms {
                display:none;
            }
        </style>
         <?php if ((!isset($_GET['view'])) ) { ?>
            <?php
            echo '<p> ' . __('Search:', 'rewardsystem') . '<input id="filtering" type="text"/>  ' . __('Page Size:', 'rewardsystem') . '
                <select id="changepagesize">

									<option value="5">5</option>
									<option value="10">10</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select></p>';
            ?>
        
        <table class="wp-list-table widefat fixed posts" data-filter = "#filtering" data-page-size="5" data-page-previous-text = "prev" data-filter-text-only = "true" data-page-next-text = "next" id="user_logs" cellspacing="0">
            <thead>
                <tr>
                    <th scope='col' data-toggle="true" class='manage-column column-serial_number'  style="">
                            <a href="#"><span><?php _e('S.No', 'rewardsystem'); ?></span>
                    </th>
                    <th scope='col' id='rs_referer_user_names' class='manage-column column-rs_referer_user_names'  style=""><?php _e('Referer Username', 'rewardsystem'); ?></th>
                    <th scope='col' id='rs_refered_people_count' class='manage-column column-rs_refered_people_count'  style=""><?php _e('Refered Person Count', 'rewardsystem'); ?></th>
                    <th scope='col' id='rs_total_referral_points' class='manage-column column-rs_total_referral_points'  style=""><?php _e('Total Referral Points', 'rewardsystem'); ?></th>
                </tr>
            </thead>
            <tbody id="the-list">
                <?php 
                $users_list = get_users();
                //var_dump($users_list);
                $i = 1;
                foreach ($users_list as $separate_user) {
                    //echo $separate_user->ID;
                    if ($i % 2 != 0) {
                        $name = 'alternate';
                    } else {
                        $name = '';
                    }
                    ?>
                <tr id="post-141"  class="type-shop_order status-publish post-password-required hentry <?php echo $name; ?> iedit author-self level-0" valign="top">
                    <td data-value="<?php echo $i; ?>">                        
                        <?php echo $i; ?>
                    </td>
                    <td class="rs_referer_user_name">
                        <?php echo $separate_user->user_login; ?>
                    </td>
                    <td class="rs_refered_person_count">
                        <?php                        
                        //var_dump(get_option('testingfu'));
                        $referreduser_count = RS_Referral_Log::get_count_of_corresponding_users($separate_user->ID);
                        if($referreduser_count > 0){
                        ?>
                        <a href="<?php echo add_query_arg('view', $separate_user->ID, get_permalink()); ?>"><?php echo $referreduser_count; ?></a>
                       <?php  
                        } else{
                            echo '0';
                        }
                        ?>
                    </td>                                        
                    <td class="rs_total_referral_points">                                                
                        <?php 
                        $total_referral_points = RS_Referral_Log::get_totals_of_referral_persons($separate_user->ID);
                        if($total_referral_points > 0){
                            $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0'; 
                            $updated_points = round($total_referral_points,$roundofftype);
                            echo $total_referral_points;
                        } else {
                            echo '0';    
                        }
                        
                        ?>
                    </td>
                </tr>
                    <?php
                    $i++;
                }                
                ?>
            </tbody>
        </table>
        <div style="clear:both;">
                <div class="pagination pagination-centered"></div>
        </div>
        <?php        
    }else{          
        ?>
        <?php
            echo '<p> ' . __('Search:', 'rewardsystem') . '<input id="filtering" type="text"/>  ' . __('Page Size:', 'rewardsystem') . '
                <select id="changepagesize">

									<option value="5">5</option>
									<option value="10">10</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select></p>';
            ?>
        <table class="wp-list-table widefat fixed posts" data-filter = "#filtering" data-page-size="5" data-page-previous-text = "prev" data-filter-text-only = "true" data-page-next-text = "next" id="user_logs" cellspacing="0">
            <thead>
                <tr>
                    <th scope='col' data-toggle="true" class='manage-column column-serial_number'  style="">
                            <a href="#"><span><?php _e('S.No', 'rewardsystem'); ?></span>
                    </th>  
                    <th scope='col' id='rs_referral_user_names' class='manage-column column-rs_referral_user_names'  style=""><?php _e('Referral Username', 'rewardsystem'); ?></th>                                        
                    <th scope='col' id='rs_referral_user_points' class='manage-column column-rs_referral_user_points'  style=""><?php _e('Referral Points', 'rewardsystem'); ?></th>                                        
                    
                </tr>
            </thead>
            <tbody id="the-list">
                <?php 
              $i=1;
             $subdatas = RS_Referral_Log::get_corresponding_users_log($_GET['view']);
            if(is_array($subdatas)) {
                //var_dump($subdatas);
                //var_dump(get_option("testingfu"));
                foreach($subdatas as $key => $value) {
                    ?>
                    <tr id="post-141"  class="type-shop_order status-publish post-password-required hentry <?php echo $name; ?> iedit author-self level-0" valign="top">
                       <td data-value="<?php echo $i; ?>">                        
                            <?php echo $i; ?>
                        </td> 
                        <td class="rs_referer_user_name">         
                            
                            <?php
                            $user_name = get_user_by('id',$key);                            
                            echo $user_name->user_nicename; ?>                              
                        </td>                    
                        <td class="rs_referral_user_points"> 
                            <?php 
//                            $current_user_id = get_current_user_id();
//                            $separate_referral_points = get_user_meta($current_user_id,'separate_referral_points',true);
//                            foreach ($separate_referral_points as $each_referral_points){                                
//                                $each_user_referral_points = $each_referral_points;
//                            }
//                            echo $each_user_referral_points;
                            $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0'; 
                            $points_new = round($value,$roundofftype);
                           echo $points_new;
                            ?>
                        </td>
                    </tr>
                    <?php 
                    $i++;
                }
            }
                ?>
            </tbody>
        </table>
        <a href="<?php echo remove_query_arg(array('view'), get_permalink()); ?>">Go Back</a>
        <?php                
    }  
    }


    /**
     * Registering Custom Field Admin Settings of RewardSystem in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemReferralRewardLog::rewardsystem_admin_fields());
    }
    
    
    /**
     * Update the Settings on Save Changes may happen in RewardSystem
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemReferralRewardLog::rewardsystem_admin_fields());
    }
    
    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemReferralRewardLog::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }
        
}

new FPRewardSystemReferralRewardLog();



/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

add_action('woocommerce_admin_field_display_referral_reward_log',  array('FPRewardSystemReferralRewardLog','list_referral_rewards_log'));

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemReferralRewardLog', 'reward_system_tab_settings'), 150);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_referral_log', array('FPRewardSystemReferralRewardLog', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemReferralRewardLog', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_referral_log', array('FPRewardSystemReferralRewardLog', 'reward_system_register_admin_settings'));





?>