<?php

class RSBookingCompatibility {

    public function __construct() {
        add_action('wp_head', array($this, 'booking_compatible'));
        add_action('wp_ajax_woocommerce_booking_sumo_reward_system', array($this, 'sumo_compatible_with_booking'));
        //add_action('woocommerce_product_options_general_product_data', array($this, 'sumo_admin_option_compatible_booking'), 1);
        add_shortcode('sumobookingpoints', array($this, 'sumo_fixed_points_compatible_with_booking'));
        add_action('woocommerce_before_single_product', array($this, 'add_woocommerce_notice'));
        add_action('woocommerce_before_cart', array($this, 'reward_points_in_top_of_content'));
        add_shortcode('bookingrspoint', array($this, 'get_each_product_price'));
        add_action('woocommerce_before_cart', array($this, 'rewardmessage_in_cart'));
        add_filter('woocommerce_rewardsystem_messages_settings', array($this, 'add_custom_field_to_message_tab'));
        add_shortcode('bookingproducttitle', array($this, 'get_woocommerce_booking_product_title'));
        include_once('rs_update_booking_points.php');
        // add_action('admin_head', array($this, 'get_order_data_for_booking_plugin'));
    }

    public static function booking_compatible() {
        if (class_exists('WC_Bookings')) {
            ?>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    var xhr;
                    jQuery('.woocommerce_booking_variations').hide();
                    jQuery('#wc-bookings-booking-form').on('change', 'input, select', function () {
                        if (xhr)
                            xhr.abort();
                        var form = jQuery(this).closest('form');
                        //alert(form.serialize());
                        var dataparam = ({
                            action: 'woocommerce_booking_sumo_reward_system',
                            form: form.serialize(),
                        });
                        jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam, function (response) {

                            if ((response.sumorewardpoints !== 0) && (response.sumorewardpoints !== '')) {
                                jQuery('.woocommerce_booking_variations').addClass('woocommerce-info');
                                jQuery('.woocommerce_booking_variations').show();
                                jQuery('.sumobookingpoints').html(response.sumorewardpoints);
                            } else {
                                jQuery('.woocommerce_booking_variations').hide();
                            }
                        }, 'json');
                    });

                    //jQuery('#wc-bookings-booking-form').bind('change', 'input', 'select');
                });
            </script>

            <?php
        }
    }

    public static function sumo_fixed_points_compatible_with_booking() {
        global $post;
        $booking_id = $post->ID;
        $getproducttype = get_product($booking_id);
        $global_enable = get_option('rs_global_enable_disable_reward');
        $global_reward_type = get_option('rs_global_reward_type');
        if ($getproducttype->is_type('booking')) {
            $enablerewards = get_post_meta($post->ID, '_rewardsystemcheckboxvalue', true);
            $getaction = get_post_meta($post->ID, '_rewardsystem_options', true);
            $getpoints = get_post_meta($post->ID, '_rewardsystempoints', true);
            $getpercent = get_post_meta($post->ID, '_rewardsystempercent', true);

            $rewardpoints = array('0');
            if ($enablerewards == 'yes') {
                if ($getaction == '1') {
                    if ($getpoints == '') {
                        $term = get_the_terms($post->ID, 'product_cat');
                        if (is_array($term)) {
                            foreach ($term as $term) {
                                $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
                                if ($enablevalue == 'yes') {
                                    if ($display_type == '1') {
                                        if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                            $global_enable = get_option('rs_global_enable_disable_reward');
                                            $global_reward_type = get_option('rs_global_reward_type');
                                            if ($global_enable == '1') {
                                                if ($global_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                } else {

                                                }
                                            }
                                        } else {
                                            $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                        }
                                    } else {
                                        $pointconversion = get_option('rs_earn_point');
                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                        $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                        $getaveragepoints = $getaverage * $getregularprice;
                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                        if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {

                                            if ($global_enable == '1') {
                                                if ($global_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                } else {

                                                }
                                            }
                                        } else {

                                        }
                                    }
                                } else {
                                    if ($global_enable == '1') {
                                        if ($global_reward_type == '1') {
                                            $rewardpoints[] = get_option('rs_global_reward_points');
                                        } else {

                                        }
                                    }
                                }
                            }
                        } else {
                            if ($global_enable == '1') {
                                if ($global_reward_type == '1') {
                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                } else {

                                }
                            }
                        }
//var_dump($rewardpoints);
                        if (!empty($rewardpoints)) {
                            $getpoints = max($rewardpoints);
                        }
                    }
                    $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
                    return round($getpoints, $roundofftype);
                }
            }
        }
    }

    public static function sumo_compatible_with_booking() {
        $posted = array();
        parse_str($_POST['form'], $posted);
        $booking_id = $posted['add-to-cart'];
        $product = get_product($booking_id);
        if (!$product) {
            die(json_encode(array('sumorewardpoints' => 0)));
        }
        $booking_form = new WC_Booking_Form($product);
        $cost = $booking_form->calculate_booking_cost($posted);
        if (is_wp_error($cost)) {
            die(json_encode(array('sumorewardpoints' => 0)));
        }
        $tax_display_mode = get_option('woocommerce_tax_display_shop');
        $display_price = $tax_display_mode == 'incl' ? $product->get_price_including_tax(1, $cost) : $product->get_price_excluding_tax(1, $cost);

        if (get_option('timezone_string') != '') {
            $timezonedate = date_default_timezone_set(get_option('timezone_string'));
        } else {
            $timezonedate = date_default_timezone_set('UTC');
        }
        global $post;
        $checkproducttype = get_product($booking_id);
        $global_enable = get_option('rs_global_enable_disable_reward');
        $global_reward_type = get_option('rs_global_reward_type');

        if ($checkproducttype->is_type('booking')) {
            $enablerewards = get_post_meta($booking_id, '_rewardsystemcheckboxvalue', true);
            $getaction = get_post_meta($booking_id, '_rewardsystem_options', true);
            $getpoints = get_post_meta($booking_id, '_rewardsystempoints', true);
            $getpercent = get_post_meta($booking_id, '_rewardsystempercent', true);

            $getregularprice = $display_price;

            $rewardpoints = array('0');
            if ($enablerewards == 'yes') {
                if ($getaction == '1') {
                    if ($getpoints == '') {
                        $term = get_the_terms($booking_id, 'product_cat');
// var_dump($term);
                        if (is_array($term)) {
//var_dump($term);
// $rewardpoints = array();
                            foreach ($term as $term) {
                                $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
                                if ($enablevalue == 'yes') {
                                    if ($display_type == '1') {
                                        if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                            $global_enable = get_option('rs_global_enable_disable_reward');
                                            $global_reward_type = get_option('rs_global_reward_type');
                                            if ($global_enable == '1') {
                                                if ($global_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        } else {
                                            $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                        }
                                    } else {
                                        $pointconversion = get_option('rs_earn_point');
                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                        $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                        $getaveragepoints = $getaverage * $getregularprice;
                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                        if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {

                                            if ($global_enable == '1') {
                                                if ($global_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        } else {
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                } else {
                                    if ($global_enable == '1') {
                                        if ($global_reward_type == '1') {
                                            $rewardpoints[] = get_option('rs_global_reward_points');
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_option('rs_global_reward_percent') / 100;
                                            $getaveragepoints = $getaverage * $getregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($global_enable == '1') {
                                if ($global_reward_type == '1') {
                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                } else {
                                    $pointconversion = get_option('rs_earn_point');
                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                    $getaveragepoints = $getaverage * $getregularprice;
                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                }
                            }
                        }
//var_dump($rewardpoints);
                        if (!empty($rewardpoints)) {
                            $getpoints = max($rewardpoints);
                        }
                    }
                    $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
                    $finalpoints = round($getpoints, $roundofftype);
                    die(json_encode(array('sumorewardpoints' => $finalpoints, 'booking_price' => $display_price, 'booking_formatted_price' => woocommerce_price($display_price) . $product->get_price_suffix())));
                } else {
                    $points = get_option('rs_earn_point');
                    $pointsequalto = get_option('rs_earn_point_value');
                    $takeaverage = $getpercent / 100;
                    $mainaveragevalue = $takeaverage * $getregularprice;
                    $addinpoint = $mainaveragevalue * $points;
                    $totalpoint = $addinpoint / $pointsequalto;
                    if ($getpercent === '') {
                        $term = get_the_terms($booking_id, 'product_cat');
// var_dump($term);
                        if (is_array($term)) {
//var_dump($term);
//                            $rewardpoints = array();
                            foreach ($term as $term) {
                                $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
                                if ($enablevalue == 'yes') {
                                    if ($display_type == '1') {
                                        if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                            $global_enable = get_option('rs_global_enable_disable_reward');
                                            $global_reward_type = get_option('rs_global_reward_type');
                                            if ($global_enable == '1') {
                                                if ($global_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        } else {
                                            $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                        }
                                    } else {
                                        $pointconversion = get_option('rs_earn_point');
                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                        $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                        $getaveragepoints = $getaverage * $getregularprice;
                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                        if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                            $global_enable = get_option('rs_global_enable_disable_reward');
                                            $global_reward_type = get_option('rs_global_reward_type');
                                            if ($global_enable == '1') {
                                                if ($global_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        } else {
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                } else {
                                    if ($global_enable == '1') {
                                        if ($global_reward_type == '1') {
                                            $rewardpoints[] = get_option('rs_global_reward_points');
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_option('rs_global_reward_percent') / 100;
                                            $getaveragepoints = $getaverage * $getregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($global_enable == '1') {
                                if ($global_reward_type == '1') {
                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                } else {
                                    $pointconversion = get_option('rs_earn_point');
                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                    $getaveragepoints = $getaverage * $getregularprice;
                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                }
                            }
                        }
//var_dump($rewardpoints);
                        if (!empty($rewardpoints)) {
                            $totalpoint = max($rewardpoints);
                        } else {
                            $totalpoint = 0;
                        }
                    }
                    $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
                    $finalpoints = round($totalpoint, $roundofftype);
                    die(json_encode(array('sumorewardpoints' => $finalpoints, 'booking_price' => $display_price, 'booking_formatted_price' => woocommerce_price($display_price) . $product->get_price_suffix())));
                }
            }
        }
    }

    public static function add_woocommerce_notice() {
        global $post;
        $order = '';
        //$banned_user_list = get_option('rs_banned-users_list');
        if (is_user_logged_in()) {
//            if (!in_array(get_current_user_id(), (array) $banned_user_list)) {
//                $getarrayofuserdata = get_userdata(get_current_user_id());
//                $banninguserrole = get_option('rs_banning_user_role');
//                if (!in_array($getarrayofuserdata->roles[0], (array) $banninguserrole)) {
                    $userid = get_current_user_id();
                    $banning_type = FPRewardSystem::check_banning_type($userid);        
                    if($banning_type!='earningonly'&&$banning_type!='both') {
                    $checkproducttype = get_product($post->ID);
//if (get_option('rs_show_hide_message_for_single_product') == '1') {
                    if ($checkproducttype->is_type('booking')) {
                        if (get_post_meta($post->ID, '_rewardsystemcheckboxvalue', true) == 'yes') {
                            if (get_post_meta($post->ID, '_rewardsystem_options', true) == '1') {
                                $rewardpoints = do_shortcode('[sumobookingpoints]');
                                if ($rewardpoints > 0) {
                                    ?>
                                    <div class="woocommerce-info"><?php _e("Book this Product and Earn <span class='sumobookingpoints'>$rewardpoints</span> Points"); ?></div>
                                    <?php
                                }
                            } else {
                                ?>
                                <div class="woocommerce_booking_variations"><?php _e("Book this Product and Earn <span class='sumobookingpoints'></span> Points"); ?></div>
                                <?php
                            }
                        }
//}
                    }
//                }
//            }
                }         
        } else {
            $checkproducttype = get_product($post->ID);
//if (get_option('rs_show_hide_message_for_single_product') == '1') {
            if ($checkproducttype->is_type('booking')) {
                if (get_post_meta($post->ID, '_rewardsystemcheckboxvalue', true) == 'yes') {
                    if (get_post_meta($post->ID, '_rewardsystem_options', true) == '1') {
                        $rewardpoints = do_shortcode('[sumobookingpoints]');
                        if ($rewardpoints > 0) {
                            ?>
                            <div class="woocommerce-info"><?php _e("Book this Product and Earn <span class='sumobookingpoints'>$rewardpoints</span> Points"); ?></div>
                            <?php
                        }
                    } else {
                        ?>
                        <div class="woocommerce_booking_variations"><?php _e("Book this Product and Earn <span class='sumobookingpoints'></span> Points"); ?></div>
                        <?php
                    }
                }
//}
            }
        }
    }

    public static function sumo_admin_option_compatible_booking() {
        ?>
        <div class="options_group show_if_booking">
            <?php
            global $post;
            woocommerce_wp_select(array(
                'id' => '_rewardsystemcheckboxvalue',
                'class' => 'rewardsystemcheckboxvalue',
                'label' => __('Enable SUMO Reward Points for Product Purchase', 'rewardsystem'),
                'options' => array(
                    '' => __('Choose Option', 'rewardsystem'),
                    'yes' => __('Enable', 'rewardsystem'),
                    'no' => __('Disable', 'rewardsystem'),
                )
                    )
            );

            woocommerce_wp_select(array(
                'id' => '_rewardsystem_options',
                'class' => 'rewardsystem_options',
                'label' => __('Reward Type', 'rewardsystem'),
                'options' => array(
                    '1' => __('By Fixed Reward Points', 'rewardsystem'),
                    '2' => __('By Percentage of Product Price', 'rewardsystem'),
                )
                    )
            );
            woocommerce_wp_text_input(
                    array(
                        'id' => '_rewardsystempoints',
                        'name' => '_rewardsystempoints',
                        'label' => __('Reward Points', 'rewardsystem')
                    )
            );
            woocommerce_wp_text_input(
                    array(
                        'id' => '_rewardsystempercent',
                        'name' => '_rewardsystempercent',
                        'label' => __('Reward Points in Percent %', 'rewardsystem')
                    )
            );
            woocommerce_wp_select(array(
                'id' => '_referral_rewardsystem_options',
                'class' => 'referral_rewardsystem_options',
                'label' => __('Referral Reward Type', 'rewardsystem'),
                'options' => array(
                    '1' => __('By Fixed Reward Points', 'rewardsystem'),
                    '2' => __('By Percentage of Product Price', 'rewardsystem'),
                )
                    )
            );
            woocommerce_wp_text_input(
                    array(
                        'id' => '_referralrewardsystempoints',
                        'name' => '_referralrewardsystempoints',
                        'label' => __('Referral Reward Points', 'rewardsystem')
                    )
            );
            woocommerce_wp_text_input(
                    array(
                        'id' => '_referralrewardsystempercent',
                        'name' => '_referralrewardsystempercent',
                        'label' => __('Referral Reward Points in Percent %', 'rewardsystem')
                    )
            );
            ?>
        </div>
        <?php
    }

    public static function reward_points_in_top_of_content() {
        global $checkproduct;
        //$banned_user_list = get_option('rs_banned-users_list');
        if (is_user_logged_in()) {
//            if (!in_array(get_current_user_id(), (array) $banned_user_list)) {
//                $getarrayofuserdata = get_userdata(get_current_user_id());
//                $banninguserrole = get_option('rs_banning_user_role');
//
//                if (!in_array($getarrayofuserdata->roles[0], (array) $banninguserrole)) {
                    $userid = get_current_user_id();
                    $banning_type = FPRewardSystem::check_banning_type($userid);        
                    if($banning_type!='earningonly'&&$banning_type!='both') {
                    global $messageglobalbooking;

                    global $totalrewardpointsnewbooking;
                    global $totalrewardpoints;
                    $rewardpoints = array('0');
                    $totalrewardpoints;
                    global $woocommerce;
                    global $bookingvalue;
                    $global_enable = get_option('rs_global_enable_disable_reward');
                    $global_reward_type = get_option('rs_global_reward_type');
                    foreach ($woocommerce->cart->cart_contents as $key => $bookingvalue) {

                        $cartquantity = $bookingvalue['quantity'];
                        $rewardspoints = get_post_meta($bookingvalue['product_id'], '_rewardsystempoints', true);
                        $checkenable = get_post_meta($bookingvalue['product_id'], '_rewardsystemcheckboxvalue', true);
                        $checkenablevariation = get_post_meta($bookingvalue['variation_id'], '_enable_reward_points', true);
                        $variablerewardpoints = get_post_meta($bookingvalue['variation_id'], '_reward_points', true);
                        $variationselectrule = get_post_meta($bookingvalue['variation_id'], '_select_reward_rule', true);
                        $variationrewardpercent = get_post_meta($bookingvalue['variation_id'], '_reward_percent', true);
                        $variable_product1 = new WC_Product_Variation($bookingvalue['variation_id']);
#Step 4: You have the data. Have fun :)
                        if (get_option('rs_set_price_percentage_reward_points') == '1') {
                            $variationregularprice = $variable_product1->regular_price;
                        } else {
                            $variationregularprice = $variable_product1->price;
                        }
                        $checkruleoption = get_post_meta($bookingvalue['product_id'], '_rewardsystem_options', true);
                        $checkrewardpercent = get_post_meta($bookingvalue['product_id'], '_rewardsystempercent', true);
//                        if (get_option('rs_set_price_percentage_reward_points') == '1') {
//                            $getregularprice = get_post_meta($bookingvalue['product_id'], '_regular_price', true);
//                        } else {
//                            $getregularprice = get_post_meta($bookingvalue['product_id'], '_price', true);
//                        }

                        $getregularprice = $bookingvalue['data']->price;


                        $user_ID = get_current_user_id();
// if ($checkenable == 'yes') {
                        $checkproduct = get_product($bookingvalue['product_id']);
                        $checkanotherproduct = get_product($bookingvalue['variation_id']);
                        if ($checkproduct->is_type('booking')) {
                            if ($checkenable == 'yes') {
                                if ($checkruleoption == '1') {
                                    if ($rewardspoints == '') {
                                        $term = get_the_terms($bookingvalue['product_id'], 'product_cat');
// var_dump($term);
                                        if (is_array($term)) {
//var_dump($term);
                                            $rewardpoints = array('0');
                                            foreach ($term as $term) {
                                                $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                                $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
                                                if ($enablevalue == 'yes') {
                                                    if ($display_type == '1') {
                                                        $checktermpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                                        if ($checktermpoints == '') {

                                                            if ($global_enable == '1') {
                                                                if ($global_reward_type == '1') {
                                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                                } else {
                                                                    $pointconversion = get_option('rs_earn_point');
                                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                }
                                                            }
                                                        } else {
                                                            $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                                        }
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                                            $global_enable = get_option('rs_global_enable_disable_reward');
                                                            $global_reward_type = get_option('rs_global_reward_type');
                                                            if ($global_enable == '1') {
                                                                if ($global_reward_type == '1') {
                                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                                } else {
                                                                    $pointconversion = get_option('rs_earn_point');
                                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                }
                                                            }
                                                        } else {
                                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                        }
                                                    }
                                                } else {
                                                    if ($global_enable == '1') {
                                                        if ($global_reward_type == '1') {
                                                            $rewardpoints[] = get_option('rs_global_reward_points');
                                                        } else {
                                                            $pointconversion = get_option('rs_earn_point');
                                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                                            $getaverage = get_option('rs_global_reward_percent') / 100;
                                                            $getaveragepoints = $getaverage * $getregularprice;
                                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            if ($global_enable == '1') {
                                                if ($global_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        }
//var_dump($rewardpoints);
                                        if (!empty($rewardpoints)) {
                                            $rewardspoints = max($rewardpoints);
                                        }
//$rewardpoints = array_search($bookingvalue, $r   ewardpoints);
                                    }
                                    $totalrewardpoints = $rewardspoints * $cartquantity;

//  echo do_shortcode(get_option('rs_message_product_in_cart')) . "<br>";
//echo "Your Current Point >>>>>>>>>>>>>>>" . get_user_meta($user_ID, '_my_reward_points', true);
                                    $totalrewardpointsnewbooking[$bookingvalue['product_id']] = $totalrewardpoints;
                                } else {
                                    $pointconversion = get_option('rs_earn_point');
                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                    $getaverage = $checkrewardpercent / 100;
                                    $getaveragepoints = $getaverage * $getregularprice;
                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                    $points = $pointswithvalue / $pointconversionvalue;
                                    if ($checkrewardpercent == '') {
                                        $term = get_the_terms($bookingvalue['product_id'], 'product_cat');
// var_dump($term);
                                        if (is_array($term)) {
//var_dump($term);
                                            $rewardpoints = array('0');
                                            foreach ($term as $term) {

                                                $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                                $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                                if ($enablevalue == 'yes') {
                                                    if ($display_type == '1') {
                                                        if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                                            $global_enable = get_option('rs_global_enable_disable_reward');
                                                            $global_reward_type = get_option('rs_global_reward_type');
                                                            if ($global_enable == '1') {
                                                                if ($global_reward_type == '1') {
                                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                                } else {
                                                                    $pointconversion = get_option('rs_earn_point');
                                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                }
                                                            }
                                                        } else {

                                                            $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                                        }
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                                            $global_enable = get_option('rs_global_enable_disable_reward');
                                                            $global_reward_type = get_option('rs_global_reward_type');
                                                            if ($global_enable == '1') {
                                                                if ($global_reward_type == '1') {
                                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                                } else {
                                                                    $pointconversion = get_option('rs_earn_point');
                                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                }
                                                            }
                                                        } else {
                                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                        }
                                                    }
                                                } else {
                                                    if ($global_enable == '1') {
                                                        if ($global_reward_type == '1') {
                                                            $rewardpoints[] = get_option('rs_global_reward_points');
                                                        } else {
                                                            $pointconversion = get_option('rs_earn_point');
                                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                                            $getaverage = get_option('rs_global_reward_percent') / 100;
                                                            $getaveragepoints = $getaverage * $getregularprice;
                                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            if ($global_enable == '1') {
                                                if ($global_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        }
//var_dump($rewardpoints);
                                        $points = max($rewardpoints);

//$rewardpoints = array_search($bookingvalue, $rewardpoints);
                                    }

                                    $totalrewardpoints = $points * $cartquantity;
// echo do_shortcode(get_option('rs_message_product_in_cart')) . "<br>";
//echo "Your Current Point >>>>>>>>>>>>>>>>" . get_user_meta($user_ID, '_my_reward_points', true);
                                    $totalrewardpointsnewbooking[$bookingvalue['product_id']] = $totalrewardpoints;
                                }
                            }
                        } else {

                        }


                        if ($checkproduct->is_type('booking')) {
                            if ($checkenable == 'yes') {
                                $validrewardpoints = do_shortcode('[bookingrspoint]');
                                if ($validrewardpoints > 0) {
                                    $messageglobalbooking[] = do_shortcode(get_option('rs_woocommerce_booking_product_cart_message')) . "<br>";
                                }
                            }
                        }
//}
                    }
                    ?>

                    <?php
//                }
//            }
                }         
        }
    }

    public static function get_each_product_price() {
        global $totalrewardpoints;
        global $checkproduct;
        global $bookingvalue;
        if ($checkproduct->is_type('booking')) {
            if (get_post_meta($bookingvalue['product_id'], '_rewardsystemcheckboxvalue', true) != 'yes') {
                return "<strong>0</strong>";
            } else {
                $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
                return round($totalrewardpoints, $roundofftype);
            }
        }
    }

    public static function rewardmessage_in_cart() {
        global $woocommerce;
        global $bookingvalue;
        global $totalrewardpointsnewbooking;
        global $messageglobalbooking;
        if (get_option('rs_show_hide_message_for_each_products') == '1') {
            if (is_array($totalrewardpointsnewbooking)) {
                if (array_sum($totalrewardpointsnewbooking) > 0) {
                    ?>
                    <div class="woocommerce-info">
                        <?php
                        if (is_array($messageglobalbooking)) {
                            foreach ($messageglobalbooking as $globalcommerce) {
                                echo $globalcommerce;
                            }
                        }
                        ?>
                    </div>
                    <?php
                }
            }
        }
    }

    public static function get_data_from_cart_item() {
        global $woocommerce;
        $cart_item = $woocommerce->cart->cart_contents;
        if (class_exists('WC_Bookings_Cart')) {
            ?>
            <div class="woocommerce-info">
                <?php
                // var_dump($cart_item);
                foreach ($cart_item as $bookingvalue) {
                    var_dump($bookingvalue);
                }
                ?>

            </div>
            <?php
        }
    }

    public static function add_custom_field_to_message_tab($settings) {
        $updated_settings = array();
        foreach ($settings as $section) {
// at the bottom of the General Options section
            if (isset($section['id']) && '_rs_reward_messages' == $section['id'] &&
                    isset($section['type']) && 'sectionend' == $section['type']) {
                $updated_settings[] = array(
                    'name' => __('Enter Cart Message for WooCommerce Booking Product', 'rewardsystem'),
                    'desc' => __('Please Enter Cart Message for WooCommerce Booking Product', 'rewardsystem'),
                    'tip' => '',
                    'id' => 'rs_woocommerce_booking_product_cart_message',
                    'css' => 'min-width:550px;',
                    'std' => 'Purchase this [bookingproducttitle] and Earn [bookingrspoint] Reward Point',
                    'type' => 'textarea',
                    'newids' => 'rs_woocommerce_booking_product_cart_message',
                    'desc_tip' => true,
                );
            }
            $updated_settings[] = $section;
        }

        return $updated_settings;
    }

    public static function get_woocommerce_booking_product_title() {
        global $checkproduct;
        global $bookingvalue;
        if ($checkproduct->is_type('booking')) {
            return "<strong>" . get_the_title($bookingvalue['product_id']) . "</strong>";
        }
    }

    public static function get_order_data_for_booking_plugin() {
        $order_id = 468;

        $order = new WC_Order($order_id);

        foreach ($order->get_items() as $item) {
            $productdata = get_product($item['product_id']);
            if ($productdata->is_type('booking')) {
                var_dump($item['line_subtotal']);
            }
        }
    }

    public static function update_reward_points_to_user($order_id) {
        $restrictuserpoints = get_option('rs_maximum_earning_points_for_user');
        $enabledisablemaxpoints = get_option('rs_enable_maximum_earning_points');
        $order = new WC_Order($order_id);
//        $banned_user_list = get_option('rs_banned-users_list');
//        if (!in_array($order->user_id, (array) $banned_user_list)) {
//
//            $getarrayofuserdata = get_userdata($order->user_id);
//            $banninguserrole = get_option('rs_banning_user_role');
//            if (!in_array(isset($getarrayofuserdata->roles[0]) ? $getarrayofuserdata->roles[0] : '0', (array) $banninguserrole)) {
        //$userid = get_current_user_id();
        $banning_type = FPRewardSystem::check_banning_type($order->user_id);        
        if($banning_type!='earningonly'&&$banning_type!='both') {

                /* Payment Rewards for WooCommerce Order */
                $getpaymentgatewayused = get_option('rs_reward_payment_gateways_' . $order->payment_method);

                if ($getpaymentgatewayused != '') {
                    $checkmypoints_payment = get_user_meta($order->user_id, '_my_reward_points', true);
                    $totalpoints_payment = $checkmypoints_payment + $getpaymentgatewayused;
                    update_user_meta($order->user_id, '_my_reward_points', $totalpoints_payment);
                    $mycurrentpoints_payment = get_user_meta($order->user_id, '_my_reward_points', true);

                    $localizemessage = str_replace('{payment_title}', $order->payment_method_title, get_option('_rs_localize_reward_for_payment_gateway_message'));

                    $pointslogs_payment[] = array('orderid' => $order_id, 'userid' => $order->user_id, 'points_earned_order' => $getpaymentgatewayused, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $mycurrentpoints_payment, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => $localizemessage, 'rewarder_for_frontend' => $localizemessage);
                    $overalllogs_payment[] = array('userid' => $order->user_id, 'totalvalue' => $getpaymentgatewayused, 'eventname' => $localizemessage, 'date' => date('Y-m-d H:i:s'));
                    $getoveralllogs_payment = get_option('rsoveralllog');
                    $logmerges_payment = array_merge((array) $getoveralllogs_payment, $overalllogs_payment);
                    update_option('rsoveralllog', $logmerges_payment);
                    $getmypointss_payment = get_user_meta($order->user_id, '_my_points_log', true);
                    $mergeds_payment = array_merge((array) $getmypointss_payment, $pointslogs_payment);
                    update_user_meta($order->user_id, '_my_points_log', $mergeds_payment);
                }

                if (get_post_meta($order_id, '_sumo_points_awarded', true) != 'yes') {
                    $pointslog = array();
                    $usernickname = get_user_meta($order->user_id, 'nickname', true);
                    $checkmypoints = get_user_meta($order->user_id, '_my_reward_points', true);
                    delete_user_meta($order->user_id, '_redeemed_points');
                    delete_user_meta($order->user_id, '_redeemed_amount');
                    $global_referral_enable = get_option('rs_global_enable_disable_reward');
                    $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                    $global_enable = get_option('rs_global_enable_disable_reward');
                    $global_reward_type = get_option('rs_global_reward_type');
                    foreach ($order->get_items() as $item) {
                        if (get_option('rs_set_price_percentage_reward_points') == '1') {
                            $getregularprice = get_post_meta($item['product_id'], '_regular_price', true);
                        } else {
                            $getregularprice = get_post_meta($item['product_id'], '_price', true);
                        }

                        /*
                         * For Booking Start
                         */

                        $mainproductdatabooking = get_product($item['product_id']);

                        if ($mainproductdatabooking->is_type('booking')) {
                            $getregularprice = $item['line_total'];
                        }


                        /*
                         * For Booking End
                         */

                        $getpercent = get_post_meta($item['product_id'], '_rewardsystempercent', true);

                        $item['qty'];
                        $order->user_id;
                        $item['product_id'];
                        $referreduser = get_post_meta($order_id, '_referrer_name', true);
                        if ($referreduser != '') {
                            $user_info = new WP_User($order->user_id);
                            $registered_date = $user_info->user_registered;
                            $limitation = false;
                            $modified_registered_date = date('Y-m-d h:i:sa', strtotime($registered_date));
                            $delay_days = get_option('_rs_select_referral_points_referee_time_content');
                            $checking_date = date('Y-m-d h:i:sa', strtotime($modified_registered_date . ' + ' . $delay_days . ' days '));
                            $modified_checking_date = strtotime($checking_date);
                            $current_date = date('Y-m-d h:i:sa');
                            $modified_current_date = strtotime($current_date);
                            //Is for Immediatly
                            if (get_option('_rs_select_referral_points_referee_time') == '1') {
                                $limitation = true;
                            } else {
                                // Is for Limited Time with Number of Days
                                if ($modified_current_date > $modified_checking_date) {
                                    $limitation = true;
                                } else {
                                    $limitation = false;
                                }
                            }

                            if ($limitation == true) {
                                $refuser = get_user_by('login', $referreduser);
                                $myid = $refuser->ID;
//                                $banned_user_list = get_option('rs_banned-users_list');
//                                if (!in_array($myid, (array) $banned_user_list)) {
//                                    $getarrayofuserdataref = get_userdata($myid);
//                                    $banninguserrole = get_option('rs_banning_user_role');
//                                    if (!in_array(isset($getarrayofuserdata->roles[0]) ? $getarrayofuserdata->roles[0] : '0', (array) $banninguserrole)) {
                                //$userid = get_current_user_id();
                                $banning_type = FPRewardSystem::check_banning_type($myid);        
                                if($banning_type!='earningonly'&&$banning_type!='both') {
                                        if (get_post_meta($item['product_id'], '_rewardsystemcheckboxvalue', true) == 'yes') {
                                            $getreferraltype = get_post_meta($item['product_id'], '_referral_rewardsystem_options', true);
                                            if ($getreferraltype == '1') {
                                                $getreferralpoints = get_post_meta($item['product_id'], '_referralrewardsystempoints', true);
                                                if ($getreferralpoints == '') {
                                                    $term = get_the_terms($item['product_id'], 'product_cat');
                                                    if (is_array($term)) {
                                                        $rewardpointer = array();
                                                        foreach ($term as $term) {
                                                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                                            $display_type = get_woocommerce_term_meta($term->term_id, 'referral_enable_rs_rule', true);
                                                            if ($enablevalue == 'yes') {
                                                                if ($display_type == '1') {
                                                                    if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true) == '') {

                                                                        if ($global_referral_enable == '1') {
                                                                            if ($global_referral_reward_type == '1') {
                                                                                $rewardpointer[] = get_option('rs_global_referral_reward_point');
                                                                            } else {
                                                                                $pointconversion = get_option('rs_earn_point');
                                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                                $getaveragepoints = $getaverage * $getregularprice;
                                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                                $rewardpointer[] = $pointswithvalue / $pointconversionvalue;
                                                                            }
                                                                        }
                                                                    } else {
                                                                        $rewardpointer[] = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true);
                                                                    }
                                                                } else {
                                                                    $pointconversion = get_option('rs_earn_point');
                                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                                    $getaverage = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) / 100;
                                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                    if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) == '') {
                                                                        $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                                                        $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                                                        if ($global_referral_enable == '1') {
                                                                            if ($global_referral_reward_type == '1') {
                                                                                $rewardpointer[] = get_option('rs_global_referral_reward_point');
                                                                            } else {
                                                                                $pointconversion = get_option('rs_earn_point');
                                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                                $getaveragepoints = $getaverage * $getregularprice;
                                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                                $rewardpointer[] = $pointswithvalue / $pointconversionvalue;
                                                                            }
                                                                        }
                                                                    } else {
                                                                        $rewardpointer[] = $pointswithvalue / $pointconversionvalue;
                                                                    }
                                                                }
                                                            } else {
                                                                if ($global_referral_enable == '1') {
                                                                    if ($global_referral_reward_type == '1') {
                                                                        $rewardpointer[] = get_option('rs_global_referral_reward_point');
                                                                    } else {
                                                                        $pointconversion = get_option('rs_earn_point');
                                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                                        $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                        $rewardpointer[] = $pointswithvalue / $pointconversionvalue;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        if ($global_referral_enable == '1') {
                                                            if ($global_referral_reward_type == '1') {
                                                                $rewardpointer[] = get_option('rs_global_referral_reward_point');
                                                            } else {
                                                                $pointconversion = get_option('rs_earn_point');
                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                $getaveragepoints = $getaverage * $getregularprice;
                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                $rewardpointer[] = $pointswithvalue / $pointconversionvalue;
                                                            }
                                                        }
                                                    }
                                                    $getreferralpoints = max($rewardpointer);
                                                }


                                                $exactpointer = $getreferralpoints * $item['qty'];

                                                $logofgetoption = get_option('_rs_localize_referral_reward_points_for_purchase');

                                                $findarrays = array('{itemproductid}', '{purchasedusername}');
                                                $replacearrays = array($item['product_id'], get_user_meta($order->user_id, 'nickname', true));

                                                $updatedvaluesarrays = str_replace($findarrays, $replacearrays, $logofgetoption);

                                                $overalllogs[] = array('userid' => $myid, 'totalvalue' => $exactpointer, 'eventname' => $updatedvaluesarrays, 'date' => $order->order_date);
                                                $getoveralllogs = get_option('rsoveralllog');
                                                $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
                                                update_option('rsoveralllog', $logmerges);

                                                $userpoints = get_user_meta($myid, '_my_reward_points', true);
                                                $newgetpoints = $exactpointer + $userpoints;
                                                if ($enabledisablemaxpoints == '1') {
                                                    if (($newgetpoints <= $restrictuserpoints) || ($restrictuserpoints == '')) {
                                                        $newgetpoints = $newgetpoints;
                                                    } else {
                                                        $newgetpoints = $restrictuserpoints;
                                                    }
                                                }
                                                update_user_meta($myid, '_my_reward_points', $newgetpoints);

                                                $pointsfixed[] = array('orderid' => $order_id, 'userid' => $myid, 'points_earned_order' => $exactpointer, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => get_user_meta($myid, '_my_reward_points', true), 'date' => $order->order_date, 'rewarder_for' => $updatedvaluesarrays, 'rewarder_for_frontend' => $updatedvaluesarrays);
                                                $getlogger = get_user_meta($myid, '_my_points_log', true);
                                                $merged = array_merge((array) $getlogger, $pointsfixed);
                                                update_user_meta($myid, '_my_points_log', $merged);
                                            } else {



                                                $points = get_option('rs_earn_point');
                                                $pointsequalto = get_option('rs_earn_point_value');
                                                $getreferralpercent = get_post_meta($item['product_id'], '_referralrewardsystempercent', true);
                                                if (get_option('rs_set_price_percentage_reward_points') == '1') {
                                                    $getregularprices = get_post_meta($item['product_id'], '_regular_price', true);
                                                } else {
                                                    $getregularprices = get_post_meta($item['product_id'], '_price', true);
                                                }

                                                /*
                                                 * For Booking Plugin Start
                                                 */

                                                $getproductdatatypebooking = get_product($item['product_id']);

                                                if ($getproductdatatypebooking->is_type('booking')) {
                                                    $getregularprices = $item['line_subtotal'];
                                                }



                                                /*
                                                 * For Booking Plugin End
                                                 */

                                                $referralpercentageproduct = $getreferralpercent / 100;
                                                $getreferralpricepercent = $referralpercentageproduct * $getregularprices;
                                                $getpointconversion = $getreferralpricepercent * $points;
                                                $getreferpoints = $getpointconversion / $pointsequalto;

                                                if ($getreferralpercent == '') {
                                                    $term = get_the_terms($item['product_id'], 'product_cat');
// var_dump($term);
                                                    if (is_array($term)) {
//var_dump($term);
                                                        $rewardpoints = array('0');
                                                        foreach ($term as $term) {

                                                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                                            $display_type = get_woocommerce_term_meta($term->term_id, 'referral_enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                                            if ($enablevalue == 'yes') {
                                                                if ($display_type == '1') {
                                                                    if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true) == '') {
                                                                        $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                                                        $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                                                        if ($global_referral_enable == '1') {
                                                                            if ($global_referral_reward_type == '1') {
                                                                                $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                                            } else {
                                                                                $pointconversion = get_option('rs_earn_point');
                                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                                $getaveragepoints = $getaverage * $getregularprice;
                                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                            }
                                                                        }
                                                                    } else {
                                                                        $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true);
                                                                    }
                                                                } else {
                                                                    $pointconversion = get_option('rs_earn_point');
                                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                                    $getaverage = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) / 100;
                                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                    if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) == '') {
                                                                        $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                                                        $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                                                        if ($global_referral_enable == '1') {
                                                                            if ($global_referral_reward_type == '1') {
                                                                                $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                                            } else {
                                                                                $pointconversion = get_option('rs_earn_point');
                                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                                $getaveragepoints = $getaverage * $getregularprice;
                                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                            }
                                                                        }
                                                                    } else {
                                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                    }
                                                                }
                                                            } else {
                                                                if ($global_referral_enable == '1') {
                                                                    if ($global_referral_reward_type == '1') {
                                                                        $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                                    } else {
                                                                        $pointconversion = get_option('rs_earn_point');
                                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                                        $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        if ($global_referral_enable == '1') {
                                                            if ($global_referral_reward_type == '1') {
                                                                $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                            } else {
                                                                $pointconversion = get_option('rs_earn_point');
                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                $getaveragepoints = $getaverage * $getregularprice;
                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                            }
                                                        }
                                                    }
                                                    $getreferpoints = max($rewardpoints);
                                                }


                                                $logofgetoption = get_option('_rs_localize_referral_reward_points_for_purchase');

                                                $findarrays = array('{itemproductid}', '{purchasedusername}');
                                                $replacearrays = array($item['product_id'], get_user_meta($order->user_id, 'nickname', true));
                                                $updatedvaluesarrays = str_replace($findarrays, $replacearrays, $logofgetoption);



                                                $exactpointer = $getreferpoints * $item['qty'];
                                                $overalllogs[] = array('userid' => $myid, 'totalvalue' => $exactpointer, 'eventname' => $updatedvaluesarrays, 'date' => $order->order_date);
                                                $getoveralllogs = get_option('rsoveralllog');
                                                $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
                                                update_option('rsoveralllog', $logmerges);
                                                $userpoints = get_user_meta($myid, '_my_reward_points', true);
                                                $updatedpoints = $exactpointer + $userpoints;
                                                if ($enabledisablemaxpoints == '1') {
                                                    if (($updatedpoints <= $restrictuserpoints) || ($restrictuserpoints == '')) {
                                                        $updatedpoints = $updatedpoints;
                                                    } else {
                                                        $updatedpoints = $restrictuserpoints;
                                                    }
                                                }
                                                update_user_meta($myid, '_my_reward_points', $updatedpoints);

                                                $pointspercent[] = array('orderid' => $order_id, 'userid' => $myid, 'points_earned_order' => $exactpointer, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => get_user_meta($myid, '_my_reward_points', true), 'date' => $order->order_date, 'rewarder_for' => $updatedvaluesarrays, 'rewarder_for_frontend' => $updatedvaluesarrays);
                                                $getlogg = get_user_meta($myid, '_my_points_log', true);
                                                $mergeds = array_merge((array) $getlogg, $pointspercent);
                                                update_user_meta($myid, '_my_points_log', $mergeds);
                                            }
                                        }




                                        /* Referral Reward Points for Variable Products */
                                        if (get_post_meta($item['variation_id'], '_enable_reward_points', true) == '1') {
                                            $variablereferralrewardpoints = get_post_meta($item['variation_id'], '_referral_reward_points', true);
                                            $variationreferralselectrule = get_post_meta($item['variation_id'], '_select_referral_reward_rule', true);
                                            $variationreferralrewardpercent = get_post_meta($item['variation_id'], '_referral_reward_percent', true);
                                            $variable_products = new WC_Product_Variation($item['variation_id']);
                                            if (get_option('rs_set_price_percentage_reward_points') == '1') {
                                                $variationregularprice = $variable_products->regular_price;
                                            } else {
                                                $variationregularprice = $variable_products->price;
                                            }

                                            if ($variationreferralselectrule == '1') {
                                                $getreferralpoints = get_post_meta($item['variation_id'], '_referral_reward_points', true);
                                                $parentvariationid = new WC_Product_Variation($item['variation_id']);
                                                $newparentid = $parentvariationid->parent->id;
                                                if ($getreferralpoints == '') {
                                                    $term = get_the_terms($newparentid, 'product_cat');
//var_dump($term);
                                                    if (is_array($term)) {
//var_dump($term);
                                                        $rewardpoints = array('0');
                                                        foreach ($term as $term) {

                                                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                                            $display_type = get_woocommerce_term_meta($term->term_id, 'referral_enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                                            if ($enablevalue == 'yes') {
                                                                if ($display_type == '1') {
                                                                    if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true) == '') {
                                                                        $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                                                        $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                                                        if ($global_referral_enable == '1') {
                                                                            if ($global_referral_reward_type == '1') {
                                                                                $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                                            } else {
                                                                                $pointconversion = get_option('rs_earn_point');
                                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                            }
                                                                        }
                                                                    } else {
                                                                        $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true);
                                                                    }
                                                                } else {
                                                                    $pointconversion = get_option('rs_earn_point');
                                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                                    $getaverage = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) / 100;
                                                                    $getaveragepoints = $getaverage * $variationregularprice;
                                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                    if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) == '') {
                                                                        $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                                                        $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                                                        if ($global_referral_enable == '1') {
                                                                            if ($global_referral_reward_type == '1') {
                                                                                $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                                            } else {
                                                                                $pointconversion = get_option('rs_earn_point');
                                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                            }
                                                                        }
                                                                    } else {
                                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                    }
                                                                }
                                                            } else {
                                                                if ($global_referral_enable == '1') {
                                                                    if ($global_referral_reward_type == '1') {
                                                                        $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                                    } else {
                                                                        $pointconversion = get_option('rs_earn_point');
                                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                                        $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                        $getaveragepoints = $getaverage * $variationregularprice;
                                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        if ($global_referral_enable == '1') {
                                                            if ($global_referral_reward_type == '1') {
                                                                $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                            } else {
                                                                $pointconversion = get_option('rs_earn_point');
                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                            }
                                                        }
                                                    }
//var_dump($rewardpoints);
                                                    $getreferralpoints = max($rewardpoints);

//$rewardpoints = array_search($bookingvalue, $rewardpoints);
                                                }

                                                $logofgetoption = get_option('_rs_localize_referral_reward_points_for_purchase');

                                                $findarrays = array('{itemproductid}', '{purchasedusername}');
                                                $replacearrays = array($item['variation_id'], get_user_meta($order->user_id, 'nickname', true));
                                                $updatedvaluesarrays = str_replace($findarrays, $replacearrays, $logofgetoption);

                                                $getvarpoints = $getreferralpoints * $item['qty'];
                                                $overalllogging[] = array('userid' => $myid, 'totalvalue' => $getvarpoints, 'eventname' => $updatedvaluesarrays, 'date' => $order->order_date);
                                                $getoveralllogging = get_option('rsoveralllog');
                                                $logmerging = array_merge((array) $getoveralllogging, $overalllogging);
                                                update_option('rsoveralllog', $logmerging);
                                                $userpoints = get_user_meta($myid, '_my_reward_points', true);
                                                $newgetpointing = $getvarpoints + $userpoints;
                                                if ($enabledisablemaxpoints == '1') {
                                                    if (($newgetpointing <= $restrictuserpoints) || ($restrictuserpoints == '')) {
                                                        $newgetpointing = $newgetpointing;
                                                    } else {
                                                        $newgetpointing = $restrictuserpoints;
                                                    }
                                                }
                                                update_user_meta($myid, '_my_reward_points', $newgetpointing);
                                                $varpointsfixed[] = array('orderid' => $order_id, 'userid' => $myid, 'points_earned_order' => $getvarpoints, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => get_user_meta($myid, '_my_reward_points', true), 'date' => $order->order_date, 'rewarder_for' => $updatedvaluesarrays, 'rewarder_for_frontend' => $updatedvaluesarrays);
                                                $getlogge = get_user_meta($myid, '_my_points_log', true);
                                                $mergedse = array_merge((array) $getlogge, $varpointsfixed);
                                                update_user_meta($myid, '_my_points_log', $mergedse);
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getvaraverage = $variationreferralrewardpercent / 100;
                                                $getvaraveragepoints = $getvaraverage * $variationregularprice;
                                                $getvarpointsvalue = $getvaraveragepoints * $pointconversion;
                                                $varpoints = $getvarpointsvalue / $pointconversionvalue;

                                                $parentvariationid = new WC_Product_Variation($item['variation_id']);
                                                $newparentid = $parentvariationid->parent->id;
                                                if ($variationreferralrewardpercent == '') {
                                                    $term = get_the_terms($newparentid, 'product_cat');
//var_dump($term);
                                                    if (is_array($term)) {
//var_dump($term);
                                                        $rewardpoints = array('0');
                                                        foreach ($term as $term) {

                                                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                                            $display_type = get_woocommerce_term_meta($term->term_id, 'referral_enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                                            if ($enablevalue == 'yes') {
                                                                if ($display_type == '1') {
                                                                    if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true) == '') {
                                                                        $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                                                        $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                                                        if ($global_referral_enable == '1') {
                                                                            if ($global_referral_reward_type == '1') {
                                                                                $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                                            } else {
                                                                                $pointconversion = get_option('rs_earn_point');
                                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                            }
                                                                        }
                                                                    } else {
                                                                        $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true);
                                                                    }
                                                                } else {
                                                                    $pointconversion = get_option('rs_earn_point');
                                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                                    $getaverage = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) / 100;
                                                                    $getaveragepoints = $getaverage * $variationregularprice;
                                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                    if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) == '') {
                                                                        $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                                                        $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                                                        if ($global_referral_enable == '1') {
                                                                            if ($global_referral_reward_type == '1') {
                                                                                $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                                            } else {
                                                                                $pointconversion = get_option('rs_earn_point');
                                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                            }
                                                                        }
                                                                    } else {
                                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                    }
                                                                }
                                                            } else {
                                                                if ($global_referral_enable == '1') {
                                                                    if ($global_referral_reward_type == '1') {
                                                                        $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                                    } else {
                                                                        $pointconversion = get_option('rs_earn_point');
                                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                                        $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                        $getaveragepoints = $getaverage * $variationregularprice;
                                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        if ($global_referral_enable == '1') {
                                                            if ($global_referral_reward_type == '1') {
                                                                $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                            } else {
                                                                $pointconversion = get_option('rs_earn_point');
                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                            }
                                                        }
                                                    }
//var_dump($rewardpoints);
                                                    $varpoints = max($rewardpoints);

//$rewardpoints = array_search($bookingvalue, $rewardpoints);
                                                }
                                                $logofgetoption = get_option('_rs_localize_referral_reward_points_for_purchase');

                                                $findarrays = array('{itemproductid}', '{purchasedusername}');
                                                $replacearrays = array($item['variation_id'], get_user_meta($order->user_id, 'nickname', true));
                                                $updatedvaluesarrays = str_replace($findarrays, $replacearrays, $logofgetoption);

                                                $getexactvar = $varpoints * $item['qty'];
                                                $overallloggered[] = array('userid' => $myid, 'totalvalue' => $getexactvar, 'eventname' => $updatedvaluesarrays, 'date' => $order->order_date);
                                                $getoverallloggered = get_option('rsoveralllog');
                                                $logmergeder = array_merge((array) $getoverallloggered, $overallloggered);
                                                update_option('rsoveralllog', $logmergeder);
                                                $userpointser = get_user_meta($myid, '_my_reward_points', true);
                                                $newgetpointser = $getexactvar + $userpointser;
                                                if ($enabledisablemaxpoints == '1') {
                                                    if (($newgetpointser <= $restrictuserpoints) || ($restrictuserpoints == '')) {
                                                        $newgetpointser = $newgetpointser;
                                                    } else {
                                                        $newgetpointser = $restrictuserpoints;
                                                    }
                                                }
                                                update_user_meta($myid, '_my_reward_points', $newgetpointser);

                                                $varpointspercent[] = array('orderid' => $order_id, 'userid' => $myid, 'points_earned_order' => $getexactvar, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => get_user_meta($myid, '_my_reward_points', true), 'date' => $order->order_date, 'rewarder_for' => $updatedvaluesarrays, 'rewarder_for_frontend' => $updatedvaluesarrays);
                                                $getlogge = get_user_meta($myid, '_my_points_log', true);
                                                $mergedse = array_merge((array) $getlogge, $varpointspercent);
                                                update_user_meta($myid, '_my_points_log', $mergedse);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (get_post_meta($item['product_id'], '_rewardsystemcheckboxvalue', true) == 'yes') {

                            if (get_post_meta($item['product_id'], '_rewardsystem_options', true) == '1') {
                                $getpoints = get_post_meta($item['product_id'], '_rewardsystempoints', true);
                                if ($getpoints == '') {
                                    $term = get_the_terms($item['product_id'], 'product_cat');
// var_dump($term);
                                    if (is_array($term)) {
//var_dump($term);
                                        $rewardpoints = array('0');
                                        foreach ($term as $term) {

                                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                            $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                            if ($enablevalue == 'yes') {
                                                if ($display_type == '1') {
                                                    if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                                        $global_enable = get_option('rs_global_enable_disable_reward');
                                                        $global_reward_type = get_option('rs_global_reward_type');
                                                        if ($global_enable == '1') {
                                                            if ($global_reward_type == '1') {
                                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                                            } else {
                                                                $pointconversion = get_option('rs_earn_point');
                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                                $getaveragepoints = $getaverage * $getregularprice;
                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                            }
                                                        }
                                                    } else {
                                                        $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                                    }
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                                        $global_enable = get_option('rs_global_enable_disable_reward');
                                                        $global_reward_type = get_option('rs_global_reward_type');
                                                        if ($global_enable == '1') {
                                                            if ($global_reward_type == '1') {
                                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                                            } else {
                                                                $pointconversion = get_option('rs_earn_point');
                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                                $getaveragepoints = $getaverage * $getregularprice;
                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                            }
                                                        }
                                                    } else {
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        if ($global_enable == '1') {
                                            if ($global_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    }
//var_dump($rewardpoints);
                                    $getpoints = max($rewardpoints);

//$rewardpoints = array_search($bookingvalue, $rewardpoints);
                                }

                                $pointsearnedforpurchaselog = get_option('_rs_localize_product_purchase_reward_points');
                                $tofindarrayslog = array('{itemproductid}', '{currentorderid}');
                                $toreplacearrayslog = array($item['product_id'], $order_id);
                                $toupdatedvaluesarrays = str_replace($tofindarrayslog, $toreplacearrayslog, $pointsearnedforpurchaselog);




                                $getpointsmulti = $getpoints * $item['qty'];
                                $overalllog[] = array('userid' => $order->user_id, 'totalvalue' => $getpointsmulti, 'eventname' => $toupdatedvaluesarrays, 'date' => $order->order_date);
                                $getoveralllog = get_option('rsoveralllog');
                                $logmerge = array_merge((array) $getoveralllog, $overalllog);
                                update_option('rsoveralllog', $logmerge);
                                $userpoints = get_user_meta($order->user_id, '_my_reward_points', true);
                                $newgetpoints = $getpointsmulti + $userpoints;
                                if ($enabledisablemaxpoints == '1') {
                                    if (($newgetpoints <= $restrictuserpoints) || ($restrictuserpoints == '')) {
                                        $newgetpoints = $newgetpoints;
                                    } else {
                                        $newgetpoints = $restrictuserpoints;
                                    }
                                }
                                update_user_meta($order->user_id, '_my_reward_points', $newgetpoints);
                            } else {
                                $points = get_option('rs_earn_point');
                                $pointsequalto = get_option('rs_earn_point_value');
                                $getpercent = get_post_meta($item['product_id'], '_rewardsystempercent', true);
                                if (get_option('rs_set_price_percentage_reward_points') == '1') {
                                    $getregularprice = get_post_meta($item['product_id'], '_regular_price', true);
                                } else {
                                    $getregularprice = get_post_meta($item['product_id'], '_price', true);
                                }

                                /*
                                 * For Booking Plugin Start
                                 */

                                $getproductdatatypebooking = get_product($item['product_id']);

                                if ($getproductdatatypebooking->is_type('booking')) {
                                    $getregularprice = $item['line_subtotal'];
                                }



                                /*
                                 * For Booking Plugin End
                                 */

                                $percentageproduct = $getpercent / 100;
                                $getpricepercent = $percentageproduct * $getregularprice;
                                $getpointconvert = $getpricepercent * $points;
                                $getexactpoint = $getpointconvert / $pointsequalto;

                                if ($getpercent == '') {
                                    $term = get_the_terms($item['product_id'], 'product_cat');
// var_dump($term);
                                    if (is_array($term)) {
//var_dump($term);
                                        $rewardpoints = array('0');
                                        foreach ($term as $term) {

                                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                            $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                            if ($enablevalue == 'yes') {
                                                if ($display_type == '1') {
                                                    if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                                        $global_enable = get_option('rs_global_enable_disable_reward');
                                                        $global_reward_type = get_option('rs_global_reward_type');
                                                        if ($global_enable == '1') {
                                                            if ($global_reward_type == '1') {
                                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                                            } else {
                                                                $pointconversion = get_option('rs_earn_point');
                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                                $getaveragepoints = $getaverage * $getregularprice;
                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                            }
                                                        }
                                                    } else {
                                                        $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                                    }
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                                        $global_enable = get_option('rs_global_enable_disable_reward');
                                                        $global_reward_type = get_option('rs_global_reward_type');
                                                        if ($global_enable == '1') {
                                                            if ($global_reward_type == '1') {
                                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                                            } else {
                                                                $pointconversion = get_option('rs_earn_point');
                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                                $getaveragepoints = $getaverage * $getregularprice;
                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                            }
                                                        }
                                                    } else {
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        if ($global_enable == '1') {
                                            if ($global_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    }
//var_dump($rewardpoints);
                                    $getexactpoint = max($rewardpoints);

//$rewardpoints = array_search($bookingvalue, $rewardpoints);
                                }

                                $pointsearnedforpurchaselog = get_option('_rs_localize_product_purchase_reward_points');
                                $tofindarrayslog = array('{itemproductid}', '{currentorderid}');
                                $toreplacearrayslog = array($item['product_id'], $order_id);
                                $toupdatedvaluesarrays = str_replace($tofindarrayslog, $toreplacearrayslog, $pointsearnedforpurchaselog);

                                $getpointsmulti = $getexactpoint * $item['qty'];
                                $overalllog[] = array('userid' => $order->user_id, 'totalvalue' => $getpointsmulti, 'eventname' => $toupdatedvaluesarrays, 'date' => $order->order_date);
                                $getoveralllog = get_option('rsoveralllog');
                                $logmerge = array_merge((array) $getoveralllog, $overalllog);
                                update_option('rsoveralllog', $logmerge);
                                $userpoints = get_user_meta($order->user_id, '_my_reward_points', true);
                                $updatedpoints = $getpointsmulti + $userpoints;
                                if ($enabledisablemaxpoints == '1') {
                                    if (($updatedpoints <= $restrictuserpoints) || ($restrictuserpoints == '')) {
                                        $updatedpoints = $updatedpoints;
                                    } else {
                                        $updatedpoints = $restrictuserpoints;
                                    }
                                }
                                update_user_meta($order->user_id, '_my_reward_points', $updatedpoints);
                            }
                        }
                        if (get_post_meta($item['variation_id'], '_enable_reward_points', true) == '1') {
                            $checkenablevariation = get_post_meta($item['variation_id'], '_enable_reward_points', true);
                            $variablerewardpoints = get_post_meta($item['variation_id'], '_reward_points', true);
                            $variationselectrule = get_post_meta($item['variation_id'], '_select_reward_rule', true);
                            $variationrewardpercent = get_post_meta($item['variation_id'], '_reward_percent', true);
                            $variable_product1 = new WC_Product_Variation($item['variation_id']);
#Step 4: You have the data. Have fun :)
                            if (get_option('rs_set_price_percentage_reward_points') == '1') {
                                $variationregularprice = $variable_product1->regular_price;
                            } else {
                                $variationregularprice = $variable_product1->price;
                            }

                            if ($variationselectrule == '1') {
                                $getpoints = get_post_meta($item['variation_id'], '_reward_points', true);
                                $parentvariationid = new WC_Product_Variation($item['variation_id']);
                                $newparentid = $parentvariationid->parent->id;
                                if ($getpoints == '') {
                                    $term = get_the_terms($newparentid, 'product_cat');
//var_dump($term);
                                    if (is_array($term)) {
//var_dump($term);
                                        $rewardpoints = array('0');
                                        foreach ($term as $term) {

                                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                            $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                            if ($enablevalue == 'yes') {
                                                if ($display_type == '1') {
                                                    if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                                        $global_enable = get_option('rs_global_enable_disable_reward');
                                                        $global_reward_type = get_option('rs_global_reward_type');
                                                        if ($global_enable == '1') {
                                                            if ($global_reward_type == '1') {
                                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                                            } else {
                                                                $pointconversion = get_option('rs_earn_point');
                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                            }
                                                        }
                                                    } else {
                                                        $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                                    }
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                                    $getaveragepoints = $getaverage * $variationregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                                        $global_enable = get_option('rs_global_enable_disable_reward');
                                                        $global_reward_type = get_option('rs_global_reward_type');
                                                        if ($global_enable == '1') {
                                                            if ($global_reward_type == '1') {
                                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                                            } else {
                                                                $pointconversion = get_option('rs_earn_point');
                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                            }
                                                        }
                                                    } else {
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $variationregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        if ($global_enable == '1') {
                                            if ($global_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    }
//var_dump($rewardpoints);
                                    $getpoints = max($rewardpoints);

//$rewardpoints = array_search($bookingvalue, $rewardpoints);
                                }

                                $pointsearnedforpurchaselog = get_option('_rs_localize_product_purchase_reward_points');
                                $tofindarrayslog = array('{itemproductid}', '{currentorderid}');
                                $toreplacearrayslog = array($item['variation_id'], $order_id);
                                $toupdatedvaluesarrays = str_replace($tofindarrayslog, $toreplacearrayslog, $pointsearnedforpurchaselog);


                                $getpointsmulti = $getpoints * $item['qty'];
                                $overalllog[] = array('userid' => $order->user_id, 'totalvalue' => $getpointsmulti, 'eventname' => $toupdatedvaluesarrays, 'date' => $order->order_date);
                                $getoveralllog = get_option('rsoveralllog');
                                $logmerge = array_merge((array) $getoveralllog, $overalllog);
                                update_option('rsoveralllog', $logmerge);
                                $userpoints = get_user_meta($order->user_id, '_my_reward_points', true);
                                $newgetpoints = $getpointsmulti + $userpoints;
                                if ($enabledisablemaxpoints == '1') {
                                    if (($newgetpoints <= $restrictuserpoints) || ($restrictuserpoints == '')) {
                                        $newgetpoints = $newgetpoints;
                                    } else {
                                        $newgetpoints = $restrictuserpoints;
                                    }
                                }
                                update_user_meta($order->user_id, '_my_reward_points', $newgetpoints);
                            } else {
                                $pointconversion = get_option('rs_earn_point');
                                $pointconversionvalue = get_option('rs_earn_point_value');
                                $getaverage = $variationrewardpercent / 100;
                                $getaveragepoints = $getaverage * $variationregularprice;
                                $getpointsvalue = $getaveragepoints * $pointconversion;
                                $points = $getpointsvalue / $pointconversionvalue;

                                $parentvariationid = new WC_Product_Variation($item['variation_id']);
                                $newparentid = $parentvariationid->parent->id;
                                if ($variationrewardpercent == '') {
                                    $term = get_the_terms($newparentid, 'product_cat');
//var_dump($term);
                                    if (is_array($term)) {
//var_dump($term);
                                        $rewardpoints = array('0');
                                        foreach ($term as $term) {

                                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                            $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                            if ($enablevalue == 'yes') {
                                                if ($display_type == '1') {
                                                    if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                                        $global_enable = get_option('rs_global_enable_disable_reward');
                                                        $global_reward_type = get_option('rs_global_reward_type');
                                                        if ($global_enable == '1') {
                                                            if ($global_reward_type == '1') {
                                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                                            } else {
                                                                $pointconversion = get_option('rs_earn_point');
                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                            }
                                                        }
                                                    } else {
                                                        $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                                    }
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                                    $getaveragepoints = $getaverage * $variationregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                                        $global_enable = get_option('rs_global_enable_disable_reward');
                                                        $global_reward_type = get_option('rs_global_reward_type');
                                                        if ($global_enable == '1') {
                                                            if ($global_reward_type == '1') {
                                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                                            } else {
                                                                $pointconversion = get_option('rs_earn_point');
                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                            }
                                                        }
                                                    } else {
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $variationregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        if ($global_enable == '1') {
                                            if ($global_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    }
//var_dump($rewardpoints);
                                    $points = max($rewardpoints);

//$rewardpoints = array_search($bookingvalue, $rewardpoints);
                                }

                                $pointsearnedforpurchaselog = get_option('_rs_localize_product_purchase_reward_points');
                                $tofindarrayslog = array('{itemproductid}', '{currentorderid}');
                                $toreplacearrayslog = array($item['variation_id'], $order_id);
                                $toupdatedvaluesarrays = str_replace($tofindarrayslog, $toreplacearrayslog, $pointsearnedforpurchaselog);


                                $getpointsmulti = $points * $item['qty'];
                                $overalllog[] = array('userid' => $order->user_id, 'totalvalue' => $getpointsmulti, 'eventname' => $toupdatedvaluesarrays, 'date' => $order->order_date);
                                $getoveralllog = get_option('rsoveralllog');
                                $logmerge = array_merge((array) $getoveralllog, $overalllog);
                                update_option('rsoveralllog', $logmerge);
                                $userpoints = get_user_meta($order->user_id, '_my_reward_points', true);
                                $newgetpoints = $getpointsmulti + $userpoints;
                                if (($newgetpoints <= $restrictuserpoints) || ($restrictuserpoints == '')) {
                                    $newgetpoints = $newgetpoints;
                                } else {
                                    $newgetpoints = $restrictuserpoints;
                                }
                                update_user_meta($order->user_id, '_my_reward_points', $newgetpoints);
                            }
                        }

                        $rewardpointscoupons = $order->get_items(array('coupon'));

                        foreach ($rewardpointscoupons as $couponcode => $bookingvalue) {
                            if (str_replace(' ', '', strtolower($usernickname)) == strtolower($bookingvalue['name'])) {
                                if (get_option('rewardsystem_looped_over_coupon' . $order_id) != '1') {
                                    $getcouponid = get_option($usernickname . 'coupon');
                                    $currentamount = get_post_meta($getcouponid, 'coupon_amount', true);
                                    //update_option('testings', $order->get_total_discount());
                                    if ($currentamount >= $bookingvalue['discount_amount']) {
                                        $current_conversion = get_option('rs_redeem_point');
                                        $point_amount = get_option('rs_redeem_point_value');
                                        $redeemedamount = $bookingvalue['discount_amount'] * $current_conversion;
                                        $redeemedpoints = $redeemedamount / $point_amount;
                                        //$overalllog[] = array('userid' => $order->user_id, 'totalvalue' => $redeemedpoints, 'eventname' => 'Points Redeemed Towards Purchase', 'date' => $order->order_date);
                                        //$getoveralllog = get_option('rsoveralllog');
                                        //$logmerge = array_merge((array) $getoveralllog, $overalllog);
                                        //update_option('rsoveralllog', $logmerge);
                                        update_user_meta($order->user_id, '_redeemed_points', $redeemedpoints);
                                        update_user_meta($order->user_id, '_redeemed_amount', $bookingvalue['discount_amount']);
                                        $yourpoints = get_user_meta($order->user_id, '_my_reward_points', true);
                                        update_user_meta($order->user_id, '_my_reward_points', $yourpoints - $redeemedpoints);
                                    }

                                    update_option('rewardsystem_looped_over_coupon' . $order_id, '1');
                                }
                            }
                        }


                        $totalpoints = get_user_meta($order->user_id, '_my_reward_points', true);

                        if (get_option('rewardsystem_looped_over_coupons' . $order_id) == '1') {
                            $getredeemedpoints = "0";
                        } else {
                            $getredeemedpoints = get_user_meta($order->user_id, '_redeemed_points', true);
                        }


                        $getpointsvalue = get_user_meta($order->user_id, '_redeemed_amount', true);
                        $post_url = admin_url('post.php?post=' . $order_id) . '&action=edit';
                        $myaccountlink = get_permalink(get_option('woocommerce_myaccount_page_id'));
                        $vieworderlink = add_query_arg('view-order', $order_id, $myaccountlink);


                        $overallmaingetoption = get_option('_rs_localize_points_earned_for_purchase_main');
                        $fromstartarray = array('{currentorderid}');

                        $frontendorderlink = '<a href="' . $vieworderlink . '">#' . $order_id . '</a>';
                        $backendorderlink = '<a href="' . $post_url . '">#' . $order_id . '</a>';
                        $toendarrayfrontend = array($frontendorderlink);
                        $toendarraybackend = array($backendorderlink);
                        $frontendstringreplace = str_replace($fromstartarray, $toendarrayfrontend, $overallmaingetoption);

                        $backendstringreplace = str_replace($fromstartarray, $toendarraybackend, $overallmaingetoption);

                        $pointslog[] = array('orderid' => $order_id, 'userid' => $order->user_id, 'points_earned_order' => $getpointsmulti, 'points_redeemed' => $getredeemedpoints, 'points_value' => $getpointsvalue, 'before_order_points' => $userpoints, 'totalpoints' => $totalpoints, 'date' => $order->order_date, 'rewarder_for' => $backendstringreplace, 'rewarder_for_frontend' => $frontendstringreplace);
                        add_option('rewardsystem_looped_over_coupons' . $order_id, '1');
                    }
                    $rewardpointscoupons = $order->get_items(array('coupon'));

                    foreach ($rewardpointscoupons as $couponcodeser => $bookingvalue) {
                        if (str_replace(' ', '', strtolower($usernickname)) == strtolower($bookingvalue['name'])) {
                            $getcouponid = get_option($usernickname . 'coupon');
                            $currentamount = get_post_meta($getcouponid, 'coupon_amount', true);
                            //update_option('testings', $order->get_total_discount());
                            if ($currentamount >= $bookingvalue['discount_amount']) {
                                $current_conversion = get_option('rs_redeem_point');
                                $point_amount = get_option('rs_redeem_point_value');
                                $redeemedamount = $bookingvalue['discount_amount'] * $current_conversion;
                                $redeemedpoints = $redeemedamount / $point_amount;

                                $redeemloginformation = get_option('_rs_localize_points_redeemed_towards_purchase');
                                $startarray = array('{currentorderid}');
                                $endarray = array($order_id);

                                $mainupdatedvaluesinarray = str_replace($startarray, $endarray, $redeemloginformation);
                                $overalllog[] = array('userid' => $order->user_id, 'totalvalue' => $redeemedpoints, 'eventname' => $mainupdatedvaluesinarray, 'date' => $order->order_date);
                                $getoveralllog = get_option('rsoveralllog');
                                $logmerge = array_merge((array) $getoveralllog, $overalllog);
                                update_option('rsoveralllog', $logmerge);
                                update_user_meta($order->user_id, '_redeemed_points', $redeemedpoints);
                                update_user_meta($order->user_id, '_redeemed_amount', $bookingvalue['discount_amount']);
                                $yourpoints = get_user_meta($order->user_id, '_my_reward_points', true);
                                //update_user_meta($order->user_id, '_my_reward_points', $yourpoints - $redeemedpoints);
                            }
                        }
                    }

                    $getmypoints = get_user_meta($order->user_id, '_my_points_log', true);
                    $merged = array_merge((array) $getmypoints, $pointslog);
                    update_user_meta($order->user_id, '_my_points_log', $merged);


                    //Mail Sending for Earning Points and Redeeming Points

                    FPRewardSystem::rsmail_sending_on_custom_rule();




                    add_post_meta($order_id, '_sumo_points_awarded', 'yes');
                //}
//            }
//        }
            //}
            }            
    }

}

new RSBookingCompatibility();
