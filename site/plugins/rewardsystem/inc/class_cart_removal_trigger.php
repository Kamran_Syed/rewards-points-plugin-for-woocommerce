<?php

class RSCartItemRemoval {

    public function __construct() {
        add_action('woocommerce_cart_updated', array($this, 'cart_item_removal'), 100);
    }

    public static function cart_item_removal() {
        global $woocommerce;
        if (is_user_logged_in()) {
            $user_ID = get_current_user_id();
// $coupon_code = get_user_meta($user_ID, 'nickname', true); // Code
            $getinfousernickname = get_user_by('id', $user_ID);
            $couponcodeuserlogin = $getinfousernickname->user_login;
            $newcouponids = get_user_meta($user_ID, 'redeemcouponids', true);
            $gettotal = get_post_meta($newcouponids, 'carttotal', true);
            $getcarttotal = get_post_meta($newcouponids, 'cartcontenttotal', true);
            if (get_post_meta($newcouponids, 'rsmaximumdiscountcart', true) == '1') {
                if ($getcarttotal > $woocommerce->cart->cart_contents_count) {
                    wp_delete_post($newcouponids, true);
                }
            }
        }
    }

}

new RSCartItemRemoval();
