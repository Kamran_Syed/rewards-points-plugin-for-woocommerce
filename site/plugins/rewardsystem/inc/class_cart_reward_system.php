<?php

class FPRewardSystemCart {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_cart'] = __('Cart', 'rewardsystem');
        return $settings_tabs;
    }

    // Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        $rs_productsearch_redeeming = array();
        $rsproduct_name = array();
        $rsproductids = array();
        $categorylist =array();
        $particular_product_id = get_option('rs_select_products_to_enable_redeeming');
        $particular_ids = !empty($particular_product_id) ? array_map('absint', (array) $particular_product_id) : null;
        if ($particular_ids != NULL) {
            foreach ($particular_ids as $particular_id) {
                $rsproductids[] = $particular_id;
                $productobject = new Wc_Product($particular_id);
                $rsproduct_name[] = $productobject->get_formatted_name($rsproductids);
            }
                
        $rs_productsearch_redeeming = array_combine((array) $rsproductids, (array) $rsproduct_name);
        }

        $rs_productsearch_exclude_redeeming = array();
        $particular_product_id = get_option('rs_exclude_products_to_enable_redeeming');
        $particular_ids = !empty($particular_product_id) ? array_map('absint', (array) $particular_product_id) : null;
        if ($particular_ids != NULL) {
            foreach ($particular_ids as $particular_id) {
                $rsproductids[] = $particular_id;
                $productobject = new Wc_Product($particular_id);
                $rsproduct_name[] = $productobject->get_formatted_name($rsproductids);
            }
                
        $rs_productsearch_exclude_redeeming = array_combine((array) $rsproductids, (array) $rsproduct_name);
        }
        
        
        $categoryname = array();
        $categoryid = array();
        $particularcategory = get_terms('product_cat');
        //var_dump($particularcategory);
         if (!is_wp_error($particularcategory)) {
            if (!empty($particularcategory)) {
                if (is_array($particularcategory)) {
                    foreach ($particularcategory as $category) {
                        $categoryname[] = $category->name;
                        //var_dump($categoryname);
                        $categoryid[] = $category->term_id;
                    }
                }
                $categorylist = array_combine((array) $categoryid, (array) $categoryname);
            }
        }

        return apply_filters('woocommerce_rewardsystem_my_account_settings', array(
            array(
                'name' => __('Cart Redeem Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => '',
                'id' => '_rs_cart_redeem_settings'
            ),
            array(
                'name' => __('Apply Redeeming Before Tax', 'rewardsystem'),
                'desc' => __('Enable this box if the coupon should be applied before calculating cart tax.', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_apply_redeem_before_tax',
                'css' => 'min-width:150px;',
                'std' => '1',
                'type' => 'select',
                'newids' => 'rs_apply_redeem_before_tax',
                'options' => array(
                    '1' => __('Enable', 'rewardsystem'),
                    '2' => __('Disable', 'rewardsystem'),
                ),
                'desc_tip' => true,
            ),
            array(
                'name' => __('Enable Free Shipping ', 'rewardsystem'),
                'desc' => __('Enable this box if shipping should be made free ', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_apply_shipping_tax',
                'css' => 'min-width:150px;',
                'std' => '2',
                'type' => 'select',
                'newids' => 'rs_apply_shipping_tax',
                'options' => array(
                    '1' => __('Enable', 'rewardsystem'),
                    '2' => __('Disable', 'rewardsystem'),
                ),
                'desc_tip' => true,
            ),
            array(
                'name' => __('Show/Hide Redeeming Field', 'rewardsystem'),
                'desc' => __('Show/Hide Redeeming Field in a Cart Page of WooCommerce', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_show_hide_redeem_field',
                'css' => '',
                'std' => '1',
                'type' => 'select',
                'newids' => 'rs_show_hide_redeem_field',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
                'desc_tip' => true,
            ),
            array(
                'name' => __('Enable Redeeming for Selected Product', 'rewardsystem'),
                'desc' => __('Enable Redeeming for Selected Product', 'rewardsystem'),
                'id' => 'rs_enable_redeem_for_selected_products',
                'css' => 'min-width:150px;',
                'type' => 'checkbox',
                'newids' => 'rs_enable_redeem_for_selected_products',
            ),
            array(
                'name' => __('Select Products', 'rewardsystem'),
                'desc' => __('Select Products to enable redeeming', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_select_products_to_enable_redeeming',
                'class' => 'rs_select_products_to_enable_redeeming',
                'css' => 'min-width:350px',
                'std' => '',
                'type' => 'include_product_selection',
                'newids' => 'rs_select_products_to_enable_redeeming',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Exclude Products for Redeeming', 'rewardsystem'),
                'desc' => __('Exclude Products for Redeeming', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_exclude_products_for_redeeming',
                'class' => 'rs_exclude_products_for_redeeming',
                'css' => '',
                'std' => '',
                'type' => 'checkbox',
                'newids' => 'rs_exclude_products_for_redeeming',
            ),
//            array(
//                'name' => __('Exclude Products', 'rewardsystem'),
//                'desc' => __('Exclude Products to enable redeeming', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_exclude_products_to_enable_redeeming',
//                'class' => 'rs_exclude_products_to_enable_redeeming rs_ajax_chosen_select_products_redeem',
//                'css' => 'min-width:350px',
//                'std' => '',
//                'type' => 'multiselect',
//                'newids' => 'rs_exclude_products_to_enable_redeeming',
//                'options' => $rs_productsearch_exclude_redeeming,
//                'desc_tip' => true,
//            ),
            array(
                'name' => __('Exclude Products', 'rewardsystem'),
                'desc' => __('Exclude Products to enable redeeming', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_exclude_products_to_enable_redeeming',
                'class' => 'rs_exclude_products_to_enable_redeeming rs_ajax_chosen_select_products_redeem',
                'css' => 'min-width:350px',
                'std' => '',
                'type' => 'exclude_product_selection',
                'newids' => 'rs_exclude_products_to_enable_redeeming',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Enable Redeeming for Selected Category', 'rewardsystem'),
                'desc' => __('Enable Redeeming for Selected Category', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_enable_redeem_for_selected_category',
                'class' => 'rs_enable_redeem_for_selected_category',
                'css' => '',
                'std' => '',
                'type' => 'checkbox',
                'newids' => 'rs_enable_redeem_for_selected_category',
            ),
            array(
                'name' => __('Select Category', 'rewardsystem'),
                'desc' => __('Select Category to enable redeeming', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_select_category_to_enable_redeeming',
                'class' => 'rs_select_category_to_enable_redeeming',
                'css' => 'min-width:350px',
                'std' => '',
                'type' => 'multiselect',
                'newids' => 'rs_select_category_to_enable_redeeming',
                'options' => $categorylist,
                'desc_tip' => true,
            ),
            array(
                'name' => __('Exclude Category for Redeeming', 'rewardsystem'),
                'desc' => __('Exclude Category for Redeeming', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_exclude_category_for_redeeming',
                'css' => '',
                'std' => '',
                'type' => 'checkbox',
                'newids' => 'rs_exclude_category_for_redeeming',
            ),
            array(
                'name' => __('Exclude Category', 'rewardsystem'),
                'desc' => __('Exclude Category to enable redeeming', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_exclude_category_to_enable_redeeming',
                'class' => 'rs_exclude_category_to_enable_redeeming',
                'css' => 'min-width:350px',
                'std' => '',
                'type' => 'multiselect',
                'newids' => 'rs_exclude_category_to_enable_redeeming',
                'options' => $categorylist,
                'desc_tip' => true,
            ),
            array(
                'name' => __('Redeeming Field Type', 'rewardsystem'),
                'desc' => __('Select the type of Redeeming to used in Cart Page of WooCommerce', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_redeem_field_type_option',
                'css' => '',
                'std' => '1',
                'type' => 'select',
                'newids' => 'rs_redeem_field_type_option',
                'options' => array(
                    '1' => __('Default', 'rewardsystem'),
                    '2' => __('Button', 'rewardsystem'),
                ),
                'desc_tip' => true,
            ),
            array(
                'name' => __('Percentage of Cart Total to be Redeemed', 'rewardsystem'),
                'desc' => __('Enter the Percentage of the cart total that has to be Redeemed', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_percentage_cart_total_redeem',
                'css' => 'min-width:550px;',
                'std' => '100 ',
                'type' => 'text',
                'newids' => 'rs_percentage_cart_total_redeem',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Redeeming Button Message ', 'rewardsystem'),
                'desc' => __('Enter the Message for the Redeeming Button', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_redeeming_button_option_message',
                'css' => 'min-width:550px;',
                'std' => '[cartredeempoints] points worth of [currencysymbol] [pointsvalue] will be Redeemed',
                'type' => 'textarea',
                'newids' => 'rs_redeeming_button_option_message',
                'desc_tip' => true,
            ),
//            array(
//                'name' => __('Redeeming Button Message when User Points Value is less than cart Total', 'rewardsystem'),
//                'desc' => __('Enter the Message for the Redeeming Button', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_redeeming_button_option_message_less',
//                'css' => 'min-width:550px;',
//                'std' => '[cartredeempoints] points will be Redeemed',
//                'type' => 'textarea',
//                'newids' => 'rs_redeeming_button_option_message_less',
//                'desc_tip' => true,
//            ),
            array(
                'name' => __('Minimum Points to be Earned for Redeeming First Time', 'rewardsystem'),
                'desc' => __('Enter Minimum Points to be Earned for Redeeming First Time in Cart/Checkout', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_first_time_minimum_user_points',
                'css' => 'min-width:550px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_first_time_minimum_user_points',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Minimum Balance Points after First Redeeming', 'rewardsystem'),
                'desc' => __('Enter Minimum Balance Points for Redeeming in Cart/Checkout', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_minimum_user_points_to_redeem',
                'css' => 'min-width:550px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_minimum_user_points_to_redeem',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Minimum Points for Redeeming', 'rewardsystem'),
                'desc' => __('Enter Minimum Points for Redeeming', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_minimum_redeeming_points',
                'css' => 'min-width:550px;',
                'std' => '',
                'type' => 'text',
                'newids' => 'rs_minimum_redeeming_points',
                'desc_tip' => true,
            ),
//            array(
//                'name' => __('Redeeming Point Apply Option', 'rewardsystem'),
//                'desc' => __('Select the Redeeming Point Apply Option through Apply Button or Slider Control', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_redeem_point_apply_option',
//                'css' => '',
//                'std' => '1',
//                'type' => 'select',
//                'options' => array('1' => __('Apply Button', 'rewardsystem'), '2' => __('Slider Control', 'rewardsystem')),
//                'desc_tip' => true,
//            ),
            array(
                'name' => __('Minimum Cart Total for Redeeming', 'rewardsystem'),
                'desc' => __('Enter Minimum Cart Total for Redeeming', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_minimum_cart_total_points',
                'css' => 'min-width:550px;',
                'std' => '',
                'type' => 'text',
                'newids' => 'rs_minimum_cart_total_points',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Redeem Field Caption', 'rewardsystem'),
                'desc' => __('Enter the Label which will be displayed in Redeem Field', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_redeem_field_caption',
                'css' => 'min-width:550px;',
                'std' => 'Redeem your Reward Points:',
                'type' => 'text',
                'newids' => 'rs_redeem_field_caption',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Redeem Field Caption Show/Hide', 'rewardsystem'),
                'desc' => __('Show/Hide Redeem Field Caption', 'rewardsystem'),
                'id' => 'rs_show_hide_redeem_caption',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'newids' => 'rs_show_hide_redeem_caption',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Redeem Field Placeholder', 'rewardsystem'),
                'desc' => __('Enter the Placeholder which will be displayed in Redeem Field', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_redeem_field_placeholder',
                'css' => 'min-width:550px;',
                'std' => 'Reward Points to Enter',
                'type' => 'text',
                'newids' => 'rs_redeem_field_placeholder',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Redeem Field Placeholder Show/Hide', 'rewardsystem'),
                'desc' => __('Show/Hide Redeem Field Placeholder', 'rewardsystem'),
                'id' => 'rs_show_hide_redeem_placeholder',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'newids' => 'rs_show_hide_redeem_placeholder',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Redeem Field Submit Button Caption', 'rewardsystem'),
                'desc' => __('Enter the Label which will be displayed in Submit Button', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_redeem_field_submit_button_caption',
                'css' => 'min-width:550px;margin-bottom:40px;',
                'std' => 'Apply Reward Points',
                'type' => 'text',
                'newids' => 'rs_redeem_field_submit_button_caption',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_cart_redeem_settings'),
            array(
                'name' => __('Cart Redeem Error Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => '',
                'id' => '_rs_cart_redeem_error_settings'
            ),
            array(
                'name' => __('Empty Redeem Point Error Message', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed when Redeem Field has Empty Value', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_redeem_empty_error_message',
                'css' => 'min-width:550px;',
                'std' => 'No Reward Points Entered',
                'type' => 'text',
                'newids' => 'rs_redeem_empty_error_message',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Redeeming Contain Characters', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed when redeeming field value contain characters', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_redeem_character_error_message',
                'css' => 'min-width:550px;',
                'std' => 'Please Enter Only Numbers',
                'type' => 'text',
                'newids' => 'rs_redeem_character_error_message',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Maximum Redeem Point Error Message', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed when Entered Reward Points is more than Earned Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_redeem_max_error_message',
                'css' => 'min-width:550px;',
                'std' => 'Reward Points you entered is more than Your Earned Reward Points ',
                'type' => 'text',
                'newids' => 'rs_redeem_max_error_message',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Minimum Redeem Point Error Message', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed when Entered Points is less than Minimum Redeeming Points which is set in this Page', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_minimum_redeem_point_error_message',
                'css' => 'min-width:550px;',
                'std' => 'Please Enter Points more than [rsminimumpoints]',
                'type' => 'text',
                'newids' => 'rs_minimum_redeem_point_error_message',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Current User Points is Empty Error Message Show/Hide', 'rewardsystem'),
                'desc' => __('Show/Hide Current User Points is Empty Error Message', 'rewardsystem'),
                'id' => 'rs_show_hide_points_empty_error_message',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'newids' => 'rs_show_hide_points_empty_error_message',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Current User Points is Empty Error Message', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed when the Current User Points is Empty', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_current_points_empty_error_message',
                'css' => 'min-width:550px;',
                'std' => 'You don\'t have Points for Redeeming',
                'type' => 'text',
                'newids' => 'rs_current_points_empty_error_message',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Minimum Points for first time Redeeming Error Message Show/Hide', 'rewardsystem'),
                'desc' => __('Show/Hide Minimum Points for first time Redeeming Error Message', 'rewardsystem'),
                'id' => 'rs_show_hide_first_redeem_error_message',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'newids' => 'rs_show_hide_first_redeem_error_message',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Minimum Points for first time Redeeming Error Message', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed when the user doesn\'t have enough points for first time redeeming', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_min_points_first_redeem_error_message',
                'css' => 'min-width:550px;',
                'std' => 'You need Minimum of [firstredeempoints] Points when redeeming for the First time',
                'type' => 'textarea',
                'newids' => 'rs_min_points_first_redeem_error_message',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Minimum Points After first time Redeeming Error Message Show/Hide', 'rewardsystem'),
                'desc' => __('Show/Hide Minimum Points After first time Redeeming Error Message', 'rewardsystem'),
                'id' => 'rs_show_hide_after_first_redeem_error_message',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'newids' => 'rs_show_hide_after_first_redeem_error_message',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Minimum Points After first time Redeeming Error Message', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed when the Current User doesn\'t have minimum points for Redeeming ', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_min_points_after_first_error',
                'css' => 'min-width:550px;',
                'std' => 'You need minimum of [points_after_first_redeem] Points for Redeeming',
                'type' => 'textarea',
                'newids' => 'rs_min_points_after_first_error',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Minimum Cart Total for Redeeming Error Message Show/Hide', 'rewardsystem'),
                'desc' => __('Show/Hide Minimum Cart Total for Redeeming Error Message', 'rewardsystem'),
                'id' => 'rs_show_hide_minimum_cart_total_error_message',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'newids' => 'rs_show_hide_minimum_cart_total_error_message',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'rewardsystem'),
                    '2' => __('Hide', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Minimum Cart Total for Redeeming Error Message', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed when current Cart total is less than minimum Cart Total for Redeeming ', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_min_cart_total_redeem_error',
                'css' => 'min-width:550px;',
                'std' => 'You need minimum cart Total of [currencysymbol][carttotal] in order to Redeem',
                'type' => 'textarea',
                'newids' => 'rs_min_cart_total_redeem_error',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_cart_redeem_error_settings'),
            array(
                'name' => __('Coupon Label Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => '',
                'id' => '_rs_cart_redeem_error_settings'
            ),
            array(
                'name' => __('Coupon Label Settings', 'rewardsystem'),
                'desc' => __('Enter the Message which will be displayed in Cart Subtotal', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_coupon_label_message',
                'css' => 'min-width:550px;',
                'std' => 'Redeem Points Value',
                'type' => 'text',
                'newids' => 'rs_coupon_label_message',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_cart_redeem_error_settings'),
            array(
                'name' => __('Extra Class Name for Button', 'rewardsystem'),
                'type' => 'title',
                'desc' => '',
                'id' => '_rs_cart_custom_class_name',
            ),
            array(
                'name' => __('Extra Class Name for Cart Apply Reward Points Button', 'rewardsystem'),
                'desc' => __('Add Extra Class Name to the Cart Apply Reward Points Button, Don\'t Enter dot(.) before Class Name', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_extra_class_name_apply_reward_points',
                'css' => 'min-width:550px;',
                'std' => '',
                'type' => 'text',
                'newids' => 'rs_extra_class_name_apply_reward_points',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_cart_custom_class_name'),
            array(
                'name' => __('Custom CSS Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => 'Try !important if styles doesn\'t apply ',
                'id' => '_rs_cart_custom_css_settings',
            ),
            array(
                'name' => __('Custom CSS', 'rewardsystem'),
                'desc' => __('Enter the Custom CSS for the Cart Page ', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_cart_page_custom_css',
                'css' => 'min-width:350px; min-height:350px;',
                'std' => '#rs_apply_coupon_code_field { } #mainsubmi { } .fp_apply_reward{ }',
                'type' => 'textarea',
                'newids' => 'rs_cart_page_custom_css',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_cart_custom_css_settings'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemCart::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemCart::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemCart::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && isset($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

    public static function save_product_selection_backward_compatibility() {
        update_option('rs_exclude_products_to_enable_redeeming', $_POST['rs_exclude_products_to_enable_redeeming']);
    }

    public static function save_product_selection_normal_compatibility() {
        update_option('rs_select_products_to_enable_redeeming', $_POST['rs_select_products_to_enable_redeeming']);
    }

    public static function add_product_selection_backward_compatibility() {

        global $woocommerce;
        if ((float) $woocommerce->version > (float) ('2.2.0')) {
            ?>
            <tr valign="top">
                <th class="titledesc" scope="row">
                    <label for="rs_exclude_products_to_enable_redeeming"><?php _e('Select Products', 'rewardsystem'); ?></label>
                </th>
                <td class="forminp forminp-select">
                    <input type="hidden" class="wc-product-search" style="width: 100%;" id="rs_exclude_products_to_enable_redeeming"  name="rs_exclude_products_to_enable_redeeming" data-placeholder="<?php _e('Search for a product&hellip;', 'rewardsystem'); ?>" data-action="woocommerce_json_search_products_and_variations" data-multiple="true" data-selected="<?php
                    $json_ids = array();
                    if (get_option('rs_exclude_products_to_enable_redeeming') != "") {
                        $list_of_produts = get_option('rs_exclude_products_to_enable_redeeming');
                        $product_ids = array_filter(array_map('absint', (array) explode(',', get_option('rs_exclude_products_to_enable_redeeming'))));

                        foreach ($product_ids as $product_id) {
                            $product = wc_get_product($product_id);
                            $json_ids[$product_id] = wp_kses_post($product->get_formatted_name());
                        } echo esc_attr(json_encode($json_ids));
                    }
                    ?>" value="<?php echo implode(',', array_keys($json_ids)); ?>" />
                </td>
            </tr>
        <?php } else { ?>
            <tr valign="top">
                <th class="titledesc" scope="row">
                    <label for="rs_exclude_products_to_enable_redeeming"><?php _e('Select Products', 'rewardsystem'); ?></label>
                </th>
                <td class="forminp forminp-select">
                    <select multiple name="rs_exclude_products_to_enable_redeeming" style='width:550px;' id='rs_exclude_products_to_enable_redeeming' class="rs_exclude_products_to_enable_redeeming rs_ajax_chosen_select_products_redeem">
                        <?php
                        if ((array) get_option('rs_exclude_products_to_enable_redeeming') != "") {
                            $list_of_produts = (array) get_option('rs_exclude_products_to_enable_redeeming');
                            foreach ($list_of_produts as $rs_free_id) {
                                echo '<option value="' . $rs_free_id . '" ';
                                selected(1, 1);
                                echo '>' . ' #' . $rs_free_id . ' &ndash; ' . get_the_title($rs_free_id);
                            }
                        } else {
                            ?>
                            <option value=""></option>
                            <?php
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <?php
        }
    }

    public static function add_product_selection_normal_compatibility() {

        global $woocommerce;
        if ((float) $woocommerce->version > (float) ('2.2.0')) {
            ?>
            <tr valign="top">
                <th class="titledesc" scope="row">
                    <label for="rs_select_products_to_enable_redeeming"><?php _e('Select Products', 'rewardsystem'); ?></label>
                </th>
                <td class="forminp forminp-select">
                    <input type="hidden" class="wc-product-search" style="width: 100%;" id="rs_select_products_to_enable_redeeming"  name="rs_select_products_to_enable_redeeming" data-placeholder="<?php _e('Search for a product&hellip;', 'rewardsystem'); ?>" data-action="woocommerce_json_search_products_and_variations" data-multiple="true" data-selected="<?php
                    $json_ids = array();
                    if (get_option('rs_select_products_to_enable_redeeming') != "") {
                        $list_of_produts = get_option('rs_select_products_to_enable_redeeming');
                        $product_ids = array_filter(array_map('absint', (array) explode(',', get_option('rs_select_products_to_enable_redeeming'))));

                        foreach ($product_ids as $product_id) {
                            $product = wc_get_product($product_id);
                            $json_ids[$product_id] = wp_kses_post($product->get_formatted_name());
                        } echo esc_attr(json_encode($json_ids));
                    }
                    ?>" value="<?php echo implode(',', array_keys($json_ids)); ?>" />
                </td>
            </tr>
        <?php } else { ?>
            <tr valign="top">
                <th class="titledesc" scope="row">
                    <label for="rs_select_products_to_enable_redeeming"><?php _e('Select Products', 'rewardsystem'); ?></label>
                </th>
                <td class="forminp forminp-select">
                    <select multiple name="rs_select_products_to_enable_redeeming" style='width:550px;' id='rs_select_products_to_enable_redeeming' class="rs_select_products_to_enable_redeeming rs_ajax_chosen_select_products_redeem">
                        <?php
                        if ((array) get_option('rs_select_products_to_enable_redeeming') != "") {
                            $list_of_produts = (array) get_option('rs_select_products_to_enable_redeeming');
                            foreach ($list_of_produts as $rs_free_id) {
                                echo '<option value="' . $rs_free_id . '" ';
                                selected(1, 1);
                                echo '>' . ' #' . $rs_free_id . ' &ndash; ' . get_the_title($rs_free_id);
                            }
                        } else {
                            ?>
                            <option value=""></option>
                            <?php
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <?php
        }
    }

    public static function apply_redeem_points_button_type_settings() {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                if (jQuery("#rs_redeem_field_type_option").val() == '1') {
                    jQuery("#rs_percentage_cart_total_redeem").parent().parent().hide();
                    jQuery("#rs_redeeming_button_option_message").parent().parent().hide();
                    //jQuery("#rs_redeeming_button_option_message_less").parent().parent().hide();
                } else {
                    jQuery("#rs_percentage_cart_total_redeem").parent().parent().show();
                    jQuery("#rs_redeeming_button_option_message").parent().parent().show();
                    // jQuery("#rs_redeeming_button_option_message_less").parent().parent().show();
                }
                jQuery("#rs_redeem_field_type_option").change(function () {
                    jQuery("#rs_percentage_cart_total_redeem").parent().parent().toggle();
                    jQuery("#rs_redeeming_button_option_message").parent().parent().toggle();
                    //  jQuery("#rs_redeeming_button_option_message_less").parent().parent().toggle();
                });
            });
        </script>
        <?php
    }

    public static function rs_redeeming_selected_products_categories() {
        global $woocommerce;
        if (isset($_GET['tab'])) {
            if ($_GET['tab'] == 'rewardsystem_cart') {
                ?>
                <script type="text/javascript">
                <?php
                if ((float) $woocommerce->version <= (float) ('2.2.0')) {
                    ?>
                        jQuery(function () {

                            // Ajax Chosen Product Selectors
                            jQuery("select.rs_ajax_chosen_select_products_redeem").ajaxChosen({
                                method: 'GET',
                                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                                dataType: 'json',
                                afterTypeDelay: 100,
                                data: {
                                    action: 'woocommerce_json_search_products_and_variations',
                                    security: '<?php echo wp_create_nonce("search-products"); ?>'
                                }
                            }, function (data) {
                                var terms = {};

                                jQuery.each(data, function (i, val) {
                                    terms[i] = val;
                                });
                                return terms;
                            });
                        });
                <?php } ?>
                    jQuery(document).ready(function () {
                <?php
                if ((float) $woocommerce->version <= (float) ('2.2.0')) {
                    ?>
                            jQuery('#rs_select_category_to_enable_redeeming').chosen();
                            jQuery('#rs_exclude_category_to_enable_redeeming').chosen();

                <?php } else {
                    ?>
                            jQuery('#rs_select_category_to_enable_redeeming').select2();
                            jQuery('#rs_exclude_category_to_enable_redeeming').select2();
                <?php }
                ?>
    var currentvalue=jQuery('#rs_show_hide_redeem_field').val();
                            if(currentvalue==='1'){
                                jQuery('#rs_enable_redeem_for_selected_products').parent().parent().parent().parent().show();
                                jQuery('#rs_exclude_products_for_redeeming').parent().parent().parent().parent().show();
                                jQuery('#rs_enable_redeem_for_selected_category').parent().parent().parent().parent().show();
                                jQuery('#rs_exclude_category_for_redeeming').parent().parent().parent().parent().show();
                                var enable_selected_product_checkbox =jQuery('#rs_enable_redeem_for_selected_products').is(':checked')?'yes':'no';
                                var enable_exclude_product_checkbox = jQuery('#rs_exclude_products_for_redeeming').is(':checked') ? 'yes' : 'no';
                                var enable_selected_category_checkbox = jQuery('#rs_enable_redeem_for_selected_category').is(':checked') ? 'yes' : 'no';
                                var enable_exclude_category_checkbox = jQuery('#rs_exclude_category_for_redeeming').is(':checked') ? 'yes' : 'no';
                                if(enable_selected_product_checkbox==='yes'){
                                    jQuery('#rs_select_products_to_enable_redeeming').parent().parent().show();
                                }else{
                                    jQuery('#rs_select_products_to_enable_redeeming').parent().parent().hide();
                                }
                                if(enable_exclude_product_checkbox==='yes'){
                                    jQuery('#rs_exclude_products_to_enable_redeeming').parent().parent().show();
                                }else{
                                    jQuery('#rs_exclude_products_to_enable_redeeming').parent().parent().hide();
                                }
                                if(enable_selected_category_checkbox==='yes'){
                                    jQuery('#rs_select_category_to_enable_redeeming').parent().parent().show();
                                }else{
                                    jQuery('#rs_select_category_to_enable_redeeming').parent().parent().hide();
                                }
                                if(enable_exclude_category_checkbox==='yes'){
                                    jQuery('#rs_exclude_category_to_enable_redeeming').parent().parent().show();
                                }else{
                                    jQuery('#rs_exclude_category_to_enable_redeeming').parent().parent().hide();
                                }
                                
                                
                                //When enabling the product and category
                                jQuery('#rs_enable_redeem_for_selected_products').click(function(){
                                    var enable_redeem_for_selected_product=jQuery('#rs_enable_redeem_for_selected_products').is(':checked')?'yes':'no';
                                    if(enable_redeem_for_selected_product=='yes'){
                                        jQuery('#rs_select_products_to_enable_redeeming').parent().parent().show();
                                    }else{
                                        jQuery('#rs_select_products_to_enable_redeeming').parent().parent().hide();
                                    }
                                });
                                jQuery('#rs_exclude_products_for_redeeming').click(function(){
                                    var enable_exclude_product_checkbox = jQuery('#rs_exclude_products_for_redeeming').is(':checked') ? 'yes' : 'no';
                                    if(enable_exclude_product_checkbox=='yes'){
                                        jQuery('#rs_exclude_products_to_enable_redeeming').parent().parent().show();
                                    }else{
                                        jQuery('#rs_exclude_products_to_enable_redeeming').parent().parent().hide();
                                    }
                                });
                                jQuery('#rs_enable_redeem_for_selected_category').click(function(){
                                    var enable_selected_category_checkbox = jQuery('#rs_enable_redeem_for_selected_category').is(':checked') ? 'yes' : 'no';
                                    if(enable_selected_category_checkbox=='yes'){
                                        jQuery('#rs_select_category_to_enable_redeeming').parent().parent().show();
                                    }else{
                                        jQuery('#rs_select_category_to_enable_redeeming').parent().parent().hide();
                                    }
                                });
                                jQuery('#rs_exclude_category_for_redeeming').click(function(){
                                    var enable_exclude_category_checkbox = jQuery('#rs_exclude_category_for_redeeming').is(':checked') ? 'yes' : 'no';             
                                    if(enable_exclude_category_checkbox=='yes'){
                                        jQuery('#rs_exclude_category_to_enable_redeeming').parent().parent().show();
                                    }else{
                                        jQuery('#rs_exclude_category_to_enable_redeeming').parent().parent().hide();
                                    }
                                });
                            }else{
                                jQuery('#rs_enable_redeem_for_selected_products').parent().parent().parent().parent().hide();
                                jQuery('#rs_exclude_products_for_redeeming').parent().parent().parent().parent().hide();
                                jQuery('#rs_enable_redeem_for_selected_category').parent().parent().parent().parent().hide();
                                jQuery('#rs_exclude_category_for_redeeming').parent().parent().parent().parent().hide();
                                jQuery('#rs_select_products_to_enable_redeeming').parent().parent().hide();
                                jQuery('#rs_exclude_products_to_enable_redeeming').parent().parent().hide();
                                jQuery('#rs_select_category_to_enable_redeeming').parent().parent().hide();
                                jQuery('#rs_exclude_category_to_enable_redeeming').parent().parent().hide();
                            }
                            
                        jQuery('#rs_show_hide_redeem_field').change(function(){
                            var currentvalue=jQuery(this).val();
                            if(currentvalue==='1'){
                                jQuery('#rs_enable_redeem_for_selected_products').parent().parent().parent().parent().show();
                                jQuery('#rs_exclude_products_for_redeeming').parent().parent().parent().parent().show();
                                jQuery('#rs_enable_redeem_for_selected_category').parent().parent().parent().parent().show();
                                jQuery('#rs_exclude_category_for_redeeming').parent().parent().parent().parent().show();
                                var enable_selected_product_checkbox =jQuery('#rs_enable_redeem_for_selected_products').is(':checked')?'yes':'no';
                                var enable_exclude_product_checkbox = jQuery('#rs_exclude_products_for_redeeming').is(':checked') ? 'yes' : 'no';
                                var enable_selected_category_checkbox = jQuery('#rs_enable_redeem_for_selected_category').is(':checked') ? 'yes' : 'no';
                                var enable_exclude_category_checkbox = jQuery('#rs_exclude_category_for_redeeming').is(':checked') ? 'yes' : 'no';
                                if(enable_selected_product_checkbox==='yes'){
                                    jQuery('#rs_select_products_to_enable_redeeming').parent().parent().show();
                                }else{
                                    jQuery('#rs_select_products_to_enable_redeeming').parent().parent().hide();
                                }
                                if(enable_exclude_product_checkbox==='yes'){
                                    jQuery('#rs_exclude_products_to_enable_redeeming').parent().parent().show();
                                }else{
                                    jQuery('#rs_exclude_products_to_enable_redeeming').parent().parent().hide();
                                }
                                if(enable_selected_category_checkbox==='yes'){
                                    jQuery('#rs_select_category_to_enable_redeeming').parent().parent().show();
                                }else{
                                    jQuery('#rs_select_category_to_enable_redeeming').parent().parent().hide();
                                }
                                if(enable_exclude_category_checkbox==='yes'){
                                    jQuery('#rs_exclude_category_to_enable_redeeming').parent().parent().show();
                                }else{
                                    jQuery('#rs_exclude_category_to_enable_redeeming').parent().parent().hide();
                                }
                                
                                
                                //When enabling the product and category
                                jQuery('#rs_enable_redeem_for_selected_products').click(function(){
                                    var enable_redeem_for_selected_product=jQuery('#rs_enable_redeem_for_selected_products').is(':checked')?'yes':'no';
                                    if(enable_redeem_for_selected_product=='yes'){
                                        jQuery('#rs_select_products_to_enable_redeeming').parent().parent().show();
                                    }else{
                                        jQuery('#rs_select_products_to_enable_redeeming').parent().parent().hide();
                                    }
                                });
                                jQuery('#rs_exclude_products_for_redeeming').click(function(){
                                    var enable_exclude_product_checkbox = jQuery('#rs_exclude_products_for_redeeming').is(':checked') ? 'yes' : 'no';
                                    if(enable_exclude_product_checkbox=='yes'){
                                        jQuery('#rs_exclude_products_to_enable_redeeming').parent().parent().show();
                                    }else{
                                        jQuery('#rs_exclude_products_to_enable_redeeming').parent().parent().hide();
                                    }
                                });
                                jQuery('#rs_enable_redeem_for_selected_category').click(function(){
                                    var enable_selected_category_checkbox = jQuery('#rs_enable_redeem_for_selected_category').is(':checked') ? 'yes' : 'no';
                                    if(enable_selected_category_checkbox=='yes'){
                                        jQuery('#rs_select_category_to_enable_redeeming').parent().parent().show();
                                    }else{
                                        jQuery('#rs_select_category_to_enable_redeeming').parent().parent().hide();
                                    }
                                });
                                jQuery('#rs_exclude_category_for_redeeming').click(function(){
                                    var enable_exclude_category_checkbox = jQuery('#rs_exclude_category_for_redeeming').is(':checked') ? 'yes' : 'no';             
                                    if(enable_exclude_category_checkbox=='yes'){
                                        jQuery('#rs_exclude_category_to_enable_redeeming').parent().parent().show();
                                    }else{
                                        jQuery('#rs_exclude_category_to_enable_redeeming').parent().parent().hide();
                                    }
                                });
                            }else{
                                jQuery('#rs_enable_redeem_for_selected_products').parent().parent().parent().parent().hide();
                                jQuery('#rs_exclude_products_for_redeeming').parent().parent().parent().parent().hide();
                                jQuery('#rs_enable_redeem_for_selected_category').parent().parent().parent().parent().hide();
                                jQuery('#rs_exclude_category_for_redeeming').parent().parent().parent().parent().hide();
                                jQuery('#rs_select_products_to_enable_redeeming').parent().parent().hide();
                                jQuery('#rs_exclude_products_to_enable_redeeming').parent().parent().hide();
                                jQuery('#rs_select_category_to_enable_redeeming').parent().parent().hide();
                                jQuery('#rs_exclude_category_to_enable_redeeming').parent().parent().hide();
                            }
                        });
                    });
                </script>
                <?php
            }
        }
    }

    public static function display_redeem_points_buttons_cart_page() {
        global $woocommerce;
        $userid = get_current_user_id();
        $banning_type = FPRewardSystem::check_banning_type($userid);
        if ($banning_type != 'redeemingonly' && $banning_type != 'both') {
            $getuserid = get_current_user_id();
            $user_current_points = get_user_meta($getuserid, '_my_reward_points', true);
            if (get_user_meta($getuserid, '_my_reward_points', true) > 0) {
                if (get_user_meta($getuserid, 'rsfirsttime_redeemed', true) != '1') {
                    if (get_user_meta($getuserid, '_my_reward_points', true) >= get_option("rs_first_time_minimum_user_points")) {
                        if (get_option('rs_show_hide_redeem_field') == '1') {
                            if (get_option('rs_redeem_field_type_option') == '2') {
                                $getuserid = get_current_user_id();
                                $user_current_points = get_user_meta($getuserid, '_my_reward_points', true);
                                $current_carttotal_amount = $woocommerce->cart->total;
                                $redeem_conversion = get_option('rs_redeem_point');
                                $current_carttotal_in_points = $current_carttotal_amount * $redeem_conversion;
                                $limitation_percentage_for_redeeming = get_option('rs_percentage_cart_total_redeem');
                                $updated_points_step1 = $current_carttotal_in_points / 100;
                                $updated_points_for_redeeming = $updated_points_step1 * $limitation_percentage_for_redeeming;
                                //var_dump($updated_points_for_redeeming);
                                $cartpoints_string_to_replace = "[cartredeempoints]";
                                $currency_symbol_string_to_find = "[currencysymbol]";
                                $cuurency_value_string_to_find = "[pointsvalue]";
                                if ($user_current_points >= $updated_points_for_redeeming) {
                                    $redeem_button_message_more = get_option('rs_redeeming_button_option_message');
                                    $percentage_string_to_replace = "[redeempercent]";
                                    $cuurency_value_string_to_find = "[pointsvalue]";
                                    $points_conversion_value = get_option('rs_redeem_point_value');
                                    $points_currency_value = $updated_points_for_redeeming / $redeem_conversion;
                                    $points_currency_amount_to_replace = $points_currency_value * $points_conversion_value;
                                    $points_for_redeeming = $updated_points_for_redeeming;
                                    $redeem_button_message_more = get_option('rs_redeeming_button_option_message');
                                    $currency_symbol_string_to_replace = get_woocommerce_currency_symbol();
                                    $redeem_button_message_replaced_first = str_replace($cuurency_value_string_to_find, $points_currency_amount_to_replace, $redeem_button_message_more);
                                    $redeem_button_message_replaced_second = str_replace($currency_symbol_string_to_find, $currency_symbol_string_to_replace, $redeem_button_message_replaced_first);
                                    $redeem_button_message_replaced_third = str_replace($cartpoints_string_to_replace, $points_for_redeeming, $redeem_button_message_replaced_second);
                                } else {
                                    $points_for_redeeming = $user_current_points;
                                    $redeem_button_message_more = get_option('rs_redeeming_button_option_message');
                                    $points_conversion_value = get_option('rs_redeem_point_value');
                                    $points_currency_value = $points_for_redeeming / $redeem_conversion;
                                    $currency_symbol_string_to_replace = get_woocommerce_currency_symbol();
                                    $points_currency_amount_to_replace = $points_currency_value * $points_conversion_value;
                                    $redeem_button_message_replaced_first = str_replace($currency_symbol_string_to_find, $currency_symbol_string_to_replace, $redeem_button_message_more);
                                    $redeem_button_message_replaced_second = str_replace($cuurency_value_string_to_find, $points_currency_value, $redeem_button_message_replaced_first);
                                    $redeem_button_message_replaced_third = str_replace($cartpoints_string_to_replace, $points_for_redeeming, $redeem_button_message_replaced_second);
                                }
                                $minimum_cart_total_redeem = get_option('rs_minimum_cart_total_points');
                                $cart_subtotal_for_redeem = $woocommerce->cart->get_cart_subtotal();
                                $cart_subtotal_redeem_amount = preg_replace('/[^0-9\.]+/', '', $cart_subtotal_for_redeem);
                                if ($cart_subtotal_redeem_amount >= $minimum_cart_total_redeem) {
                                    ?>
                                    <form method="post">
                                        <div class="woocommerce-info"><?php echo $redeem_button_message_replaced_third; ?><input id="rs_apply_coupon_code_field" class="input-text" type="hidden" placeholder="<?php echo $placeholder; ?>" value="<?php echo $points_for_redeeming; ?> " name="rs_apply_coupon_code_field"><input class="button <?php echo get_option('rs_extra_class_name_apply_reward_points'); ?>" type="submit" id='mainsubmi' value="<?php echo get_option('rs_redeem_field_submit_button_caption'); ?>" name="rs_apply_coupon_code">
                                        </div>
                                    </form>
                                    <?php
                                }
                            }
                        }
                    }
                } else {
                    if (get_user_meta($getuserid, '_my_reward_points', true) >= get_option("rs_first_time_minimum_user_points")) {
                        if (get_option('rs_show_hide_redeem_field') == '1') {
                            if (get_option('rs_redeem_field_type_option') == '2') {
                                $getuserid = get_current_user_id();
                                $user_current_points = get_user_meta($getuserid, '_my_reward_points', true);
                                $current_carttotal_amount = $woocommerce->cart->total;
                                $redeem_conversion = get_option('rs_redeem_point');
                                $current_carttotal_in_points = $current_carttotal_amount * $redeem_conversion;
                                $limitation_percentage_for_redeeming = get_option('rs_percentage_cart_total_redeem');
                                $updated_points_step1 = $current_carttotal_in_points / 100;
                                $updated_points_for_redeeming = $updated_points_step1 * $limitation_percentage_for_redeeming;
                                //var_dump($updated_points_for_redeeming);
                                $cartpoints_string_to_replace = "[cartredeempoints]";
                                $currency_symbol_string_to_find = "[currencysymbol]";
                                $cuurency_value_string_to_find = "[pointsvalue]";
                                if ($user_current_points >= $updated_points_for_redeeming) {
                                    $redeem_button_message_more = get_option('rs_redeeming_button_option_message');
                                    //$percentage_string_to_replace = "[redeempercent]";
                                    $cuurency_value_string_to_find = "[pointsvalue]";
                                    $points_conversion_value = get_option('rs_redeem_point_value');
                                    $points_currency_value = $updated_points_for_redeeming / $redeem_conversion;
                                    $points_currency_amount_to_replace = $points_currency_value * $points_conversion_value;
                                    $points_for_redeeming = $updated_points_for_redeeming;
                                    $redeem_button_message_more = get_option('rs_redeeming_button_option_message');
                                    $currency_symbol_string_to_replace = get_woocommerce_currency_symbol();

                                    $redeem_button_message_replaced_first = str_replace($cuurency_value_string_to_find, $points_currency_amount_to_replace, $redeem_button_message_more);
                                    $redeem_button_message_replaced_second = str_replace($currency_symbol_string_to_find, $currency_symbol_string_to_replace, $redeem_button_message_replaced_first);
                                    $redeem_button_message_replaced_third = str_replace($cartpoints_string_to_replace, $points_for_redeeming, $redeem_button_message_replaced_second);
                                } else {
                                    $points_for_redeeming = $user_current_points;
                                    $redeem_button_message_more = get_option('rs_redeeming_button_option_message_checkout');
                                    $points_conversion_value = get_option('rs_redeem_point_value');
                                    $points_currency_value = $points_for_redeeming / $redeem_conversion;
                                    $currency_symbol_string_to_replace = get_woocommerce_currency_symbol();
                                    $points_currency_amount_to_replace = $points_currency_value * $points_conversion_value;
                                    $redeem_button_message_replaced_first = str_replace($currency_symbol_string_to_find, $currency_symbol_string_to_replace, $redeem_button_message_more);
                                    $redeem_button_message_replaced_second = str_replace($cuurency_value_string_to_find, $points_currency_amount_to_replace, $redeem_button_message_replaced_first);
                                    $redeem_button_message_replaced_third = str_replace($cartpoints_string_to_replace, $points_for_redeeming, $redeem_button_message_replaced_second);
                                }
                                $minimum_cart_total_redeem = get_option('rs_minimum_cart_total_points');
                                $cart_subtotal_for_redeem = $woocommerce->cart->get_cart_subtotal();
                                $cart_subtotal_redeem_amount = preg_replace('/[^0-9\.]+/', '', $cart_subtotal_for_redeem);
                                if ($cart_subtotal_redeem_amount >= $minimum_cart_total_redeem) {
                                    ?>
                                    <form method="post">
                                        <div class="woocommerce-info"><?php echo $redeem_button_message_replaced_third; ?>
                                            <input id="rs_apply_coupon_code_field" class="input-text" type="hidden" placeholder="<?php echo $placeholder; ?>" value="<?php echo $points_for_redeeming; ?> " name="rs_apply_coupon_code_field">                                            <input class="button <?php echo get_option('rs_extra_class_name_apply_reward_points'); ?>" type="submit" id='mainsubmi' value="<?php echo get_option('rs_redeem_field_submit_button_caption'); ?>" name="rs_apply_coupon_code">
                                        </div>
                                    </form>
                                    <?php
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}

new FPRewardSystemCart();


/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemCart', 'reward_system_tab_settings'), 106);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_cart', array('FPRewardSystemCart', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemCart', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_cart', array('FPRewardSystemCart', 'reward_system_register_admin_settings'));


add_action('admin_head', array('FPRewardSystemCart', 'apply_redeem_points_button_type_settings'));

add_action('admin_head', array('FPRewardSystemCart', 'rs_redeeming_selected_products_categories'));

add_action('woocommerce_before_cart', array('FPRewardSystemCart', 'display_redeem_points_buttons_cart_page'));

add_action('woocommerce_update_option_exclude_product_selection', array('FPRewardSystemCart', 'save_product_selection_backward_compatibility'));
add_action('woocommerce_update_option_include_product_selection', array('FPRewardSystemCart', 'save_product_selection_normal_compatibility'));

add_action('woocommerce_admin_field_exclude_product_selection', array('FPRewardSystemCart', 'add_product_selection_backward_compatibility'));
add_action('woocommerce_admin_field_include_product_selection', array('FPRewardSystemCart', 'add_product_selection_normal_compatibility'));
?>