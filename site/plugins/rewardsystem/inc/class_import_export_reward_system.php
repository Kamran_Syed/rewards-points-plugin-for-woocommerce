<?php

class FPRewardSystemImportExportTab {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_import_export_csv'] = __('Import/Export Points in CSV', 'rewardsystem');
        return $settings_tabs;
    }

// Add Admin Fields in the Array Format
    /**
     *
     */
    public static function export_points_selection() {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                var selected_option = jQuery('input[name="rs_export_import_user_option"]').val();
                var data = {
                    action: "rs_export_option",
                    exporttype: selected_option,
                };
                jQuery.post('<?php echo admin_url('admin-ajax.php') ?>', data, function (response) {
                    console.log('Got this from the server: ' + response);
                });
                jQuery('input[name="rs_export_import_user_option"]').change(function () {
                    var selected_option = jQuery(this).val();
                    var data = {
                        action: "rs_export_option",
                        exporttype: selected_option,
                    };
                    jQuery.post('<?php echo admin_url('admin-ajax.php') ?>', data, function (response) {
                        console.log('Got this from the server: ' + response);
                    });
                });
            });
            jQuery(document).ready(function () {
                var selected_users_for_export;
                selected_users_for_export = jQuery('#rs_import_export_users_list').val();

                var selected_users_data = {
                    action: "rs_list_of_users_to_export",
                    exportlist: selected_users_for_export
                };
                jQuery.post('<?php echo admin_url('admin-ajax.php') ?>', selected_users_data, function (response) {
                    console.log('Got this from the server: ' + response);
                });
                jQuery('#rs_import_export_users_list').change(function () {
                    selected_users_for_export = jQuery('#rs_import_export_users_list').val();

                    var selected_users_data = {
                        action: "rs_list_of_users_to_export",
                        exportlist: selected_users_for_export
                    };
                    jQuery.post('<?php echo admin_url('admin-ajax.php') ?>', selected_users_data, function (response) {
                        console.log('Got this from the server: ' + response);
                    });
                });
            });
            jQuery(document).ready(function () {
                var selected_option_date = jQuery('input[name="rs_export_import_date_option"]').val();
                var selected_date_option_param = {
                    action: "rs_selected_date_option",
                    dateoption: selected_option_date,
                };
                jQuery.post('<?php echo admin_url('admin-ajax.php') ?>', selected_date_option_param, function (response) {
                    console.log('Got this from the server: ' + response);
                });
                jQuery('input[name="rs_export_import_date_option"]').change(function () {
                    var selected_option_date = jQuery(this).val();
                    var selected_date_option_param = {
                        action: "rs_selected_date_option",
                        dateoption: selected_option_date,
                    };
                    jQuery.post('<?php echo admin_url('admin-ajax.php') ?>', selected_date_option_param, function (response) {
                        console.log('Got this from the server: ' + response);
                    });
                });
            });
            jQuery(document).ready(function(){
            //alert("Goole");
                var selected_format=jQuery('input[name="rs_csv_format"]').val();
                var selected_format ={
                  action:"rs_select_csv_format",
                  exportformat:selected_format,
                };
                jQuery.post('<?php echo admin_url('admin-ajax.php') ?>', selected_format, function (response) {
                    
                    console.log('Got this from the server: ' + response);
                });
                jQuery(document).on('change','.rs_csv_format',function(){
                    var selected_format=jQuery(this).val();
                   //alert(jQuery(this).val());
                    var selected_format ={
                        action:"rs_select_csv_format",
                        exportformat:selected_format,
                };
                jQuery.post('<?php echo admin_url('admin-ajax.php') ?>', selected_format, function (response) {
                    //alert("goo");
                    console.log('Got this from the server: ' + response);
                });
                });
                
            });
        </script>
        <?php
    }

    public static function export_option_selected_callback() {
        global $wpdb; // this is how you get access to the database
        if (isset($_POST['exporttype'])) {
            $export_user_type_value = $_POST['exporttype'];
            update_option('selected_user_type', $export_user_type_value);
        }
        exit();
    }

    public static function selected_users_for_exporting_csv_callback() {
        global $wpdb; // this is how you get access to the database
        if (isset($_POST['exportlist'])) {
            $rs_selected_list_export = $_POST['exportlist'];
            update_option('rs_selected_user_list_export', $rs_selected_list_export);
        }
    }
    
    public static function select_csv_format(){
         global $wpdb;
         if(isset($_POST['exportformat'])){
             $select_export_format=$_POST['exportformat'];
             //delete_option('selected_format');
             update_option('selected_format',$select_export_format);
         }
         exit();
    }

    public static function export_option_selected_date_callback() {
        global $wpdb; // this is how you get access to the database
        if (isset($_POST['dateoption'])) {
            $export_selected_date_option = $_POST['dateoption'];
            delete_option('selected_start_date');
            delete_option('selected_end_date');
            update_option('selected_date_type', $export_selected_date_option);
        }
        exit();
    }

    public static function export_start_date_callback() {
        global $wpdb; // this is how you get access to the database
        if (isset($_POST['export_startdate'])) {
            $export_start_date = $_POST['export_startdate'];
            delete_option('selected_start_date');

            update_option('selected_start_date', $export_start_date);
        }
//        if(isset($_POST['export_enddate'])) {
//        $export_end_date = $_POST['export_enddate'];
//	update_option('selected_end_date',$export_end_date);
//        }
        exit();
    }

    public static function export_end_date_callback() {
        global $wpdb; // this is how you get access to the database
        if (isset($_POST['export_enddate'])) {
            $export_end_date = $_POST['export_enddate'];
            delete_option('selected_end_date');
            update_option('selected_end_date', $export_end_date);
        }
        exit();
    }

    public static function export_points_for_selected_time() {
//    $test_get_all_users = get_users();
//    foreach ($test_get_all_users as $test_user_value){
//        $test_users_id = $test_user_value -> ID;
//      $total_points = get_user_meta($test_users_id, '_my_points_log', true);
//
//
////    $user_ID = get_current_user_id();
////    $total_points = get_user_meta($user_ID, '_my_points_log', true);
////    var_dump($total_points);
//
//        //var_dump($total_points);
//            foreach ($total_points as $new_total_points){
//
//                if(is_array($new_total_points)){
//                    $sample_date = $new_total_points['date'];
//                    $sample_points = $new_total_points['totalpoints'];
//                    //var_dump($sample_date);
//                    //var_dump($sample_points);
//                    $testConverted_time = strtotime($sample_date);
//                    //var_dump($testConverted_time);
//                    $testing_selected_start_date = get_option('selected_start_date');
//                    $testing_selected_start_time = '00:00:00';
//                    $testing_selected_start_date_time = $testing_selected_start_date . ' '. $testing_selected_start_time;
//                    //var_dump($testing_selected_start_date_time);
//                    $converted_testing_start_time = strtotime($testing_selected_start_date_time);
//                    //var_dump($converted_testing_start_time);
//                    $testing_selected_end_date = get_option('selected_end_date');
//                    $testing_selected_end_time = '23:59:00';
//                    $testing_selected_end_date_time = $testing_selected_end_date . ' '. $testing_selected_end_time;
//                    //var_dump($testing_selected_end_date_time);
//                    $converted_testing_end_time = strtotime($testing_selected_end_date_time);
//                    //var_dump($converted_testing_end_time);
//                    if($converted_testing_start_time <= $testConverted_time && $converted_testing_end_time >= $testConverted_time){
//                        $updated_test_points[] = $sample_points;
//                        //var_dump($updated_test_points);
//                        $updated_total_test_points = end($updated_test_points);
//
//                    }
//                }
//
//            }
//                $arraylist[] = array($test_user_value->user_login, $updated_total_test_points);
//            }
//            return $arraylist;
    }

    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        $user_list = get_users();
        foreach ($user_list as $user) {
            $separate_user_importexport[] = $user->display_name;
            $seperate_userid_importexport[] = $user->ID;
        }


        $newcombineddatas_importexport = array_combine((array) $seperate_userid_importexport, (array) $separate_user_importexport);
        return apply_filters('woocommerce_rewardsystem_import_export_csv_settings', array(
            array(
                'name' => __('Import/Export User Points in CSV Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Here you can Import/Export Users and their Points in CSV', 'rewardsystem'),
                'id' => '_rs_reward_system_import_export_csv'
            ),
            array(
                'name' => __('Export User Points for', 'rewardsystem'),
                'desc' => __('Here you can set whether to Export Reward Points for All Users or Selected Users', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_export_import_user_option',
                'css' => '',
                'std' => '1',
                'type' => 'radio',
                'options' => array('1' => 'All Users', '2' => 'Selected Users'),
                'newids' => 'rs_export_import_user_option',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Select the users that you wish to Export Reward Points', 'rewardsystem'),
                'desc' => __('Here you select the users to whom you wish to Export Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_import_export_users_list',
                'css' => 'min-width:400px;',
                'std' => '',
                'type' => 'multiselect',
                'options' => $newcombineddatas_importexport,
                'newids' => 'rs_import_export_users_list',
                'desc_tip' => true,
            ),
            array(
                'name'=>__('CSV Format','rewardsystem'),
                'desc'=>__('Here you can set whether to Export CSV Format with Username or Userid or Emailid','rewardsystem'),
                'tip'=>'',
                'id'=>'rs_csv_format',
                'class'=>'rs_csv_format',
                'newids'=>'rs_csv_format',
                'std'=>'1',
                'type'=>'radio',
                'options'=>array('1'=>'Username/Points','2'=>'Email-Id/Points'),
                'desc_tip'=>true,
            ),
            array(
                'name' => __('Export User Points from', 'rewardsystem'),
                'desc' => __('Here you can set whether to Export Reward Points for All Time or Selected Date', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_export_import_date_option',
                'class' => 'rs_export_import_date_option',
                'css' => '',
                'std' => '1',
                'type' => 'radio',
                'options' => array('1' => 'All Time', '2' => 'Selected Date'),
                'newids' => 'rs_export_import_date_option',
                'desc_tip' => true,
            ),
            array(
                'type' => 'import_export',
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_system_import_export_csv'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemImportExportTab::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemImportExportTab::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemImportExportTab::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

    public static function outputCSV($data) {
        $output = fopen("php://output", "w");
// var_dump($data);
        foreach ($data as $row) {
            fputcsv($output, $row); // here you can change delimiter/enclosure
        }
        fclose($output);
    }

    public static function inputCSV($data_path) {
        $row = 1;
        if (($handle = fopen($data_path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
//            $num = count($data);
                $row++;
                $collection[] = array_filter(array($data[0], $data[1]));
            }
            update_option('rewardsystem_csv_array', array_merge(array_filter($collection)));
            fclose($handle);
        }
    }

    public static function import_export_user_selection() {
        global $woocommerce;
        if (isset($_GET['page'])) {
            if ($_GET['page'] == 'rewardsystem_callback') {
                if ((float) $woocommerce->version <= (float) ('2.2.0')) {
                    ?>
                    <script type="text/javascript">
                        jQuery(document).ready(function () {
                            jQuery('#rs_import_export_users_list').chosen();
                        });
                    </script>
                    <?php
                } else {
                    ?>
                    <script type="text/javascript">
                        jQuery(document).ready(function () {
                            jQuery('#rs_import_export_users_list').select2();
                        });
                    </script>
                    <?php
                }
            }
        }
        ?>

        <script type="text/javascript">
            jQuery(document).ready(function () {
                if ((jQuery('input[name=rs_export_import_user_option]:checked').val()) === '2') {
                    jQuery('#rs_import_export_users_list').parent().parent().show();
                } else {
                    jQuery('#rs_import_export_users_list').parent().parent().hide();
                }
                jQuery('input[name=rs_export_import_user_option]:radio').change(function () {
                    jQuery('#rs_import_export_users_list').parent().parent().toggle();
                });

            });
            jQuery(document).ready(function () {
                // alert(jQuery('input[name=rs_export_import_date_option]:checked').val());
                if ((jQuery('input[name=rs_export_import_date_option]:checked').val()) === '2') {
                    jQuery('#rs_point_export_start_date').parent().parent().show();
                    jQuery('#rs_point_export_end_date').parent().parent().show();
                } else {
                    jQuery('#rs_point_export_start_date').parent().parent().hide();
                    jQuery('#rs_point_export_end_date').parent().parent().hide();
                }
                jQuery('input[name=rs_export_import_date_option]:radio').change(function () {
                    jQuery('#rs_point_export_start_date').parent().parent().toggle();
                    jQuery('#rs_point_export_end_date').parent().parent().toggle();
                });

            });
        </script>
        <?php
    }

    function wps_wp_admin_area_notice() {
        echo ' <div class="error">
                 <p>We are performing website Maintenance. Please dont do any activity until further notice!</p>
          </div>';
    }

    public static function reward_system_page_customization() {
        $export_type_selection = get_option('selected_user_type');
        $export_date_selection = get_option('selected_date_type');
        $export_csvformat_selection = get_option('selected_format');
        if ($export_type_selection == '1') {
            if ($export_date_selection == '1') {
                foreach (get_users() as $users) {
                    $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';                                
                    $current_points = get_user_meta($users->ID, '_my_reward_points', true);
                    if ($export_csvformat_selection == '1') {
                        $arraylist[] = array($users->user_login, round($current_points,$roundofftype));
                    }else{
                        $arraylist[] = array($users->user_email, round($current_points,$roundofftype));
                    }
                }
            } else {
                $get_all_users = get_users();
                foreach ($get_all_users as $user_value) {
                    $users_id = $user_value->ID;
                    $date_from_log = "";
                    $points_from_log = "";
                    $updated_total_points = "";
                    $total_points = get_user_meta($users_id, '_my_points_log', true);
                    if ((get_option('selected_start_date') != NULL) && (get_option('selected_end_date') != NULL)) {
                        if (is_array($total_points)) {
                            foreach ($total_points as $new_total_points) {
                                if (isset($new_total_points['date'])) {
                                    $date_from_log = $new_total_points['date'];
//var_dump($date_from_log);
                                }
                                if (isset($new_total_points['totalpoints'])) {
                                    $points_from_log = $new_total_points['totalpoints'];
//var_dump($new_total_points);
                                }
                                $converted_time = strtotime($date_from_log);
                                $selected_start_date = get_option('selected_start_date');
                                $selected_start_time = '00:00:00';
                                $selected_start_date_time = $selected_start_date . ' ' . $selected_start_time;
                                $converted_start_time = strtotime($selected_start_date_time);
//var_dump($converted_start_time);
                                $selected_end_date = get_option('selected_end_date');
                                $selected_end_time = '23:59:00';
                                $selected_end_date_time = $selected_end_date . ' ' . $selected_end_time;
                                $converted_end_time = strtotime($selected_end_date_time);
//var_dump($converted_end_time);

                                if ($converted_start_time <= $converted_time && $converted_end_time >= $converted_time) {
                                    $updated_points[] = $points_from_log;
                                    $updated_total_points = end($updated_points);
//var_dump($updated_total_points);
                                }
                            }
                            $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';                                                            
                            $points = round($updated_total_points,$roundofftype);
                            if ($export_csvformat_selection == '1') {
                                $arraylist[] = array($user_value->user_login, $points);
                            }else{
                                $arraylist[] = array($user_value->user_email, $points);
                            }
                        }
                    } else {
                        $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';                                                            
                        $points = round(get_user_meta($user_value->ID, '_my_reward_points', true),$roundofftype);
                        if ($export_csvformat_selection == '1') {
                            $arraylist[] = array($user_value->user_login, $points);
                        }else{
                            $arraylist[] = array($user_value->user_email, $points);
                        }
                    }
                }
            }
        } else {
            if ($export_date_selection == '1') {
                if (get_option('rs_selected_user_list_export') != NULL) {
                    $list_of_users_to_export_csv = get_option('rs_selected_user_list_export');
                    foreach (get_users() as $users) {
                        $user_id = $users->ID;
                        if (in_array($user_id, $list_of_users_to_export_csv)) {
                            $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';                                                            
                            $points = round(get_user_meta($users->ID, '_my_reward_points', true),$roundofftype);
                             if ($export_csvformat_selection == '1') {
                                $arraylist[] = array($users->user_login,$points);
                             }else{
                                $arraylist[] = array($users->user_email,$points);
                             }
                        }
                    }
                } else {
                    foreach (get_users() as $users) {
                        $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';                                                            
                        $points = round(get_user_meta($users->ID, '_my_reward_points', true),$roundofftype);
                        if ($export_csvformat_selection == '1') {
                            $arraylist[] = array($users->user_login, $points);
                        }else{
                            $arraylist[] = array($users->user_email, $points);
                        }    
                    }
                }
            } else {
                if (get_option('rs_selected_user_list_export') != NULL) {
                    $list_of_users_to_export_csv = get_option('rs_selected_user_list_export');
                    foreach (get_users() as $users) {
                        $user_id = $users->ID;
                        if (in_array($user_id, $list_of_users_to_export_csv)) {
                            $total_points = get_user_meta($user_id, '_my_points_log', true);
                            foreach ($total_points as $new_total_points) {
                                if (is_array($total_points)) {
                                    if (is_array($new_total_points)) {
                                        if (isset($new_total_points['date'])) {
                                            $date_from_log = $new_total_points['date'];
                                        }
                                        if (isset($new_total_points['totalpoints'])) {
                                            $points_from_log = $new_total_points['totalpoints'];
                                        }
//var_dump($date_from_log);
//var_dump($points_from_log);
                                        $converted_time = strtotime($date_from_log);
//var_dump($converted_time);
                                        $selected_start_date = get_option('selected_start_date');
                                        $selected_start_time = '00:00:00';
                                        $selected_start_date_time = $selected_start_date . ' ' . $selected_start_time;
//var_dump($selected_start_date_time);
                                        $converted_start_time = strtotime($selected_start_date_time);
//var_dump($converted_start_time);
                                        $selected_end_date = get_option('selected_end_date');
                                        $selected_end_time = '23:59:00';
                                        $selected_end_date_time = $selected_end_date . ' ' . $selected_end_time;
//var_dump($selected_end_date_time);
                                        $converted_end_time = strtotime($selected_end_date_time);
//var_dump($converted_end_time);
                                        if ($converted_start_time <= $converted_time && $converted_end_time >= $converted_time) {
                                            $updated_points[] = $points_from_log;
//var_dump($updated_points);
                                            $updated_total_points = end($updated_points);
                                        }
                                    }
                                }
                            }
                            $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';                                
                            $current_points = round($updated_total_points,$roundofftype);
                            if ($export_csvformat_selection == '1') {
                                $arraylist[] = array($users->user_login, $current_points);
                            }else{
                                $arraylist[] = array($users->user_email, $current_points);
                            }
                        }
                    }
                } else {
                    foreach (get_users() as $users) {
                        $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';                                
                        $current_points = round(get_user_meta($users->ID, '_my_reward_points', true),$roundofftype);
                        if ($export_csvformat_selection == '1') {
                            $arraylist[] = array($users->user_login, $current_points);
                        }  else {
                            $arraylist[] = array($users->user_email, $current_points);
                        }
                    }
                }
            }
        }






// var_dump(get_option('testing_csv_array'));
        if (isset($_POST['rs_import_user_points'])) {
//  echo "google";
//    var_dump($_FILES);
            if ($_FILES["file"]["error"] > 0) {
                echo "Error: " . $_FILES["file"]["error"] . "<br>";
            } else {
                $mimes = array('text/csv',
                    'text/plain',
                    'application/csv',
                    'text/comma-separated-values',
                    'application/excel',
                    'application/vnd.ms-excel',
                    'application/vnd.msexcel',
                    'text/anytext',
                    'application/octet-stream',
                    'application/txt');
                if (in_array($_FILES['file']['type'], $mimes)) {
// do something
                    FPRewardSystemImportExportTab::inputCSV($_FILES["file"]["tmp_name"]);
                } else {
                    ?>
                    <style type="text/css">
                        div.error {
                            display:block;
                        }
                    </style>
                    <?php
//  add_action('admin_notices', array('FPRewardSystemImportExportTab', 'wps_wp_admin_area_notice'));
                }
            }
            $myurl = get_permalink();
//  header("Location: $myurl");
        }
        if (isset($_POST['rs_export_user_points_csv'])) {
//var_dump($_POST);
            ob_end_clean();
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=reward_points_" . date("Y-m-d") . ".csv");
            header("Pragma: no-cache");
            header("Expires: 0");
            FPRewardSystemImportExportTab::outputCSV($arraylist);
            exit();
        }

//
//        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
//            /// do your magic
//            session_start();
//            $_SESSION['error'] = "Thanks for your message!";
//
//            // this should be the full URL per spec, but "/yourscript.php" will work
//            $myurl = get_permalink();
//
//            header("Location: $myurl");
//            header("HTTP/1.1 303 See Other");
//            die("redirecting");
//        }
//
//        if (isset($_SESSION['error'])) {
//            print "The result of your submission: " . $_SESSION['error'];
//            unset($_SESSION['error']);
//        }
        ?>


        <style type="text/css">
            p.submit {
                display:none;
            }
            #mainforms {
                display:none;
            }
        </style>
        <tr valign="top">
            <th class="titledesc" scope="row">
                <label for="rs_point_export_start_date"><?php _e('Start Date', 'rewardsystem'); ?></label>
            </th>
            <td class="forminp forminp-select">
                <input type="text" class="rs_point_export_start_date" value="" name="rs_point_export_start_date" id="rs_point_export_start_date" />
            </td>
        </tr>

        <tr valign="top">
            <th class="titledesc" scope="row">
                <label for="rs_point_export_end_date"><?php _e('End Date', 'rewardsystem'); ?></label>
            </th>
            <td class="forminp forminp-select">
                <input type="text" class="rs_point_export_end_date" value="" name="rs_point_export_end_date" id="rs_point_export_end_date" />
            </td>
        </tr>
        <tr valign ="top">
            <th class="titledesc" scope="row">
                <label for="rs_export_user_points_csv"><?php _e('Export User Points to CSV', 'rewardsystem'); ?></label>
            </th>
            <td class="forminp forminp-select">
                <input type="submit" id="rs_export_user_points_csv" name="rs_export_user_points_csv" value="Export User Points"/>
            </td>
        </tr>

        <tr valign="top">
            <th class="titledesc" scope="row">
                <label for="rs_import_user_points_csv"><?php _e('Import User Points to CSV', 'rewardsystem'); ?></label>
            </th>
            <td class="forminp forminp-select">
                <input type="file" id="rs_import_user_points_csv" name="file" />
                <input type="submit" id="rs_import_user_points" name="rs_import_user_points" value="Import"/>
            </td>

        </tr>

        <?php if (get_option('rewardsystem_csv_array') != '') { ?>
            <table class="wp-list-table widefat fixed posts">
                <thead>
                    <tr>
                        <th>
                            User Name
                        </th>
                        <th>
                            User Reward Points
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach (get_option('rewardsystem_csv_array')as $newcsv) {
                        ?>
                        <tr>
                            <td><?php echo $newcsv[0]; ?></td><td><?php echo isset($newcsv[1]) != '' ? $newcsv[1] : '0'; ?></td>
                        </tr>
                    <?php }
                    ?>
                </tbody>
            </table>

            <table>
                <tr valign="top">
                    <td>
                        <input type="submit" id="rs_new_action_reward_points" name="rs_new_action_reward_points" value="Override Existing User Points"/>
                    </td>
                    <td>
                        <input type="submit" id="rs_exist_action_reward_points" name="rs_exist_action_reward_points" value="Add Points with Already Earned Points"/>
                    </td>
                </tr>
            </table>
            <?php
        }

        if (isset($_POST['rs_new_action_reward_points'])) {
            $getvalues = get_option('rewardsystem_csv_array');
            if (is_array($getvalues)) {
                foreach ($getvalues as $newvalues) {
                    $datalogin = get_user_by('login', $newvalues[0]);
                    if (!empty($datalogin)) {
                        update_user_meta($datalogin->ID, '_my_reward_points', $newvalues[1]);
                    }
                }
            }
            delete_option('rewardsystem_csv_array');
            $redirect = add_query_arg(array('saved' => 'true'));
            wp_safe_redirect($redirect);
            exit();
        }

        if (isset($_POST['rs_exist_action_reward_points'])) {
            $getvalues = get_option('rewardsystem_csv_array');
            if (is_array($getvalues)) {
                foreach ($getvalues as $newvalues) {
                    $datalogin = get_user_by('login', $newvalues[0]);
                    if (!empty($datalogin)) {
// echo "Data Found <br>";
//echo $newvalues[0] . " has been found with user id " . $datalogin->ID . " has going to earn points of $newvalues[2]<br>";
                        $oldpoints = get_user_meta($datalogin->ID, '_my_reward_points', true);
                        $currentpoints = $oldpoints + $newvalues[1];
                        update_user_meta($datalogin->ID, '_my_reward_points', $currentpoints);
                    }
//var_dump($datalogin);
                }
            }
            delete_option('rewardsystem_csv_array');
            $myurl = get_permalink();
//  header("Location: $myurl");
            $redirect = add_query_arg(array('saved' => 'true'));
            wp_safe_redirect($redirect);
            exit();
        }
    }

    public static function select_custom_date() {
        if (isset($_GET['tab'])) {
            if ($_GET['tab'] == 'rewardsystem_import_export_csv') {
                ?>
                <script type="text/javascript">
                    jQuery(document).ready(function () {
                        jQuery('#rs_point_export_start_date').datepicker({dateFormat: 'yy-mm-dd'});
                        jQuery('#rs_point_export_end_date').datepicker({dateFormat: 'yy-mm-dd'});
                        jQuery('#rs_point_export_start_date').change(function () {
                            var export_start_date = jQuery('#rs_point_export_start_date').val();
                            //var export_end_date = jQuery('#rs_point_export_end_date').val();
                            var export_param_start_date = {
                                action: "rs_import_export_start_date",
                                export_startdate: export_start_date,
                                //export_enddate:export_end_date

                            };

                            jQuery.post('<?php echo admin_url('admin-ajax.php') ?>', export_param_start_date, function (response) {
                                console.log('Got this from the server: ' + response);
                            });
                        });
                        jQuery('#rs_point_export_end_date').change(function () {
                            var export_end_date = jQuery('#rs_point_export_end_date').val();
                            var export_param_end_date = {
                                action: "rs_import_export_end_date",
                                export_enddate: export_end_date,
                            };

                            jQuery.post('<?php echo admin_url('admin-ajax.php') ?>', export_param_end_date, function (response) {
                                console.log('Got this from the server: ' + response);
                            });
                        });
                    });
                </script>
                <?php
            }
        }
    }

    public static function datepickerenqueue(&$enqueuescript) {
        if ($_GET['tab'] == 'rewardsystem_import_export_csv') {
            wp_enqueue_script('wp_reward_jquery_ui');
            wp_enqueue_style('wp_reward_jquery_ui_css');
        }
    }

}

new FPRewardSystemImportExportTab();

add_action('enqueuescriptforadmin', array('FPRewardSystemImportExportTab', 'datepickerenqueue'));
add_action('woocommerce_admin_field_import_export', array('FPRewardSystemImportExportTab', 'reward_system_page_customization'));
/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

//add_action('admin_head', array('FPRewardSystemImportExportTab', 'reward_system_page_customization'));
//
// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemImportExportTab', 'reward_system_tab_settings'), 999);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_import_export_csv', array('FPRewardSystemImportExportTab', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemImportExportTab', 'reward_system_default_settings'));

add_action('admin_head', array('FPRewardSystemImportExportTab', 'import_export_user_selection'));
add_action('admin_footer', array('FPRewardSystemImportExportTab', 'select_custom_date'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_import_export_csv', array('FPRewardSystemImportExportTab', 'reward_system_register_admin_settings'));
if (isset($_GET['tab'])) {
    if ($_GET['tab'] == 'rewardsystem_import_export_csv') {
        add_action('admin_footer', array('FPRewardSystemImportExportTab', 'export_points_selection'));
    }
}
add_action('wp_ajax_rs_export_option', array('FPRewardSystemImportExportTab', 'export_option_selected_callback'));
add_action('wp_ajax_rs_list_of_users_to_export', array('FPRewardSystemImportExportTab', 'selected_users_for_exporting_csv_callback'));
add_action('wp_ajax_rs_select_csv_format',array('FPRewardSystemImportExportTab','select_csv_format'));
add_action('wp_ajax_rs_selected_date_option', array('FPRewardSystemImportExportTab', 'export_option_selected_date_callback'));
add_action('wp_ajax_rs_import_export_start_date', array('FPRewardSystemImportExportTab', 'export_start_date_callback'));
add_action('wp_ajax_rs_import_export_end_date', array('FPRewardSystemImportExportTab', 'export_end_date_callback'));
add_action('admin_head', array('FPRewardSystemImportExportTab', 'export_points_for_selected_time'));
?>