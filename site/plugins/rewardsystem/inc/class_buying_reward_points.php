<?php

class RSBuyingRewardPoints {
    /*
     * Call Construct Function
     */

    public function __construct() {

        add_action('woocommerce_product_options_pricing', array($this, 'add_admin_field_buying_reward_points'), 1);
        add_action('woocommerce_process_product_meta', array($this, 'save_admin_field_buying_reward_points'));
        add_action('rs_perform_action_for_order', array($this, 'award_points_after_buyied'));
    }

    /* Add Admin Field Buying Reward Points */

    public static function add_admin_field_buying_reward_points() {
        global $post;
        ?>
        <div class="options_group show_if_simple">
            <?php
            woocommerce_wp_select(array(
                'id' => '_rewardsystem_buying_reward_points',
                'class' => '_rewardsystem_buying_reward_points',
                'label' => __('Enable Buying of SUMO Reward Points', 'rewardsystem'),
                'options' => array(
                    '' => __('Choose Option', 'rewardsystem'),
                    'yes' => __('Enable', 'rewardsystem'),
                    'no' => __('Disable', 'rewardsystem'),
                )
            ));
            woocommerce_wp_text_input(
                    array(
                        'id' => '_rewardsystem_assign_buying_points',
                        'name' => '_rewardsystem_assign_buying_points',
                        'label' => __('Buy Reward Points', 'rewardsystem')
            ));
            ?>
        </div>
        <?php
    }

    /* Save Admin Field for Buying Reward Points */

    public static function save_admin_field_buying_reward_points($post_id) {
        $woocommerce_buying_reward_select = $_POST['_rewardsystem_buying_reward_points'];
        update_post_meta($post_id, '_rewardsystem_buying_reward_points', $woocommerce_buying_reward_select);
        $woocommerce_rewardpoints = $_POST['_rewardsystem_assign_buying_points'];
        update_post_meta($post_id, '_rewardsystem_assign_buying_points', $woocommerce_rewardpoints);
    }

    /* Display Reward Points in Single Product Page */

    public static function display_reward_points_for_buying_reward_points($price, $product) {
        return $price;
    }

    /* Add Points based on Order Status as you set it in Admin Side */

    public static function award_points_after_buyied($order_id) {
        $order = new WC_Order($order_id);
        foreach ($order->get_items() as $item) {
            $productobject = get_product($item['product_id']);
            if ($productobject->product_type == 'simple') {
                $checkbuyingpoints = get_post_meta($item['product_id'], '_rewardsystem_buying_reward_points', true);
                $getpointstobuy = get_post_meta($item['product_id'], '_rewardsystem_assign_buying_points', true);
                $getpointstobuy = $getpointstobuy * $item['qty'];
                $getpreviouspoints = get_user_meta($order->user_id, '_my_reward_points', true);

                $localizationmessage = get_option('_rs_localize_buying_reward_points_log');
                $find = '{rsbuyiedrewardpoints}';
                $replace = $getpointstobuy;
                $translatedmessage = str_replace($find, $replace, $localizationmessage);
                if ($checkbuyingpoints == 'yes') {
                    $newpoints = $getpreviouspoints + $getpointstobuy;
                    update_user_meta($order->user_id, '_my_reward_points', $newpoints);
                    $pointslogs_buying[] = array('orderid' => $order_id, 'userid' => $order->user_id, 'points_earned_order' => $getpointstobuy, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $newpoints, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => $translatedmessage, 'rewarder_for_frontend' => $translatedmessage);
                    $overalllogs_buying[] = array('userid' => $order->user_id, 'totalvalue' => $getpointstobuy, 'eventname' => $translatedmessage, 'date' => date('Y-m-d H:i:s'));
                    $getoveralllogs_buying = get_option('rsoveralllog');
                    $logmerges_buying = array_merge((array) $getoveralllogs_buying, $overalllogs_buying);
                    update_option('rsoveralllog', $logmerges_buying);
                    $getmypointss_buying = get_user_meta($order->user_id, '_my_points_log', true);
                    $mergeds_buying = array_merge((array) $getmypointss_buying, $pointslogs_buying);
                    update_user_meta($order->user_id, '_my_points_log', $mergeds_buying);
                }
            }
        }
    }

}

new RSBuyingRewardPoints();
