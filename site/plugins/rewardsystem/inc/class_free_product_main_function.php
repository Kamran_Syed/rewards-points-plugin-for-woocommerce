<?php

class FPRewardSystem_Free_Product {

    public function __construct() {
        add_action('wp_head', array($this, 'fp_main_function_add_to_cart'));
        add_action('woocommerce_checkout_update_order_meta', array($this, 'save_data_to_order'));
        //add_action('wp_head',array($this,'main_itemid_testing'));
        add_filter('woocommerce_order_item_name', array($this, 'show_item_id_after_checkout'), 10, 2);
        add_action('woocommerce_before_calculate_totals', array($this, 'alter_free_product_price'));
        add_filter('woocommerce_cart_item_name', array($this, 'show_message_next_to_free_product'), 10, 3);
        add_action('woocommerce_after_cart_table', array($this, 'display_free_product_after_cart_table'));
        add_action('wp_ajax_delete_meta_current_key', array('RSUserRoleRewardPoints', 'delete_saved_product_key_callback'));
        add_filter('woocommerce_cart_item_quantity', array($this, 'alter_quantity_in_free_product'), 10, 2);
    }

    public static function fp_get_free_product_level_id($total_earned_points) {
        if (!is_admin()) {
            if (is_user_logged_in()) {
                global $woocommerce;
                $each_member_level = RSUserRoleRewardPoints::multi_dimensional_sort(get_option('rewards_dynamic_rule'), 'rewardpoints');
                if ($each_member_level != "") {
                    foreach ($each_member_level as $key => $value) {
                        // $free_products_list = self::rs_get_products_in_dynamic_rule($each_member_level, 'rewardpoints', $total_earned_points);
                        $current_user_total_earned_points = $total_earned_points;
                        $current_level_earning_points_limit = $value["rewardpoints"];
                        if ($current_level_earning_points_limit >= $current_user_total_earned_points) {
                            //RSUserRoleRewardPoints::add_free_product_to_user_cart();
                            //var_dump($key);
                            return $key;
                        }
                    }
                }
            }
        }
    }

    public static function fp_get_corresponding_product($dynamicruleid, $total_earned_points) {
        global $woocommerce;

        $get_datas = get_option('rewards_dynamic_rule');
        if (isset($dynamicruleid)) {
            if ((float) $woocommerce->version <= (float) ('2.2.0')) {
                $free_products_list = $get_datas[$dynamicruleid]['product_list'];

                return $free_products_list;
            } else {
                $free_products_list = $get_datas[$dynamicruleid]['product_list']['0'];
                $free_products_list = explode(',', $free_products_list);
                return $free_products_list;
            }
        }
    }

    public static function fp_check_if_free_product_is_in_cart($productid, $dynamicruleid, $total_earned_points) {
        global $woocommerce;
        $productids = array();
        $soloproductids = array();
        foreach ($woocommerce->cart->cart_contents as $key => $values) {
            $getids = $values['variation_id'] != '' ? $values['variation_id'] : $values['product_id'];

            $productids[] = $getids;
            $getfreeproductids = FPRewardSystem_Free_Product::fp_get_corresponding_product($dynamicruleid, $total_earned_points);
            if (!in_array($getids, (array) $getfreeproductids)) {
                $soloproductids[] = $getids;
            }
        }

        if (in_array($productid, (array) $productids)) {
            if (count($soloproductids) > 1) {
                return "true"; //Found
            } else {
                // $woocommerce->cart->empty_cart();
                return "true";
            }
        } else {
            return "false"; //Not Found
        }
    }

    public static function fp_add_free_product_to_cart($productid) {
        global $woocommerce;
        $woocommerce->cart->add_to_cart($productid);
    }

    public static function fp_main_function_add_to_cart() {
        global $woocommerce;
        $total_earned_points = get_user_meta(get_current_user_id(), 'rs_user_total_earned_points', true);
        //var_dump($total_earned_points);
        $dynamicruleid = FPRewardSystem_Free_Product::fp_get_free_product_level_id($total_earned_points);
        //var_dump($dynamicruleid);
        $getcorrespondingproducts = FPRewardSystem_Free_Product::fp_get_corresponding_product($dynamicruleid, $total_earned_points);
        if (get_option('rs_enable_earned_level_based_reward_points') == 'yes') {
            if (!empty($getcorrespondingproducts)) {
                if (is_array($getcorrespondingproducts)) {
                    foreach ($getcorrespondingproducts as $eachproduct) {
                        $listofcartitemkeys[] = $woocommerce->cart->generate_cart_id($eachproduct);
                        $combinedproductids[] = $eachproduct;
                        $getcurrentcartids = $woocommerce->cart->generate_cart_id($eachproduct);

                        $cartremovedlist = get_user_meta(get_current_user_id(), 'listsetofids', true);
                        //var_dump($getcurrentcartids);


                        $found_or_not = FPRewardSystem_Free_Product::fp_check_if_free_product_is_in_cart($eachproduct, $dynamicruleid, $total_earned_points);
                        //var_dump($found_or_not);
                        if (!in_array($getcurrentcartids, (array) $cartremovedlist)) {
                            if ($found_or_not == 'false') {
                                $getcurrentcartids = $woocommerce->cart->generate_cart_id($eachproduct);
                                $cartremovedlist = get_user_meta(get_current_user_id(), 'listsetofids', true);
                                self::fp_add_free_product_to_cart($eachproduct);
                                WC()->session->set('setruleids', $dynamicruleid);
                                WC()->session->set('excludedummyids', $dynamicruleid);
                                WC()->session->set('dynamicruleproducts', $getcorrespondingproducts);
                            }
                        }
                    }
                    WC()->session->set('freeproductcartitemkeys', $listofcartitemkeys);
                }
            }
        }
    }

    public static function save_data_to_order($orderid) {
        global $woocommerce;
        $getsavedsession = WC()->session->get('setruleids');
        $listofcartitemkeys = WC()->session->get("freeproductcartitemkeys");
        $getfreeproductmsg = WC()->session->get('freeproductmsg');
        update_post_meta($orderid, 'listruleids', $getsavedsession);
        $getalldatas = get_option('rewards_dynamic_rule');
        update_post_meta($orderid, 'ruleidsdata', $getalldatas[$getsavedsession]);
        $order = new WC_Order($orderid);
        foreach ($order->get_items() as $item_id => $eachitem) {
            $productid = $eachitem['variation_id'] != '0' ? $eachitem['variation_id'] : $eachitem['product_id'];
            // $mainids[] = $productid;
            //update_option('mainids',$mainids);
            $getlistofproducts = self::fp_get_corresponding_product($getsavedsession, '');
            //update_option('correspon',$getlistofproducts);
            if (in_array($productid, (array) $getlistofproducts)) {
              if (get_option('rs_enable_earned_level_based_reward_points') == 'yes') {
                wc_add_order_item_meta($item_id, '_ruleidsdata', $getalldatas[$getsavedsession]);
                wc_add_order_item_meta($item_id, '_rsfreeproductmsg', $getfreeproductmsg);
               }
            }
        }
        WC()->session->__unset('setruleids');
        WC()->session->__unset('freeproductcartitemkeys');
        WC()->session->__unset('freeproductmsg');
    }

    public static function alter_free_product_price($cart_object) {
         if (get_option('rs_enable_earned_level_based_reward_points') == 'yes') {
        $getsessiondata = WC()->session->get('setruleids');
        (array) $getcorrespondingproducts = self::fp_get_corresponding_product($getsessiondata, '');
        foreach ($cart_object->cart_contents as $key => $value) {
            $productid = $value['variation_id'] != '' ? $value['variation_id'] : $value['product_id'];
            if (in_array($productid, (array) $getcorrespondingproducts)) {
                $value['data']->price = '0';
            }
        }
        
         }
    }

    public static function display_free_product_after_cart_table() {
        global $woocommerce;
        ?>
        <style type="text/css">
            .fp_rs_display_free_product h3 {
                display:none;
            }
        </style>
        <?php
        echo "<div class='fp_rs_display_free_product'>";
        echo "<h3>";
        echo get_option('rs_free_product_msg_caption');
        echo "</h3>";
        $getsessiondata = WC()->session->get('excludedummyids');
        //var_dump($getsessiondata);
        if ($getsessiondata != NULL) {
            (array) $getproductlists = self::fp_get_corresponding_product($getsessiondata, '');
            if (!empty($getproductlists)) {
                if (is_array($getproductlists)) {
                    foreach ($getproductlists as $eachproduct) {
                        $cartitemkey = $woocommerce->cart->generate_cart_id($eachproduct);
                        $listofdeletedkeys = get_user_meta(get_current_user_id(), 'listsetofids', true);
                        if (in_array($cartitemkey, (array)$listofdeletedkeys)) {
                            $getthetitle = get_the_title($eachproduct);
                            ?>
                            <style type="text/css">
                                .fp_rs_display_free_product h3 {
                                    display:block;
                                }
                            </style>
                            <a href="javascript:void(0)" class="add_removed_free_product_to_cart" data-cartkey="<?php echo $cartitemkey; ?>"><?php echo $getthetitle; ?></a><br/>
                            <?php
                        }
                    }
                }
            }
        }
        echo "</div>";
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery('.add_removed_free_product_to_cart').click(function () {
                    var removed_key = jQuery(this).attr('data-cartkey');
                    var current_user_id = '<?php echo get_current_user_id(); ?>';
                    var removed_key_param = {
                        action: "delete_meta_current_key",
                        key_to_remove: removed_key,
                        current_user_id: current_user_id,
                    };
                    jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", removed_key_param, function (response) {
                        if (response === '1') {
                            location.reload();
                        }
                        console.log('Success');
                    });
                    return false;
                });
                return false;
            });
        </script>
        <?php
    }

    public static function show_message_next_to_free_product($product_name, $cart_item, $cart_item_key) {


        //var_dump($cart_item['product_id']);
        $productid = $cart_item['variation_id'] != '' ? $cart_item['variation_id'] : $cart_item['product_id'];
        $sessiondata = WC()->session->get('setruleids');
        (array) $getproductfromrule = self::fp_get_corresponding_product($sessiondata, '');
        $total_earned_points = get_user_meta(get_current_user_id(), 'rs_user_total_earned_points', true);
        $free_product_message_to_find = "[current_level_points]";
        $free_product_message_to_replace = $total_earned_points;
        $free_product_message_replaced = str_replace($free_product_message_to_find, $free_product_message_to_replace, get_option('rs_free_product_message_info'));
        if (in_array($productid, (array) $getproductfromrule)) {
            if (get_option('rs_remove_msg_from_cart_order') == 'yes') {
                 if (get_option('rs_enable_earned_level_based_reward_points') == 'yes') {
                WC()->session->set('freeproductmsg', $free_product_message_replaced);
                return $product_name . "<br>" . $free_product_message_replaced;
                 }else {
                     return $product_name;
                 }
            } else {
                return $product_name;
            }
        } else {
            return $product_name;
        }
    }

    public static function alter_quantity_in_free_product($productquantity, $values) {
        (array) $getcartitemkeys = WC()->session->get('freeproductcartitemkeys')==NULL?array():WC()->session->get('freeproductcartitemkeys');
        if(is_array($getcartitemkeys)){
            if (in_array($values, (array)$getcartitemkeys)) {
                if (get_option('rs_enable_earned_level_based_reward_points') == 'yes') {
                echo sprintf('1 <input type="hidden" name="cart[%s][qty]" value="1" />', $values);
                    return;
                }else {
                     return $productquantity;
                }
            } else {
                return $productquantity;
            }
        }        
    }

    public static function main_itemid_testing() {

        $orderid = '14';
        $order = new WC_Order($orderid);
        $listofcartitemkeys = WC()->session->get("freeproductcartitemkeys");
        var_dump($listofcartitemkeys);
        var_dump(get_option('mainids'));
        var_dump(get_option('correspn'));
        foreach ($order->get_items() as $itemid => $eachitem) {
            echo "<pre>";
            wc_get_order_item_meta($itemid, '_ruleidsdata', true);
            echo "</pre>";
        }
    }

    public static function show_item_id_after_checkout($item_name, $item) {
        // var_dump($item);
        @$freeproductmsg = $item['rsfreeproductmsg'];
        if ($freeproductmsg != NULL && $freeproductmsg != '') {
            if (get_option('rs_remove_msg_from_cart_order') == 'yes') {
                return $item_name . "<br>" . $freeproductmsg;
            } else {
                return $item_name;
            }
        } else {
            return $item_name;
        }
    }

}

new FPRewardSystem_Free_Product();
