<?php
/*
 * Encash Points Tab Settings
 *
 */

class FPRewardSystemEncashTab {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_encashtab'] = __('Form For Cash Back', 'rewardsystem');
        return $settings_tabs;
    }

    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        return apply_filters('woocommerce_rewardsystem_point_encash_settings', array(
            array(
                'name' => __('Use the Shortcode [rsencashform] to display the Cash Back Form', 'rewardsystem'),
                'type' => 'title',
                'desc' => '',
                'id' => '_rs_shortcode_for_encashing'
            ),
            array('type' => 'sectionend', 'id' => '_rs_shortcode_for_encashing'),
            array(
                'name' => __('Cash Back Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Here you can select the Cash Back Customization Options', 'rewardsystem'),
                'id' => '_rs_reward_point_encashing_settings'
            ),
            array(
                'name' => __('Enable Cash Back for Reward Points', 'rewardsystem'),
                'desc' => __('Enable this option to provide the feature to Cash Back the Reward Points earned by the Users', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_enable_disable_encashing',
                'css' => '',
                'std' => '2',
                'type' => 'select',
                'newids' => 'rs_enable_disable_encashing',
                'options' => array(
                    '1' => __('Enable', 'rewardsystem'),
                    '2' => __('Disable', 'rewardsystem'),
                ),
                'desc_tip' => true,
            ),
            array(
                'name' => __('Minimum Points for Cash Back of Reward Points', 'rewardsystem'),
                'desc' => __('Enter the Minimum points that the user should have in order to Submit the Cash Back Request', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_minimum_points_encashing_request',
                'css' => '',
                'std' => '',
                'type' => 'text',
                'newids' => 'rs_minimum_points_encashing_request',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Maximum Points for Cash Back of Reward Points', 'rewardsystem'),
                'desc' => __('Enter the Maximum points that the user should enter order to Submit the Cash Back Request', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_maximum_points_encashing_request',
                'css' => '',
                'std' => '',
                'type' => 'text',
                'newids' => 'rs_maximum_points_encashing_request',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Points for Cash Back Label', 'rewardsystem'),
                'desc' => __('Please Enter Points the Label for Cash Back', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_encashing_points_label',
                'css' => 'min-width:300px;',
                'std' => 'Points for Cash Back',
                'type' => 'text',
                'newids' => 'rs_encashing_points_label',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Reason for Cash Back Label', 'rewardsystem'),
                'desc' => __('Please Enter label for Reason Cash Back', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_encashing_reason_label',
                'css' => 'min-width:300px;',
                'std' => 'Reason for Cash Back',
                'type' => 'text',
                'newids' => 'rs_encashing_reason_label',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Payment Method Label', 'rewardsystem'),
                'desc' => __('Please Enter Payment Method Label for Cash Back', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_encashing_payment_method_label',
                'css' => 'min-width:300px;',
                'std' => 'Payment Method',
                'type' => 'text',
                'newids' => 'rs_encashing_payment_method_label',
                'desc_tip' => true,
            ),
            array(
                'name' => __('PayPal Email Address Label', 'rewardsystem'),
                'desc' => __('Please Enter PayPal Email Address Label for Cash Back', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_encashing_payment_paypal_label',
                'css' => 'min-width:300px;',
                'std' => 'PayPal Email Address',
                'type' => 'text',
                'newids' => 'rs_encashing_payment_paypal_label',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Custom Payment Details Label', 'rewardsystem'),
                'desc' => __('Please Enter Custom Payment Details Label for Cash Back', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_encashing_payment_custom_label',
                'css' => 'min-width:300px;',
                'std' => 'Custom Payment Details',
                'type' => 'text',
                'newids' => 'rs_encashing_payment_custom_label',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Cash Back Form Submit Button Label', 'rewardsystem'),
                'desc' => __('Please Enter Cash Back Form Submit Button Label ', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_encashing_submit_button_label',
                'css' => 'min-width:200px;',
                'std' => 'Submit',
                'type' => 'text',
                'newids' => 'rs_encashing_submit_button_label',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_point_checkout_settings'),
            array(
                'name' => __('Message Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => '',
                'id' => '_rs_message_settings_encashing'
            ),
            array(
                'name' => __('Message Displayed for Guest', 'rewardsystem'),
                'desc' => __('Please Enter Message Displayed for Guest', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_message_for_guest_encashing',
                'css' => 'min-width:500px;',
                'std' => 'Please [rssitelogin] to Cash Back your Reward Points.',
                'type' => 'textarea',
                'newids' => 'rs_message_for_guest_encashing',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Login link for Guest Label', 'rewardsystem'),
                'desc' => __('Please Enter Login link for Guest Label', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_encashing_login_link_label',
                'css' => 'min-width:200px;',
                'std' => 'Login',
                'type' => 'text',
                'newids' => 'rs_encashing_login_link_label',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Message Displayed for Banned Users', 'rewardsystem'),
                'desc' => __('Please Enter Message Displayed for Banned Users', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_message_for_banned_users_encashing',
                'css' => 'min-width:500px;',
                'std' => 'You cannot Cash Back Your points',
                'type' => 'textarea',
                'newids' => 'rs_message_for_banned_users_encashing',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Message Displayed when Users dont have Reward Points', 'rewardsystem'),
                'desc' => __('Please Enter Message to be Displayed when Users dont have Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_message_users_nopoints_encashing',
                'css' => 'min-width:500px;',
                'std' => 'You Don\'t have points for Cash back',
                'type' => 'textarea',
                'newids' => 'rs_message_users_nopoints_encashing',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Message Displayed when Cash Back Request is Submitted', 'rewardsystem'),
                'desc' => __('Please Enter Message to be Displayed when Cash Back Request is Submitted', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_message_encashing_request_submitted',
                'css' => 'min-width:500px;',
                'std' => 'Cash Back Request Submitted',
                'type' => 'textarea',
                'newids' => 'rs_message_encashing_request_submitted',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_message_settings_encashing'),
            array(
                'name' => __('CSV Settings (Export CSV for Paypal Mass Payment)', 'rewardsystem'),
                'type' => 'title',
                'desc' => '',
                'id' => '_rs_csv_message_settings_encashing'
            ),
            array(
                'name' => __('Custom Note for Paypal', 'rewardsystem'),
                'desc' => __('A Custom Note for Paypal', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_encashing_paypal_custom_notes',
                'css' => 'min-width:200px;',
                'std' => 'Thanks for your Business',
                'type' => 'textarea',
                'newids' => 'rs_encashing_paypal_custom_notes',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_csv_message_settings_encashing'),
            array(
                'name' => __('Error Message Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => '',
                'id' => '_rs_error_settings_encashing'
            ),
            array(
                'name' => __('Error Message Displayed when Points for Cash Back Field is Empty', 'rewardsystem'),
                'desc' => __('Please Enter Message to be Displayed when Points for Cash Back Field is Empty', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_error_message_points_empty_encash',
                'css' => 'min-width:500px;',
                'std' => 'Points for Cash Back Field cannot be empty',
                'type' => 'text',
                'newids' => 'rs_error_message_points_empty_encash',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Error Message Displayed when Points To Cash Back value is not a number', 'rewardsystem'),
                'desc' => __('Please Enter Message to be Displayed when Points To Cash Back Field value is not a number', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_error_message_points_number_val_encash',
                'css' => 'min-width:500px;',
                'std' => 'Please Enter only Numbers',
                'type' => 'text',
                'newids' => 'rs_error_message_points_number_val_encash',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Error Message Displayed when Points entered for Cash Back is more than the Points Earned', 'rewardsystem'),
                'desc' => __('Please Enter Message to be Displayed when Points entered for Cash Back is more than the Points Earned', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_error_message_points_greater_than_earnpoints',
                'css' => 'min-width:500px;',
                'std' => 'Points Entered for Cash Back is more than the Earned Points',
                'type' => 'text',
                'newids' => 'rs_error_message_points_greater_than_earnpoints',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Error Message Displayed when Current User Points is less than the Minimum points for Cash Back', 'rewardsystem'),
                'desc' => __('Please Enter Message to be Displayed when Points entered for Cash Back is more than the Maximum Points for Cash Back', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_error_message_currentpoints_less_than_minimum_points',
                'css' => 'min-width:500px;',
                'std' => 'You need a Minimum of [minimum_encash_points] points in order for Cash Back',
                'type' => 'textarea',
                'newids' => 'rs_error_message_currentpoints_less_than_minimum_points',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Error Message Displayed when Points entered to Cash Back is less than the Minimum Points and more than Maximum Points', 'rewardsystem'),
                'desc' => __('Please Enter Message to be Displayed when Points entered to Cash Back is less than the Minimum Points and more than Maximum Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_error_message_points_lesser_than_minimum_points',
                'css' => 'min-width:500px;',
                'std' => 'Please Enter Between [minimum_encash_points] and [maximum_encash_points] ',
                'type' => 'textarea',
                'newids' => 'rs_error_message_points_lesser_than_minimum_points',
                'desc_tip' => true,
            ),
//            array(
//                'name' => __('Error Message Displayed when Points entered to Cash Back is more than the Maximum Points for Cash Back', 'rewardsystem'),
//                'desc' => __('Please Enter Message to be Displayed when Points entered to Cash Back is more than the Maximum Points for Cash Back', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_error_message_points_more_than_maximum_points',
//                'css' => 'min-width:500px;',
//                'std' => 'You cannot Cash Back more than [maximum_encash_points] Points',
//                'type' => 'textarea',
//                'newids' => 'rs_error_message_points_more_than_maximum_points',
//                'desc_tip' => true,
//            ),
            array(
                'name' => __('Error Message Displayed when Reason To Cash Back Field is Empty', 'rewardsystem'),
                'desc' => __('Please Enter Message to be Displayed when Reason To Cash Back Field is Empty', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_error_message_reason_encash_empty',
                'css' => 'min-width:500px;',
                'std' => 'Reason to Encash Field cannot be empty',
                'type' => 'text',
                'newids' => 'rs_error_message_reason_encash_empty',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Error Message Displayed when PayPal Email Address is Empty', 'rewardsystem'),
                'desc' => __('Please Enter Message to be Displayed when PayPal Email Address is Empty', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_error_message_paypal_email_empty',
                'css' => 'min-width:500px;',
                'std' => 'Paypal Email Field cannot be empty',
                'type' => 'text',
                'newids' => 'rs_error_message_paypal_email_empty',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Error Message Displayed when PayPal Email Address format is wrong', 'rewardsystem'),
                'desc' => __('Please Enter Message to be Displayed when PayPal Email Address format is wrong', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_error_message_paypal_email_wrong',
                'css' => 'min-width:500px;',
                'std' => 'Enter a Correct Email Address',
                'type' => 'text',
                'newids' => 'rs_error_message_paypal_email_wrong',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Error Message Displayed when Custom Payment Details field is Empty', 'rewardsystem'),
                'desc' => __('Please Enter Message to be Displayed when Custom Payment Details field is Empty', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_error_custom_payment_field_empty',
                'css' => 'min-width:500px;',
                'std' => 'Custom Payment Details  Field cannot be empty',
                'type' => 'text',
                'newids' => 'rs_error_custom_payment_field_empty',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_error_settings_encashing'),
            array(
                'name' => __('Cash Back Form CSS Customization Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => '',
                'id' => '_rs_style_settings_encashing'
            ),
            array(
                'name' => __('Inbuilt Design', 'rewardsystem'),
                'desc' => __('Please Select you want to load the Inbuilt Design', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_encash_form_inbuilt_design',
                'css' => '',
                'std' => '1',
                'type' => 'radio',
                'options' => array('1' => 'Inbuilt Design'),
                'newids' => 'rs_encash_form_inbuilt_design',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Inbuilt CSS (Non Editable)', 'rewardsystem'),
                'desc' => __('These are element IDs in the Shop Page ', 'rewardsystem'),
                'tip' => '',
                'css' => 'min-width:550px;min-height:260px;margin-bottom:80px;',
                'id' => 'rs_encash_form_default_css',
                'std' => '#encashing_form{}
.rs_encash_points_value{}
.error{color:#ED0514;}
.rs_encash_points_reason{}
.rs_encash_payment_method{}
.rs_encash_paypal_address{}
.rs_encash_custom_payment_option_value{}
.rs_encash_submit{}
#rs_encash_submit_button{}
.success_info{}
#encash_form_success_info{}',
                'type' => 'textarea',
                'newids' => 'rs_encash_form_default_css',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Custom Design', 'rewardsystem'),
                'desc' => __('Please Select you want to load the Custom Design', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_encash_form_inbuilt_design',
                'css' => '',
                'std' => '1',
                'type' => 'radio',
                'options' => array('2' => 'Custom Design'),
                'newids' => 'rs_encash_form_inbuilt_design',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Custom CSS', 'rewardsystem'),
                'desc' => __('Customize the following element of Cash Back Request form', 'galaxyfunder'),
                'tip' => '',
                'css' => 'min-width:550px;min-height:260px;margin-bottom:80px;',
                'id' => 'rs_encash_form_custom_css',
                'std' => '',
                'type' => 'textarea',
                'newids' => 'rs_encash_form_custom_css',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_style_settings_encashing'),
        ));
        ?>    
<?php 
    }

    public static function encashing_front_end_form() {
        if (get_option('rs_enable_disable_encashing') == '1') {
            if (is_user_logged_in()) {
                $user_ID = get_current_user_id();
                if (get_user_meta($user_ID, '_my_reward_points', true) > 0) {
                    $userid = get_current_user_id();
                    $banning_type = FPRewardSystem::check_banning_type($userid);        
                    if($banning_type!='earningonly'&&$banning_type!='both') {
                            ob_start();
                            $encash_form_style_option = get_option('rs_encash_form_inbuilt_design');                            
                            ?>

                            <style type="text/css">
                               <?php 
                                    if($encash_form_style_option == '1'){
                                        echo get_option('rs_encash_form_default_css');
                                    }else{
                                        echo get_option('rs_encash_form_custom_css');
                                    }
                               
                               ?>

                            </style>
                            <?php
                            $rs_minimum_points_for_encash = get_option('rs_minimum_points_encashing_request');
                            $rs_maximum_points_for_encash = get_option('rs_maximum_points_encashing_request');                                                                                                                               
                            $minimum_encash_to_find = "[minimum_encash_points]";
                            $maximum_encash_to_find = "[maximum_encash_points]";
                            $rs_error_mesage_minimum_encash = get_option('rs_error_message_points_lesser_than_minimum_points'); 
                            
                            $rs_current_points_less_than_minimum_points = get_option('rs_error_message_currentpoints_less_than_minimum_points');                                                          
                            $rs_current_points_less_than_minimum_points_replaced = str_replace($minimum_encash_to_find,$rs_minimum_points_for_encash, $rs_current_points_less_than_minimum_points);                                                                                    
                            //var_dump($rs_current_points_less_than_minimum_points_replaced);
                             $currentuserpoints =  get_user_meta(get_current_user_id(), '_my_reward_points', true);
                            $rs_error_mesage_minimum_encash_replaced = str_replace($minimum_encash_to_find, $rs_minimum_points_for_encash !=''?$rs_minimum_points_for_encash:'0', $rs_error_mesage_minimum_encash);                             
                            $rs_error_mesage_min_max_encash_replaced = str_replace($maximum_encash_to_find,$rs_maximum_points_for_encash !=''?$rs_maximum_points_for_encash:$currentuserpoints, $rs_error_mesage_minimum_encash_replaced);
                                                                                    
                            echo '<form id="encashing_form" method="post" enctype="multipart/form-data">';
                            echo '<div class ="rs_encash_points_value"><p><label><b>' . get_option("rs_encashing_points_label") . '</b></label></p><p><input type = "text" id = "rs_encash_points_value" name = "rs_encash_points_value" value=""></p></div>';
                            echo '<div class = "error" for = "rs_encash_points_value" id ="points_empty_error">' . get_option("rs_error_message_points_empty_encash") . '</div>';
                            echo '<div class = "error" for = "rs_encash_points_value" id ="points_number_error">' . get_option("rs_error_message_points_number_val_encash") . '</div>';
                            echo '<div class = "error" for = "rs_encash_points_value" id ="points_greater_than_earnpoints_error">' . get_option("rs_error_message_points_greater_than_earnpoints") . '</div>';
                            echo '<div class = "error" for = "rs_encash_points_value" id ="currentpoints_lesser_than_minimumpoints_error">' . $rs_current_points_less_than_minimum_points_replaced . '</div>';
                            echo '<div class = "error" for = "rs_encash_points_value" id ="points_lesser_than_minpoints_error">' . $rs_error_mesage_min_max_encash_replaced . '</div>';                            
                            echo '<div class ="rs_encash_points_reason"><p><label><b>' . get_option("rs_encashing_reason_label") . '</b></label></p><p><textarea name ="rs_encash_points_reason" id="rs_encash_points_reason" rows= "3" cols= "50"></textarea></p></div>';
                            echo '<div class = "error" for = "rs_encash_points_reason" id ="reason_empty_error">' . get_option("rs_error_message_reason_encash_empty") . '</div>';
                            echo '<div class ="rs_encash_payment_method"><p><label><b>' . get_option("rs_encashing_payment_method_label") . '</b></label></p><p><select id= "rs_encash_payment_method"><option value="encash_through_paypal_method">PayPal</option><option value="encash_through_custom_payment">Custom Payment</option></select></p></div>';
                            echo '<div class ="rs_encash_paypal_address"><p><label><b>' . get_option("rs_encashing_payment_paypal_label") . '</b></label></p><p><input type = "text" id = "rs_encash_paypal_address" name = "rs_encash_paypal_address" value=""></p></div>';
                            echo '<div class = "error" for = "rs_encash_paypal_address" id ="paypal_email_empty_error">' . get_option("rs_error_message_paypal_email_empty") . '</div>';
                            echo '<div class = "error" for = "rs_encash_paypal_address" id ="paypal_email_format_error">' . get_option("rs_error_message_paypal_email_wrong") . '</div>';
                            echo '<div class ="rs_encash_custom_payment_option_value"><p><label><b>' . get_option("rs_encashing_payment_custom_label") . '</b></label></p><p><textarea name ="rs_encash_custom_payment_option_value" id="rs_encash_custom_payment_option_value" rows= "3" cols= "50"></textarea></p></div>';
                            echo '<div class = "error" for = "rs_encash_custom_payment_option_value" id ="paypal_custom_option_empty_error">' . get_option("rs_error_custom_payment_field_empty") . '</div>';
                            echo '<div class ="rs_encash_submit"><input type = "submit" name= "rs_encash_submit_button" value="' . get_option("rs_encashing_submit_button_label") . '" id="rs_encash_submit_button"></div>';
                            echo '<div class = "success_info" for = "rs_encash_submit_button" id ="encash_form_success_info"><b>' . get_option("rs_message_encashing_request_submitted") . '</b></div>';
                            echo '</form>';
                            ?>
                            <script type ="text/javascript">
                                jQuery(document).ready(function () {                                                                        
                                    var encash_current_user_points = "<?php echo $currentuserpoints =  get_user_meta(get_current_user_id(), '_my_reward_points', true); ?>";                                    
                                    var minimum_points_to_encash = "<?php echo $rs_minimum_points_for_encash !=''?$rs_minimum_points_for_encash:'0'; ?>";                                                                    
                                    var maximum_points_to_encash = "<?php echo $rs_maximum_points_for_encash !=''?$rs_maximum_points_for_encash:$currentuserpoints; ?>";                  
                                   
                                   //       alert(maximum_points_to_encash);
                                    jQuery(".error").hide();
                                    jQuery(".success_info").hide();
                                    jQuery(".rs_encash_custom_payment_option_value").hide();
                                    jQuery(".rs_encash_payment_method").change(function () {
                                        jQuery(".rs_encash_paypal_address").toggle();
                                        jQuery(".rs_encash_custom_payment_option_value").toggle();
                                        jQuery("#paypal_email_empty_error").hide();
                                        jQuery("#paypal_custom_option_empty_error").hide();
                                    });
                                    jQuery("#rs_encash_submit_button").click(function () {
                                        var encash_points = jQuery("#rs_encash_points_value").val();                                                                                
                                        //var maximum_points_to_encash = jQuery("#rs_maximum_points_encashing_request").val();
                                        var encash_points_validated = /^[0-9\b]+$/.test(encash_points);
                                        if (encash_points == "") {
                                            jQuery("#points_empty_error").fadeIn().delay(5000).fadeOut();
                                                                                        return false;
                                        } else {
                                            
                                            jQuery("#points_empty_error").hide();
                                            if (encash_points_validated == false) {
                                                jQuery("#points_number_error").fadeIn().delay(5000).fadeOut();
                                                return false;
                                            } else {
                                              
                                                jQuery("#points_number_error").hide();
                                               
                                                if (Number(encash_points) > Number(encash_current_user_points)) {
                                                    jQuery("#points_greater_than_earnpoints_error").fadeIn().delay(5000).fadeOut();
                                                    return false;
                                                } else {
                                                    //alert(minimum_points_to_encash);
                                                   // return false;
                                                     //   if(encash_current_user_points >= minimum_points_to_encash){  
                                                                if((Number(encash_points) >= Number(minimum_points_to_encash))&&(Number(encash_points) <= Number(maximum_points_to_encash))){
                                                                //if(encash_points <= maximum_points_to_encash){    
                                                                    jQuery("#points_greater_than_earnpoints_error").hide();
                                                                    //return true;
                                                                    jQuery("#currentpoints_lesser_than_minimumpoints_error").hide();
                                                                    jQuery("#points_lesser_than_minpoints_error").hide();          
                                                                    jQuery("#rs_error_message_points_lesser_than_minimum_points").hide();                                                                    
                                                                    jQuery("#points_greater_than_maxpoints_error").hide();
                                                                    var points_value = <?php echo get_option('rs_redeem_point'); ?>;
                                                                    var amount_value = <?php echo get_option('rs_redeem_point_value'); ?>;
                                                                    var conversion_step1 = encash_points / points_value;
                                                                    var currency_converted_value = conversion_step1 * amount_value;                                                                                                                                                                                
                                                                   // }else{                                                                        
                                                                   //     jQuery("#points_greater_than_maxpoints_error").fadeIn().delay(5000).fadeOut();
                                                                   //     return false;
                                                                   // }
                                                                }else{
                                                                    jQuery("#points_lesser_than_minpoints_error").fadeIn().delay(5000).fadeOut();
                                                                    return false;
                                                                }
                                                       // }else{
                                                         //   jQuery("#currentpoints_lesser_than_minimumpoints_error").fadeIn().delay(5000).fadeOut();
                                                         //   return false;
                                                       // }
                                                }
                                             //  return false;
                                            }
                                        }
                                        var reason_to_encash = jQuery("#rs_encash_points_reason").val();
                                        if (reason_to_encash == "") {
                                            jQuery("#reason_empty_error").fadeIn().delay(5000).fadeOut();
                                            return false;
                                        } else {
                                            jQuery("#reason_empty_error").hide();
                                        }

                                        var encash_selected_option = jQuery("#rs_encash_payment_method").val();
                                        if (encash_selected_option == "encash_through_paypal_method") {
                                            var encash_paypal_email = jQuery("#rs_encash_paypal_address").val();
                                            var encash_paypal_email_validated = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(encash_paypal_email);
                                            if (encash_paypal_email == "") {
                                                jQuery("#paypal_email_empty_error").fadeIn().delay(5000).fadeOut();
                                                return false;
                                            } else {
                                                jQuery("#paypal_email_empty_error").hide();
                                                if (encash_paypal_email_validated == false) {
                                                    jQuery("#paypal_email_format_error").fadeIn().delay(5000).fadeOut();
                                                    return false;
                                                } else {
                                                    jQuery("#paypal_email_format_error").hide();
                                                }
                                            }
                                        } else {
                                            var encash_custom_option = jQuery("#rs_encash_custom_payment_option_value").val();
                                            if (encash_custom_option == "") {
                                                jQuery("#paypal_custom_option_empty_error").fadeIn().delay(5000).fadeOut();
                                                return false;
                                            } else {
                                                jQuery("#paypal_custom_option_empty_error").hide();
                                            }
                                        }
                                        jQuery(".success_info").show();
                                        jQuery(".success_info").fadeOut(3000);
                                        jQuery("#encashing_form")[0].reset();
                                        jQuery(".rs_encash_custom_payment_option_value").hide();
                                        jQuery(".rs_encash_paypal_address").show();
                                        var encash_request_user_id = <?php echo get_current_user_id(); ?>;
                            <?php
                            $user_details = get_user_by('id', get_current_user_id());
                            ?>
                                        var encash_request_user_name = "<?php echo $user_details->user_login; ?>";
                                        var encash_default_status = "Due";

                                        var encash_form_params = ({
                                            action: "rs_encash_form_value",
                                            points_to_encash: encash_points,
                                            reason_to_encash: reason_to_encash,
                                            payment_method: encash_selected_option,
                                            paypal_email_id: encash_paypal_email,
                                            custom_payment_details: encash_custom_option,
                                            userid_of_encash_request: encash_request_user_id,
                                            username_of_encash_request: encash_request_user_name,
                                            encasher_current_points: encash_current_user_points,
                                            converted_value_of_points: currency_converted_value,
                                            encash_default_status: encash_default_status,
                                            //encash_currency_symbol: encash_currency_symbol,
                                        });
                                        jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", encash_form_params, function (response) {
                                            console.log('Got this from the server: ' + response);
                                        });
                                        return false;
                                    });
                                });
                            </script>
                            <?php
                            $getcontent = ob_get_clean();
                            return $getcontent;
                        } else {
                            echo get_option("rs_message_for_banned_users_encashing");
                        }                    
                } else {
                    echo get_option("rs_message_users_nopoints_encashing");
                }
            } else {
                ?>
                <p><?php ob_start(); ?> <a href="<?php echo wp_login_url(); ?>" title="__('Login', 'rewardsystem')"><?php echo get_option("rs_encashing_login_link_label"); ?></a>
                <?php
                $message_for_guest = get_option("rs_message_for_guest_encashing");
                $guest_encash_string_to_find = "[rssitelogin]";
                $guest_encash_string_to_replace = ob_get_clean();
                $guest_encash_replaced_content = str_replace($guest_encash_string_to_find, $guest_encash_string_to_replace, $message_for_guest);
                echo $guest_encash_replaced_content;
            }
        }
    }

    public static function encash_reward_points_submitted_data() {
        global $wpdb;
        $charset_collate = '';
        $table_name = $wpdb->prefix . "sumo_reward_encashing_submitted_data";
        if (!empty($wpdb->charset)) {
            $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
        }

        if (!empty($wpdb->collate)) {
            $charset_collate .= " COLLATE {$wpdb->collate}";
        }
        $sql = "CREATE TABLE $table_name (
  id mediumint(9) NOT NULL AUTO_INCREMENT,
  userid INT(225),
  userloginname VARCHAR(200),
  pointstoencash VARCHAR(200),
  pointsconvertedvalue VARCHAR(200),
  encashercurrentpoints VARCHAR(200),
  reasonforencash LONGTEXT,
  encashpaymentmethod VARCHAR(200),
  paypalemailid VARCHAR(200),
  otherpaymentdetails LONGTEXT,
  status VARCHAR(200),
  date VARCHAR(300),
  UNIQUE KEY id (id)
) $charset_collate;";
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta($sql);
    }

    public static function process_encashing_points_to_users() {
        global $wpdb;

        if (isset($_POST['points_to_encash']) && isset($_POST['reason_to_encash']) && isset($_POST['payment_method']) && isset($_POST['converted_value_of_points']) && isset($_POST['username_of_encash_request']) && isset($_POST['encash_default_status'])) {
            //echo "google";
            $custom_option_details_for_encashing = '';
            $encasher_userid = $_POST['userid_of_encash_request'];
            $encasher_username = $_POST['username_of_encash_request'];
            $points_to_be_encashed = $_POST['points_to_encash'];
            $converted_value_of_encash_points = $_POST['converted_value_of_points'];
            $current_points_for_user = $_POST['encasher_current_points'];
            $reason_for_encashing = $_POST['reason_to_encash'];
            $payment_method_for_encashing = $_POST['payment_method'];
            $paypal_email_for_encashing = $_POST['paypal_email_id'];
            if(isset($_POST['custom_payment_details'])){
            $custom_option_details_for_encashing = $_POST['custom_payment_details'];
            }
            $table_name = $wpdb->prefix . "sumo_reward_encashing_submitted_data";
            $default_status_of_encash_request = $_POST['encash_default_status'];
            $wpdb->insert($table_name, array('userid' => $encasher_userid, 'userloginname' => $encasher_username, 'pointstoencash' => $points_to_be_encashed, 'encashercurrentpoints' => $current_points_for_user, 'reasonforencash' => $reason_for_encashing, 'encashpaymentmethod' => $payment_method_for_encashing, 'paypalemailid' => $paypal_email_for_encashing, 'otherpaymentdetails' => $custom_option_details_for_encashing, 'status' => $default_status_of_encash_request, 'pointsconvertedvalue' => $converted_value_of_encash_points, 'date' => date('Y-m-d H:i:s')));
        }
        exit();
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemEncashTab::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemEncashTab::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemEncashTab::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }
    
    public static function rs_css_customization_encash_form(){
        ?>
                <script type="text/javascript">
                    jQuery(document).ready(function(){
                        jQuery('#rs_encash_form_default_css').attr('readonly', 'readonly');
                    });
                </script>
        <?php
    }

}

add_shortcode('rsencashform', array('FPRewardSystemEncashTab', 'encashing_front_end_form'));

new FPRewardSystemEncashTab();


/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemEncashTab', 'reward_system_tab_settings'), 999);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_encashtab', array('FPRewardSystemEncashTab', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemEncashTab', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_encashtab', array('FPRewardSystemEncashTab', 'reward_system_register_admin_settings'));

add_action('wp_ajax_rs_encash_form_value', array('FPRewardSystemEncashTab', 'process_encashing_points_to_users'));
add_action('admin_head', array('FPRewardSystemEncashTab','rs_css_customization_encash_form'));