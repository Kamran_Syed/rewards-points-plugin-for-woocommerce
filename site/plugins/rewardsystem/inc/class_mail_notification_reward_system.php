<?php

class FPRewardSystemMailTab {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_mail'] = __('Mail', 'rewardsystem');
        return $settings_tabs;
    }

// Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        $checkmandrill = 'wpmandrill/wpmandrill.php';
        if (function_exists('is_plugin_active')) {
            if (is_plugin_active($checkmandrill)) {
                $arraymailoption = array(
                    '1' => 'mail()',
                    '2' => 'wp_mail()',
                    '3' => 'wpmandrill',
                );
            } else {
                $arraymailoption = array(
                    '1' => 'mail()',
                    '2' => 'wp_mail()',
                );
            }
        } else {
            $arraymailoption = array(
                '1' => 'mail()',
                '2' => 'wp_mail()',
            );
        }
        return apply_filters('woocommerce_rewardsystem_mail_settings', array(
            array(
                'name' => __('Mail Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Here you can select the mail function for sending mail', 'rewardsystem'),
                'id' => '_rs_reward_mail_settings'
            ),
            array(
                'name' => __('Select Mail Function', 'rewardsystem'),
                'desc' => __('Select Mail Function for Reward System', 'rewardsystem'),
                'id' => 'rs_select_mail_function',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'newids' => 'rs_select_mail_function',
                'type' => 'select',
                'options' => $arraymailoption,
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_mail_settings'),
            array(
                'name' => __('Mail Cron Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => 'Here you can set the time duration for which mail will be sent repeatedly. For example if you set the time duration as 3 days then for every 3 days mail will be sent',
                'id' => 'rs_cron_settings',
            ),
            array(
                'name' => __('Mail Cron Time Type', 'rewardsystem'),
                'desc' => __('Please Select wether the time should be in Minutes/Hours/Days', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_mail_cron_type',
                'css' => 'min-width:150px;',
                'type' => 'select',
                'newids' => 'rs_mail_cron_type',
                'desc_tip' => true,
                'options' => array('minutes' => 'Minutes', 'hours' => 'Hours', 'days' => 'Days'),
                'std' => 'days',
                'default' => 'days',
            ),
            array(
                'name' => __('Mail Cron Time', 'rewardsystem'),
                'desc' => __('Please Enter time after which Email cron job should run', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_mail_cron_time',
                'newids' => 'rs_mail_cron_time',
                'css' => 'min-width:150px;',
                'type' => 'text',
                'desc_tip' => true,
                'std' => '3',
                'default' => '3',
            ),
            array('type' => 'sectionend', 'id' => 'rs_cron_settings'), //Time Settings END
        ));
    }

    public static function add_header_script_for_js() {
        global $woocommerce;
        if (isset($_GET['tab'])) {
            if ($_GET['tab'] == 'rewardsystem_mail') {
                ?>
                <script type="text/javascript">
                    jQuery(document).ready(function () {
                <?php if ((float) $woocommerce->version > (float) ('2.2.0')) { ?>
                            var troubleemail = jQuery('#rs_select_mail_function').val();
                            if (troubleemail === '1') {
                                jQuery('.prependedrc').remove();
                                jQuery('#rs_select_mail_function').parent().append('<span class="prependedrc">For WooCommerce 2.3 or higher version mail() function will not load the woocommerce default template. This option will be deprecated </span>');
                            } else {
                                jQuery('.prependedrc').remove();
                            }
                            jQuery('#rs_select_mail_function').change(function () {
                                if (jQuery(this).val() === '1') {
                                    jQuery('.prependedrc').remove();
                                    jQuery('#rs_select_mail_function').parent().append('<span class="prependedrc">For WooCommerce 2.3 or higher version mail() function will not load the woocommerce default template. This option will be deprecated </span>');
                                } else {
                                    jQuery('.prependedrc').remove();
                                }
                            });

                <?php } ?>
                    });
                </script>
                <?php
            }
        }
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemMailTab::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemMailTab::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemMailTab::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

    public static function rs_cron_job_setting() {
        wp_clear_scheduled_hook('rscronjob');
        delete_option('rscheckcronsafter');
        if (wp_next_scheduled('rscronjob') == false) {
            wp_schedule_event(time(), 'rshourly', 'rscronjob');
        }
    }

    public static function rs_add_x_hourly($schedules) {

        $interval = get_option('rs_mail_cron_time');
        if (get_option('rs_mail_cron_type') == 'minutes') {
            $interval = $interval * 60;
        } else if (get_option('rs_mail_cron_type') == 'hours') {
            $interval = $interval * 3600;
        } else if (get_option('rs_mail_cron_type') == 'days') {
            $interval = $interval * 86400;
        }
        $schedules['rshourly'] = array(
            'interval' => $interval,
            'display' => 'RS Hourly'
        );
        return $schedules;
    }

    public static function main_function_for_mail_sending() {
        global $wpdb;
        global $woocommerce;
        $emailtemplate_table_name = $wpdb->prefix . 'rs_templates_email';
        $email_templates = $wpdb->get_results("SELECT * FROM $emailtemplate_table_name"); //all email templates
        if (is_array($email_templates)) {
            foreach ($email_templates as $emails) {
                if ($emails->mailsendingoptions == '1') {
                    if (get_option('rsemailtemplates' . $emails->id) != '1') {
                        if ($emails->sendmail_options == '1') {
                            if ($emails->rsmailsendingoptions == '3') {
                                $checksendingmailoptions = 1;
                                $maindta = $checksendingmailoptions + get_option('rscheckcronsafter');
                                $newdatavalues = update_option('rscheckcronsafter', $maindta);

                                if (get_option('rscheckcronsafter') > 1) {
                                    //if()
                                    foreach (get_users() as $myuser) {
                                        $user = get_userdata($myuser->ID);
                                        $user_wmpl_lang = get_user_meta($myuser->ID, 'rs_wpml_lang', true);
                                        if (empty($user_wmpl_lang)) {
                                            $user_wmpl_lang = 'en';
                                        }
                                        $to = $user->user_email;
                                        $subject = RSWPMLSupport::fp_rs_get_wpml_text('rs_template_' . $emails->id . '_subject', $user_wmpl_lang, $emails->subject);
                                        $firstname = $user->user_firstname;
                                        $lastname = $user->user_lastname;
                                        $url_to_click = "<a href=" . site_url() . ">" . site_url() . "</a>";
                                        $userpoint = get_user_meta($myuser->ID, '_my_reward_points', true);
                                        $minimumuserpoints = $emails->minimum_userpoints;
                                        if ($minimumuserpoints == '') {
                                            $minimumuserpoints = 0;
                                        } else {
                                            $minimumuserpoints = $emails->minimum_userpoints;
                                        }
                                        if ($minimumuserpoints < $userpoint) {

                                            $message = RSWPMLSupport::fp_rs_get_wpml_text('rs_template_' . $emails->id . '_message', $user_wmpl_lang, $emails->message);
                                            $message = str_replace('{rssitelink}', $url_to_click, $message);
                                            $message = str_replace('{rsfirstname}', $firstname, $message);
                                            $message = str_replace('{rslastname}', $lastname, $message);
                                            $message = str_replace('{rspoints}', $userpoint, $message);
                                            $message = do_shortcode($message); //shortcode feature
                                            ob_start();
                                            wc_get_template('emails/email-header.php', array('email_heading' => $subject));
                                            echo $message;
                                            wc_get_template('emails/email-footer.php');
                                            $woo_temp_msg = ob_get_clean();
                                            $headers = "MIME-Version: 1.0\r\n";
                                            $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
                                            if ($emails->sender_opt == 'local') {
                                                // $headers .= "From: " . $emails->from_name . " <" . $emails->from_email . ">\r\n";
                                                $headers .= 'From: ' . $emails->from_name . ' <webmaster@' . $_SERVER['SERVER_NAME'] . '>' . "\r\n";
                                                //$headers .= "Reply-To: " . $emails->from_name . " <" . $emails->from_email . ">\r\n";
                                            } else {
                                                // $headers .= "From: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
                                                $headers .= 'From: ' . get_option('woocommerce_email_from_name') . ' <webmaster@' . $_SERVER['SERVER_NAME'] . '>' . "\r\n";
                                                //$headers .= "Reply-To: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
                                            }
                                            //wp_mail($to, $subject, $woo_temp_msg, $headers='');
                                            if ('2' == get_option('rs_select_mail_function')) {
                                                if (wp_mail($to, $subject, $woo_temp_msg, $headers, '-fwebmaster@' . $_SERVER['SERVER_NAME'])) {

                                                }
                                            } elseif ('1' == get_option('rs_select_mail_function')) {
                                                if (mail($to, $subject, $woo_temp_msg, $headers, '-fwebmaster@' . $_SERVER['SERVER_NAME'])) {

                                                }
                                            } else {
                                                if (wp_mail($to, $subject, $woo_temp_msg, $headers = '')) {

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($emails->rsmailsendingoptions == '3') {
                                $emailusers = unserialize($emails->sendmail_to);
                                $checksendingmailoptions = 1;
                                $maindta = $checksendingmailoptions + get_option('rscheckcronsafter');
                                $newdatavalues = update_option('rscheckcronsafter', $maindta);

                                if (get_option('rscheckcronsafter') > 1) {
                                    foreach ($emailusers as $myuser) {
                                        $user = get_userdata($myuser);
                                        $user_wmpl_lang = get_user_meta($myuser, 'rs_wpml_lang', true);
                                        if (empty($user_wmpl_lang)) {
                                            $user_wmpl_lang = 'en';
                                        }
                                        $to = $user->user_email;
                                        $subject = RSWPMLSupport::fp_rs_get_wpml_text('rs_template_' . $emails->id . '_subject', $user_wmpl_lang, $emails->subject);
                                        $firstname = $user->user_firstname;
                                        $lastname = $user->user_lastname;
                                        $url_to_click = site_url();
                                        $userpoint = get_user_meta($myuser, '_my_reward_points', true);
                                        $minimumuserpoints = $emails->minimum_userpoints;
                                        if ($minimumuserpoints == '') {
                                            $minimumuserpoints = 0;
                                        } else {
                                            $minimumuserpoints = $emails->minimum_userpoints;
                                        }
                                        if ($minimumuserpoints < $userpoint) {
                                            $message = RSWPMLSupport::fp_rs_get_wpml_text('rs_template_' . $emails->id . '_message', $user_wmpl_lang, $emails->message);
                                            $message = str_replace('{rssitelink}', $url_to_click, $message);
                                            $message = str_replace('{rsfirstname}', $firstname, $message);
                                            $message = str_replace('{rslastname}', $lastname, $message);
                                            $message = str_replace('{rspoints}', $userpoint, $message);
                                            $message = do_shortcode($message); //shortcode feature
                                            ob_start();
                                            wc_get_template('emails/email-header.php', array('email_heading' => $subject));
                                            echo $message;
                                            wc_get_template('emails/email-footer.php');
                                            $woo_temp_msg = ob_get_clean();
                                            $headers = "MIME-Version: 1.0\r\n";
                                            $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
                                            if ($emails->sender_opt == 'local') {
                                                // $headers .= "From: " . $emails->from_name . " <" . $emails->from_email . ">\r\n";
                                                $headers .= 'From: ' . $emails->from_name . ' <webmaster@' . $_SERVER['SERVER_NAME'] . '>' . "\r\n";
                                                //$headers .= "Reply-To: " . $emails->from_name . " <" . $emails->from_email . ">\r\n";
                                            } else {
                                                // $headers .= "From: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
                                                $headers .= 'From: ' . get_option('woocommerce_email_from_name') . ' <webmaster@' . $_SERVER['SERVER_NAME'] . '>' . "\r\n";
                                                //$headers .= "Reply-To: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
                                            }
                                            if ('2' == get_option('rs_select_mail_function')) {
                                                if (wp_mail($to, $subject, $woo_temp_msg, $headers, '-fwebmaster@' . $_SERVER['SERVER_NAME'])) {

                                                }
                                            } elseif ('1' == get_option('rs_select_mail_function')) {
                                                if (mail($to, $subject, $woo_temp_msg, $headers, '-fwebmaster@' . $_SERVER['SERVER_NAME'])) {

                                                }
                                            } else {
                                                if (wp_mail($to, $subject, $woo_temp_msg, $headers = '')) {

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        update_option('rsemailtemplates' . $emails->id, '1');
                    }
                } else {
                    if ($emails->sendmail_options == '1') {
                        if ($emails->rsmailsendingoptions == '3') {
                            $checksendingmailoptions = 1;
                            $maindta = $checksendingmailoptions + get_option('rscheckcronsafter');
                            $newdatavalues = update_option('rscheckcronsafter', $maindta);

                            if (get_option('rscheckcronsafter') > 1) {
                                foreach (get_users() as $myuser) {
                                    $user = get_userdata($myuser->ID);
                                    $user_wmpl_lang = get_user_meta($myuser->ID, 'rs_wpml_lang', true);
                                    if (empty($user_wmpl_lang)) {
                                        $user_wmpl_lang = 'en';
                                    }
                                    $to = $user->user_email;
                                    $subject = RSWPMLSupport::fp_rs_get_wpml_text('rs_template_' . $emails->id . '_subject', $user_wmpl_lang, $emails->subject);
                                    $firstname = $user->user_firstname;
                                    $lastname = $user->user_lastname;
                                    $url_to_click = "<a href=" . site_url() . ">" . site_url() . "</a>";
                                    $userpoint = get_user_meta($myuser->ID, '_my_reward_points', true);
                                    $minimumuserpoints = $emails->minimum_userpoints;
                                    if ($minimumuserpoints == '') {
                                        $minimumuserpoints = 0;
                                    } else {
                                        $minimumuserpoints = $emails->minimum_userpoints;
                                    }
                                    if ($minimumuserpoints < $userpoint) {


                                        $message = RSWPMLSupport::fp_rs_get_wpml_text('rs_template_' . $emails->id . '_message', $user_wmpl_lang, $emails->message);
                                        $message = str_replace('{rssitelink}', $url_to_click, $message);
                                        $message = str_replace('{rsfirstname}', $firstname, $message);
                                        $message = str_replace('{rslastname}', $lastname, $message);
                                        $message = str_replace('{rspoints}', $userpoint, $message);
                                        $message = do_shortcode($message); //shortcode feature
                                        ob_start();
                                        wc_get_template('emails/email-header.php', array('email_heading' => $subject));
                                        echo $message;
                                        wc_get_template('emails/email-footer.php');
                                        $woo_temp_msg = ob_get_clean();
                                        $headers = "MIME-Version: 1.0\r\n";
                                        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
                                        if ($emails->sender_opt == 'local') {
                                            // $headers .= "From: " . $emails->from_name . " <" . $emails->from_email . ">\r\n";
                                            $headers .= 'From: ' . $emails->from_name . ' <webmaster@' . $_SERVER['SERVER_NAME'] . '>' . "\r\n";
                                            //$headers .= "Reply-To: " . $emails->from_name . " <" . $emails->from_email . ">\r\n";
                                        } else {
                                            // $headers .= "From: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
                                            $headers .= 'From: ' . get_option('woocommerce_email_from_name') . ' <webmaster@' . $_SERVER['SERVER_NAME'] . '>' . "\r\n";
                                            //$headers .= "Reply-To: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
                                        }
                                        //wp_mail($to, $subject, $woo_temp_msg, $headers='');
                                        if ('2' == get_option('rs_select_mail_function')) {
                                            if (wp_mail($to, $subject, $woo_temp_msg, $headers, '-fwebmaster@' . $_SERVER['SERVER_NAME'])) {

                                            }
                                        } elseif ('1' == get_option('rs_select_mail_function')) {
                                            if (mail($to, $subject, $woo_temp_msg, $headers, '-fwebmaster@' . $_SERVER['SERVER_NAME'])) {

                                            }
                                        } else {
                                            if (wp_mail($to, $subject, $woo_temp_msg, $headers = '')) {

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if ($emails->rsmailsendingoptions == '3') {
                            $emailusers = unserialize($emails->sendmail_to);
                            $checksendingmailoptions = 1;
                            $maindta = $checksendingmailoptions + get_option('rscheckcronsafter');
                            $newdatavalues = update_option('rscheckcronsafter', $maindta);

                            if (get_option('rscheckcronsafter') > 1) {
                                foreach ($emailusers as $myuser) {
                                    $user = get_userdata($myuser);
                                    $user_wmpl_lang = get_user_meta($myuser, 'rs_wpml_lang', true);
                                    if (empty($user_wmpl_lang)) {
                                        $user_wmpl_lang = 'en';
                                    }
                                    $to = $user->user_email;
                                    $subject = RSWPMLSupport::fp_rs_get_wpml_text('rs_template_' . $emails->id . '_subject', $user_wmpl_lang, $emails->subject);
                                    $firstname = $user->user_firstname;
                                    $lastname = $user->user_lastname;
                                    $url_to_click = site_url();
                                    $userpoint = get_user_meta($myuser, '_my_reward_points', true);
                                    $minimumuserpoints = $emails->minimum_userpoints;
                                    if ($minimumuserpoints == '') {
                                        $minimumuserpoints = 0;
                                    } else {
                                        $minimumuserpoints = $emails->minimum_userpoints;
                                    }
                                    if ($minimumuserpoints < $userpoint) {
                                        $message = RSWPMLSupport::fp_rs_get_wpml_text('rs_template_' . $emails->id . '_message', $user_wmpl_lang, $emails->message);
                                        $message = str_replace('{rssitelink}', $url_to_click, $message);
                                        $message = str_replace('{rsfirstname}', $firstname, $message);
                                        $message = str_replace('{rslastname}', $lastname, $message);
                                        $message = str_replace('{rspoints}', $userpoint, $message);
                                        $message = do_shortcode($message); //shortcode feature
                                        ob_start();
                                        wc_get_template('emails/email-header.php', array('email_heading' => $subject));
                                        echo $message;
                                        wc_get_template('emails/email-footer.php');
                                        $woo_temp_msg = ob_get_clean();
                                        $headers = "MIME-Version: 1.0\r\n";
                                        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
                                        if ($emails->sender_opt == 'local') {
                                            // $headers .= "From: " . $emails->from_name . " <" . $emails->from_email . ">\r\n";
                                            $headers .= 'From: ' . $emails->from_name . ' <webmaster@' . $_SERVER['SERVER_NAME'] . '>' . "\r\n";
                                            //$headers .= "Reply-To: " . $emails->from_name . " <" . $emails->from_email . ">\r\n";
                                        } else {
                                            // $headers .= "From: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
                                            $headers .= 'From: ' . get_option('woocommerce_email_from_name') . ' <webmaster@' . $_SERVER['SERVER_NAME'] . '>' . "\r\n";
                                            //$headers .= "Reply-To: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
                                        }
                                        if ('2' == get_option('rs_select_mail_function')) {
                                            if (wp_mail($to, $subject, $woo_temp_msg, $headers, '-fwebmaster@' . $_SERVER['SERVER_NAME'])) {

                                            }
                                        } elseif ('1' == get_option('rs_select_mail_function')) {
                                            if (mail($to, $subject, $woo_temp_msg, $headers, '-fwebmaster@' . $_SERVER['SERVER_NAME'])) {

                                            }
                                        } else {
                                            if (wp_mail($to, $subject, $woo_temp_msg, $headers = '')) {

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}

new FPRewardSystemMailTab();


/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemMailTab', 'reward_system_tab_settings'), 999);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_mail', array('FPRewardSystemMailTab', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemMailTab', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_mail', array('FPRewardSystemMailTab', 'reward_system_register_admin_settings'));


add_action('update_option_rs_mail_cron_type', array('FPRewardSystemMailTab', 'rs_cron_job_setting'));
add_action('update_option_rs_mail_cron_time', array('FPRewardSystemMailTab', 'rs_cron_job_setting'));

add_action('admin_head', array('FPRewardSystemMailTab', 'add_header_script_for_js'));
?>