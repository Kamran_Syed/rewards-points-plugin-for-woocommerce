<?php

class FPRewardSystemLocalizationTab {

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem_localization'] = __('Localization', 'rewardsystem');
        return $settings_tabs;
    }

    // Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        return apply_filters('woocommerce_rewardsystem_localization_settings', array(
            array(
                'name' => __('Localization Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Here you can control the localization options', 'rewardsystem'),
                'id' => '_rs_reward_point_localization_settings'
            ),
            array(
                'name' => __('Referral Log Localization', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Referral Log Information for the localization options', 'rewardsystem'),
                'id' => '_rs_referral_log_localization_settings',
            ),
            array(
                'name' => __('Referral Reward Points', 'rewardsystem'),
                'desc' => __('Localize your Referral Reward Points earned for Purchase', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_localize_referral_reward_points_for_purchase',
                'css' => 'min-width:550px;',
                'type' => 'textarea',
                'std' => 'Referral Reward Points earned for Purchase {itemproductid} by {purchasedusername}',
                'newids' => '_rs_localize_referral_reward_points_for_purchase',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_referral_log_localization_settings'),
            array(
                'name' => __('Product Purchase Log Localization', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Product Purchase Log Information for localization options', 'rewardsystem'),
                'id' => '_rs_product_purchase_log_localization_settings',
            ),
            array(
                'name' => __('Points earned for Purchase ', 'rewardsystem'),
                'desc' => __('Localize Reward Points earned for purchase log', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_localize_product_purchase_reward_points',
                'css' => 'min-width:550px;',
                'type' => 'textarea',
                'std' => 'Points Earned for Purchasing the Product #{itemproductid} with Order #{currentorderid}',
                'newids' => '_rs_localize_product_purchase_reward_points',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Points earned for Purchase --> Main', 'rewardsystem'),
                'desc' => __('Localize Points earned for Purchase -->Main', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_localize_points_earned_for_purchase_main',
                'css' => 'min-width:550px;',
                'type' => 'textarea',
                'std' => 'Points Earned for Purchasing the Product of Order {currentorderid}',
                'newids' => '_rs_localize_points_earned_for_purchase_main',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_product_purchase_settings'),
            array(
                'name' => __('Product Redeeming Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Product Redeeming Log Information for localization options', 'rewardsystem'),
                'id' => '_rs_product_redeeming_settings',
            ),
            array(
                'name' => __('Points Redeemed Towards Purchase', 'rewardsystem'),
                'desc' => __('Localize Redeeming Towards Purchase log Information', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_localize_points_redeemed_towards_purchase',
                'css' => 'min-width:550px;',
                'type' => 'textarea',
                'std' => 'Points Redeemed Towards Purchase for Order {currentorderid}',
                'newids' => '_rs_localize_points_redeemed_towards_purchase',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_product_redeeming_settings'),
            array(
                'name' => __('Registration Reward Points Log Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Registration Reward Points Log Settings', 'rewardsystem'),
                'id' => '_rs_log_registration_reward_points',
            ),
            array(
                'name' => __('Points Earned for Registration', 'rewardsystem'),
                'desc' => __('Localize the Points Earned for Registration Message', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_localize_points_earned_for_registration',
                'css' => 'min-width:550px',
                'type' => 'textarea',
                'std' => 'Points Earned for Registration',
                'newids' => '_rs_localize_points_earned_for_registration',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Points Earned for Referral Registration', 'rewardsystem'),
                'desc' => __('Localize the Points Earned for Referral Registration Message', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_localize_points_earned_for_referral_registration',
                'css' => 'min-width:550px;',
                'type' => 'textarea',
                'std' => 'Points Earned for Referral Registration by {registereduser}',
                'newids' => '_rs_localize_points_earned_for_referral_registration',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_log_registration_reward_points'),
            array(
                'name' => __('Product Review Log Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Localize Product Review Log Settings', 'rewardsystem'),
                'id' => '_rs_review_localize_settings',
            ),
            array(
                'name' => __('Points Earned for Product Review', 'rewardsystem'),
                'desc' => __('Localize Points Earned for Product Review Message', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_localize_points_earned_for_product_review',
                'css' => 'min-width:550px;',
                'type' => 'textarea',
                'std' => 'Reward for Reviewing a Product {reviewproductid}',
                'newids' => '_rs_localize_points_earned_for_product_review',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_review_localize_settings'),
            array(
                'name' => __('Revise Referral Purchase Log Message', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Localize Revise Referral Purchased Log Message', 'rewardsystem'),
                'id' => '_rs_revise_referral_purchase_log_settings',
            ),
            array(
                'name' => __('Revise Referral Reward Points', 'rewardsystem'),
                'desc' => __('Localize Revise Referral Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_log_revise_referral_product_purchase',
                'css' => 'min-width:550px;',
                'type' => 'textarea',
                'std' => 'Revised Referral Product Purchase {productid}',
                'newids' => '_rs_log_revise_referral_product_purchase',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_revise_referral_purchase_log_settings'),
            array(
                'name' => __('Revise Purchase Log Message', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Localize Revise Purchased Log Message', 'rewardsystem'),
                'id' => '_rs_revise_purchase_log_settings',
            ),
            array(
                'name' => __('Revise Product Purchase', 'rewardsystem'),
                'desc' => __('Localize Revise Product Purchase for Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_log_revise_product_purchase',
                'css' => 'min-width:550px;',
                'type' => 'textarea',
                'std' => 'Revised Product Purchase {productid}',
                'newids' => '_rs_log_revise_product_purchase',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Revise Product Purchase --> Main', 'rewardsystem'),
                'desc' => __('Localize Revise Product Purchase for Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_log_revise_product_purchase_main',
                'css' => 'min-width:550px;',
                'type' => 'textarea',
                'std' => 'Revised Product Purchase {currentorderid}',
                'newids' => '_rs_log_revise_product_purchase_main',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_revise_purchase_log_settings'),
            array(
                'name' => __('Revised Product Redeeming Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Localize Revised Product Redeeming Settings', 'rewardsystem'),
                'id' => '_rs_revise_product_redeeming_settings',
            ),
            array(
                'name' => __('Revised Points Redeemed Towards Purchase', 'rewardsystem'),
                'desc' => __('Localize your Revised Points Redeemed Towards Purchase', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_log_revise_points_redeemed_towards_purchase',
                'css' => 'min-width:550px;',
                'type' => 'textarea',
                'std' => 'Revise Points Redeemed Towards Purchase',
                'newids' => '_rs_log_revise_points_redeemed_towards_purchase',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_revise_product_redeeming_settings'),
            array(
                'name' => __('Revised Points on Deleting Referral Registration Settings', 'rewardsystem'),
                'type' => 'title',
                'desc' => __('Localize Revised Points for Deleted Referral User Settings', 'rewardsystem'),
                'id' => '_rs_localize_revise_points_for_deleted_user',
            ),
            array(
                'name' => __('Referral Account Signup Points Revised', 'rewardsystem'),
                'desc' => __('Localize Referral Account Signup Points Revised Option', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_localize_referral_account_signup_points_revised',
                'css' => 'min-width:550px;',
                'type' => 'textarea',
                'std' => 'Referral Account Signup Points Revised with Referred User Deleted {usernickname}',
                'newids' => '_rs_localize_referral_account_signup_points_revised',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Revised Points for Referral Purchase', 'rewardsystem'),
                'desc' => __('Localize Revised Points for Referral Purchase Message', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_localize_revise_points_for_referral_purchase',
                'css' => 'min-width:550px;',
                'type' => 'textarea',
                'std' => 'Revised Referral Reward Points earned for Purchase {productid} by deleted user {usernickname}',
                'newids' => '_rs_localize_revise_points_for_referral_purchase',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_localize_revise_points_for_deleted_user'),
            array(
                'name' => __('Social Rewards Localization Settings', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_localize_social_reward_points',
            ),
            array(
                'name' => __('Reward for Social Facebook Like', 'rewardsystem'),
                'desc' => __('Localize Reward for Social Facebook Like', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_localize_reward_for_facebook_like',
                'css' => 'min-width:550px',
                'type' => 'textarea',
                'std' => 'Reward for Social Facebook Like',
                'newids' => '_rs_localize_reward_for_facebook_like',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Reward for Social Twitter Tweet', 'rewardsystem'),
                'desc' => __('Localize Reward for Social Twitter Tweet', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_localize_reward_for_twitter_tweet',
                'css' => 'min-width:550px',
                'type' => 'textarea',
                'std' => 'Reward for Social Twitter Tweet',
                'newids' => '_rs_localize_reward_for_twitter_tweet',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Reward for Social Google Plus', 'rewardsystem'),
                'desc' => __('Localize Reward for Social Google+', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_localize_reward_for_google_plus',
                'css' => 'min-width:550px',
                'type' => 'textarea',
                'std' => 'Reward for Social Google Plus',
                'newids' => '_rs_localize_reward_for_google_plus',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_localize_social_reward_points'),
            array(
                'name' => __('Revision of Social Rewards Localization', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_localize_social_redeeming',
            ),
            array(
                'name' => __('Reward for Social Facebook Like is Revised', 'rewardsystem'),
                'desc' => __('Localize Revised Reward for Social Facebook Like ', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_localize_reward_for_facebook_like_revised',
                'css' => 'min-width:550px',
                'type' => 'textarea',
                'std' => 'Reward for Social Facebook Like is Revised',
                'newids' => '_rs_localize_reward_for_facebook_like_revised',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Reward for Social Google Plus is Revised', 'rewardsystem'),
                'desc' => __('Localize Revised Reward for Social Google Plus ', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_localize_reward_for_google_plus_revised',
                'css' => 'min-width:550px',
                'type' => 'textarea',
                'std' => 'Reward for Social Google Plus is Revised',
                'newids' => '_rs_localize_reward_for_google_plus_revised',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_localize_social_redeeming'),
            array(
                'name' => __('Payment Gateway Reward Points Message', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_payment_gateway_reward_points',
            ),
            array(
                'name' => __('Reward for Using Payment Gateway', 'rewardsystem'),
                'desc' => __('Localize Reward for Using Payment Gateway', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_localize_reward_for_payment_gateway_message',
                'css' => 'min-width:550px;',
                'type' => 'textarea',
                'std' => 'Reward Points for Using Payment Gateway {payment_title}',
                'newids' => '_rs_localize_reward_for_payment_gateway_message',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Revised Reward Points for Using Payment Gateway ', 'rewardsystem'),
                'desc' => __('Localize Revised Reward Points for Using Payment Gateway', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_localize_revise_reward_for_payment_gateway_message',
                'css' => 'min-width:550px;',
                'type' => 'textarea',
                'std' => 'Revised Reward Points for Using Payment Gateway {payment_title}',
                'newids' => '_rs_localize_revise_reward_for_payment_gateway_message',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_payment_gateways_reward_points'),
            array(
                'name' => __('Voucher Code Log Localization', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_voucher_code_log_localization',
            ),
            array(
                'name' => __('Voucher Code Log Localization', 'rewardsystem'),
                'desc' => __('Localize Voucher Code Log Message in SUMO Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_localize_voucher_code_usage_log_message',
                'css' => 'min-width:550px;',
                'type' => 'textarea',
                'std' => 'Redeem Voucher Code {rsusedvouchercode}',
                'newids' => '_rs_localize_voucher_code_usage_log_message',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_voucher_code_log_localization'),
            array(
                'name' => __('Buying Reward Points Log Localization', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_buying_reward_points_localization',
            ),
            array(
                'name' => __('Buying Reward Points Log Localization', 'rewardsystem'),
                'desc' => __('Localize Buying Reward Points Log', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_localize_buying_reward_points_log',
                'css' => 'min-width:550px;',
                'type' => 'textarea',
                'std' => 'Buyied Reward Points  {rsbuyiedrewardpoints}',
                'newids' => '_rs_localize_buying_reward_points_log',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_buying_reward_points_localization'),
             array(
                'name' => __('Coupon Reward Points Log Localization', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_coupon_reward_points_localization',
            ),
            array(
                'name' => __('Coupon Reward Points Log Localization', 'rewardsystem'),
                'desc' => __('Localize Coupon Reward Points Log', 'rewardsystem'),
                'tip' => '',
                'id' => '_rs_localize_coupon_reward_points_log',
                'css' => 'min-width:550px;',
                'type' => 'textarea',
                'std' => 'Points Earned for using Coupons',
                'newids' => '_rs_localize_coupon_reward_points_log',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_coupon_reward_points_localization'),
            array('type' => 'sectionend', 'id' => '_rs_reward_point_localization_settings'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystemLocalizationTab::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystemLocalizationTab::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        global $woocommerce;
        foreach (FPRewardSystemLocalizationTab::rewardsystem_admin_fields() as $setting)
            if (isset($setting['newids']) && ($setting['std'])) {
                add_option($setting['newids'], $setting['std']);
            }
    }

}

new FPRewardSystemLocalizationTab();


/* * ***************************************************************************************
 * ***************Essential Stuff to Register the New Tabs in WooCommerce*******************
 * *****************************************************************************************
 */

// Add Filter for WooCommerce Update Options Reward System
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystemLocalizationTab', 'reward_system_tab_settings'), 999);

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem_localization', array('FPRewardSystemLocalizationTab', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystemLocalizationTab', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem_localization', array('FPRewardSystemLocalizationTab', 'reward_system_register_admin_settings'));
?>