<?php
/* /
  Plugin Name: SUMO Reward Points
  Plugin URI:
  Description: SUMO Reward Points is a WooCommerce Loyalty Reward System using which you can Reward your Customers using Reward Points for Purchasing Products, Writing Reviews, Sign up on your site etc
  Version: 7.7.5
  Author: Fantastic Plugins
  Author URI:
  / */

function rs_check_dependencies() {
    $woocommerce = "woocommerce/woocommerce.php";
    $mainpluginpath = "rewardsystem/rewardsystem.php";
    if (!is_plugin_active($woocommerce)) {
        deactivate_plugins($mainpluginpath);
    }
}

add_action('admin_init', 'rs_check_dependencies', 999);

class FPRewardSystem {

//Adding Admin Options in a Registered Metabox
    public static function reward_system_admin_option_simple_product() {
        global $post;
        ?>
        <div class="options_group show_if_simple show_if_subscription show_if_booking show_if_external">
            <?php
            woocommerce_wp_select(array(
                'id' => '_rewardsystemcheckboxvalue',
                'class' => 'rewardsystemcheckboxvalue',
                'label' => __('Enable SUMO Reward Points for Product Purchase', 'rewardsystem'),
                'options' => array(
                    '' => __('Choose Option', 'rewardsystem'),
                    'yes' => __('Enable', 'rewardsystem'),
                    'no' => __('Disable', 'rewardsystem'),
                )
                    )
            );

            woocommerce_wp_select(array(
                'id' => '_rewardsystem_options',
                'class' => 'rewardsystem_options',
                'label' => __('Reward Type', 'rewardsystem'),
                'options' => array(
                    '1' => __('By Fixed Reward Points', 'rewardsystem'),
                    '2' => __('By Percentage of Product Price', 'rewardsystem'),
                )
                    )
            );
            woocommerce_wp_text_input(
                    array(
                        'id' => '_rewardsystempoints',
                        'name' => '_rewardsystempoints',
                        'label' => __('Reward Points', 'rewardsystem')
                    )
            );
            woocommerce_wp_text_input(
                    array(
                        'id' => '_rewardsystempercent',
                        'name' => '_rewardsystempercent',
                        'label' => __('Reward Points in Percent %', 'rewardsystem')
                    )
            );
            woocommerce_wp_select(array(
                'id' => '_referral_rewardsystem_options',
                'class' => 'referral_rewardsystem_options',
                'label' => __('Referral Reward Type', 'rewardsystem'),
                'options' => array(
                    '1' => __('By Fixed Reward Points', 'rewardsystem'),
                    '2' => __('By Percentage of Product Price', 'rewardsystem'),
                )
                    )
            );
            woocommerce_wp_text_input(
                    array(
                        'id' => '_referralrewardsystempoints',
                        'name' => '_referralrewardsystempoints',
                        'label' => __('Referral Reward Points', 'rewardsystem')
                    )
            );
            woocommerce_wp_text_input(
                    array(
                        'id' => '_referralrewardsystempercent',
                        'name' => '_referralrewardsystempercent',
                        'label' => __('Referral Reward Points in Percent %', 'rewardsystem')
                    )
            );
            ?>
        </div>
        <?php
    }

    public static function reward_system_social_input_field() {

        woocommerce_wp_select(array(
            'id' => '_socialrewardsystemcheckboxvalue',
            'class' => 'socialrewardsystemcheckboxvalue',
            'label' => __('Enable SUMO Reward Points for Social Promotion', 'rewardsystem'),
            'options' => array(
                '' => __('Choose Option', 'rewardsystem'),
                'yes' => __('Enable', 'rewardsystem'),
                'no' => __('Disable', 'rewardsystem'),
            )
                )
        );

        woocommerce_wp_select(
                array(
                    'id' => '_social_rewardsystem_options_facebook',
                    'class' => 'social_rewardsystem_options_facebook',
                    'label' => __('Social Reward Options for Facebook', 'rewardsystem'),
                    'options' => array(
                        '1' => __('By Fixed Reward Points', 'rewardsystem'),
                        '2' => __('By Percentage of Product Price', 'rewardsystem')
                    )
                )
        );
        woocommerce_wp_text_input(
                array(
                    'id' => '_socialrewardsystempoints_facebook',
                    'name' => '_socialrewardsystempoints_facebook',
                    'label' => __('Social Reward Points Facebook', 'rewardsystem')
                )
        );
        woocommerce_wp_text_input(
                array(
                    'id' => '_socialrewardsystempercent_facebook',
                    'name' => '_socialrewardsystempercent_facebook',
                    'label' => __('Social Reward Facebook in Percent %', 'rewardsystem')
                )
        );
        woocommerce_wp_select(
                array(
                    'id' => '_social_rewardsystem_options_twitter',
                    'class' => 'social_rewardsystem_options_twitter',
                    'label' => __('Social Reward Options for Twitter', 'rewardsystem'),
                    'options' => array(
                        '1' => __('By Fixed Reward Points', 'rewardsystem'),
                        '2' => __('By Percentage of Product Price', 'rewardsystem')
                    )
                )
        );
        woocommerce_wp_text_input(
                array(
                    'id' => '_socialrewardsystempoints_twitter',
                    'name' => '_socialrewardsystempoints_twitter',
                    'label' => __('Social Reward Points Twitter', 'rewardsystem')
                )
        );
        woocommerce_wp_text_input(
                array(
                    'id' => '_socialrewardsystempercent_twitter',
                    'name' => '_socialrewardsystempercent_twitter',
                    'label' => __('Social Reward Twitter in Percent %', 'rewardsystem')
                )
        );
        woocommerce_wp_select(
                array(
                    'id' => '_social_rewardsystem_options_google',
                    'class' => 'social_rewardsystem_options_google',
                    'label' => __('Social Reward Options for Google+', 'rewardsystem'),
                    'options' => array(
                        '1' => __('By Fixed Reward Points', 'rewardsystem'),
                        '2' => __('By Percentage of Product Price', 'rewardsystem')
                    )
                )
        );
        woocommerce_wp_text_input(
                array(
                    'id' => '_socialrewardsystempoints_google',
                    'name' => '_socialrewardsystempoints_google',
                    'label' => __('Social Reward Points Google+ ', 'rewardsystem')
                )
        );
        woocommerce_wp_text_input(
                array(
                    'id' => '_socialrewardsystempercent_google',
                    'name' => '_socialrewardsystempercent_google',
                    'label' => __('Social Reward Google+ in Percent %', 'rewardsystem')
                )
        );
    }

    public static function add_script_to_head() {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                //jQuery('#newoptions').chosen();
                //alert(jQuery('.rewardsystem_options').val());
                if (jQuery('.rewardsystem_options').val() === '') {
                    jQuery('._rewardsystempercent_field').css('display', 'none');
                    jQuery('._rewardsystempoints_field').css('display', 'none');
                } else if (jQuery('.rewardsystem_options').val() === '1') {
                    jQuery('._rewardsystempercent_field').css('display', 'none');
                    jQuery('._rewardsystempoints_field').css('display', 'block');
                } else {
                    jQuery('._rewardsystempercent_field').css('display', 'block');
                    jQuery('._rewardsystempoints_field').css('display', 'none');
                }


                if (jQuery('.referral_rewardsystem_options').val() === '') {
                    jQuery('._referralrewardsystempercent_field').css('display', 'none');
                    jQuery('._referralrewardsystempoints_field').css('display', 'none');
                } else if (jQuery('.referral_rewardsystem_options').val() === '1') {
                    jQuery('._referralrewardsystempercent_field').css('display', 'none');
                    jQuery('._referralrewardsystempoints_field').css('display', 'block');
                } else {
                    jQuery('._referralrewardsystempercent_field').css('display', 'block');
                    jQuery('._referralrewardsystempoints_field').css('display', 'none');
                }


                jQuery('.rewardsystem_options').change(function () {
                    if (jQuery(this).val() === '') {
                        jQuery('._rewardsystempercent_field').css('display', 'none');
                        jQuery('._rewardsystempoints_field').css('display', 'none');
                    } else if (jQuery(this).val() === '1') {
                        jQuery('._rewardsystempercent_field').css('display', 'none');
                        jQuery('._rewardsystempoints_field').css('display', 'block');
                    } else {
                        jQuery('._rewardsystempercent_field').css('display', 'block');
                        jQuery('._rewardsystempoints_field').css('display', 'none');
                    }
                    // alert(jQuery(this).val());

                    return false;
                });
                jQuery('.referral_rewardsystem_options').change(function () {
                    if (jQuery(this).val() === '') {
                        jQuery('._referralrewardsystempercent_field').css('display', 'none');
                        jQuery('._referralrewardsystempoints_field').css('display', 'none');
                    } else if (jQuery(this).val() === '1') {
                        jQuery('._referralrewardsystempercent_field').css('display', 'none');
                        jQuery('._referralrewardsystempoints_field').css('display', 'block');
                    } else {
                        jQuery('._referralrewardsystempercent_field').css('display', 'block');
                        jQuery('._referralrewardsystempoints_field').css('display', 'none');
                    }
                    // alert(jQuery(this).val());

                    return false;
                });
                /* Social Reward System for facebook */
                if (jQuery('.social_rewardsystem_options_facebook').val() === '') {
                    jQuery('._socialrewardsystempoints_facebook_field').css('display', 'none');
                    jQuery('._socialrewardsystempercent_facebook_field').css('display', 'none');
                } else if (jQuery('.social_rewardsystem_options_facebook').val() === '1') {
                    jQuery('._socialrewardsystempercent_facebook_field').css('display', 'none');
                    jQuery('._socialrewardsystempoints_facebook_field').css('display', 'block');
                } else {
                    jQuery('._socialrewardsystempercent_facebook_field').css('display', 'block');
                    jQuery('._socialrewardsystempoints_facebook_field').css('display', 'none');
                }

                /* On Change Event Triggering for Social Rewards Facebook */
                jQuery('.social_rewardsystem_options_facebook').change(function () {
                    if (jQuery(this).val() === '') {
                        jQuery('._socialrewardsystempoints_facebook_field').css('display', 'none');
                        jQuery('._socialrewardsystempercent_facebook_field').css('display', 'none');
                    } else if (jQuery(this).val() === '1') {
                        jQuery('._socialrewardsystempercent_facebook_field').css('display', 'none');
                        jQuery('._socialrewardsystempoints_facebook_field').css('display', 'block');
                    } else {
                        jQuery('._socialrewardsystempercent_facebook_field').css('display', 'block');
                        jQuery('._socialrewardsystempoints_facebook_field').css('display', 'none');
                    }
                });
                /* Social Reward System for twitter */
                if (jQuery('.social_rewardsystem_options_twitter').val() === '') {
                    jQuery('._socialrewardsystempoints_twitter_field').css('display', 'none');
                    jQuery('._socialrewardsystempercent_twitter_field').css('display', 'none');
                } else if (jQuery('.social_rewardsystem_options_twitter').val() === '1') {
                    jQuery('._socialrewardsystempercent_twitter_field').css('display', 'none');
                    jQuery('._socialrewardsystempoints_twitter_field').css('display', 'block');
                } else {
                    jQuery('._socialrewardsystempercent_twitter_field').css('display', 'block');
                    jQuery('._socialrewardsystempoints_twitter_field').css('display', 'none');
                }

                /* On Change Event Triggering for Social Rewards twitter */
                jQuery('.social_rewardsystem_options_twitter').change(function () {
                    if (jQuery(this).val() === '') {
                        jQuery('._socialrewardsystempoints_twitter_field').css('display', 'none');
                        jQuery('._socialrewardsystempercent_twitter_field').css('display', 'none');
                    } else if (jQuery(this).val() === '1') {
                        jQuery('._socialrewardsystempercent_twitter_field').css('display', 'none');
                        jQuery('._socialrewardsystempoints_twitter_field').css('display', 'block');
                    } else {
                        jQuery('._socialrewardsystempercent_twitter_field').css('display', 'block');
                        jQuery('._socialrewardsystempoints_twitter_field').css('display', 'none');
                    }
                });
                /* Social Reward System for Google+ */
                if (jQuery('.social_rewardsystem_options_google').val() === '') {
                    jQuery('._socialrewardsystempoints_google_field').css('display', 'none');
                    jQuery('._socialrewardsystempercent_google_field').css('display', 'none');
                } else if (jQuery('.social_rewardsystem_options_google').val() === '1') {
                    jQuery('._socialrewardsystempercent_google_field').css('display', 'none');
                    jQuery('._socialrewardsystempoints_google_field').css('display', 'block');
                } else {
                    jQuery('._socialrewardsystempercent_google_field').css('display', 'block');
                    jQuery('._socialrewardsystempoints_google_field').css('display', 'none');
                }

                /* On Change Event Triggering for Social Rewards Google+ */
                jQuery('.social_rewardsystem_options_google').change(function () {
                    if (jQuery(this).val() === '') {
                        jQuery('._socialrewardsystempoints_google_field').css('display', 'none');
                        jQuery('._socialrewardsystempercent_google_field').css('display', 'none');
                    } else if (jQuery(this).val() === '1') {
                        jQuery('._socialrewardsystempercent_google_field').css('display', 'none');
                        jQuery('._socialrewardsystempoints_google_field').css('display', 'block');
                    } else {
                        jQuery('._socialrewardsystempercent_google_field').css('display', 'block');
                        jQuery('._socialrewardsystempoints_google_field').css('display', 'none');
                    }
                });
            });</script>
        <?php
    }

    public static function reward_point_delay_for_referee() {
        ?>
        <script type = "text/javascript">
            jQuery(document).ready(function () {
                if ((jQuery('#_rs_select_referral_points_referee_time').val()) === '1') {
                    jQuery('#_rs_select_referral_points_referee_time_content').parent().parent().hide();
                } else {
                    jQuery('#_rs_select_referral_points_referee_time_content').parent().parent().show();
                }
                jQuery('#_rs_select_referral_points_referee_time').change(function () {
                    jQuery('#_rs_select_referral_points_referee_time_content').parent().parent().toggle();
                });
            });</script>
        <?php
    }

    public static function save_rewardpoint_meta($post_id) {
//$woocommerce_checkbox = isset($_POST['_rewardsystemcheckboxvalue']) ? 'yes' : 'no';
        $woocommerce_checkbox_select = $_POST['_rewardsystemcheckboxvalue'];
        update_post_meta($post_id, '_rewardsystemcheckboxvalue', $woocommerce_checkbox_select);
        $woocommerce_rewardpoints = $_POST['_rewardsystempoints'];
        update_post_meta($post_id, '_rewardsystempoints', $woocommerce_rewardpoints);
        $woocommerce_reward_rule = $_POST['_rewardsystem_options'];
        update_post_meta($post_id, '_rewardsystem_options', $woocommerce_reward_rule);
        $woocommerce_rewardpercent = $_POST['_rewardsystempercent'];
        update_post_meta($post_id, '_rewardsystempercent', $woocommerce_rewardpercent);

        /* Saving Control For Referral Reward Points/Percent */
        $woocommerce_referral_rewardpoints = $_POST['_referralrewardsystempoints'];
        update_post_meta($post_id, '_referralrewardsystempoints', $woocommerce_referral_rewardpoints);
        $woocommerce_referral_reward_rule = $_POST['_referral_rewardsystem_options'];
        update_post_meta($post_id, '_referral_rewardsystem_options', $woocommerce_referral_reward_rule);
        $woocommerce_referral_rewardpercent = $_POST['_referralrewardsystempercent'];
        update_post_meta($post_id, '_referralrewardsystempercent', $woocommerce_referral_rewardpercent);
        /* Saving has been End for Referral Reward Points */

        $woocommerce_social_rewards_check = $_POST['_socialrewardsystemcheckboxvalue'];
        update_post_meta($post_id, '_socialrewardsystemcheckboxvalue', $woocommerce_social_rewards_check);

        /* Saving Options for Social Rewards Facebook Start */
        $woocommerce_social_facebook_options = $_POST['_social_rewardsystem_options_facebook'];
        update_post_meta($post_id, '_social_rewardsystem_options_facebook', $woocommerce_social_facebook_options);
        $woocommerce_social_facebook_reward_points = $_POST['_socialrewardsystempoints_facebook'];
        update_post_meta($post_id, '_socialrewardsystempoints_facebook', $woocommerce_social_facebook_reward_points);
        $woocommerce_social_facebook_reward_percent = $_POST['_socialrewardsystempercent_facebook'];
        update_post_meta($post_id, '_socialrewardsystempercent_facebook', $woocommerce_social_facebook_reward_percent);
        /* Saving Options for Social Rewards Facebook End */


        /* Saving Options for Social Rewards Twitter Start */
        $woocommerce_social_twitter_options = $_POST['_social_rewardsystem_options_twitter'];
        update_post_meta($post_id, '_social_rewardsystem_options_twitter', $woocommerce_social_twitter_options);
        $woocommerce_social_twitter_reward_points = $_POST['_socialrewardsystempoints_twitter'];
        update_post_meta($post_id, '_socialrewardsystempoints_twitter', $woocommerce_social_twitter_reward_points);
        $woocommerce_social_twitter_reward_percent = $_POST['_socialrewardsystempercent_twitter'];
        update_post_meta($post_id, '_socialrewardsystempercent_twitter', $woocommerce_social_twitter_reward_percent);
        /* Saving Options for Social Rewards Twitter End */


        /* Saving Options for Social Rewards Google+ Start */
        $woocommerce_social_google_options = $_POST['_social_rewardsystem_options_google'];
        update_post_meta($post_id, '_social_rewardsystem_options_google', $woocommerce_social_google_options);
        $woocommerce_social_google_reward_points = $_POST['_socialrewardsystempoints_google'];
        update_post_meta($post_id, '_socialrewardsystempoints_google', $woocommerce_social_google_reward_points);
        $woocommerce_social_google_reward_percent = $_POST['_socialrewardsystempercent_google'];
        update_post_meta($post_id, '_socialrewardsystempercent_google', $woocommerce_social_google_reward_percent);
        /* Saving Options for Social Rewards Twitter End */
    }

    public static function reward_system_after_single_product_summary($price, $product) {
        global $post;
        if (is_user_logged_in()) {
//            $banned_user_list = get_option('rs_banned-users_list');
//            if (!in_array(get_current_user_id(), (array) $banned_user_list)) {
//
//                $getarrayofuserdata = get_userdata(get_current_user_id());
//                $banninguserrole = get_option('rs_banning_user_role');
//                if (!in_array(isset($getarrayofuserdata->roles[0]) ? $getarrayofuserdata->roles[0] : '0', (array) $banninguserrole)) {
            $userid = get_current_user_id();
            $banning_type = FPRewardSystem::check_banning_type($userid);
            if ($banning_type != 'earningonly' && $banning_type != 'both') {
                if ((is_shop() || is_product()) && !is_admin()) {
                    if (function_exists('get_product')) {
                        $gettheproducts = get_product($post->ID);
                        if ($gettheproducts->is_type('variable')) {
                            if (is_product()) {
                                if (get_option('rs_show_hide_message_for_variable_gift_message') == '1') {
                                    if (get_option('_rs_enable_disable_gift_icon') == '1') {
                                        if (get_option('rs_image_url_upload') != '') {
                                            return "<span class='variableshopmessage'><img src=" . get_option('rs_image_url_upload') . " style='width:16px;height:16px;display:inline;' />&nbsp;" . do_shortcode(get_option('rs_message_for_single_product_variation')) . "</span>" . $price;
                                        } else {
                                            return "<span class='variableshopmessage'>" . do_shortcode(get_option('rs_message_for_single_product_variation')) . "</span>" . $price;
                                        }
                                    } else {
                                        return "<span class='variableshopmessage'>" . do_shortcode(get_option('rs_message_for_single_product_variation')) . "</span>" . $price;
                                    }
                                }
                            }
                        } else {
                            $getpostpoints = get_post_meta($post->ID, '_rewardsystemcheckboxvalue', true);
                            $rewardpoint = get_post_meta($post->ID, '_rewardsystempoints', true);
                            $getshortcodevalues = do_shortcode('[rewardpoints]');
                            if ($getshortcodevalues > 0) {
                                if (!is_admin() && (is_shop() || is_product())) {
                                    if (get_option('_rs_enable_disable_gift_icon') == '1') {
                                        if (get_option('rs_image_url_upload') != '') {
                                            $stylerewardpoint = "<span class='simpleshopmessage'><img src=" . get_option('rs_image_url_upload') . " style='width:16px;height:16px;display:inline;' />&nbsp; " . do_shortcode(get_option("rs_message_for_single_products")) . "</span>";
                                        } else {
                                            $stylerewardpoint = "<span class='simpleshopmessage'>" . do_shortcode(get_option("rs_message_for_single_products")) . "</span>";
                                        }
                                    } else {
                                        $stylerewardpoint = "<span class='simpleshopmessage'>" . do_shortcode(get_option("rs_message_for_single_products")) . "</span>";
                                    }
                                    //if (get_option('rs_show_hide_message_for_shop_archive') == '1') {
                                    if ($getpostpoints == 'yes') {
                                        if (is_product()) {
                                            if (get_option('rs_show_hide_message_for_shop_archive_single') == '1') {
                                                if (get_option('rs_message_position_for_single_simple_products') == '1') {
                                                    return $stylerewardpoint . "\r\n" . $price;
                                                } else {
                                                    return $price . "\r\n" . $stylerewardpoint;
                                                }
                                            }
                                        } else {
                                            if (get_option('rs_show_hide_message_for_shop_archive') == '1') {
                                                if (is_shop()) {
                                                    if (get_option('rs_message_position_for_single_simple_products') == '1') {
                                                        return "<small>" . $stylerewardpoint . "</small><br>" . $price;
                                                    } else {
                                                        return $price . "<br><small>" . $stylerewardpoint . "</small>";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    //}
                                }
                            }
                        }
                    }
                }
//                }
//            }
            }
        } else {
            if ((is_shop() || is_product()) && !is_admin()) {
                if (function_exists('get_product')) {
                    $gettheproducts = get_product($post->ID);
                    if ($gettheproducts->is_type('variable')) {
                        if (is_product()) {
                            if (get_option('rs_show_hide_message_for_variable_gift_message') == '1') {
                                if (get_option('_rs_enable_disable_gift_icon') == '1') {
                                    if (get_option('rs_image_url_upload') != '') {
                                        return "<span class='variableshopmessage'><img src=" . get_option('rs_image_url_upload') . " style='width:16px;height:16px;display:inline;' />&nbsp;" . do_shortcode(get_option('rs_message_for_single_product_variation')) . "</span>" . $price;
                                    } else {
                                        return "<span class='variableshopmessage'>" . do_shortcode(get_option('rs_message_for_single_product_variation')) . "</span>" . $price;
                                    }
                                } else {
                                    return "<span class='variableshopmessage'>" . do_shortcode(get_option('rs_message_for_single_product_variation')) . "</span>" . $price;
                                }
                            }
                        }
                    } else {
                        $getpostpoints = get_post_meta($post->ID, '_rewardsystemcheckboxvalue', true);
                        $rewardpoint = get_post_meta($post->ID, '_rewardsystempoints', true);
                        $getshortcodevalues = do_shortcode('[rewardpoints]');
                        if ($getshortcodevalues > 0) {
                            if (!is_admin() && (is_shop() || is_product())) {
                                if (get_option('_rs_enable_disable_gift_icon') == '1') {
                                    if (get_option('rs_image_url_upload') != '') {
                                        $stylerewardpoint = "<span class='simpleshopmessage'><img src=" . get_option('rs_image_url_upload') . " style='width:16px;height:16px;display:inline;' />&nbsp; " . do_shortcode(get_option("rs_message_for_single_products")) . "</span>";
                                    } else {
                                        $stylerewardpoint = "<span class='simpleshopmessage'>" . do_shortcode(get_option("rs_message_for_single_products")) . "</span>";
                                    }
                                } else {
                                    $stylerewardpoint = "<span class='simpleshopmessage'>" . do_shortcode(get_option("rs_message_for_single_products")) . "</span>";
                                }
                                if (get_option('rs_show_hide_message_for_shop_archive') == '1') {
                                    if ($getpostpoints == 'yes') {
                                        if (is_product()) {
                                            if (get_option('rs_message_position_for_single_simple_products') == '1') {
                                                return $stylerewardpoint . "<br>" . $price;
                                            } else {
                                                return $price . "<br>" . $stylerewardpoint;
                                            }
                                        } else {
                                            if (is_shop()) {
                                                if (get_option('rs_message_position_for_single_simple_products') == '1') {
                                                    return "<small>" . $stylerewardpoint . "</small><br>" . $price;
                                                } else {
                                                    return $price . "<br><small>" . $stylerewardpoint . "</small>";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $price;
    }

    public static function reward_system_variable_product() {
        global $post;
        if (get_option('rs_show_hide_message_for_variable_product') == '1') {
            ?>
            <div id='value_variable_product'></div>
            <?php
        }
    }

    public static function add_variation_shortcode_div() {
        return "<span class='variationrewardpoints' style='display:inline-block'></span>";
    }

    public static function add_variation_point_values_shortcode() {
        return get_woocommerce_currency_symbol() . "<div class='variationrewardpointsamount' style='display:inline-block'></div>";
    }

    public static function reward_system_variable_jquery() {
        if (is_product()) {
            ?>
            <style type="text/css">
                .variableshopmessage {
                    display:none;
                }
            </style>
            <script type='text/javascript'>
                jQuery(document).ready(function () {
                    jQuery('#value_variable_product').hide();
                    // jQuery('.variableshopmessage').hide();
                    jQuery(document).on('change', 'select', function () {
                        var variationid = jQuery('input:hidden[name=variation_id]').val();
                        if (variationid === '') {
                            return false;
                        }
                        //alert(variationid);
                        var dataparam = ({
                            action: 'getvariationid',
                            variationproductid: variationid,
                            userid: "<?php echo get_current_user_id(); ?>",
                        });
                        jQuery.post("<?php echo admin_url('admin-ajax.php');
            ?>", dataparam,
                                function (response) {
                                    //alert(response);
                                    if (response !== '') {
            <?php
            $banned_user_list = get_option('rs_banned-users_list');
            if (is_user_logged_in()) {
//                if (!in_array(get_current_user_id(), (array) $banned_user_list)) {
//
//                    $getarrayofuserdata = get_userdata(get_current_user_id());
//                    $banninguserrole = get_option('rs_banning_user_role');
//                    if (!in_array(isset($getarrayofuserdata->roles[0]) ? $getarrayofuserdata->roles[0] : '0', (array) $banninguserrole)) {
                $userid = get_current_user_id();
                $banning_type = FPRewardSystem::check_banning_type($userid);
                if ($banning_type != 'earningonly' && $banning_type != 'both') {
                    ?>
                                                var splitresponse = response.split('_');
                                                if (splitresponse[0] > 0) {
                                                    jQuery('.variableshopmessage').show();
                                                    jQuery('#value_variable_product').addClass('woocommerce-info');
                                                    jQuery('#value_variable_product').show();
                                                    jQuery('#value_variable_product').html("<?php echo do_shortcode(get_option('rs_message_for_variation_products')); ?>");
                                                    jQuery('.variationrewardpoints').html(splitresponse[0]);
                                                    jQuery('.variationrewardpointsamount').html(splitresponse[1]);
                                                } else {
                                                    jQuery('#value_variable_product').hide();
                                                    jQuery('.variableshopmessage').hide();
                                                }
                    <?php
//                    }
//                }
                }
            } else {
                ?>
                                            var splitresponse = response.split('_');
                                            if (splitresponse[0] > 0) {
                                                jQuery('.variableshopmessage').show();
                                                jQuery('#value_variable_product').addClass('woocommerce-info');
                                                jQuery('#value_variable_product').show();
                                                jQuery('#value_variable_product').html("<?php echo do_shortcode(get_option('rs_message_for_variation_products')); ?>");
                                                jQuery('.variationrewardpoints').html(splitresponse[0]);
                                                jQuery('.variationrewardpointsamount').html(splitresponse[1]);
                                            } else {
                                                jQuery('#value_variable_product').hide();
                                                jQuery('.variableshopmessage').hide();
                                            }
                <?php
            }
            ?>
                                    }
                                });
                    });
                    //                jQuery(document).on('click', '.wcva_attribute_radio', function() {
                    //
                    //                });
                    jQuery(document).on('change', '.wcva_attribute_radio', function (e) {
                        e.preventDefault();
                        var variationid = jQuery('input:hidden[name=variation_id]').val();
                        if (variationid === '') {
                            return false;
                        }
                        //alert(variationid);
                        var dataparam = ({
                            action: 'getvariationid',
                            variationproductid: variationid,
                            userid: "<?php echo get_current_user_id(); ?>",
                        });
                        jQuery.post("<?php echo admin_url('admin-ajax.php');
            ?>", dataparam,
                                function (response) {
                                    //alert(response);
                                    if (response !== '') {
            <?php
            $banned_user_list = get_option('rs_banned-users_list');
            if (is_user_logged_in()) {
//                if (!in_array(get_current_user_id(), (array) $banned_user_list)) {
//
//                    $getarrayofuserdata = get_userdata(get_current_user_id());
//                    $banninguserrole = get_option('rs_banning_user_role');
//                    if (!in_array(isset($getarrayofuserdata->roles[0]) ? $getarrayofuserdata->roles[0] : '0', (array) $banninguserrole)) {
                $userid = get_current_user_id();
                $banning_type = FPRewardSystem::check_banning_type($userid);
                if ($banning_type != 'earningonly' && $banning_type != 'both') {
                    ?>
                                                var splitresponse = response.split('_');
                                                if (splitresponse[0] > 0) {
                                                    jQuery('.variableshopmessage').show();
                                                    jQuery('#value_variable_product').show();
                                                    jQuery('#value_variable_product').html("<?php echo do_shortcode(get_option('rs_message_for_variation_products')); ?>");
                                                    jQuery('.variationrewardpoints').html(splitresponse[0]);
                                                    jQuery('.variationrewardpointsamount').html(splitresponse[1]);
                                                } else {
                                                    jQuery('#value_variable_product').hide();
                                                    jQuery('.variableshopmessage').hide();
                                                }
                    <?php
//                    }
//                }
                }
            } else {
                ?>
                                            var splitresponse = response.split('_');
                                            if (splitresponse[0] > 0) {
                                                jQuery('.variableshopmessage').show();
                                                jQuery('#value_variable_product').show();
                                                jQuery('#value_variable_product').html("<?php echo do_shortcode(get_option('rs_message_for_variation_products')); ?>");
                                                jQuery('.variationrewardpoints').html(splitresponse[0]);
                                                jQuery('.variationrewardpointsamount').html(splitresponse[1]);
                                            } else {
                                                jQuery('#value_variable_product').hide();
                                                jQuery('.variableshopmessage').hide();
                                            }
                <?php
            }
            ?>
                                    }
                                });
                    });
                });</script>
            <?php
        }
    }

    public static function wp_product_variation() {
        if (isset($_POST['variationproductid'])) {
            $checkenable = get_post_meta($_POST['variationproductid'], '_enable_reward_points', true);
            $checkrule = get_post_meta($_POST['variationproductid'], '_select_reward_rule', true);
            $getpoints = get_post_meta($_POST['variationproductid'], '_reward_points', true);
            $getpercent = get_post_meta($_POST['variationproductid'], '_reward_percent', true);
            $global_enable = get_option('rs_global_enable_disable_reward');
            $global_reward_type = get_option('rs_global_reward_type');
            $rewardpoints = array('0');
            if ($checkenable == '1') {
                if ($checkrule == '1') {

                    $variable_product1 = new WC_Product_Variation($_POST['variationproductid']);
                    $newparentid = $variable_product1->parent->id;
                    if (get_option('rs_set_price_percentage_reward_points') == '1') {
                        $variationregularprice = $variable_product1->regular_price;
                    } else {
                        $variationregularprice = $variable_product1->price;
                    }
                    if ($getpoints == '') {
                        $term = get_the_terms($newparentid, 'product_cat');
                        if (is_array($term)) {
                            $rewardpoints = array('0');
                            foreach ($term as $term) {
                                $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                if ($enablevalue == 'yes') {
                                    if ($display_type == '1') {
                                        if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                            if ($global_enable == '1') {
                                                if ($global_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $variationregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        } else {
                                            $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                        }
                                    } else {
                                        $pointconversion = get_option('rs_earn_point');
                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                        $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                        $getaveragepoints = $getaverage * $variationregularprice;
                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                        if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                            $global_enable = get_option('rs_global_enable_disable_reward');
                                            $global_reward_type = get_option('rs_global_reward_type');
                                            if ($global_enable == '1') {
                                                if ($global_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $variationregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        } else {
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                } else {
                                    if ($global_enable == '1') {
                                        if ($global_reward_type == '1') {
                                            $rewardpoints[] = get_option('rs_global_reward_points');
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_option('rs_global_reward_percent') / 100;
                                            $getaveragepoints = $getaverage * $variationregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($global_enable == '1') {
                                if ($global_reward_type == '1') {
                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                } else {
                                    $pointconversion = get_option('rs_earn_point');
                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                    $getaveragepoints = $getaverage * $variationregularprice;
                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                }
                            }
                        }

//var_dump($rewardpoints);
                        $getpoints = max($rewardpoints);

//$rewardpoints = array_search($value, $rewardpoints);
                    }
                    if ($_POST['userid'] > 0) {
                        $getpoints = RSUserRoleRewardPoints::user_role_based_reward_points($_POST['userid'], $getpoints);
                    } else {
                        $getpoints = $getpoints;
                    }
                    $redeemingrspoints = $getpoints / get_option('rs_redeem_point');
                    $updatedredeemingpoints = $redeemingrspoints * get_option('rs_redeem_point_value');
                    $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
                    echo round($getpoints, $roundofftype) . '_' . round($updatedredeemingpoints, $roundofftype);
                } else {
                    $getpercent = $getpercent / 100;
                    $variable_product1 = new WC_Product_Variation($_POST['variationproductid']);
                    if (get_option('rs_set_price_percentage_reward_points') == '1') {
                        $variationregularprice = $variable_product1->regular_price;
                    } else {
                        $variationregularprice = $variable_product1->price;
                    }
                    $getpercent = $getpercent * $variationregularprice;
                    $pointconversion = get_option('rs_earn_point');
                    $pointconversionvalue = get_option('rs_earn_point_value');
                    $pointswithvalue = $getpercent * $pointconversion;

                    $rsoutput = $pointswithvalue / $pointconversionvalue;
                    if ($getpercent == '') {
                        $term = get_the_terms($newparentid, 'product_cat');
//var_dump($term);
                        if (is_array($term)) {
//var_dump($term);
                            $rewardpoints = array('0');
                            foreach ($term as $term) {

                                $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                if ($enablevalue == 'yes') {
                                    if ($display_type == '1') {
                                        if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                            $global_enable = get_option('rs_global_enable_disable_reward');
                                            $global_reward_type = get_option('rs_global_reward_type');
                                            if ($global_enable == '1') {
                                                if ($global_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $variationregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        } else {
                                            $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                        }
                                    } else {
                                        $pointconversion = get_option('rs_earn_point');
                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                        $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                        $getaveragepoints = $getaverage * $variationregularprice;
                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                        if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                            $global_enable = get_option('rs_global_enable_disable_reward');
                                            $global_reward_type = get_option('rs_global_reward_type');
                                            if ($global_enable == '1') {
                                                if ($global_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $variationregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        } else {
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                } else {
                                    if ($global_enable == '1') {
                                        if ($global_reward_type == '1') {
                                            $rewardpoints[] = get_option('rs_global_reward_points');
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_option('rs_global_reward_percent') / 100;
                                            $getaveragepoints = $getaverage * $variationregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($global_enable == '1') {
                                if ($global_reward_type == '1') {
                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                } else {
                                    $pointconversion = get_option('rs_earn_point');
                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                    $getaveragepoints = $getaverage * $variationregularprice;
                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                }
                            }
                        }
//var_dump($rewardpoints);
                        $rsoutput = max($rewardpoints);
                    }

                    if ($_POST['userid'] > 0) {
                        $rsoutput = RSUserRoleRewardPoints::user_role_based_reward_points($_POST['userid'], $rsoutput);
                    } else {
                        $rsoutput = $rsoutput;
                    }
                    $redeemingrspoints = $rsoutput / get_option('rs_redeem_point');
                    $updatedredeemingpoints = $redeemingrspoints * get_option('rs_redeem_point_value');
                    $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
                    echo round($rsoutput, $roundofftype) . '_' . round($updatedredeemingpoints, $roundofftype);
                }
            } else {
                echo "0_0";
            }
        }
        exit();
    }

    public static function get_term_meta_info() {
        global $post;
        $term = get_the_terms($post->ID, 'product_cat');
        $rewardenablesingle = get_post_meta($post->ID, '_rewardsystemcheckboxvalue', true);
        $rewardsystem_optionssingle = get_post_meta($post->ID, '_rewardsystem_options', true);
        $rewardsystem_pointssingle = get_post_meta($post->ID, '_rewardsystempoints', true);
        if ($rewardenablesingle == 'yes') {
            if ($rewardsystem_optionssingle == '1') {
                $rewardcheckfeasibility = get_post_meta($post->ID, '_rewardsystempoints', true);
            } else {
                $rewardcheckfeasibility = get_post_meta($post->ID, '_rewardsystempercent', true);
            }
            if ($rewardcheckfeasibility == '') {
                if (is_array($term)) {
                    foreach ($term as $term) {
                        echo $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true) . "<br>";
                        echo $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true) . "<br>";
                        echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
                        echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                    }
                }
            }
        }
    }

    public static function reward_points_in_top_of_content() {
        global $checkproduct;
//        $banned_user_list = get_option('rs_banned-users_list');
//        if (!in_array(get_current_user_id(), (array) $banned_user_list)) {
//            $getarrayofuserdata = get_userdata(get_current_user_id());
//            $banninguserrole = get_option('rs_banning_user_role');
//            if (!in_array(isset($getarrayofuserdata->roles[0]) ? $getarrayofuserdata->roles[0] : '0', (array) $banninguserrole)) {
        $userid = get_current_user_id();
        $banning_type = FPRewardSystem::check_banning_type($userid);
        if ($banning_type != 'earningonly' && $banning_type != 'both') {
            global $messageglobal;
            if (is_user_logged_in()) {
                global $totalrewardpointsnew;
                global $totalrewardpoints;
                $rewardpoints = array('0');
                $totalrewardpoints;
                global $woocommerce;
                global $value;
                $global_enable = get_option('rs_global_enable_disable_reward');
                $global_reward_type = get_option('rs_global_reward_type');

                //var_dump($woocommerce->cart->cart_contents);

                foreach ($woocommerce->cart->cart_contents as $key => $value) {
                    $cartquantity = $value['quantity'];
                    $rewardspoints = get_post_meta($value['product_id'], '_rewardsystempoints', true);
                    $checkenable = get_post_meta($value['product_id'], '_rewardsystemcheckboxvalue', true);
                    $checkenablevariation = get_post_meta($value['variation_id'], '_enable_reward_points', true);
                    $variablerewardpoints = get_post_meta($value['variation_id'], '_reward_points', true);
                    $variationselectrule = get_post_meta($value['variation_id'], '_select_reward_rule', true);
                    $variationrewardpercent = get_post_meta($value['variation_id'], '_reward_percent', true);
                    $variable_product1 = new WC_Product_Variation($value['variation_id']);
#Step 4: You have the data. Have fun :)
                    if (get_option('rs_set_price_percentage_reward_points') == '1') {
                        $variationregularprice = $variable_product1->regular_price;
                    } else {
                        $variationregularprice = $variable_product1->price;
                    }


                    // do_action('rs_price_rule_checker_variant', $variationregularprice, $value);
                    do_action_ref_array('rs_price_rule_checker_variant', array(&$variationregularprice, &$value));

                    $checkruleoption = get_post_meta($value['product_id'], '_rewardsystem_options', true);
                    $checkrewardpercent = get_post_meta($value['product_id'], '_rewardsystempercent', true);
                    if (get_option('rs_set_price_percentage_reward_points') == '1') {
                        $getregularprice = get_post_meta($value['product_id'], '_regular_price', true);
                    } else {
                        $getregularprice = get_post_meta($value['product_id'], '_price', true);
                    }



                    //do_action('rs_price_rule_checker_simple', $getregularprice, $value);
                    do_action_ref_array('rs_price_rule_checker_simple', array(&$getregularprice, &$value));

                    $user_ID = get_current_user_id();
// if ($checkenable == 'yes') {
                    $checkproduct = get_product($value['product_id']);
                    $checkanotherproduct = get_product($value['variation_id']);
                    if ($checkproduct->is_type('simple') || ($checkproduct->is_type('subscription'))) {
                        if ($checkenable == 'yes') {
                            if ($checkruleoption == '1') {
                                if ($rewardspoints == '') {
                                    $term = get_the_terms($value['product_id'], 'product_cat');
// var_dump($term);
                                    if (is_array($term)) {
//var_dump($term);
                                        $rewardpoints = array('0');
                                        foreach ($term as $term) {
                                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                            $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
                                            if ($enablevalue == 'yes') {
                                                if ($display_type == '1') {
                                                    $checktermpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                                    if ($checktermpoints == '') {

                                                        if ($global_enable == '1') {
                                                            if ($global_reward_type == '1') {
                                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                                            } else {
                                                                $pointconversion = get_option('rs_earn_point');
                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                                $getaveragepoints = $getaverage * $getregularprice;
                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                            }
                                                        }
                                                    } else {
                                                        $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                                    }
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                                        $global_enable = get_option('rs_global_enable_disable_reward');
                                                        $global_reward_type = get_option('rs_global_reward_type');
                                                        if ($global_enable == '1') {
                                                            if ($global_reward_type == '1') {
                                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                                            } else {
                                                                $pointconversion = get_option('rs_earn_point');
                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                                $getaveragepoints = $getaverage * $getregularprice;
                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                            }
                                                        }
                                                    } else {
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        if ($global_enable == '1') {
                                            if ($global_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    }
//var_dump($rewardpoints);
                                    if (!empty($rewardpoints)) {
                                        $rewardspoints = max($rewardpoints);
                                    }
//$rewardpoints = array_search($value, $r   ewardpoints);
                                }
                                $totalrewardpoints = RSUserRoleRewardPoints::user_role_based_reward_points(get_current_user_id(), $rewardspoints) * $cartquantity;

//  echo do_shortcode(get_option('rs_message_product_in_cart')) . "<br>";
//echo "Your Current Point >>>>>>>>>>>>>>>" . get_user_meta($user_ID, '_my_reward_points', true);
                                $totalrewardpointsnew[$value['product_id']] = $totalrewardpoints;
                            } else {
                                $pointconversion = get_option('rs_earn_point');
                                $pointconversionvalue = get_option('rs_earn_point_value');
                                $getaverage = $checkrewardpercent / 100;
                                $getaveragepoints = $getaverage * $getregularprice;
                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                $points = $pointswithvalue / $pointconversionvalue;
                                if ($checkrewardpercent == '') {
                                    $term = get_the_terms($value['product_id'], 'product_cat');
// var_dump($term);
                                    if (is_array($term)) {
//var_dump($term);
                                        $rewardpoints = array('0');
                                        foreach ($term as $term) {

                                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                            $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                            if ($enablevalue == 'yes') {
                                                if ($display_type == '1') {
                                                    if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                                        $global_enable = get_option('rs_global_enable_disable_reward');
                                                        $global_reward_type = get_option('rs_global_reward_type');
                                                        if ($global_enable == '1') {
                                                            if ($global_reward_type == '1') {
                                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                                            } else {
                                                                $pointconversion = get_option('rs_earn_point');
                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                                $getaveragepoints = $getaverage * $getregularprice;
                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                            }
                                                        }
                                                    } else {

                                                        $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                                    }
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                                        $global_enable = get_option('rs_global_enable_disable_reward');
                                                        $global_reward_type = get_option('rs_global_reward_type');
                                                        if ($global_enable == '1') {
                                                            if ($global_reward_type == '1') {
                                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                                            } else {
                                                                $pointconversion = get_option('rs_earn_point');
                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                                $getaveragepoints = $getaverage * $getregularprice;
                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                            }
                                                        }
                                                    } else {
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        if ($global_enable == '1') {
                                            if ($global_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    }
//var_dump($rewardpoints);
                                    $points = max($rewardpoints);

//$rewardpoints = array_search($value, $rewardpoints);
                                }

                                $totalrewardpoints = RSUserRoleRewardPoints::user_role_based_reward_points(get_current_user_id(), $points) * $cartquantity;
// echo do_shortcode(get_option('rs_message_product_in_cart')) . "<br>";
//echo "Your Current Point >>>>>>>>>>>>>>>>" . get_user_meta($user_ID, '_my_reward_points', true);
                                $totalrewardpointsnew[$value['product_id']] = $totalrewardpoints;
                            }
                        }
                    } else {
                        if ($checkenablevariation == '1') {
                            if ($variationselectrule == '1') {
                                $parentvariationid = new WC_Product_Variation($value['variation_id']);
                                $newparentid = $parentvariationid->parent->id;
                                if ($variablerewardpoints == '') {
                                    $term = get_the_terms($newparentid, 'product_cat');
//var_dump($term);
                                    if (is_array($term)) {
//var_dump($term);
                                        $rewardpoints = array('0');
                                        foreach ($term as $term) {

                                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                            $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                            if ($enablevalue == 'yes') {
                                                if ($display_type == '1') {
                                                    if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                                        $global_enable = get_option('rs_global_enable_disable_reward');
                                                        $global_reward_type = get_option('rs_global_reward_type');
                                                        if ($global_enable == '1') {
                                                            if ($global_reward_type == '1') {
                                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                                            } else {
                                                                $pointconversion = get_option('rs_earn_point');
                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                            }
                                                        }
                                                    } else {
                                                        $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                                    }
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                                    $getaveragepoints = $getaverage * $variationregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                                        $global_enable = get_option('rs_global_enable_disable_reward');
                                                        $global_reward_type = get_option('rs_global_reward_type');
                                                        if ($global_enable == '1') {
                                                            if ($global_reward_type == '1') {
                                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                                            } else {
                                                                $pointconversion = get_option('rs_earn_point');
                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                            }
                                                        }
                                                    } else {
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $variationregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        if ($global_enable == '1') {
                                            if ($global_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    }
//var_dump($rewardpoints);
                                    $variablerewardpoints = max($rewardpoints);

//$rewardpoints = array_search($value, $rewardpoints);
                                }
                                $totalrewardpoints = RSUserRoleRewardPoints::user_role_based_reward_points(get_current_user_id(), $variablerewardpoints) * $cartquantity;
//  echo do_shortcode('[titleofproduct]') . "Complete the Purchase to Earn " . do_shortcode('[rspoint]') . " Points" . "<br/>";
//echo "Your Current Point >>>>>>>>>>>>>>>" . get_user_meta($user_ID, '_my_reward_points', true);
                                $totalrewardpointsnew[$value['product_id']] = $totalrewardpoints;
                            } else {
                                $pointconversion = get_option('rs_earn_point');
                                $pointconversionvalue = get_option('rs_earn_point_value');
                                $getaverage = $variationrewardpercent / 100;
                                $getaveragepoints = $getaverage * $variationregularprice;
                                $getpointsvalue = $getaveragepoints * $pointconversion;
                                $points = $getpointsvalue / $pointconversionvalue;
                                $parentvariationid = new WC_Product_Variation($value['variation_id']);
                                $newparentid = $parentvariationid->parent->id;
                                if ($variationrewardpercent == '') {
                                    $term = get_the_terms($newparentid, 'product_cat');
//var_dump($term);
                                    if (is_array($term)) {
//var_dump($term);
                                        $rewardpoints = array('0');
                                        foreach ($term as $term) {

                                            $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                            $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                            if ($enablevalue == 'yes') {
                                                if ($display_type == '1') {
                                                    if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                                        $global_enable = get_option('rs_global_enable_disable_reward');
                                                        $global_reward_type = get_option('rs_global_reward_type');
                                                        if ($global_enable == '1') {
                                                            if ($global_reward_type == '1') {
                                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                                            } else {
                                                                $pointconversion = get_option('rs_earn_point');
                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                            }
                                                        }
                                                    } else {
                                                        $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                                    }
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                                    $getaveragepoints = $getaverage * $variationregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                                        $global_enable = get_option('rs_global_enable_disable_reward');
                                                        $global_reward_type = get_option('rs_global_reward_type');
                                                        if ($global_enable == '1') {
                                                            if ($global_reward_type == '1') {
                                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                                            } else {
                                                                $pointconversion = get_option('rs_earn_point');
                                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                            }
                                                        }
                                                    } else {
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $variationregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        if ($global_enable == '1') {
                                            if ($global_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    }
//var_dump($rewardpoints);
                                    $points = max($rewardpoints);

//$rewardpoints = array_search($value, $rewardpoints);
                                }

                                $totalrewardpoints = RSUserRoleRewardPoints::user_role_based_reward_points(get_current_user_id(), $points) * $cartquantity;
//echo do_shortcode(get_option('rs_message_product_in_cart')) . "<br>";
//echo "Your Current Point >>>>>>>>>>>>>>>>>" . get_user_meta($user_ID, '_my_reward_points', true);
                                $totalrewardpointsnew[$value['product_id']] = $totalrewardpoints;
                            }
                        }
                    }
                    if ($checkproduct->is_type('simple') || ($checkproduct->is_type('subscription'))) {
                        if ($checkenable == 'yes') {
                            $validrewardpoints = do_shortcode('[rspoint]');
                            if ($validrewardpoints > 0) {
                                $messageglobal[$value['product_id']] = do_shortcode(get_option('rs_message_product_in_cart')) . "<br>";
                            }
                        }
                    } else {
                        $validrewardpoints = do_shortcode('[rspoint]');
                        if ($validrewardpoints > 0) {
                            if ($checkenablevariation == '1') {
                                $messageglobal[$value['product_id']] = do_shortcode(get_option('rs_message_product_in_cart')) . "<br>";
                            }
                        }
                    }
//}
                }
                ?>

                <?php
            }
//            }
//        }
        }
    }

    public static function rewardmessage_in_cart_page() {
        global $woocommerce;
        global $value;
        global $totalrewardpointsnew;
        global $messageglobal;
        if (get_option('rs_show_hide_message_for_each_products') == '1') {
            if (is_array($totalrewardpointsnew)) {
                if (array_sum($totalrewardpointsnew) > 0) {
// var_dump($messageglobal);
                    ?>

                    <div class="woocommerce-info">
                        <?php
                        if (is_array($messageglobal)) {
                            foreach ($messageglobal as $globalcommerce) {
                                echo $globalcommerce;
                            }
                        }
                        ?>
                    </div>
                    <?php
                }
            }
        }
    }

    public static function rewardmessage_in_checkout_page() {
        global $woocommerce;
        global $value;
        global $totalrewardpointsnew;
        global $messageglobal;
        if (get_option('rs_show_hide_message_for_each_products_checkout_page') == '1') {
            if (is_array($totalrewardpointsnew)) {
                if (array_sum($totalrewardpointsnew) > 0) {
// var_dump($messageglobal);
                    ?>

                    <div class="woocommerce-info">
                        <?php
                        if (is_array($messageglobal)) {
                            foreach ($messageglobal as $globalcommerce) {
                                echo $globalcommerce;
                            }
                        }
                        ?>
                    </div>
                    <?php
                }
            }
        }
    }

    public static function add_redeem_point() {
        global $woocommerce;
        global $value;

        //   var_dump($woocommerce->cart->coupon_discount_amounts['admin']);
//        $currentuserid = get_current_user_id();

        $user_ID = get_current_user_id();

        // $coupon_code = get_user_meta($user_ID, 'nickname', true); // Code
        $getinfousernickname = get_user_by('id', $user_ID);
        $couponcodeuserlogin = $getinfousernickname->user_login;

        $usernickname = 'sumo_' . strtolower("$couponcodeuserlogin");
        if (isset($woocommerce->cart->coupon_discount_amounts["$usernickname"])) {
            $total = $woocommerce->cart->coupon_discount_amounts[$usernickname];

            $current_conversion = get_option('rs_redeem_point');
            $point_amount = get_option('rs_redeem_point_value');
            $newtotal = $total * $current_conversion;
            $newtotal = $newtotal / $point_amount;

            $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
            return round($newtotal, $roundofftype);
        }
    }

    public static function add_redeem_header_message_cart_page() {
        global $woocommerce;
        if (is_user_logged_in()) {
            if (get_option('rs_show_hide_message_for_redeem_points') == '1') {
                if (is_array($woocommerce->cart->get_applied_coupons())) {
                    $currentuserid = get_current_user_id();

                    $user_ID = get_current_user_id();

                    // $coupon_code = get_user_meta($user_ID, 'nickname', true); // Code
                    $getinfousernickname = get_user_by('id', $user_ID);
                    $couponcodeuserlogin = $getinfousernickname->user_login;

                    // $usernickname = 'sumo_'.strtolower("$couponcodeuserlogin");

                    $usernickname = 'sumo_' . strtolower("$couponcodeuserlogin");
                    if (isset($woocommerce->cart->coupon_discount_amounts["$usernickname"])) {
                        $total = $woocommerce->cart->coupon_discount_amounts["$usernickname"];
                        if ($total != 0) {
                            foreach ($woocommerce->cart->get_applied_coupons() as $coupons) {
                                if (strtolower($coupons) == $usernickname) {

//                                    $banned_user_list = get_option('rs_banned-users_list');
//                                    if (!in_array(get_current_user_id(), (array) $banned_user_list)) {
//                                        $getarrayofuserdata = get_userdata(get_current_user_id());
//                                        $banninguserrole = get_option('rs_banning_user_role');
//                                        if (!in_array(isset($getarrayofuserdata->roles[0]) ? $getarrayofuserdata->roles[0] : '0', (array) $banninguserrole)) {
                                    $userid = get_current_user_id();
                                    $banning_type = FPRewardSystem::check_banning_type($userid);
                                    if ($banning_type != 'redeemingonly' && $banning_type != 'both') {
                                        ?>
                                        <div class="woocommerce-message">
                                            <?php echo do_shortcode(get_option('rs_message_user_points_redeemed')); ?>
                                        </div>
                                        <?php
                                        if (get_option('rs_redeem_field_type_option') == '2') {
                                            ?>
                                            <script type="text/javascript">
                                                jQuery(document).ready(function () {
                                                    jQuery("#mainsubmi").parent().hide();
                                                });</script>
                                            <?php
                                        }
//                                        }
//                                    }
                                    }
                                    ?>
                                    <?php
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static function add_redeem_header_message_checkout_page() {
        global $woocommerce;
        if (get_option('rs_show_hide_message_for_redeem_points_checkout_page') == '1') {
            if (is_user_logged_in()) {
                if (is_array($woocommerce->cart->get_applied_coupons())) {
                    $currentuserid = get_current_user_id();

                    $user_ID = get_current_user_id();

                    // $coupon_code = get_user_meta($user_ID, 'nickname', true); // Code
                    $getinfousernickname = get_user_by('id', $user_ID);
                    $couponcodeuserlogin = $getinfousernickname->user_login;

                    // $usernickname = 'sumo_'.strtolower("$couponcodeuserlogin");

                    $usernickname = 'sumo_' . strtolower("$couponcodeuserlogin");
                    if (isset($woocommerce->cart->coupon_discount_amounts["$usernickname"])) {
                        $total = $woocommerce->cart->coupon_discount_amounts["$usernickname"];
                        if ($total != 0) {
                            foreach ($woocommerce->cart->get_applied_coupons() as $coupons) {
                                if (strtolower($coupons) == $usernickname) {

//                                    $banned_user_list = get_option('rs_banned-users_list');
//                                    if (!in_array(get_current_user_id(), (array) $banned_user_list)) {
//                                        $getarrayofuserdata = get_userdata(get_current_user_id());
//                                        $banninguserrole = get_option('rs_banning_user_role');
//                                        if (!in_array(isset($getarrayofuserdata->roles[0]) ? $getarrayofuserdata->roles[0] : '0', (array) $banninguserrole)) {
                                    $userid = get_current_user_id();
                                    $banning_type = FPRewardSystem::check_banning_type($userid);
                                    if ($banning_type != 'redeemingonly' && $banning_type != 'both') {
                                        ?>
                                        <div class="woocommerce-message">
                                            <?php echo do_shortcode(get_option('rs_message_user_points_redeemed')); ?>
                                        </div>
                                        <?php
                                        if (get_option('rs_redeem_field_type_option') == '2') {
                                            ?>
                                            <script type="text/javascript">
                                                jQuery(document).ready(function () {
                                                    jQuery("#mainsubmi").parent().hide();
                                                });</script>
                                            <?php
                                        }
//                                        }
//                                    }
                                    }
                                    ?>
                                    <?php
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static function redeem_points_user_control() {
        global $woocommerce;

        $currentuserid = get_current_user_id();

        $user_ID = get_current_user_id();

        // $coupon_code = get_user_meta($user_ID, 'nickname', true); // Code
        $getinfousernickname = get_user_by('id', $user_ID);
        $couponcodeuserlogin = $getinfousernickname->user_login;

        // $usernickname = 'sumo_'.strtolower("$couponcodeuserlogin");

        $usernickname = 'sumo_' . strtolower("$couponcodeuserlogin");

        // $usernickname = strtolower(str_replace(' ', '', get_user_meta($currentuserid, 'nickname', true)));
        if (isset($woocommerce->cart->coupon_discount_amounts["$usernickname"])) {
            $total = $woocommerce->cart->coupon_discount_amounts[$usernickname];
			//print_r($total);exit;
            $current_conversion = get_option('rs_redeem_point');
            $point_amount = get_option('rs_redeem_point_value');
            $total = $total * $current_conversion;
            $total = $total / $point_amount;
//var_dump($total);
            $user_ID = get_current_user_id();
            $myrewardpoint = get_user_meta($user_ID, '_my_reward_points', true);
            $majorpoint = $myrewardpoint - $total;

            $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
            return round($majorpoint, $roundofftype);
        }
    }

    public static function add_shortcode_cart_id() {
        global $checkproduct;
        global $value;
        if ($checkproduct->is_type('simple') || ($checkproduct->is_type('subscription'))) {
            return "<strong>" . get_the_title($value['product_id']) . "</strong>";
        } else if ($checkproduct->is_type('variable') || ($checkproduct->is_type('variable-subscription'))) {
            return "<strong>" . get_the_title($value['variation_id']) . "</strong>";
        }
    }

    public static function get_each_product_price() {
        global $totalrewardpoints;
        global $checkproduct;
        global $value;
        if ($checkproduct->is_type('simple') || ($checkproduct->is_type('subscription'))) {
            if (get_post_meta($value['product_id'], '_rewardsystemcheckboxvalue', true) != 'yes') {
                return "<strong>0</strong>";
            } else {
                $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
                return round($totalrewardpoints, $roundofftype);
            }
        } else {
            if (get_post_meta($value['variation_id'], '_enable_reward_points', true) != '1') {
                return "<strong>0</strong>";
            } else {
                $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
                return round($totalrewardpoints, $roundofftype);
            }
        }
    }

    public static function cart_each_product_points_value() {
        $getpoints = do_shortcode('[rspoint]');
        $redeemconver = $getpoints / get_option('rs_redeem_point');
        $updatedvalue = $redeemconver * get_option('rs_redeem_point_value');
        $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
        return get_woocommerce_currency_symbol() . round($updatedvalue, $roundofftype);
    }

    public static function totalrewardpoints_cart_page() {
        global $totalrewardpointsnew;
        if (is_user_logged_in()) {
            if (get_option('rs_show_hide_message_for_total_points') == '1') {
                if (is_array($totalrewardpointsnew)) {
                    if (array_sum($totalrewardpointsnew) > 0) {
                        $totalrewardpoints = do_shortcode('[totalrewards]');
                        if ($totalrewardpoints > 0) {
                            ?>
                            <div class="woocommerce-info">
                                <?php
                                echo do_shortcode(get_option('rs_message_total_price_in_cart'));
                                ?>
                            </div>
                            <?php
                        }
                    }
                }
            }
        }
    }

    public static function totalrewardpoints_checkout_page() {
        global $totalrewardpointsnew;
        if (is_user_logged_in()) {
            if (get_option('rs_show_hide_message_for_total_points_checkout_page') == '1') {
                if (is_array($totalrewardpointsnew)) {
                    if (array_sum($totalrewardpointsnew) > 0) {
                        $totalrewardpoints = do_shortcode('[totalrewards]');
                        if ($totalrewardpoints > 0) {
                            ?>
                            <div class="woocommerce-info">
                                <?php
                                echo do_shortcode(get_option('rs_message_total_price_in_cart'));
                                ?>
                            </div>
                            <?php
                        }
                    }
                }
            }
        }
    }

    public static function getshortcodetotal_rewards() {
        global $totalrewardpointsnew;
        if (is_array($totalrewardpointsnew)) {
            $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
            return round(array_sum($totalrewardpointsnew), $roundofftype);
        } else {
            return "<strong> 0 </strong>";
        }
    }

    public static function getvalueshortcodetotal_rewards() {
        $getrstotal = do_shortcode('[totalrewards]');
        $getcals = $getrstotal / get_option('rs_redeem_point');
        $updatedvalue = $getcals * get_option('rs_redeem_point_value');
        $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
        return get_woocommerce_currency_symbol() . round($updatedvalue, $roundofftype);
    }

    public static function your_current_points_cart_page() {
        if (get_option('rs_show_hide_message_for_my_rewards') == '1') {
            if (is_user_logged_in()) {
                $user_ID = get_current_user_id();
                if (get_user_meta($user_ID, '_my_reward_points', true) > 0) {

//                    $banned_user_list = get_option('rs_banned-users_list');
//                    if (!in_array(get_current_user_id(), (array) $banned_user_list)) {
//                        $getarrayofuserdata = get_userdata(get_current_user_id());
//                        $banninguserrole = get_option('rs_banning_user_role');
//                        if (!in_array(isset($getarrayofuserdata->roles[0]) ? $getarrayofuserdata->roles[0] : '0', (array) $banninguserrole)) {
                    $userid = get_current_user_id();
                    $banning_type = FPRewardSystem::check_banning_type($userid);
                    if ($banning_type != 'earningonly' && $banning_type != 'both') {
                        ?>
                        <div class="woocommerce-info">
                            <?php
                            $user_ID = get_current_user_id();
                            echo do_shortcode(get_option('rs_message_user_points_in_cart'));
                            ?>
                        </div>
                        <?php
//                        }
//                    }
                    }
                    ?>
                    <?php
                }
            }
        }
    }

    public static function your_current_points_checkout_page() {
        if (get_option('rs_show_hide_message_for_my_rewards_checkout_page') == '1') {
            if (is_user_logged_in()) {
                $user_ID = get_current_user_id();
                if (get_user_meta($user_ID, '_my_reward_points', true) > 0) {

//                    $banned_user_list = get_option('rs_banned-users_list');
//                    if (!in_array(get_current_user_id(), (array) $banned_user_list)) {
//                        $getarrayofuserdata = get_userdata(get_current_user_id());
//                        $banninguserrole = get_option('rs_banning_user_role');
//                        if (!in_array(isset($getarrayofuserdata->roles[0]) ? $getarrayofuserdata->roles[0] : '0', (array) $banninguserrole)) {
                    $userid = get_current_user_id();
                    $banning_type = FPRewardSystem::check_banning_type($userid);
                    if ($banning_type != 'earningonly' && $banning_type != 'both') {
                        ?>
                        <div class="woocommerce-info">
                            <?php
                            $user_ID = get_current_user_id();
                            echo do_shortcode(get_option('rs_message_user_points_in_cart'));
                            ?>
                        </div>
                        <?php
//                        }
//                    }
                    }
                    ?>
                    <?php
                }
            }
        }
    }

    public static function add_shortcode_for_user_points() {
        $user_ID = get_current_user_id();
        $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
        return "<strong>" . round(get_user_meta($user_ID, '_my_reward_points', true), $roundofftype) . "</strong>";
    }

    public static function add_shortcode_for_user_points_value() {
        $user_ID = get_current_user_id();
        $current_user_points = get_user_meta($user_ID, '_my_reward_points', true);
        $pointconversion = get_option('rs_redeem_point');
        $pointconversionvalue = get_option('rs_redeem_point_value');
        $pointswithvalue = $current_user_points / $pointconversion;
        $rewardpoints_amount = $pointswithvalue * $pointconversionvalue;
        $currency_symbol = get_woocommerce_currency_symbol();
        $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
        return $currency_symbol . round($rewardpoints_amount, $roundofftype);
    }

    public static function show_message_for_guest_cart_page() {
        if (!is_user_logged_in()) {
            if (get_option('rs_show_hide_message_for_guest') == '1') {
                ?>
                <div class="woocommerce-info"><?php echo do_shortcode(get_option('rs_message_for_guest_in_cart')); ?></div>
                <?php
            }
        }
    }

    public static function show_message_for_guest_checkout_page() {
        if (!is_user_logged_in()) {
            if (get_option('rs_show_hide_message_for_guest_checkout_page') == '1') {
                ?>
                <div class="woocommerce-info"><?php echo do_shortcode(get_option('rs_message_for_guest_in_cart')); ?></div>
                <?php
            }
        }
    }

    public static function update_reward_points() {
        global $woocommerce;
        if (isset($woocommerce->cart->cart_contents)) {
            ?>
            <div class="header_cart">
                <div class="cart_contents">
                    <a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'rewardsystem'); ?>"><?php sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'rewardsystem'), $woocommerce->cart->cart_contents_count); ?> <?php //echo $woocommerce->cart->cart_contents->product_id;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ?></a>
                </div>
            </div>
            <?php
        }
    }

    public static function add_category_field() {
        ?>
        <div class="form-field">
            <label for="enable_reward_system_category"><?php _e('Enable SUMO Reward Points for Product Purchase', 'rewardsystem'); ?></label>
        <!--            <input type="checkbox" style="width:0px;" name="enable_reward_system_category" value="yes"/>-->
            <select id="enable_reward_system_category" name="enable_reward_system_category" class="postform">
                <option value="yes"><?php _e('Enable', 'rewardsystem'); ?></option>
                <option value="no"><?php _e('Disable', 'rewardsystem'); ?></option>
            </select>
        </div>
        <div class="form-field">
            <label for="enable_rs_rule"><?php _e('Reward Type', 'rewardsystem'); ?></label>
            <select id="enable_rs_rule" name="enable_rs_rule" class="postform">
                <option value="1"><?php _e('By Fixed Reward Points', 'rewardsystem'); ?></option>
                <option value="2"><?php _e('By Percentage of Product Price', 'rewardsystem'); ?></option>
            </select>
        </div>
        <div class="form-field">
            <label for="rs_category_points"><?php _e('Reward Points', 'rewardsystem'); ?></label>
            <input type="text" name="rs_category_points" id="rs_category_points" value=""/>
        </div>
        <div class="form-field">
            <label for="rs_category_percent"><?php _e('Reward Percent in %', 'rewardsystem'); ?></label>
            <input type="text" name="rs_category_percent" id="rs_category_percent" value=""/>
        </div>
        <div class="form-field">
            <label for="referral_enable_rs_rule"><?php _e('Referral Reward Type', 'rewardsystem'); ?></label>
            <select id="referral_enable_rs_rule" name="referral_enable_rs_rule" class="postform">
                <option value="1"><?php _e('By Fixed Reward Points', 'rewardsystem'); ?></option>
                <option value="2"><?php _e('By Percentage of Product Price', 'rewardsystem'); ?></option>
            </select>
        </div>
        <div class="form-field">
            <label for="referral_rs_category_points"><?php _e('Referral Reward Points', 'rewardsystem'); ?></label>
            <input type="text" name="referral_rs_category_points" id="referral_rs_category_points" value=""/>
        </div>
        <div class="form-field">
            <label for="referral_rs_category_percent"><?php _e('Reward Percent in %', 'rewardsystem'); ?></label>
            <input type="text" name="referral_rs_category_percent" id="referral_rs_category_percent" value=""/>
        </div>

        <div class="form-field">
            <label for="enable_social_reward_system_category"><?php _e('Enable SUMO Reward Points for Social Promotion', 'rewardsystem'); ?></label>
            <select id="enable_social_reward_system_category" name="enable_social_reward_system_category" class="postform">
                <option value="yes"><?php _e('Enable', 'rewardsystem'); ?></option>
                <option value="no"><?php _e('Disable', 'rewardsystem'); ?></option>
            </select>
        </div>
        <!-- Social Rewards Field for Facebook in Category Start -->
        <div class="form-field">
            <label for="social_facebook_enable_rs_rule"><?php _e('Social Reward Type for Facebook', 'rewardsystem'); ?></label>
            <select id="social_facebook_enable_rs_rule" name="social_facebook_enable_rs_rule" class="postform">
                <option value="1"><?php _e('By Fixed Reward Points', 'rewardsystem'); ?></option>
                <option value="2"><?php _e('By Percentage of Product Price', 'rewardsystem'); ?></option>
            </select>
        </div>
        <div class="form-field">
            <label for="social_facebook_rs_category_points"><?php _e('Social Facebook Reward Points', 'rewardsystem'); ?></label>
            <input type="text" name="social_facebook_rs_category_points" id="social_facebook_rs_category_points" value=""/>
        </div>
        <div class="form-field">
            <label for="social_facebook_rs_category_percent"><?php _e('Social Facebook Reward in Percent %'); ?></label>
            <input type="text" name="social_facebook_rs_category_percent" id="social_facebook_rs_category_percent" value=""/>
        </div>
        <!-- Social Rewards Field for Facebook in Category which is End -->


        <!-- Social Rewards Field for Twitter in Category Start -->
        <div class="form-field">
            <label for="social_twitter_enable_rs_rule"><?php _e('Social Reward Type for Twitter', 'rewardsystem'); ?></label>
            <select id="social_twitter_enable_rs_rule" name="social_twitter_enable_rs_rule" class="postform">
                <option value="1"><?php _e('By Fixed Reward Points', 'rewardsystem'); ?></option>
                <option value="2"><?php _e('By Percentage of Product Price', 'rewardsystem'); ?></option>
            </select>
        </div>
        <div class="form-field">
            <label for="social_twitter_rs_category_points"><?php _e('Social Twitter Reward Points', 'rewardsystem'); ?></label>
            <input type="text" name="social_twitter_rs_category_points" id="social_twitter_rs_category_points" value=""/>
        </div>
        <div class="form-field">
            <label for="social_twitter_rs_category_percent"><?php _e('Social Twitter Reward in Percent %'); ?></label>
            <input type="text" name="social_twitter_rs_category_percent" id="social_twitter_rs_category_percent" value=""/>
        </div>
        <!-- Social Rewards Field for Twitter in Category which is End -->

        <!-- Social Rewards Field for Google in Category Start -->
        <div class="form-field">
            <label for="social_google_enable_rs_rule"><?php _e('Social Reward Type for Google', 'rewardsystem'); ?></label>
            <select id="social_google_enable_rs_rule" name="social_google_enable_rs_rule" class="postform">
                <option value="1"><?php _e('By Fixed Reward Points', 'rewardsystem'); ?></option>
                <option value="2"><?php _e('By Percentage of Product Price', 'rewardsystem'); ?></option>
            </select>
        </div>
        <div class="form-field">
            <label for="social_google_rs_category_points"><?php _e('Social Google Reward Points', 'rewardsystem'); ?></label>
            <input type="text" name="social_google_rs_category_points" id="social_google_rs_category_points" value=""/>
        </div>
        <div class="form-field">
            <label for="social_google_rs_category_percent"><?php _e('Social Google Reward in Percent %'); ?></label>
            <input type="text" name="social_google_rs_category_percent" id="social_google_rs_category_percent" value=""/>
        </div>
        <!-- Social Rewards Field for Google in Category which is End -->


        <?php
    }

    public static function edit_category_field($term, $taxonomy) {
        $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
        $enablesocialvalue = get_woocommerce_term_meta($term->term_id, 'enable_social_reward_system_category', true);
        $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
        $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
        $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true);
        $referralrewardpoints = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true);
        $referralrewardpercent = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true);
        $referralrewardrule = get_woocommerce_term_meta($term->term_id, 'referral_enable_rs_rule', true);

        $socialfacebooktype = get_woocommerce_term_meta($term->term_id, 'social_facebook_enable_rs_rule', true);
        $socialfacebookpoints = get_woocommerce_term_meta($term->term_id, 'social_facebook_rs_category_points', true);
        $socialfacebookpercent = get_woocommerce_term_meta($term->term_id, 'social_facebook_rs_category_percent', true);

        $socialtwittertype = get_woocommerce_term_meta($term->term_id, 'social_twitter_enable_rs_rule', true);
        $socialtwitterpoints = get_woocommerce_term_meta($term->term_id, 'social_twitter_rs_category_points', true);
        $socialtwitterpercent = get_woocommerce_term_meta($term->term_id, 'social_twitter_rs_category_percent', true);

        $socialgoogletype = get_woocommerce_term_meta($term->term_id, 'social_google_enable_rs_rule', true);
        $socialgooglepoints = get_woocommerce_term_meta($term->term_id, 'social_google_rs_category_points', true);
        $socialgooglepercent = get_woocommerce_term_meta($term->term_id, 'social_google_rs_category_percent', true);
        ?>
        <tr class="form-field">
            <th scope="row" valign="top"><label><?php _e('Enable SUMO Reward Points for Product Purchase', 'rewardsystem'); ?></label></th>
            <td>
                <select id="enable_reward_system_category" name="enable_reward_system_category" class="postform">
                    <option value="yes" <?php selected('yes', $enablevalue); ?>><?php _e('Enable', 'rewardsystem'); ?></option>
                    <option value="no" <?php selected('no', $enablevalue); ?>><?php _e('Disable', 'rewardsystem'); ?></option>
                </select>
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row" valign="top"><label><?php _e('Reward Type', 'rewardsystem'); ?></label></th>
            <td>
                <select id="enable_rs_rule" name="enable_rs_rule" class="postform">
                    <option value="1" <?php selected('1', $display_type); ?>><?php _e('By Fixed Reward Points', 'rewardsystem'); ?></option>
                    <option value="2" <?php selected('2', $display_type); ?>><?php _e('By Percentage of Product Price', 'rewardsystem'); ?></option>
                </select>
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row" valign="top"><label><?php _e('Reward Points', 'rewardsystem'); ?></label></th>
            <td>
                <input type="text" name="rs_category_points" id="rs_category_points" value="<?php echo $rewardpoints; ?>"/>
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row" valign="top"><label><?php _e('Reward Percent', 'rewardsystem'); ?></label></th>
            <td>
                <input type="text" name="rs_category_percent" id="rs_category_percent" value="<?php echo $rewardpercent; ?>"/>
            </td>
        </tr>

        <tr class="form-field">
            <th scope="row" valign="top"><label><?php _e('Referral Reward Type', 'rewardsystem'); ?></label></th>
            <td>
                <select id="enable_rs_rule" name="referral_enable_rs_rule" class="postform">
                    <option value="1" <?php selected('1', $referralrewardrule); ?>><?php _e('By Fixed Reward Points', 'rewardsystem'); ?></option>
                    <option value="2" <?php selected('2', $referralrewardrule); ?>><?php _e('By Percentage of Product Price', 'rewardsystem'); ?></option>
                </select>
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row" valign="top"><label><?php _e('Referral Reward Points', 'rewardsystem'); ?></label></th>
            <td>
                <input type="text" name="referral_rs_category_points" id="referral_rs_category_points" value="<?php echo $referralrewardpoints; ?>"/>
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row" valign="top"><label><?php _e('Referral Reward Percent', 'rewardsystem'); ?></label></th>
            <td>
                <input type="text" name="referral_rs_category_percent" id="referral_rs_category_percent" value="<?php echo $referralrewardpercent; ?>"/>
            </td>
        </tr>



        <tr class="form-field">
            <th scope="row" valign="top"><label><?php _e('Enable SUMO Reward Points for Social Promotion', 'rewardsystem'); ?></label></th>
            <td>
                <select id="enable_social_reward_system_category" name="enable_social_reward_system_category" class="postform">
                    <option value="yes" <?php selected('yes', $enablesocialvalue); ?>><?php _e('Enable', 'rewardsystem'); ?></option>
                    <option value="no" <?php selected('no', $enablesocialvalue); ?>><?php _e('Disable', 'rewardsystem'); ?></option>
                </select>
            </td>
        </tr>
        <!-- Below Field is for Facebook Social Rewards in Category Level Start-->
        <tr class="form-field">
            <th scope="row" valign="top"><label><?php _e('Social Reward Type for Facebook', 'rewardsystem'); ?></label></th>
            <td>
                <select id="social_facebook_enable_rs_rule" name="social_facebook_enable_rs_rule" class="postform">
                    <option value="1" <?php selected('1', $socialfacebooktype); ?>><?php _e('By Fixed Reward Points', 'rewardsystem'); ?></option>
                    <option value="2" <?php selected('2', $socialfacebooktype); ?>><?php _e('By Percentage of Product Price', 'rewardsystem'); ?></option>
                </select>
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row" valign="top"><label><?php _e('Social Facebook Reward Points', 'rewardsystem'); ?></label></th>
            <td>
                <input type="text" name="social_facebook_rs_category_points" id="social_facebook_rs_category_points" value="<?php echo $socialfacebookpoints; ?>"/>
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row" valign="top"><label><?php _e('Social Facebook Reward in Percent %', 'rewardsystem'); ?></label></th>
            <td>
                <input type="text" name="social_facebook_rs_category_percent" id="social_facebook_rs_category_percent" value="<?php echo $socialfacebookpercent; ?>"/>
            </td>
        </tr>
        <!-- Below Field is for Facebook Social Rewards in Category Level Ends -->

        <!-- Below Field is for Twitter Social Rewards in Category Level Start-->
        <tr class="form-field">
            <th scope="row" valign="top"><label><?php _e('Social Reward Type for Twitter', 'rewardsystem'); ?></label></th>
            <td>
                <select id="social_twitter_enable_rs_rule" name="social_twitter_enable_rs_rule" class="postform">
                    <option value="1" <?php selected('1', $socialtwittertype); ?>><?php _e('By Fixed Reward Points', 'rewardsystem'); ?></option>
                    <option value="2" <?php selected('2', $socialtwittertype); ?>><?php _e('By Percentage of Product Price', 'rewardsystem'); ?></option>
                </select>
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row" valign="top"><label><?php _e('Social Twitter Reward Points', 'rewardsystem'); ?></label></th>
            <td>
                <input type="text" name="social_twitter_rs_category_points" id="social_twitter_rs_category_points" value="<?php echo $socialtwitterpoints; ?>"/>
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row" valign="top"><label><?php _e('Social Twitter Reward in Percent %', 'rewardsystem'); ?></label></th>
            <td>
                <input type="text" name="social_twitter_rs_category_percent" id="social_twitter_rs_category_percent" value="<?php echo $socialtwitterpercent; ?>"/>
            </td>
        </tr>
        <!-- Below Field is for Twitter Social Rewards in Category Level Ends -->

        <!-- Below Field is for Google Social Rewards in Category Level Start-->
        <tr class="form-field">
            <th scope="row" valign="top"><label><?php _e('Social Reward Type for Google', 'rewardsystem'); ?></label></th>
            <td>
                <select id="social_google_enable_rs_rule" name="social_google_enable_rs_rule" class="postform">
                    <option value="1" <?php selected('1', $socialgoogletype); ?>><?php _e('By Fixed Reward Points', 'rewardsystem'); ?></option>
                    <option value="2" <?php selected('2', $socialgoogletype); ?>><?php _e('By Percentage of Product Price', 'rewardsystem'); ?></option>
                </select>
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row" valign="top"><label><?php _e('Social Google Reward Points', 'rewardsystem'); ?></label></th>
            <td>
                <input type="text" name="social_google_rs_category_points" id="social_twitter_rs_category_points" value="<?php echo $socialgooglepoints; ?>"/>
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row" valign="top"><label><?php _e('Social Google Reward in Percent %', 'rewardsystem'); ?></label></th>
            <td>
                <input type="text" name="social_google_rs_category_percent" id="social_google_rs_category_percent" value="<?php echo $socialgooglepercent; ?>"/>
            </td>
        </tr>
        <!-- Below Field is for Google Social Rewards in Category Level Ends -->


        <?php
    }

    public static function save_category_fields($term_id, $tt_id, $taxonomy) {
        if (isset($_POST['enable_rs_rule']))
            update_woocommerce_term_meta($term_id, 'enable_rs_rule', $_POST['enable_rs_rule']);

        if (isset($_POST['referral_enable_rs_rule']))
            update_woocommerce_term_meta($term_id, 'referral_enable_rs_rule', $_POST['referral_enable_rs_rule']);
//$checkboxvalue = isset($_POST['enable_reward_system_category']) ? 'yes' : 'no';

        if (isset($_POST['enable_reward_system_category']))
            update_woocommerce_term_meta($term_id, 'enable_reward_system_category', $_POST['enable_reward_system_category']);

//update_woocommerce_term_meta($term_id, 'enable_reward_system_category', $checkboxvalue);
        if (isset($_POST['rs_category_points']))
            update_woocommerce_term_meta($term_id, 'rs_category_points', $_POST['rs_category_points']);
        if (isset($_POST['rs_category_percent']))
            update_woocommerce_term_meta($term_id, 'rs_category_percent', $_POST['rs_category_percent']);

        if (isset($_POST['referral_rs_category_points']))
            update_woocommerce_term_meta($term_id, 'referral_rs_category_points', $_POST['referral_rs_category_points']);
        if (isset($_POST['referral_rs_category_percent']))
            update_woocommerce_term_meta($term_id, 'referral_rs_category_percent', $_POST['referral_rs_category_percent']);

//social updation for facebook,twitter,google
        if (isset($_POST['enable_social_reward_system_category']))
            update_woocommerce_term_meta($term_id, 'enable_social_reward_system_category', $_POST['enable_social_reward_system_category']);

        /* Facebook Rule and its Points Start */
        if (isset($_POST['social_facebook_enable_rs_rule']))
            update_woocommerce_term_meta($term_id, 'social_facebook_enable_rs_rule', $_POST['social_facebook_enable_rs_rule']);
        if (isset($_POST['social_facebook_rs_category_points']))
            update_woocommerce_term_meta($term_id, 'social_facebook_rs_category_points', $_POST['social_facebook_rs_category_points']);
        if (isset($_POST['social_facebook_rs_category_percent']))
            update_woocommerce_term_meta($term_id, 'social_facebook_rs_category_percent', $_POST['social_facebook_rs_category_percent']);
        /* Facebook Rule and its Points End */

        /* Twitter Rule and Its Points updation Start */
        if (isset($_POST['social_twitter_enable_rs_rule']))
            update_woocommerce_term_meta($term_id, 'social_twitter_enable_rs_rule', $_POST['social_twitter_enable_rs_rule']);
        if (isset($_POST['social_twitter_rs_category_points']))
            update_woocommerce_term_meta($term_id, 'social_twitter_rs_category_points', $_POST['social_twitter_rs_category_points']);
        if (isset($_POST['social_twitter_rs_category_percent']))
            update_woocommerce_term_meta($term_id, 'social_twitter_rs_category_percent', $_POST['social_twitter_rs_category_percent']);
        /* Twitter Rule and Its Points Updation End */


        /* Google Rule and Its Points updation Start */
        if (isset($_POST['social_google_enable_rs_rule']))
            update_woocommerce_term_meta($term_id, 'social_google_enable_rs_rule', $_POST['social_google_enable_rs_rule']);
        if (isset($_POST['social_google_rs_category_points']))
            update_woocommerce_term_meta($term_id, 'social_google_rs_category_points', $_POST['social_google_rs_category_points']);
        if (isset($_POST['social_google_rs_category_percent']))
            update_woocommerce_term_meta($term_id, 'social_google_rs_category_percent', $_POST['social_google_rs_category_percent']);
        /* Google Rule and Its Points Updation End */


        delete_transient('wc_term_counts');
    }

// For Admin Settings

    public static function register_reward_system_sub_menu() {
        global $my_admin_page;
        $my_admin_page = add_submenu_page('woocommerce', 'Sumo Reward Points', 'Sumo Reward Points', 'manage_options', 'rewardsystem_callback', array('FPRewardSystem', 'reward_system_admin_tab_settings'));
    }

    public static function reward_system_tab_settings($settings_tabs) {
        $settings_tabs['rewardsystem'] = __('General', 'rewardsystem');
        return $settings_tabs;
    }

//Function to Create the Reward System Settings Page
    public static function reward_system_admin_tab_settings() {
        global $woocommerce, $woocommerce_settings, $current_section, $current_tab;
        do_action('woocommerce_rs_settings_start');
        $current_tab = ( empty($_GET['tab']) ) ? 'rewardsystem' : sanitize_text_field(urldecode($_GET['tab']));
        $current_section = ( empty($_REQUEST['section']) ) ? '' : sanitize_text_field(urldecode($_REQUEST['section']));
        if (!empty($_POST['save'])) {
            if (empty($_REQUEST['_wpnonce']) || !wp_verify_nonce($_REQUEST['_wpnonce'], 'woocommerce-settings'))
                die(__('Action failed. Please refresh the page and retry.', 'rewardsystem'));

            if (!$current_section) {
//include_once('settings/settings-save.php');
                switch ($current_tab) {
                    default :
                        if (isset($woocommerce_settings[$current_tab]))
                            woocommerce_update_options($woocommerce_settings[$current_tab]);

// Trigger action for tab
                        do_action('woocommerce_update_options_' . $current_tab);
                        break;
                }

                do_action('woocommerce_update_options');
            } else {
// Save section onlys
                do_action('woocommerce_update_options_' . $current_tab . '_' . $current_section);
            }

// Clear any unwanted data
// $woocommerce->clear_product_transients();
            delete_transient('woocommerce_cache_excluded_uris');
// Redirect back to the settings page
            $redirect = add_query_arg(array('saved' => 'true'));
//  $redirect .= add_query_arg('noheader', 'true');

            if (isset($_POST['subtab'])) {
                wp_safe_redirect($redirect);
                exit;
            }
        }
// Get any returned messages
        $error = ( empty($_GET['wc_error']) ) ? '' : urldecode(stripslashes($_GET['wc_error']));
        $message = ( empty($_GET['wc_message']) ) ? '' : urldecode(stripslashes($_GET['wc_message']));

        if ($error || $message) {

            if ($error) {
                echo '<div id="message" class="error fade"><p><strong>' . esc_html($error) . '</strong></p></div>';
            } else {
                echo '<div id="message" class="updated fade"><p><strong>' . esc_html($message) . '</strong></p></div>';
            }
        } elseif (!empty($_GET['saved'])) {

            echo '<div id="message" class="updated fade"><p><strong>' . __('Your settings have been saved.', 'rewardsystem') . '</strong></p></div>';
        }
        ?>
        <div class="wrap woocommerce">
            <form method="post" id="mainform" action="" enctype="multipart/form-data">
                <div class="icon32 icon32-woocommerce-settings" id="icon-woocommerce"><br /></div><h2 class="nav-tab-wrapper woo-nav-tab-wrapper">
                    <?php
                    $tabs = '';
                    $tabs = apply_filters('woocommerce_rs_settings_tabs_array', $tabs);

                    foreach ($tabs as $name => $label) {
                        // echo $current_tab;
                        echo '<a href="' . admin_url('admin.php?page=rewardsystem_callback&tab=' . $name) . '" class="nav-tab ';
                        if ($current_tab == $name)
                            echo 'nav-tab-active';
                        echo '">' . $label . '</a>';
                    }
                    do_action('woocommerce_rs_settings_tabs');
                    ?>
                </h2>

                <?php
                switch ($current_tab) :
                    case "rewardsystem_pointslog" :
                        FPRewardSystem::list_reward_points_log();
                        break;
                    case "rewardsystem_addremove_points":
                        FPRewardSystem::list_add_remove_option();
                        break;
                    default :
                        do_action('woocommerce_rs_settings_tabs_' . $current_tab);
                        break;
                endswitch;
                ?>
                <p class="submit">
                    <?php if (!isset($GLOBALS['hide_save_button'])) : ?>
                        <input name="save" class="button-primary" type="submit" value="<?php _e('Save changes', 'woocommerce'); ?>" />
                    <?php endif; ?>
                    <input type="hidden" name="subtab" id="last_tab" />
                    <?php wp_nonce_field('woocommerce-settings', '_wpnonce', true, true); ?>
                </p>
            </form>
            <form method="post" id="mainforms" action="" enctype="multipart/form-data" style="float: left; margin-top: -52px; margin-left: 159px;">
                <input name="reset" class="button-secondary" type="submit" value="<?php _e('Reset All', 'woocommerce'); ?>"/>
                <?php wp_nonce_field('woocommerce-reset_settings', '_wpnonce', true, true); ?>
            </form>
        </div>
        <?php
    }

    public static function list_add_remove_option() {
        global $woocommerce;
        if (get_option('timezone_string') != '') {
            $timezonedate = date_default_timezone_set(get_option('timezone_string'));
        } else {
            $timezonedate = date_default_timezone_set('UTC');
        }
        $enablemaxpoints = get_option('rs_enable_maximum_earning_points');
        ?>

        <style type="text/css">
            p.submit {
                display:none;
            }
            #mainforms {
                display:none;
            }
            #rs_select_customers_chosen {
                width:370px !important;
            }
        </style>
        <h4><?php _e('Add or Remove Points for User', 'rewardsystem'); ?></h4>
        <?php
        $selectuserby = get_option('_rs_select_user_by_name_email');
        if ($selectuserby == '1') {
            $whoname = ' by  Usename';
        } else {
            $whoname = ' by Email';
        }
        ?>
        <form name="rs_addremove" method="post">
            <table class="form-table">
                <tbody>
                    <tr valign="top">
                        <th class="titledesc" scope="row">
                            <label for="rs_select_users">Select User <?php echo $whoname; ?></label>
                        </th>
                        <td class="forminp forminp-text">
                            <select name="rs_select_customers" placeholder='Select User' class="ajax_chosen_select_customer" style="width:370px;" id="rs_select_customers">
                                <?php
                                $userlist = get_users();

                                foreach ($userlist as $user) {
                                    if (get_option('_rs_select_user_by_name_email') == '1') {
                                        $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
                                        $current_points = round(get_user_meta($user->ID, '_my_reward_points', true), $roundofftype);
                                        ?>
                                        <option data-userpoints ="<?php echo $current_points; ?>" value="<?php echo $user->ID; ?>"><?php echo $user->user_login; ?></option>

                                        <?php
                                    } else {
                                        ?>
                                        <option data-userpoints ="<?php echo $current_points; ?>" value="<?php echo $user->ID; ?>"><?php echo $user->user_email; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr valign ="top">
                        <th class="titledesc" scope="row">
                            <label for="rs_reward_current_user_points"><?php _e('Current Points for User', 'rewardsystem'); ?></label>
                        </th>
                        <td class="forminp forminp-text">
                            <input type="text" class="" value="" style="min-width:150px;" readonly="readonly" id="rs_reward_current_user_points" value=""/>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th class="titledesc" scope="row">
                            <label for="rs_reward_addremove_points"><?php _e('Enter Points', 'rewardsystem'); ?></label>
                        </th>
                        <td class="forminp forminp-text">
                            <input type="text" class="" value="" style="min-width:150px;" required='required' id="rs_reward_addremove_points" name="rs_reward_addremove_points"> 	                    </td>
                    </tr>
                    <tr valign="top">
                        <th class="titledesc" scope="row">
                            <label for="rs_reward_addremove_reason"><?php _e('Reason in Detail'); ?></label>
                        </th>
                        <td class="forminp forminp-text">
                           <!-- <input type="text" class="" value="" style="min-width:150px;" id="rs_reward_addremove_reason" name="rs_reward_addremove_reason"> -->
                            <textarea cols='40' rows='5' name='rs_reward_addremove_reason' required='required'></textarea>
                        </td>
                    </tr>
                    <tr valig='top'>
                        <td>
                            <input type='submit' name='rs_remove_points' id='rs_remove_points'  class='button-primary' value='Remove Points'/>

                        </td>
                        <td>
                            <input type='submit' name='rs_add_points' id='rs_add_points' class='button-primary' value='Add Points'/>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                var datauserpoints = jQuery("#rs_select_customers").find(':selected').attr('data-userpoints');
                jQuery('#rs_reward_current_user_points').val(datauserpoints);
                //jQuery('#rs_select_customers').css('width', '150px');
                jQuery(document).on('change', '#rs_select_customers', function () {
                    // alert(jQuery(this).data('userpoints'));
                    var datauserpoints = jQuery("#rs_select_customers").find(':selected').attr('data-userpoints');
                    jQuery('#rs_reward_current_user_points').val(datauserpoints);
                });
        <?php
        if ((float) $woocommerce->version < (float) ('2.2.0')) {
            ?>
                    jQuery('#rs_select_customers').chosen();
            <?php
        } else {
            ?>
                    jQuery('#rs_select_customers').select2();
            <?php
        }
        ?>
                jQuery('#rs_add_points').click(function () {
                    //var checkpoints = jQuery('#rs_reward_addremove_points')
                });
                jQuery('#rs_remove_points').click(function () {

                });
            });</script>
        <?php
        if (isset($_POST['rs_add_points'])) {
            $overalllogs[] = array('userid' => $_POST['rs_select_customers'], 'totalvalue' => $_POST['rs_reward_addremove_points'], 'eventname' => $_POST['rs_reward_addremove_reason'], 'date' => date('Y-m-d H:i:s'));
            $getoveralllogs = get_option('rsoveralllog');
            $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
            update_option('rsoveralllog', $logmerges);
            $restrictuserpoints = get_option('rs_maximum_earning_points_for_user');

            $userpoints = get_user_meta($_POST['rs_select_customers'], '_my_reward_points', true);
            $updatedpoints = $_POST['rs_reward_addremove_points'] + $userpoints;
            $rs_new_points_to_add = $_POST['rs_reward_addremove_points'];
            if ($enablemaxpoints == 'yes') {
                if ($rs_new_points_to_add < $restrictuserpoints) {
                    FPRewardSystem::save_total_earned_points($_POST['rs_select_customers'], $_POST['rs_reward_addremove_points']);
                } else {
                    FPRewardSystem::save_total_earned_points($_POST['rs_select_customers'], $restrictuserpoints);
                }
            } else {
                FPRewardSystem::save_total_earned_points($_POST['rs_select_customers'], $_POST['rs_reward_addremove_points']);
            }
            if ($enablemaxpoints == 'yes') {
                if (($updatedpoints <= $restrictuserpoints) || ($restrictuserpoints == '')) {
                    $updatedpoints = $updatedpoints;
                } else {
                    $updatedpoints = $restrictuserpoints;
                }
            }
            update_user_meta($_POST['rs_select_customers'], '_my_reward_points', $updatedpoints);
            $pointspercent[] = array('orderid' => '', 'userid' => $_POST['rs_select_customers'], 'points_earned_order' => $_POST['rs_reward_addremove_points'], 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => get_user_meta($_POST['rs_select_customers'], '_my_reward_points', true), 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => $_POST['rs_reward_addremove_reason'], 'rewarder_for_frontend' => $_POST['rs_reward_addremove_reason']);
            $getlogg = get_user_meta($_POST['rs_select_customers'], '_my_points_log', true);
            $mergeds = array_merge((array) $getlogg, $pointspercent);
            update_user_meta($_POST['rs_select_customers'], '_my_points_log', $mergeds);
            $newredirect = add_query_arg('saved', 'true', get_permalink());
            wp_safe_redirect($newredirect);
            exit();
        }
        if (isset($_POST['rs_remove_points'])) {
            $overalllogs[] = array('userid' => $_POST['rs_select_customers'], 'totalvalue' => $_POST['rs_reward_addremove_points'], 'eventname' => $_POST['rs_reward_addremove_reason'], 'date' => date('Y-m-d H:i:s'));
            $getoveralllogs = get_option('rsoveralllog');
            $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
            update_option('rsoveralllog', $logmerges);
            $userpoints = get_user_meta($_POST['rs_select_customers'], '_my_reward_points', true);
            $updatedpoints = $userpoints - $_POST['rs_reward_addremove_points'];
            update_user_meta($_POST['rs_select_customers'], '_my_reward_points', $updatedpoints);
            $pointspercent[] = array('orderid' => '', 'userid' => $_POST['rs_select_customers'], 'points_earned_order' => '', 'points_redeemed' => $_POST['rs_reward_addremove_points'], 'points_value' => '', 'before_order_points' => '', 'totalpoints' => get_user_meta($_POST['rs_select_customers'], '_my_reward_points', true), 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => $_POST['rs_reward_addremove_reason'], 'rewarder_for_frontend' => $_POST['rs_reward_addremove_reason']);
            $getlogg = get_user_meta($_POST['rs_select_customers'], '_my_points_log', true);
            $mergeds = array_merge((array) $getlogg, $pointspercent);
            update_user_meta($_POST['rs_select_customers'], '_my_points_log', $mergeds);
            $newredirect = add_query_arg('saved', 'true', get_permalink());
            wp_safe_redirect($newredirect);
            exit();
        }
    }

    public static function list_reward_points_log() {
//delete_user_meta(1, '_my_points_log');
//delete_option('rsoveralllog');
        ?>
        <p>Following List shows the Total Reward Points for each User</p>

        <style type="text/css">
            p.submit {
                display:none;
            }
            #mainforms {
                display:none;
            }
        </style>

        <?php
//delete_user_meta('1', '_my_points_log');
//delete_user_meta('1', '_my_reward_points');
        ?>
        <?php if ((!isset($_GET['view'])) && (!isset($_GET['edit']))) { ?>
            <?php
            echo '<p> ' . __('Search:', 'rewardsystem') . '<input id="filtering" type="text"/>  ' . __('Page Size:', 'rewardsystem') . '
                <select id="changepagesize">

									<option value="5">5</option>
									<option value="10">10</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select></p>';
            ?>
            <table class="wp-list-table widefat fixed posts" data-filter = "#filtering" data-page-size="5" data-page-previous-text = "prev" data-filter-text-only = "true" data-page-next-text = "next" id="user_logs" cellspacing="0">
                <thead>
                    <tr>
                        <th scope='col' data-toggle="true" class='manage-column column-serial_number'  style="">
                            <a href="#"><span><?php _e('S.No', 'rewardsystem'); ?></span>
                        </th>
                        <th scope='col' id='rs_user_names' class='manage-column column-rs_user_name'  style=""><?php _e('Username', 'rewardsystem'); ?></th>
                        <th scope="col" id="rs_user_email" class="manage-column column-rs_user_email" style=""><?php _e('User Email', 'rewardsystem'); ?></th>
                        <th scope='col' id='rs_totalpoints' class='manage-column column-rs_totalpoints'  style=""><?php _e('Total Points', 'rewardsystem'); ?></th>
                        <th scope='col' id='rs_view' data-hide="phone" class='manage-column column-rs_view'  style="">
                            <span><?php _e('View', 'rewardsystem'); ?></span>
                        </th>
                        <th scope="col" id="rs_edit" data-hide="phone" class="manage-column column-rs_edit" style="">
                            <span><?php _e('Edit', 'rewardsystem'); ?></span>
                        </th>
                    </tr>
                </thead>
                <tbody id="the-list">
                    <?php
                    $blogusers = get_users();
                    // var_dump($blogusers);
                    $i = 1;
                    foreach ($blogusers as $eachuser) {
                        // echo $eachuser->ID;
                        if ($i % 2 != 0) {
                            $name = 'alternate';
                        } else {
                            $name = '';
                        }
                        ?>
                        <tr id="post-141"  class="type-shop_order status-publish post-password-required hentry <?php echo $name; ?> iedit author-self level-0" valign="top">
                            <td data-value="<?php echo $i; ?>">
                                <?php echo $i; ?>
                            </td>
                            <td class="rs_user_name">
                                <?php echo $eachuser->user_login; ?>
                            </td>
                            <td class="rs_user_email">
                                <?php echo $eachuser->user_email; ?>
                            </td>
                            <td>
                                <?php
                                if (get_user_meta($eachuser->ID, '_my_reward_points', true) != '') {
                                    //echo number_format(get_user_meta($eachuser->ID, '_my_reward_points', true));
                                    $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
                                    echo round(get_user_meta($eachuser->ID, '_my_reward_points', true), $roundofftype);
                                } else {
                                    echo "0";
                                }
                                ?>
                            </td>
                            <td>
                                <a href="<?php echo add_query_arg('view', $eachuser->ID, get_permalink()); ?>"><?php _e('View Log', 'rewardsystem'); ?></a>
                            </td>
                            <td>
                                <a href="<?php echo add_query_arg('edit', $eachuser->ID, get_permalink()); ?>"><?php _e('Edit Total Points', 'rewardsystem'); ?></a>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                </tbody>

            </table>

            <div style="clear:both;">
                <div class="pagination pagination-centered"></div>
            </div>

            <?php
        } elseif (isset($_GET['view'])) {
            $user_ID = $_GET['view'];
            $fetcharray = get_user_meta($user_ID, '_my_points_log', true);
            if (is_array($fetcharray)) {
                ?>
                <?php
                echo '<p> ' . __('Search:', 'rewardsystem') . '<input id="filterings" type="text"/>  ' . __('Page Size:', 'rewardsystem') . '
                <select id="changepagesizes">
									<option value="5">5</option>
									<option value="10">10</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select></p>';
                ?>
                <table class="wp-list-table widefat fixed posts" data-filter = "#filterings" data-page-size="5" data-page-previous-text = "prev" data-filter-text-only = "true" data-page-next-text = "next" id="individual_log" cellspacing="0">
                    <thead>
                        <tr>
                            <th scope='col' data-toggle="true" class='manage-column column-serial_number'  style="">
                                <a href="#"><span><?php _e('S.No', 'rewardsystem'); ?></span>
                            </th>
                            <!--<th scope='col' id='rs_order_id' class='manage-column column-rs_order_id'  style=""><?php _e('Order ID', 'rewardsystem'); ?></th>-->
                            <th scope ="col" id="rs_user_id" class="manage-column column-rs_user_id" style=""><?php _e('User Name', 'rewardsystem'); ?></th>
                            <th scope="col" id="rs_rewarder_for" class="manage-column column-rs_rewarder_for"><?php _e('Reward for', 'rewardsystem'); ?></th>
                            <th scope ="col" id="rs_points_earned" class="manage-column column-rs_points_earned" style=""><?php _e('Earned Points', 'rewardsystem'); ?></th>
                           <!-- <th scope ="col" id="rs_points_before_order" class="manage-column column-rs_points_before_order" style=""><?php _e('Before Order Your Points', 'rewardsystem'); ?></th> -->
                            <th scope ='col' id='rs_redeemed_points' class='manage-column-rs_redeemed_points'><?php _e('Redeemed Points', 'rewardsystem'); ?></th>
                            <th scope="col" id="rs_points_total_points" class="manage-column column-rs_points_total_points"><?php _e('Total Points', 'rewardsystem'); ?></th>
                            <th scope="col" id="rs_points_date" class="manage-column column-rs_points_date"><?php _e('Date', 'rewardsystem'); ?></th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($fetcharray as $newarray) {
                            //$newarray['totalpoints'];
                            if ($i % 2 != 0) {
                                $name = 'alternate';
                            } else {
                                $name = '';
                            }
                            if (is_array($newarray)) {
                                if (!empty($newarray['points_earned_order'])) {
                                    if (is_float($newarray['points_earned_order'])) {
                                        $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
                                        $pointsearned = round(number_format($newarray['points_earned_order'], 2), $roundofftype);
                                    } else {
                                        $pointsearned = number_format($newarray['points_earned_order']);
                                    }
                                } else {
                                    $pointsearned = "0";
                                }

                                if (!empty($newarray['before_order_points'])) {
                                    if (is_float($newarray['before_order_points'])) {
                                        $beforepoints = number_format($newarray['before_order_points'], 2);
                                    } else {
                                        $beforepoints = number_format($newarray['before_order_points']);
                                    }
                                } else {
                                    $beforepoints = "0";
                                }

                                if (!empty($newarray['points_redeemed'])) {
                                    if (is_float($newarray['points_earned_order'])) {
                                        $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
                                        $redeemedpoints = round(number_format($newarray['points_redeemed'], 2), $roundofftype);
                                    } else {
                                        $redeemedpoints = number_format((float) $newarray['points_redeemed']);
                                    }
                                } else {
                                    $redeemedpoints = "0";
                                }
                                if (!empty($newarray['totalpoints'])) {
                                    if (get_option('rs_round_off_type') == '1') {
                                        $totalpoints = $newarray['totalpoints'];
                                    } else {
                                        $totalpoints = number_format($newarray['totalpoints']);
                                    }
                                } else {
                                    $totalpoints = "0";
                                }

                                $usernickname = get_user_meta($newarray['userid'], 'nickname', true);
                                if (!empty($newarray['rewarder_for'])) {
                                    $rewarderfor = $newarray['rewarder_for'];
                                } else {
                                    $rewarderfor = '';
                                }
                                ?>
                                <tr class='<?php echo $name; ?>'>
                                    <td data-value='<?php echo $i; ?>'><?php echo $i; ?></td>
                                   <!-- <td><?php echo $newarray['orderid']; ?> </td> -->
                                    <td><?php echo $usernickname; ?> </td>
                                    <td><?php echo $rewarderfor; ?></td>
                                    <td><?php echo $pointsearned; ?> </td>
                                    <!-- <td><?php echo $beforepoints; ?></td> -->
                                    <td><?php echo $redeemedpoints; ?></td>
                                    <td><?php echo $totalpoints; ?> </td>
                                    <td><?php echo $newarray['date']; ?></td>

                                </tr>
                                <?php
                                $i++;
                            }
                        }
                        ?>
                    </tbody>
                </table>
                <div style="clear:both;">
                    <div class="pagination pagination-centered"></div>
                </div>

                <?php
            } else {
                _e('Sorry No Records Found', 'rewardsystem');
            }
            ?>
            <a href="<?php echo remove_query_arg(array('view'), get_permalink()); ?>">Go Back</a>
            <?php
        } else {
            $user_ID = $_GET['edit'];
            if (isset($_POST['my_reward_points']) != '') {
                update_user_meta($user_ID, '_my_reward_points', trim($_POST['my_reward_points']));
                $newredirect = add_query_arg('saved', 'true', get_permalink());
                wp_safe_redirect($newredirect);
                exit();
            }
//update_user_meta(1, '_my_reward_points', 50000);
            $my_rewards = get_user_meta($user_ID, '_my_reward_points', true);
            ?>
            <style type="text/css">
                p.submit {
                    display:none;
                }
                #mainforms {
                    display:none;
                }
            </style>
            <h3><?php _e('Update User Reward Points', 'rewardsystem'); ?></h3>
            <input type='text' name='my_reward_points' value='<?php
            $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
            echo round($my_rewards, $roundofftype);
            ?>'/>
            <input type='submit' name='submitrs' class='button-primary' value='Update'/>
            <a href="<?php echo remove_query_arg(array('edit', 'saved'), get_permalink()); ?>">Go Back</a>
            <?php
        }
    }

    public static function list_all_points_log() {
        ?>
        <?php
        echo '<p> ' . __('Search:', 'rewardsystem') . '<input id="filterings_masters" type="text"/>  ' . __('Page Size:', 'rewardsystem') . '
                <select id="changepagesizer">
									<option value="5">5</option>
									<option value="10">10</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select><tr valign ="top">
            <td class="forminp forminp-select">
                <input type="submit" id="rs_export_master_log_csv" name="rs_export_master_log_csv" value="Export Master Log as CSV"/>
            </td>
        </tr></p>';
        ?>
        <style type="text/css">
            p.submit {
                display:none;
            }
            #mainforms {
                display:none;
            }
        </style>
        <table class="wp-list-table widefat fixed posts" data-filter = "#filterings_masters" data-page-size="5" data-page-previous-text = "prev" data-filter-text-only = "true" data-page-next-text = "next" id="overall_points" cellspacing="0">
            <thead>
                <tr>
                    <th scope='col' data-toggle="true" class='manage-column column-serial_number'  style="">
                        <a href="#"><span><?php _e('S.No', 'rewardsystem'); ?></span>
                    </th>
                    <th scope='col' id='rs_user_names' class='manage-column column-rs_user_name'  style=""><?php _e('User Name', 'rewardsystem'); ?></th>
                    <th scope='col' id='rs_totalpoints' class='manage-column column-rs_totalpoints'  style=""><?php _e('Points', 'rewardsystem'); ?></th>
                    <th scope='col' id='rs_eventname' class='manage-column column-rs_eventname'  style=""><?php _e('Event', 'rewardsystem'); ?></th>
                    <th scope="col" id="rs_date" class="manage-column column-rs_date" style=""><?php _e('Date', 'rewardsystem'); ?></th>
                </tr>
            </thead>

            <tbody id="the-list">
                <?php
                $i = 1;
                // var_dump(get_option('rsoveralllog'));
                //delete_option('rsoveralllog');
                if (is_array(get_option('rsoveralllog'))) {
                    foreach (get_option('rsoveralllog') as $value) {
                        if ($i % 2 != 0) {
                            $name = 'alternate';
                        } else {
                            $name = '';
                        }

                        if ($value != '') {
                            if (!empty($value['totalvalue'])) {
                                if (is_float($value['totalvalue'])) {
                                    $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
                                    $total = round(number_format($value['totalvalue'], 2), $roundofftype);
                                } else {
                                    $total = number_format($value['totalvalue']);
                                }
                            } else {
                                $total = $value['totalvalue'];
                            }

                            $getusernickname = get_user_meta($value['userid'], 'nickname', true);
                            if ($getusernickname == '') {
                                $getusernickname = $value['userid'];
                            }
                            ?>
                            <tr class='<?php echo $name; ?>'>
                                <td data-value='<?php echo $i; ?>'>
                                    <?php echo $i; ?>
                                </td>
                                <td>
                                    <?php echo $getusernickname ?>
                                </td>
                                <td>
                                    <?php echo $total; ?>
                                </td>
                                <td>
                                    <?php echo $value['eventname']; ?>
                                </td>
                                <td>
                                    <?php echo $value['date']; ?>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                }
                ?>
            </tbody>

        </table>
        <div style="clear:both;">
            <div class="pagination pagination-centered"></div>
        </div>
        <?php
//        if (isset($_POST['rs_export_user_points_csv'])) {
////var_dump($_POST['rs_export_user_points_csv']);
//            ob_end_clean();
//            echo $export_masterlog_heading;
//            header("Content-type: text/csv");
//            header("Content-Disposition: attachment; filename=reward_points_masterlog" . date("Y-m-d") . ".csv");
//            header("Pragma: no-cache");
//            header("Expires: 0");
//            FPRewardSystemImportExportTab::outputCSV($masterloglist);
//            exit();
//        }
    }

    public static function reset_reward_system_admin_settings() {
        if (!empty($_POST['reset'])) {
            if (empty($_REQUEST['_wpnonce']) || !wp_verify_nonce($_REQUEST['_wpnonce'], 'woocommerce-reset_settings'))
                die(__('Action failed. Please refresh the page and retry.', 'rewardsystem'));

            foreach (FPRewardSystem::rewardsystem_admin_fields() as $setting) {
                if (isset($setting['newids']) && isset($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }
            foreach (FPRewardSystemPointsRule::rewardsystem_admin_fields() as $setting) {
                if (isset($setting['newids']) && isset($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }
            foreach (FPRewardSystemMessages::rewardsystem_admin_fields() as $setting) {
                if (isset($setting['newids']) && isset($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }
            foreach (FPRewardSystemMyAccount::rewardsystem_admin_fields() as $setting) {
                if (isset($setting['newids']) && isset($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }
            foreach (FPRewardSystemCart::rewardsystem_admin_fields() as $setting) {
                if (isset($setting['newids']) && isset($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }

            foreach (FPRewardSystemCheckoutTab::rewardsystem_admin_fields() as $setting) {
                if (isset($setting['newids']) && isset($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }

            foreach (FPRewardSystemReferFriendTab::rewardsystem_admin_fields() as $setting) {
                if (isset($setting['newids']) && isset($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }

            foreach (RSUserRoleRewardPoints::rewardsystem_admin_fields() as $setting) {
                if (isset($setting['newids']) && isset($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }

            foreach (FPRewardSystemTroubleshootTab::rewardsystem_admin_fields() as $setting) {
                if (isset($setting['newids']) && isset($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }

            foreach (FPRewardSystemSocialRewardsTab::rewardsystem_admin_fields() as $setting) {
                if (isset($setting['newids']) && isset($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }
            foreach (FPRewardSystemMailTab::rewardsystem_admin_fields() as $setting) {
                if (isset($setting['newids']) && isset($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }

            foreach (FPRewardSystemStatusTab::rewardsystem_admin_fields() as $setting) {
                if (isset($setting['newids']) && isset($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }

            foreach (FPRewardSystemLocalizationTab::rewardsystem_admin_fields() as $setting) {
                if (isset($setting['newids']) && isset($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }

            foreach (FPRewardSystemEncashTab::rewardsystem_admin_fields() as $setting) {
                if (isset($setting['newids']) && isset($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }
            foreach (FPRewardSystemPointsVoucher::rewardsystem_admin_fields() as $setting) {
                if (isset($setting['newids']) && isset($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }
            foreach (FPRewardSystemCouponPoints::rewardsystem_admin_fields() as $setting) {
                if (isset($setting['newids']) && isset($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }
            delete_option('rewards_dynamic_rule_couponpoints');
            foreach (FPRewardSystemSms::rewardsystem_admin_fields() as $setting) {
                if (isset($setting['newids']) && isset($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }

            foreach (FPRewardSystemUpdate::rewardsystem_admin_fields() as $setting) {
                if (isset($setting['newids']) && isset($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }

            delete_option('rewards_dynamic_rule_manual');

//$woocommerce->clear_product_transients();
            delete_transient('woocommerce_cache_excluded_uris');
// Redirect back to the settings page
            $redirect = add_query_arg(array('saved' => 'true'));
//  $redirect .= add_query_arg('noheader', 'true');
            if (isset($_POST['reset'])) {
                wp_safe_redirect($redirect);
                exit;
            }
        }
    }

// Add Admin Fields in the Array Format
    /**
     * Crowdfunding Add Custom Field to the CrowdFunding Admin Settings
     */
    public static function rewardsystem_admin_fields() {
        global $woocommerce;
        $user_list = get_users();
        // Array of stdClass objects.
        foreach ($user_list as $user) {
            $separate_user[] = $user->display_name;
            $seperate_userid[] = $user->ID;
        }

        $newcombineddatas = array_combine((array) $seperate_userid, (array) $separate_user);

        global $wp_roles;
        foreach ($wp_roles->roles as $values => $key) {
            $userroleslug[] = $values;
            $userrolename[] = $key['name'];
        }

        $newcombineduserrole = array_combine((array) $userroleslug, (array) $userrolename);

        return apply_filters('woocommerce_rewardsystem_settings', array(
            array(
                'name' => __('', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_reward_settings',
            ),
            array(
                'name' => __('Reward Points by Percentage is Calculated based on', 'rewardsystem'),
                'desc' => __('If second Option is chosen then calculation will be based on sale price if present else regular price will be used for calculation', 'rewardsystem'),
                'id' => 'rs_set_price_percentage_reward_points',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_set_price_percentage_reward_points',
                'type' => 'select',
                'options' => array(
                    '1' => __('Regular Price', 'rewardsystem'),
                    '2' => __('Sale Price (Regular Price if Sale Price is unavailable)', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Enable/Disable Maximum Earning Points for User', 'rewardsystem'),
                'desc' => __('Enable/Disable Maximum Earning Points for User in SUMO Rewards', 'rewardsystem'),
                'id' => 'rs_enable_maximum_earning_points',
                'css' => 'min-width:150px;',
                'std' => '1',
                'type' => 'checkbox',
                'newids' => 'rs_enable_maximum_earning_points',
                ''
            ),
            array(
                'name' => __('Maximum Earning Points for User', 'rewardsystem'),
                'desc' => __('Restrict the Maximum Earning Points for the User in different Behaviour', 'rewardsystem'),
                'id' => 'rs_maximum_earning_points_for_user',
                'css' => 'min-width:150px;',
                'std' => '100000',
                'desc_tip' => true,
                'newids' => 'rs_maximum_earning_points_for_user',
                'type' => 'text',
            ),
//            array(
//                'name' => __('Default Option for New Product', 'rewardsystem'),
//                'desc' => __('This helps to Enable/Disable Reward Points for New Products', 'rewardsystem'),
//                'id' => 'rs_enable_disable_default_new_product',
//                'css' => 'min-width:150px;',
//                'std' => '2',
//                'default' => '2',
//                'desc_tip' => true,
//                'newids' => 'rs_enable_disable_default_new_product',
//                'type' => 'select',
//                'options' => array(
//                    '1' => __('Enable SUMO Reward Points', 'rewardsystem'),
//                    '2' => __('Disable SUMO Reward Points', 'rewardsystem'),
//                ),
//            ),
            array('type' => 'sectionend', 'id' => '_rs_reward_settings'),
            array(
                'name' => __('Reward Points Global', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_global_reward_points'
            ),
            array(
                'name' => __('Enable SUMO Reward Points', 'rewardsystem'),
                'desc' => __('This helps to Enable Reward Points in Global Level', 'rewardsystem'),
                'id' => 'rs_global_enable_disable_reward',
                'css' => 'min-width:150px;',
                'std' => '2',
                'default' => '2',
                'desc_tip' => true,
                'newids' => 'rs_global_enable_disable_reward',
                'type' => 'select',
                'options' => array(
                    '1' => __('Enable', 'rewardsystem'),
                    '2' => __('Disable', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Reward Type', 'rewardsystem'),
                'desc' => __('Select Reward Type by Points/Percentage', 'rewardsystem'),
                'id' => 'rs_global_reward_type',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'desc_tip' => true,
                'newids' => 'rs_global_reward_type',
                'type' => 'select',
                'options' => array(
                    '1' => __('By Fixed Reward Points', 'rewardsystem'),
                    '2' => __('By Percentage of Product Price', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Reward Points', 'rewardsystem'),
                'desc' => __('Please Enter Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_global_reward_points',
                'css' => 'min-width:150px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_global_reward_points',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Reward Points in Percent %', 'rewardsystem'),
                'desc' => __('Please Enter Percentage value of Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_global_reward_percent',
                'css' => 'min-width:150px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_global_reward_percent',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Referral Reward Type', 'rewardsystem'),
                'desc' => __('Select Reward Type by Points/Percentage', 'rewardsystem'),
                'id' => 'rs_global_referral_reward_type',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'newids' => 'rs_global_referral_reward_type',
                'type' => 'select',
                'desc_tip' => true,
                'options' => array(
                    '1' => __('By Fixed Reward Points', 'rewardsystem'),
                    '2' => __('By Percentage of Product Price', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Referral Reward Points', 'rewardsystem'),
                'desc' => __('Please Enter Referral Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_global_referral_reward_point',
                'css' => 'min-width:150px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_global_referral_reward_point',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Referral Reward Points in Percent %', 'rewardsystem'),
                'desc' => __('Please Enter Percentage value of Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_global_referral_reward_percent',
                'css' => 'min-width:150px;',
                'std' => ' ',
                'type' => 'text',
                'newids' => 'rs_global_referral_reward_percent',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_global_reward_points'),
            array(
                'name' => __('Earning Points Conversion Settings', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_point_conversion'
            ),
            array(
                'name' => __('Earning Point', 'rewardsystem'),
                'desc' => __('Please Enter Earning Point', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_earn_point',
                'css' => 'min-width:150px;',
                'std' => '1',
                'type' => 'text',
                'newids' => 'rs_earn_point',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Earning Point Value in ' . get_woocommerce_currency_symbol(), 'rewardsystem'),
                'desc' => __('Please Enter Earning Point Value', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_earn_point_value',
                'css' => 'min-width:150px;',
                'std' => '1',
                'type' => 'text',
                'newids' => 'rs_earn_point_value',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_point_conversion'),
            array(
                'name' => __('Redeeming Points Conversion Settings', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_redeem_point_conversion'
            ),
            array(
                'name' => __('Redeeming Point', 'rewardsystem'),
                'desc' => __('Please Enter Redeeming Point', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_redeem_point',
                'css' => 'min-width:150px;',
                'std' => '100',
                'type' => 'text',
                'newids' => 'rs_redeem_point',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Redeeming Point Value in ' . get_woocommerce_currency_symbol(), 'rewardsystem'),
                'desc' => __('Please Enter Redeeming Point Value', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_redeem_point_value',
                'css' => 'min-width:150px;',
                'std' => '1',
                'type' => 'text',
                'newids' => 'rs_redeem_point_value',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_redeem_point_conversion'),
            array(
                'name' => __('Maximum Discount Settings', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_discount_control'
            ),
            array(
                'name' => __('Maximum Discount Type', 'rewardsystem'),
                'desc' => __('This helps to control the maximum discount for cart', 'rewardsystem'),
                'id' => 'rs_max_redeem_discount',
                'css' => 'min-width:150px;',
                'std' => '',
                'default' => '',
                'newids' => 'rs_max_redeem_discount',
                'type' => 'select',
                'options' => array(
                    '1' => __('By Fixed Value', 'rewardsystem'),
                    '2' => __('By Percentage of Cart Total', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Maximum Discount for Cart in ' . get_woocommerce_currency_symbol(), 'rewardsystem'),
                'desc' => __('Please Enter Maximum Discount for Cart that can be availed by Redeeming Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_fixed_max_redeem_discount',
                'css' => 'min-width:150px;',
                'std' => '',
                'type' => 'text',
                'newids' => 'rs_fixed_max_redeem_discount',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Maximum Discount for Cart in Percentage %', 'rewardsystem'),
                'desc' => __('Please Enter Maximum Discount for Cart in Percentage that can be availed by Redeeming Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_percent_max_redeem_discount',
                'css' => 'min-width:150px;',
                'std' => '',
                'type' => 'text',
                'newids' => 'rs_percent_max_redeem_discount',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_discount_control'),
            array(
                'name' => __('Round Off Settings', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_round_off_settings',
            ),
            array(
                'name' => __('Round Off Type', 'rewardsystem'),
                'desc' => __('Select the Round Off Type either 2 Decimal or Whole Number', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_round_off_type',
                'css' => 'min-width:150px;',
                'std' => '1',
                'type' => 'select',
                'options' => array(
                    '1' => __('2 Decimal Places', 'rewardsystem'),
                    '2' => __('Whole Number', 'rewardsystem'),
                ),
                'newids' => 'rs_round_off_type',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_round_off_settings'),
            array(
                'name' => __('Referral Cookies Settings', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_referral_cookies_settings'
            ),
            array(
                'name' => __('Referral Cookies Expires in', 'rewardsystem'),
                'desc' => __('This helps to control the Cookies Expiry for Referral', 'rewardsystem'),
                'id' => 'rs_referral_cookies_expiry',
                'css' => 'min-width:150px;',
                'std' => '3',
                'default' => '3',
                'newids' => 'rs_referral_cookies_expiry',
                'type' => 'select',
                'options' => array(
                    '1' => __('Minutes', 'rewardsystem'),
                    '2' => __('Hours', 'rewardsystem'),
                    '3' => __('Days', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('Referral Cookies Expiry in Minutes', 'rewardsystem'),
                'desc' => __('Please Enter Referral Cookies Expiry in Minutes', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_referral_cookies_expiry_in_min',
                'css' => 'min-width:150px;',
                'std' => '',
                'type' => 'text',
                'newids' => 'rs_referral_cookies_expiry_in_min',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Referral Cookies Expiry in Hours', 'rewardsystem'),
                'desc' => __('Please Enter Referral Cookies Expiry in Hours', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_referral_cookies_expiry_in_hours',
                'css' => 'min-width:150px;',
                'std' => '',
                'type' => 'text',
                'newids' => 'rs_referral_cookies_expiry_in_hours',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Referral Cookies Expiry in Days', 'rewardsystem'),
                'desc' => __('Please Enter Referral Cookies Expiry in Days', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_referral_cookies_expiry_in_days',
                'css' => 'min-width:150px;',
                'std' => '1',
                'type' => 'text',
                'newids' => 'rs_referral_cookies_expiry_in_days',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_referral_cookies_settings'),
            array(
                'name' => __('Gift Icon Selection', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_gift_icon_selection',
            ),
            array(
                'name' => __('Enable/Disable Gift Icon', 'rewardsystem'),
                'desc' => __('This helps to show/hide Gift Icon in Reward System', 'rewardsystem'),
                'id' => '_rs_enable_disable_gift_icon',
                'css' => 'min-width:150px;',
                'std' => '2',
                'default' => '2',
                'newids' => '_rs_enable_disable_gift_icon',
                'type' => 'select',
                'options' => array(
                    '1' => __('Enable', 'rewardsystem'),
                    '2' => __('Disable', 'rewardsystem'),
                ),
            ),
            array(
                'type' => 'uploader',
            ),
            array('type' => 'sectionend', 'id' => '_rs_gift_icon_selection'),
            array(
                'name' => __('Add/Remove Points Tab -- Select User by Username/Email', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_email_or_username',
            ),
            array(
                'name' => __('Select User by Username/Email', 'rewardsystem'),
                'desc' => __('This helps to Select User by Username or Email in Add/Remove Points Tab', 'rewardsystem'),
                'id' => '_rs_select_user_by_name_email',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'newids' => '_rs_select_user_by_name_email',
                'type' => 'select',
                'options' => array(
                    '1' => __('Username', 'rewardsystem'),
                    '2' => __('Email', 'rewardsystem'),
                ),
            ),
            array('type' => 'sectionend', 'id' => '_rs_email_or_username'),
            array(
                'name' => __('Restrict/Ban Users From Using Reward Points', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_ban_users',
            ),
            array(
                'name' => __('Earning Points', 'rewardsystem'),
                'desc' => __('Enable/Disable Banning Users from Earning Points', 'rewardsystem'),
                'id' => 'rs_enable_banning_users_earning_points',
                'css' => 'min-width:150px;',
                'std' => '1',
                'type' => 'checkbox',
                'newids' => 'rs_enable_banning_users_earning_points',
            ),
            array(
                'name' => __('Redeeming Points', 'rewardsystem'),
                'desc' => __('Enable/Disable Banning Users from Redeeming Points', 'rewardsystem'),
                'id' => 'rs_enable_banning_users_redeeming_points',
                'css' => 'min-width:150px;',
                'std' => 'no',
                'type' => 'checkbox',
                'newids' => 'rs_enable_banning_users_redeeming_points',
            ),
            array(
                'name' => __('Select the users you wish to Restrict/Ban from using reward points', 'rewardsystem'),
                'desc' => __('Here you select the users whom you wish to ban from using Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_banned-users_list',
                'css' => 'min-width:400px;',
                'std' => '',
                'type' => 'multiselect',
                'options' => $newcombineddatas,
                'newids' => 'rs_banned-users_list',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Select the User Role you wish to Restrict/Ban from using Reward Points', 'rewardsystem'),
                'desc' => __('Here you can select the User Role where you wish to ban from using Reward Points', 'rewardsystem'),
                'tip' => '',
                'id' => 'rs_banning_user_role',
                'css' => 'min-width:400px;',
                'std' => '',
                'type' => 'multiselect',
                'options' => $newcombineduserrole,
                'newids' => 'rs_banning_user_role',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_ban_users'),
            array(
                'name' => __('Refferal Reward should be applied when members are X Days Old', 'rewardsystem'),
                'type' => 'title',
                'id' => '_rs_ban_referee_points_time',
            ),
            array(
                'name' => __('Refferal Reward should be applied when members are X Days Old', 'rewardsystem'),
                'desc' => __('This helps to Select when the Refferal Reward should be applied', 'rewardsystem'),
                'id' => '_rs_select_referral_points_referee_time',
                'css' => 'min-width:150px;',
                'std' => '1',
                'default' => '1',
                'newids' => '_rs_select_referral_points_referee_time',
                'type' => 'select',
                'desc_tip' => true,
                'options' => array(
                    '1' => __('Unlimited', 'rewardsystem'),
                    '2' => __('Limited', 'rewardsystem'),
                ),
            ),
            array(
                'name' => __('No. Of days applies to the Members who are making Purchases ', 'rewardsystem'),
                'desc' => __('Enter the Number of Days the Referral Member should be active Before the Referee Earn Points', 'rewardsystem'),
                'id' => '_rs_select_referral_points_referee_time_content',
                'css' => 'min-width:150px;',
                'newids' => '_rs_select_referral_points_referee_time_content',
                'type' => 'text',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_rs_ban_referee_points_time'),
//            array(
//                'name' => __('Timezone Settings', 'rewardsystem'),
//                'type' => 'title',
//                'id' => '_rs_timezone_settings',
//            ),
//            array(
//                'name' => __('Time Zone Selection', 'rewardsystem'),
//                'desc' => __('Here you can set on which TimeZone Selection that time should appear', 'rewardsystem'),
//                'tip' => '',
//                'id' => 'rs_timezone_options',
//                'css' => '',
//                'std' => '1',
//                'type' => 'radio',
//                'options' => array('1' => 'UTC', '2' => 'WordPress Local Time'),
//                'newids' => 'rs_timezone_options',
//                'desc_tip' => true,
//            ),
//            array('type' => 'sectionend', 'id' => '_rs_timezone_settings'),
        ));
    }

    /**
     * Registering Custom Field Admin Settings of Crowdfunding in woocommerce admin fields funtion
     */
    public static function reward_system_register_admin_settings() {
        woocommerce_admin_fields(FPRewardSystem::rewardsystem_admin_fields());
    }

    /**
     * Update the Settings on Save Changes may happen in crowdfunding
     */
    public static function reward_system_update_settings() {
        woocommerce_update_options(FPRewardSystem::rewardsystem_admin_fields());
    }

    /**
     * Initialize the Default Settings by looping this function
     */
    public static function reward_system_default_settings() {
        $newwoocommerce = "woocommerce/woocommerce.php";
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );


        if (is_plugin_active($newwoocommerce)) {
            global $woocommerce;
            foreach (FPRewardSystem::rewardsystem_admin_fields() as $setting)
                if (isset($setting['newids']) && isset($setting['std'])) {
                    add_option($setting['newids'], $setting['std']);
                }
            $path = plugins_url() . '/rewardsystem/img/gift.png';
            add_option('rs_image_url_upload', $path);
        }
    }

    /**
     * Load the Default JAVASCRIPT and CSS
     */
    public static function reward_system_load_default_enqueues() {
        global $my_admin_page;
//  echo $_GET['page'];
        $newscreenids = get_current_screen();
        //var_dump($newscreenids->id);
        if (isset($_GET['page'])) {
            if (($_GET['page'] == 'rewardsystem_callback')) {
                //$wc_screen_id = sanitize_title(__('WooCommerce', 'woocommerce'));
                $array[] = $newscreenids->id;
                return $array;
            } else {
                $array[] = '';
                return $array;
            }
        }
    }

    public static function reward_system_get_output_buffer() {
        ob_start();
    }

    public static function reward_checkout_redeeming_type_button($cart_subtotal_redeem_amount_checkout, $minimum_cart_total_redeem_checkout) {
        global $woocommerce;
        //$current_points_user = get_user_meta(get_current_user_id(), '_my_reward_points', true);
        //if($current_points_user > '0'){
        $getuserid = get_current_user_id();
        $user_current_points = get_user_meta($getuserid, '_my_reward_points', true);
        $current_carttotal_amount = $woocommerce->cart->total;
        $redeem_conversion = get_option('rs_redeem_point');
        $current_carttotal_in_points = $current_carttotal_amount * $redeem_conversion;
        $limitation_percentage_for_redeeming = get_option('rs_percentage_cart_total_redeem');
        $updated_points_step1 = $current_carttotal_in_points / 100;
        $updated_points_for_redeeming = $updated_points_step1 * $limitation_percentage_for_redeeming;
        $currency_symbol_string_to_find = "[currencysymbol]";
        $cartpoints_string_to_replace = "[cartredeempoints]";
        $currency_symbol_string_to_find = "[currencysymbol]";
        $cuurency_value_string_to_find = "[pointsvalue]";
        if ($user_current_points >= $updated_points_for_redeeming) {
            $points_for_redeeming = $updated_points_for_redeeming;
            //$percentage_string_to_replace = "[redeempercent]";
            $cuurency_value_string_to_find = "[pointsvalue]";
            $points_conversion_value = get_option('rs_redeem_point_value');
            $points_currency_value = $updated_points_for_redeeming / $redeem_conversion;
            $points_currency_amount_to_replace = $points_currency_value * $points_conversion_value;
            $points_for_redeeming = $updated_points_for_redeeming;
            $redeem_button_message_more = get_option('rs_redeeming_button_option_message_checkout');

            $currency_symbol_string_to_replace = get_woocommerce_currency_symbol();
            $redeem_button_message_replaced_first = str_replace($cuurency_value_string_to_find, $points_currency_amount_to_replace, $redeem_button_message_more);
            $redeem_button_message_replaced_second = str_replace($currency_symbol_string_to_find, $currency_symbol_string_to_replace, $redeem_button_message_replaced_first);
            $redeem_button_message_replaced_third = str_replace($cartpoints_string_to_replace, $points_for_redeeming, $redeem_button_message_replaced_second);
        } else {
            $points_for_redeeming = $user_current_points;
            $redeem_button_message_more = get_option('rs_redeeming_button_option_message_checkout');
            $points_conversion_value = get_option('rs_redeem_point_value');
            $points_currency_value = $points_for_redeeming / $redeem_conversion;
            $currency_symbol_string_to_replace = get_woocommerce_currency_symbol();
            $points_currency_amount_to_replace = $points_currency_value * $points_conversion_value;
            $redeem_button_message_replaced_first = str_replace($currency_symbol_string_to_find, $currency_symbol_string_to_replace, $redeem_button_message_more);
            $redeem_button_message_replaced_second = str_replace($cuurency_value_string_to_find, $points_currency_amount_to_replace, $redeem_button_message_replaced_first);
            $redeem_button_message_replaced_third = str_replace($cartpoints_string_to_replace, $points_for_redeeming, $redeem_button_message_replaced_second);
        }
        if (get_user_meta(get_current_user_id(), '_my_reward_points', true) >= get_option("rs_first_time_minimum_user_points")) {
            if ($cart_subtotal_redeem_amount_checkout >= $minimum_cart_total_redeem_checkout) {
                ?>
                <form method="post">
                    <div class="woocommerce-info"><?php echo $redeem_button_message_replaced_third; ?>
                        <input id="rs_apply_coupon_code_field" class="input-text" type="hidden" placeholder="<?php echo $placeholder; ?>" value="<?php echo $points_for_redeeming; ?> " name="rs_apply_coupon_code_field">                                            <input class="button <?php echo get_option('rs_extra_class_name_apply_reward_points'); ?>" type="submit" id='mainsubmi' value="<?php echo get_option('rs_redeem_field_submit_button_caption'); ?>" name="rs_apply_coupon_code">
                    </div>
                </form>
                <?php
                // }
            }
        }
    }

    public static function reward_system_checkout_page_redeeming() {
        if (is_user_logged_in()) {
            global $woocommerce;
            $getinfousernickname = get_user_by('id', get_current_user_id());
            $couponcodeuserlogin = $getinfousernickname->user_login;
            $minimum_cart_total_redeem = get_option('rs_minimum_cart_total_points');
            $cart_subtotal_for_redeem = $woocommerce->cart->get_cart_subtotal();
            $cart_subtotal_redeem_amount = preg_replace('/[^0-9\.]+/', '', $cart_subtotal_for_redeem);
            $getinfousernickname = get_user_by('id', get_current_user_id());
            $couponcodeuserlogin = $getinfousernickname->user_login;
            $current_points_user = get_user_meta(get_current_user_id(), '_my_reward_points', true);
            if ($current_points_user > '0') {
                $minimum_cart_total_redeem_checkout = get_option('rs_minimum_cart_total_points');
                $cart_subtotal_for_redeem_checkout = $woocommerce->cart->subtotal;
                $cart_subtotal_redeem_amount_checkout = $cart_subtotal_for_redeem_checkout;
                //var_dump($cart_subtotal_redeem_amount_checkout);
                if (get_option('rs_show_hide_redeem_field_checkout') == '1') {
                    if (get_user_meta(get_current_user_id(), 'rsfirsttime_redeemed', true) != '1') {
                        //if ($cart_subtotal_redeem_amount_checkout >= $minimum_cart_total_redeem_checkout) {
//                        $banned_user_list = get_option('rs_banned-users_list');
//                        if (!in_array(get_current_user_id(), (array) $banned_user_list)) {
//                            $getarrayofuserdata = get_userdata(get_current_user_id());
//                            $banninguserrole = get_option('rs_banning_user_role');
//                            if (!in_array(isset($getarrayofuserdata->roles[0]) ? $getarrayofuserdata->roles[0] : '0', (array) $banninguserrole)) {
                        $userid = get_current_user_id();
                        $banning_type = FPRewardSystem::check_banning_type($userid);
                        if ($banning_type != 'redeemingonly' && $banning_type != 'both') {
                            if (get_option('rs_show_hide_redeem_it_field_checkout') == '1') {
                                if (get_option('rs_redeem_field_type_option_checkout') == '1') {
                                    //$current_points_user = get_user_meta(get_current_user_id(), '_my_reward_points', true);
                                    //if($current_points_user > '0'){
                                    if (get_user_meta(get_current_user_id(), '_my_reward_points', true) >= get_option("rs_first_time_minimum_user_points")) {
                                        if ($cart_subtotal_redeem_amount_checkout >= $minimum_cart_total_redeem_checkout) {
                                            if (get_option('rs_redeem_field_type_option') == '1') {
                                                ?>
                                                <div class="woocommerce-info"><?php echo get_option('rs_reedming_field_label_checkout'); ?> <a href="javascript:void(0)" class="redeemit"> <?php echo get_option('rs_reedming_field_link_label_checkout'); ?></a></div>
                                                <?php
                                            } else {
                                                self::reward_checkout_redeeming_type_button($cart_subtotal_redeem_amount_checkout, $minimum_cart_total_redeem_checkout);
                                            }
                                        }
                                    }
                                    // }
                                } else {
                                    //$current_points_user = get_user_meta(get_current_user_id(), '_my_reward_points', true);
                                    //if($current_points_user > '0'){
                                    $getuserid = get_current_user_id();
                                    $user_current_points = get_user_meta($getuserid, '_my_reward_points', true);
                                    $current_carttotal_amount = $woocommerce->cart->total;
                                    $redeem_conversion = get_option('rs_redeem_point');
                                    $current_carttotal_in_points = $current_carttotal_amount * $redeem_conversion;
                                    $limitation_percentage_for_redeeming = get_option('rs_percentage_cart_total_redeem');
                                    $updated_points_step1 = $current_carttotal_in_points / 100;
                                    $updated_points_for_redeeming = $updated_points_step1 * $limitation_percentage_for_redeeming;
                                    $currency_symbol_string_to_find = "[currencysymbol]";
                                    $cartpoints_string_to_replace = "[cartredeempoints]";
                                    $currency_symbol_string_to_find = "[currencysymbol]";
                                    $cuurency_value_string_to_find = "[pointsvalue]";
                                    if ($user_current_points >= $updated_points_for_redeeming) {
                                        $points_for_redeeming = $updated_points_for_redeeming;
                                        //$percentage_string_to_replace = "[redeempercent]";
                                        $cuurency_value_string_to_find = "[pointsvalue]";
                                        $points_conversion_value = get_option('rs_redeem_point_value');
                                        $points_currency_value = $updated_points_for_redeeming / $redeem_conversion;
                                        $points_currency_amount_to_replace = $points_currency_value * $points_conversion_value;
                                        $points_for_redeeming = $updated_points_for_redeeming;
                                        $redeem_button_message_more = get_option('rs_redeeming_button_option_message_checkout');

                                        $currency_symbol_string_to_replace = get_woocommerce_currency_symbol();
                                        $redeem_button_message_replaced_first = str_replace($cuurency_value_string_to_find, $points_currency_amount_to_replace, $redeem_button_message_more);
                                        $redeem_button_message_replaced_second = str_replace($currency_symbol_string_to_find, $currency_symbol_string_to_replace, $redeem_button_message_replaced_first);
                                        $redeem_button_message_replaced_third = str_replace($cartpoints_string_to_replace, $points_for_redeeming, $redeem_button_message_replaced_second);
                                    } else {
                                        $points_for_redeeming = $user_current_points;
                                        $redeem_button_message_more = get_option('rs_redeeming_button_option_message_checkout');
                                        $points_conversion_value = get_option('rs_redeem_point_value');
                                        $points_currency_value = $points_for_redeeming / $redeem_conversion;
                                        $currency_symbol_string_to_replace = get_woocommerce_currency_symbol();
                                        $points_currency_amount_to_replace = $points_currency_value * $points_conversion_value;
                                        $redeem_button_message_replaced_first = str_replace($currency_symbol_string_to_find, $currency_symbol_string_to_replace, $redeem_button_message_more);
                                        $redeem_button_message_replaced_second = str_replace($cuurency_value_string_to_find, $points_currency_amount_to_replace, $redeem_button_message_replaced_first);
                                        $redeem_button_message_replaced_third = str_replace($cartpoints_string_to_replace, $points_for_redeeming, $redeem_button_message_replaced_second);
                                    }
                                    if (get_user_meta(get_current_user_id(), '_my_reward_points', true) >= get_option("rs_first_time_minimum_user_points")) {
                                        if ($cart_subtotal_redeem_amount_checkout >= $minimum_cart_total_redeem_checkout) {
                                            ?>
                                            <form method="post">
                                                <div class="woocommerce-info"><?php echo $redeem_button_message_replaced_third; ?>
                                                    <input id="rs_apply_coupon_code_field" class="input-text" type="hidden" placeholder="<?php echo $placeholder; ?>" value="<?php echo $points_for_redeeming; ?> " name="rs_apply_coupon_code_field">                                            <input class="button <?php echo get_option('rs_extra_class_name_apply_reward_points'); ?>" type="submit" id='mainsubmi' value="<?php echo get_option('rs_redeem_field_submit_button_caption'); ?>" name="rs_apply_coupon_code">
                                                </div>
                                            </form>
                                            <?php
                                            // }
                                        }
                                    }
                                }
                            }
//                            }
//                        }
                        }
                        ?>
                        <form name="checkout_redeeming" class="checkout_redeeming" method="post">
                            <?php
                            FPRewardSystem::reward_system_add_message_after_cart_table();
                            ?>
                        </form>
                        <?php
                        //  }
                    } else {
                        if (get_user_meta(get_current_user_id(), '_my_reward_points', true) >= get_option("rs_minimum_user_points_to_redeem")) {
                            //if ($cart_subtotal_redeem_amount_checkout >= $minimum_cart_total_redeem_checkout) {
//                            $banned_user_list = get_option('rs_banned-users_list');
//                            if (!in_array(get_current_user_id(), (array) $banned_user_list)) {
//                                $getarrayofuserdata = get_userdata(get_current_user_id());
//                                $banninguserrole = get_option('rs_banning_user_role');
//                                if (!in_array(isset($getarrayofuserdata->roles[0]) ? $getarrayofuserdata->roles[0] : '0', (array) $banninguserrole)) {
                            $userid = get_current_user_id();
                            $banning_type = FPRewardSystem::check_banning_type($userid);
                            if ($banning_type != 'redeemingonly' && $banning_type != 'both') {
                                if (get_option('rs_show_hide_redeem_it_field_checkout') == '1') {
                                    if (get_option('rs_redeem_field_type_option_checkout') == '1') {
                                        //$current_points_user = get_user_meta(get_current_user_id(), '_my_reward_points', true);
                                        // if($current_points_user > '0'){
                                        if (get_user_meta(get_current_user_id(), '_my_reward_points', true) >= get_option("rs_first_time_minimum_user_points")) {
                                            if ($cart_subtotal_redeem_amount >= $minimum_cart_total_redeem) {
                                                if (get_option('rs_redeem_field_type_option') == '1') {
                                                    ?>
                                                    <div class="woocommerce-info"><?php echo get_option('rs_reedming_field_label_checkout'); ?> <a href="javascript:void(0)" class="redeemit"> <?php echo get_option('rs_reedming_field_link_label_checkout'); ?></a></div>
                                                    <?php
                                                } else {
                                                    self::reward_checkout_redeeming_type_button($cart_subtotal_redeem_amount_checkout, $minimum_cart_total_redeem_checkout);
                                                }
                                            }
                                        }
                                        //}
                                    } else {
                                        //$current_points_user = get_user_meta(get_current_user_id(), '_my_reward_points', true);
                                        //if($current_points_user > '0'){
                                        $getuserid = get_current_user_id();
                                        $user_current_points = get_user_meta($getuserid, '_my_reward_points', true);
                                        $current_carttotal_amount = $woocommerce->cart->total;
                                        $redeem_conversion = get_option('rs_redeem_point');
                                        $current_carttotal_in_points = $current_carttotal_amount * $redeem_conversion;
                                        $limitation_percentage_for_redeeming = get_option('rs_percentage_cart_total_redeem');
                                        $updated_points_step1 = $current_carttotal_in_points / 100;
                                        $updated_points_for_redeeming = $updated_points_step1 * $limitation_percentage_for_redeeming;
                                        $currency_symbol_string_to_find = "[currencysymbol]";
                                        $cartpoints_string_to_replace = "[cartredeempoints]";
                                        $currency_symbol_string_to_find = "[currencysymbol]";
                                        $cuurency_value_string_to_find = "[pointsvalue]";
                                        if ($user_current_points >= $updated_points_for_redeeming) {
                                            $points_for_redeeming = $updated_points_for_redeeming;
                                            //$percentage_string_to_replace = "[redeempercent]";
                                            $cuurency_value_string_to_find = "[pointsvalue]";
                                            $points_conversion_value = get_option('rs_redeem_point_value');
                                            $points_currency_value = $updated_points_for_redeeming / $redeem_conversion;
                                            $points_currency_amount_to_replace = $points_currency_value * $points_conversion_value;
                                            $points_for_redeeming = $updated_points_for_redeeming;
                                            $redeem_button_message_more = get_option('rs_redeeming_button_option_message_checkout');

                                            $currency_symbol_string_to_replace = get_woocommerce_currency_symbol();
                                            $redeem_button_message_replaced_first = str_replace($cuurency_value_string_to_find, $points_currency_amount_to_replace, $redeem_button_message_more);
                                            $redeem_button_message_replaced_second = str_replace($currency_symbol_string_to_find, $currency_symbol_string_to_replace, $redeem_button_message_replaced_first);
                                            $redeem_button_message_replaced_third = str_replace($cartpoints_string_to_replace, $points_for_redeeming, $redeem_button_message_replaced_second);
                                        } else {
                                            $points_for_redeeming = $user_current_points;
                                            $redeem_button_message_more = get_option('rs_redeeming_button_option_message_checkout');
                                            $points_conversion_value = get_option('rs_redeem_point_value');
                                            $points_currency_value = $points_for_redeeming / $redeem_conversion;
                                            $currency_symbol_string_to_replace = get_woocommerce_currency_symbol();
                                            $points_currency_amount_to_replace = $points_currency_value * $points_conversion_value;
                                            $redeem_button_message_replaced_first = str_replace($currency_symbol_string_to_find, $currency_symbol_string_to_replace, $redeem_button_message_more);
                                            $redeem_button_message_replaced_second = str_replace($cuurency_value_string_to_find, $points_currency_amount_to_replace, $redeem_button_message_replaced_first);
                                            $redeem_button_message_replaced_third = str_replace($cartpoints_string_to_replace, $points_for_redeeming, $redeem_button_message_replaced_second);
                                        }
                                        if ($cart_subtotal_redeem_amount >= $minimum_cart_total_redeem) {
                                            ?>
                                            <form method="post">
                                                <div class="woocommerce-info"><?php echo $redeem_button_message_replaced_third; ?>
                                                    <input id="rs_apply_coupon_code_field" class="input-text" type="hidden" placeholder="<?php echo $placeholder; ?>" value="<?php echo $points_for_redeeming; ?> " name="rs_apply_coupon_code_field">                                            <input class="button <?php echo get_option('rs_extra_class_name_apply_reward_points'); ?>" type="submit" id='mainsubmi' value="<?php echo get_option('rs_redeem_field_submit_button_caption'); ?>" name="rs_apply_coupon_code">
                                                </div>
                                            </form>
                                            <?php
                                            //}
                                        }
                                    }
                                }
//                                }
//                            }
                            }
                            ?>
                            <form name="checkout_redeeming" class="checkout_redeeming" method="post">
                                <?php
                                FPRewardSystem::reward_system_add_message_after_cart_table();
                                ?>
                            </form>
                            <?php
                            //}
                        } else {
                            $rs_minpoints_after_first_redeem = get_option('rs_min_points_after_first_error');
                            $min_points_to_replace = get_option('rs_minimum_user_points_to_redeem');
                            $min_points_to_find = "[points_after_first_redeem]";
                            $min_points_after_first_replaced = str_replace($min_points_to_find, $min_points_to_replace, $rs_minpoints_after_first_redeem);
                            ?>
                            <div class="woocommerce-info"><?php echo $min_points_after_first_replaced; ?></div>
                            <?php
                        }
                    }
                }
                ?>

                <script type = "text/javascript">
                <?php if (get_option('rs_show_hide_redeem_it_field_checkout') == '1') { ?>
                        jQuery('.fp_apply_reward').css("display", "none");
                        jQuery('.woocommerce-info a.redeemit').click(function () {
                            jQuery('.fp_apply_reward').toggle();
                        });
                <?php } ?>
                </script>
                <?php
            } else {
                if (get_option('rs_show_hide_points_empty_error_message') == '1') {
                    $userid = get_current_user_id();
                    $banning_type = FPRewardSystem::check_banning_type($userid);
                    if ($banning_type != 'redeemingonly' && $banning_type != 'both') {
                        $user_points_empty_message = get_option('rs_current_points_empty_error_message');
                        ?>
                        <div class="woocommerce-info"><?php echo $user_points_empty_message; ?></div>
                        <?php
                    }
                }
            }
        }
    }

    public static function reward_system_add_message_after_cart_table() {
//        $banned_user_list = get_option('rs_banned-users_list');
//        if (!in_array(get_current_user_id(), (array) $banned_user_list)) {
//            $getarrayofuserdata = get_userdata(get_current_user_id());
//            $banninguserrole = get_option('rs_banning_user_role');
//            if (!in_array(isset($getarrayofuserdata->roles[0]) ? $getarrayofuserdata->roles[0] : '0', (array) $banninguserrole)) {
        $userid = get_current_user_id();
        $banning_type = FPRewardSystem::check_banning_type($userid);
        if ($banning_type != 'redeemingonly' && $banning_type != 'both') {
            ?>
            <style type="text/css">
            <?php echo get_option('rs_cart_page_custom_css'); ?>
            </style>
            <?php
            global $woocommerce;
            global $coupon_code;
            if (is_user_logged_in()) {
                $user_ID = get_current_user_id();
                //$user_ID = get_current_user_id();
                // $coupon_code = get_user_meta($user_ID, 'nickname', true); // Code
                $getinfousernickname = get_user_by('id', $user_ID);
                $couponcodeuserlogin = $getinfousernickname->user_login;
                $minimum_cart_total_redeem = get_option('rs_minimum_cart_total_points');
                $cart_subtotal_for_redeem = $woocommerce->cart->subtotal;



                $cart_subtotal_redeem_amount = $cart_subtotal_for_redeem;
                // var_dump($cart_subtotal_redeem_amount);
                $getinfousernickname = get_user_by('id', $user_ID);
                $couponcodeuserlogin = $getinfousernickname->user_login;

                // $usernickname = 'sumo_'.strtolower("$couponcodeuserlogin");
                //$usernickname = 'sumo_'.strtolower("$couponcodeuserlogin");
                if (get_user_meta($user_ID, '_my_reward_points', true) > 0) {
                    //if ($cart_subtotal_redeem_amount >= $minimum_cart_total_redeem) {
                    $coupon_code = 'sumo_' . strtolower($couponcodeuserlogin); // Code
                    $coupon = new WC_Coupon($coupon_code);
                    // var_dump($coupon->id);
                    if (get_user_meta($user_ID, 'rsfirsttime_redeemed', true) != '1') {
                        //if ($cart_subtotal_redeem_amount >= $minimum_cart_total_redeem) {
                        if (get_user_meta($user_ID, '_my_reward_points', true) >= get_option("rs_first_time_minimum_user_points")) {
                            if ($cart_subtotal_redeem_amount >= $minimum_cart_total_redeem) {
                                //if (get_option('rs_show_hide_redeem_field') == '1') {
                                if (get_option('rs_redeem_field_type_option') == '1') {
                                    ?>
                                    <div class="fp_apply_reward">
                                        <?php if (get_option("rs_show_hide_redeem_caption") == '1') { ?>
                                            <label for="rs_apply_coupon_code_field"><?php echo get_option('rs_redeem_field_caption'); ?></label>
                                        <?php } ?>
                                        <?php
                                        if (get_option('rs_show_hide_redeem_placeholder') == '1') {
                                            $placeholder = get_option('rs_redeem_field_placeholder');
                                        }
                                        ?>
                                        <input id="rs_apply_coupon_code_field" class="input-text" type="text" placeholder="<?php echo $placeholder; ?>" value="" name="rs_apply_coupon_code_field">
                                        <input class="button <?php echo get_option('rs_extra_class_name_apply_reward_points'); ?>" type="submit" id='mainsubmi' value="<?php echo get_option('rs_redeem_field_submit_button_caption'); ?>" name="rs_apply_coupon_code">
                                    </div>
                                    <div class='rs_warning_message' style='display:inline-block;color:red'></div>
                                    <?php
                                }
                                //}
                            } else {
                                if (get_option('rs_show_hide_minimum_cart_total_error_message') == '1') {
                                    $userid = get_current_user_id();
                                    $banning_type = FPRewardSystem::check_banning_type($userid);
                                    if ($banning_type != 'redeemingonly' && $banning_type != 'both') {
                                        $min_cart_total_redeeming = get_option('rs_min_cart_total_redeem_error');
                                        $min_cart_amount_to_find = "[carttotal]";
                                        $min_cart_total_currency_to_find = "[currencysymbol]";
                                        $min_cart_total_currency_to_replace = get_woocommerce_currency_symbol();
                                        $min_cart_amount_to_replace = get_option('rs_minimum_cart_total_points');
                                        $min_cart_total_msg1 = str_replace($min_cart_amount_to_find, $min_cart_amount_to_replace, $min_cart_total_redeeming);
                                        $min_cart_total_replaced = str_replace($min_cart_total_currency_to_find, $min_cart_total_currency_to_replace, $min_cart_total_msg1);
                                        ?>
                                        <div class="woocommerce-info"><?php echo $min_cart_total_replaced; ?></div>
                                        <?php
                                    }
                                }
                            }
                        } else {
                            if (get_option('rs_show_hide_first_redeem_error_message') == '1') {
                                $userid = get_current_user_id();
                                $banning_type = FPRewardSystem::check_banning_type($userid);
                                if ($banning_type != 'redeemingonly' && $banning_type != 'both') {
                                    $rs_first_redeem_message = get_option('rs_min_points_first_redeem_error_message');
                                    $rs_first_redeem_to_find = "[firstredeempoints]";
                                    $rs_first_redeem_to_replace = get_option('rs_first_time_minimum_user_points');
                                    $rs_first_redeem_replaced = str_replace($rs_first_redeem_to_find, $rs_first_redeem_to_replace, $rs_first_redeem_message);
                                    ?>

                                    <div class="woocommerce-info"><?php echo $rs_first_redeem_replaced; ?></div>
                                    <?php
                                }
                            }
                        }
                        //}
                    } else {
//                                if ($cart_subtotal_redeem_amount >= $minimum_cart_total_redeem) {
                        if (get_user_meta($user_ID, '_my_reward_points', true) >= get_option("rs_first_time_minimum_user_points")) {
                            if ($cart_subtotal_redeem_amount >= $minimum_cart_total_redeem) {
                                //if (get_option('rs_show_hide_redeem_field') == '1') {
                                if (get_option('rs_redeem_field_type_option') == '1') {
                                    ?>
                                    <div class="fp_apply_reward">
                                        <?php if (get_option("rs_show_hide_redeem_caption") == '1') { ?>
                                            <label for="rs_apply_coupon_code_field"><?php echo get_option('rs_redeem_field_caption'); ?></label>
                                        <?php } ?>
                                        <?php
                                        if (get_option('rs_show_hide_redeem_placeholder') == '1') {
                                            $placeholder = get_option('rs_redeem_field_placeholder');
                                        }
                                        ?>
                                        <input id="rs_apply_coupon_code_field" class="input-text" type="text" placeholder="<?php echo $placeholder; ?>" value="" name="rs_apply_coupon_code_field">
                                        <input class="button <?php echo get_option('rs_extra_class_name_apply_reward_points'); ?>" type="submit" id='mainsubmi' value="<?php echo get_option('rs_redeem_field_submit_button_caption'); ?>" name="rs_apply_coupon_code">
                                    </div>
                                    <div class='rs_warning_message' style='display:inline-block;color:red'></div>
                                    <?php
                                }
                                //}
                            } else {
                                if (get_option('rs_show_hide_minimum_cart_total_error_message') == '1') {
                                    $userid = get_current_user_id();
                                    $banning_type = FPRewardSystem::check_banning_type($userid);
                                    if ($banning_type != 'redeemingonly' && $banning_type != 'both') {
                                        $min_cart_total_redeeming = get_option('rs_min_cart_total_redeem_error');
                                        $min_cart_amount_to_find = "[carttotal]";
                                        $min_cart_total_currency_to_find = "[currencysymbol]";
                                        $min_cart_total_currency_to_replace = get_woocommerce_currency_symbol();
                                        $min_cart_amount_to_replace = get_option('rs_minimum_cart_total_points');
                                        $min_cart_total_msg1 = str_replace($min_cart_amount_to_find, $min_cart_amount_to_replace, $min_cart_total_redeeming);
                                        $min_cart_total_replaced = str_replace($min_cart_total_currency_to_find, $min_cart_total_currency_to_replace, $min_cart_total_msg1);
                                        ?>
                                        <div class="woocommerce-info"><?php echo $min_cart_total_replaced; ?></div>
                                        <?php
                                    }
                                }
                            }
                        } else {
                            if (get_option('rs_show_hide_after_first_redeem_error_message') == '1') {
                                $userid = get_current_user_id();
                                $banning_type = FPRewardSystem::check_banning_type($userid);
                                if ($banning_type != 'redeemingonly' && $banning_type != 'both') {
                                    $rs_minpoints_after_first_redeem = get_option('rs_min_points_after_first_error');
                                    $min_points_to_replace = get_option('rs_minimum_user_points_to_redeem');
                                    $min_points_to_find = "[points_after_first_redeem]";
                                    $min_points_after_first_replaced = str_replace($min_points_to_find, $min_points_to_replace, $rs_minpoints_after_first_redeem);
                                    ?>
                                    <div class="woocommerce-info"><?php echo $min_points_after_first_replaced; ?></div>
                                    <?php
                                }
                            }
                        }
//                            }else{
//                                $min_cart_total_redeeming = get_option('rs_min_cart_total_redeem_error');
//                            $min_cart_amount_to_find = "[carttotal]";
//                            $min_cart_total_currency_to_find ="[currencysymbol]";
//                            $min_cart_total_currency_to_replace = get_woocommerce_currency_symbol();
//                            $min_cart_amount_to_replace = get_option('rs_minimum_cart_total_points');
//                            $min_cart_total_msg1 = str_replace($min_cart_amount_to_find,$min_cart_amount_to_replace,$min_cart_total_redeeming );
//                            $min_cart_total_replaced = str_replace($min_cart_total_currency_to_find,$min_cart_total_currency_to_replace,$min_cart_total_msg1 );
                        ?>
                        <!--                         <div class="woocommerce-info"><?php echo $min_cart_total_replaced; ?></div>-->
                        <?php
                        //}
                    }
                    //}
                } else {
                    if (get_option('rs_show_hide_points_empty_error_message') == '1') {
                        $userid = get_current_user_id();
                        $banning_type = FPRewardSystem::check_banning_type($userid);
                        if ($banning_type != 'redeemingonly' && $banning_type != 'both') {
                            $user_points_empty_message = get_option('rs_current_points_empty_error_message');
                            ?>
                            <div class="woocommerce-info"><?php echo $user_points_empty_message; ?></div>
                            <?php
                        }
                    }
                }
            }
//            }
//        }
        }
    }

    public static function add_chosen_to_general_tab() {
        global $woocommerce;
        if (isset($_GET['page'])) {
            if ($_GET['page'] == 'rewardsystem_callback') {
                if ((float) $woocommerce->version > (float) ('2.2.0')) {
                    ?>
                    <script type="text/javascript">
                        jQuery(document).ready(function () {
                            jQuery('#rs_banned-users_list').select2();
                            jQuery('#rs_banning_user_role').select2();
                        });
                    </script>
                    <?php
                } else {
                    ?>
                    <script type="text/javascript">
                        jQuery(document).ready(function () {
                            jQuery('#rs_banned-users_list').chosen();
                            jQuery('#rs_banning_user_role').chosen();
                        });
                    </script>
                    <?php
                }
            }
        }
    }

//    public static function ban_users_from_reward_points(){
//    //echo sizeof($banned_user_list);
//        //var_dump($banned_user_list);
//            //echo $banned_user_list[0];
////        foreach ($tempo as $value){
////            $banned_user_list = $value."<br>";
////            echo $banned_user_list;
////            //var_dump($banned_user_list);
////        }
////        $banned_user_list = get_option('rs_banned-users_list');
////
////        foreach ($banned_user_list as $value){
////            for($i= 0; $i < sizeof($banned_user_list);$i++){
////
////                if($banned_user_list[$i] == $value){
////                    echo 'You are a Banned User'."<br>";
////                }
////            }
////        }
//
//    }

    public static function validation_in_my_cart() {
        ?>
        <script type='text/javascript'>
            jQuery(document).ready(function () {
                jQuery('#mainsubmi').click(function () {
                    var getvalue = jQuery('#rs_apply_coupon_code_field').val();
                    if (getvalue === '') {
                        jQuery('.rs_warning_message').html('<?php echo get_option('rs_redeem_empty_error_message'); ?>');
                        return false;
                    } else if (jQuery.isNumeric(getvalue) == false) {
                        jQuery('.rs_warning_message').html('<?php echo get_option('rs_redeem_character_error_message'); ?>');
                        return false;
                    } else if (getvalue > parseInt('<?php echo get_user_meta(get_current_user_id(), '_my_reward_points', true); ?>')) {
                        jQuery('.rs_warning_message').html('<?php echo get_option('rs_redeem_max_error_message'); ?>');
                        return false;
                    } else if (jQuery.isNumeric(getvalue) == true) {
                        if (getvalue < 0) {
                            jQuery('.rs_warning_message').html('<?php echo get_option('rs_redeem_character_error_message'); ?>');
                            return false;
                        }
                    }
        <?php if (get_option('rs_minimum_redeeming_points') != '') { ?>
                        if (getvalue < parseInt('<?php echo get_option("rs_minimum_redeeming_points"); ?>')) {
                            jQuery('.rs_warning_message').html('<?php echo do_shortcode(get_option("rs_minimum_redeem_point_error_message")); ?>');
                            return false;
                        }
        <?php } ?>
                });
            });</script>
        <?php
    }

    public static function add_message_box_on_above_cart() {
        global $woocommerce;
        global $coupon_code;
        $getcartcontents = $woocommerce->cart;
        foreach ($getcartcontents->applied_coupons as $coupons) {
            $page = get_page_by_title($coupons);
            echo $page->ID;
        }
    }

    public static function add_reward_points_to_header($order_id) {
        if (get_option('timezone_string') != '') {
            $timezonedate = date_default_timezone_set(get_option('timezone_string'));
        } else {
            $timezonedate = date_default_timezone_set('UTC');
        }
        $order = new WC_Order($order_id);


        /* Payment Rewards for WooCommerce Order */
        $getpaymentgatewayused = RSUserRoleRewardPoints::user_role_based_reward_points($order->user_id, get_option('rs_reward_payment_gateways_' . $order->payment_method));

        if ($getpaymentgatewayused != '') {
            $checkmypoints_payment = get_user_meta($order->user_id, '_my_reward_points', true);
            $totalpoints_payment = $checkmypoints_payment - $getpaymentgatewayused;
            update_user_meta($order->user_id, '_my_reward_points', $totalpoints_payment);
            $mycurrentpoints_payment = get_user_meta($order->user_id, '_my_reward_points', true);

            $localizemessage = str_replace('{payment_title}', $order->payment_method_title, get_option('_rs_localize_revise_reward_for_payment_gateway_message'));


            $pointslogs_payment[] = array('orderid' => $order_id, 'userid' => $order->user_id, 'points_earned_order' => '', 'points_redeemed' => $getpaymentgatewayused, 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $mycurrentpoints_payment, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => $localizemessage, 'rewarder_for_frontend' => $localizemessage);
            $overalllogs_payment[] = array('userid' => $order->user_id, 'totalvalue' => $getpaymentgatewayused, 'eventname' => $localizemessage, 'date' => date('Y-m-d H:i:s'));
            $getoveralllogs_payment = get_option('rsoveralllog');
            $logmerges_payment = array_merge((array) $getoveralllogs_payment, $overalllogs_payment);
            update_option('rsoveralllog', $logmerges_payment);
            $getmypointss_payment = get_user_meta($order->user_id, '_my_points_log', true);
            $mergeds_payment = array_merge((array) $getmypointss_payment, $pointslogs_payment);
            update_user_meta($order->user_id, '_my_points_log', $mergeds_payment);
        }



        $new = get_option('testing_points');
        update_option('testing_points', $new + 1);
        $pointslog = array();
        $usernickname = get_user_meta($order->user_id, 'nickname', true);
        delete_user_meta($order->user_id, '_redeemed_points');
        delete_user_meta($order->user_id, '_redeemed_amount');
        $global_referral_enable = get_option('rs_global_enable_disable_reward');
        $global_referral_reward_type = get_option('rs_global_referral_reward_type');
        $global_enable = get_option('rs_global_enable_disable_reward');
        $global_reward_type = get_option('rs_global_reward_type');
        foreach ($order->get_items() as $item) {
            if (get_option('rs_set_price_percentage_reward_points') == '1') {
                $getregularprice = get_post_meta($item['product_id'], '_regular_price', true);
            } else {
                $getregularprice = get_post_meta($item['product_id'], '_price', true);
            }


            do_action_ref_array('rs_redo_points_for_referral_simple', array(&$getregularprice, &$item));

            $getpercent = get_post_meta($item['product_id'], '_rewardsystempercent', true);

            $item['qty'];
            $order->user_id;
            $item['product_id'];
            $referreduser = get_post_meta($order_id, '_referrer_name', true);
            if ($referreduser != '') {
                $refuser = get_user_by('login', $referreduser);
                if ($refuser != false) {
                    $myid = $refuser->ID;
                } else {
                    $myid = $referreduser;
                }
                if (get_post_meta($item['product_id'], '_rewardsystemcheckboxvalue', true) == 'yes') {
                    $getreferraltype = get_post_meta($item['product_id'], '_referral_rewardsystem_options', true);
                    if ($getreferraltype == '1') {
                        $getreferralpoints = get_post_meta($item['product_id'], '_referralrewardsystempoints', true);
                        if ($getreferralpoints == '') {
                            $term = get_the_terms($item['product_id'], 'product_cat');
                            if (is_array($term)) {
                                $rewardpointer = array();
                                foreach ($term as $term) {
                                    $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                    $display_type = get_woocommerce_term_meta($term->term_id, 'referral_enable_rs_rule', true);
                                    if ($enablevalue == 'yes') {
                                        if ($display_type == '1') {
                                            if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true) == '') {
                                                if ($global_referral_enable == '1') {
                                                    if ($global_referral_reward_type == '1') {
                                                        $rewardpointer[] = get_option('rs_global_referral_reward_point');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpointer[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpointer[] = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true);
                                            }
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) / 100;
                                            $getaveragepoints = $getaverage * $getregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) == '') {
                                                $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                                $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                                if ($global_referral_enable == '1') {
                                                    if ($global_referral_reward_type == '1') {
                                                        $rewardpointer[] = get_option('rs_global_referral_reward_point');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpointer[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpointer[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        if ($global_referral_enable == '1') {
                                            if ($global_referral_reward_type == '1') {
                                                $rewardpointer[] = get_option('rs_global_referral_reward_point');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpointer[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if ($global_referral_enable == '1') {
                                    if ($global_referral_reward_type == '1') {
                                        $rewardpointer[] = get_option('rs_global_referral_reward_point');
                                    } else {
                                        $pointconversion = get_option('rs_earn_point');
                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                        $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                        $getaveragepoints = $getaverage * $getregularprice;
                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                        $rewardpointer[] = $pointswithvalue / $pointconversionvalue;
                                    }
                                }
                            }
                            $getreferralpoints = max($rewardpointer);
                        }
                        $exactpointer = RSUserRoleRewardPoints::user_role_based_reward_points($order->user_id, $getreferralpoints) * $item['qty'];

                        $revisereferralproduct = get_option('_rs_log_revise_referral_product_purchase');
                        $revisefromarray = array('{productid}');
                        $revisetoarray = array($item['product_id']);
                        $reviseoverallarray = str_replace($revisefromarray, $revisetoarray, $revisereferralproduct);
                        $overalllogs[] = array('userid' => $myid, 'totalvalue' => $exactpointer, 'eventname' => $reviseoverallarray, 'date' => date('Y-m-d H:i:s'));
                        $getoveralllogs = get_option('rsoveralllog');
                        $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
                        update_option('rsoveralllog', $logmerges);

                        $userpoints = get_user_meta($myid, '_my_reward_points', true);
                        $newgetpoints = $userpoints - $exactpointer;
                        update_user_meta($myid, '_my_reward_points', $newgetpoints);
                        $pointsfixed[] = array('orderid' => $order_id, 'userid' => $myid, 'points_earned_order' => '', 'points_redeemed' => $exactpointer, 'points_value' => '', 'before_order_points' => '', 'totalpoints' => get_user_meta($myid, '_my_reward_points', true), 'date' => $order->order_date, 'rewarder_for' => $reviseoverallarray, 'rewarder_for_frontend' => $reviseoverallarray);
                        $getlogger = get_user_meta($myid, '_my_points_log', true);
                        $merged = array_merge((array) $getlogger, $pointsfixed);
                        update_user_meta($myid, '_my_points_log', $merged);
                    } else {
                        $points = get_option('rs_earn_point');
                        $pointsequalto = get_option('rs_earn_point_value');
                        $getreferralpercent = get_post_meta($item['product_id'], '_referralrewardsystempercent', true);
                        if (get_option('rs_set_price_percentage_reward_points') == '1') {
                            $getregularprices = get_post_meta($item['product_id'], '_regular_price', true);
                        } else {
                            $getregularprices = get_post_meta($item['product_id'], '_price', true);
                        }

                        do_action_ref_array('rs_redo_points_for_referral_simples', array(&$getregularprices, &$item));

                        $referralpercentageproduct = $getreferralpercent / 100;
                        $getreferralpricepercent = $referralpercentageproduct * $getregularprices;
                        $getpointconversion = $getreferralpricepercent * $points;
                        $getreferpoints = $getpointconversion / $pointsequalto;

                        if ($getreferralpercent == '') {
                            $term = get_the_terms($item['product_id'], 'product_cat');
// var_dump($term);
                            if (is_array($term)) {
//var_dump($term);
                                $rewardpoints = array('0');
                                foreach ($term as $term) {

                                    $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                    $display_type = get_woocommerce_term_meta($term->term_id, 'referral_enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                    if ($enablevalue == 'yes') {
                                        if ($display_type == '1') {
                                            if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true) == '') {
                                                $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                                $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                                if ($global_referral_enable == '1') {
                                                    if ($global_referral_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true);
                                            }
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) / 100;
                                            $getaveragepoints = $getaverage * $getregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) == '') {
                                                $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                                $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                                if ($global_referral_enable == '1') {
                                                    if ($global_referral_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        if ($global_referral_enable == '1') {
                                            if ($global_referral_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if ($global_referral_enable == '1') {
                                    if ($global_referral_reward_type == '1') {
                                        $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                    } else {
                                        $pointconversion = get_option('rs_earn_point');
                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                        $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                        $getaveragepoints = $getaverage * $getregularprice;
                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                    }
                                }
                            }
                            $getreferpoints = max($rewardpoints);
                        }
                        $exactpointer = RSUserRoleRewardPoints::user_role_based_reward_points($order->user_id, $getreferpoints) * $item['qty'];

                        $revisereferralproduct = get_option('_rs_log_revise_referral_product_purchase');
                        $revisefromarray = array('{productid}');
                        $revisetoarray = array($item['product_id']);
                        $reviseoverallarray = str_replace($revisefromarray, $revisetoarray, $revisereferralproduct);


                        $overalllogs[] = array('userid' => $myid, 'totalvalue' => $exactpointer, 'eventname' => $reviseoverallarray, 'date' => date('Y-m-d H:i:s'));
                        $getoveralllogs = get_option('rsoveralllog');
                        $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
                        update_option('rsoveralllog', $logmerges);
                        $userpoints = get_user_meta($myid, '_my_reward_points', true);
                        $updatedpoints = $userpoints - $exactpointer;
                        update_user_meta($myid, '_my_reward_points', $updatedpoints);

                        $pointspercent[] = array('orderid' => $order_id, 'userid' => $myid, 'points_earned_order' => '', 'points_redeemed' => $exactpointer, 'points_value' => '', 'before_order_points' => '', 'totalpoints' => get_user_meta($myid, '_my_reward_points', true), 'date' => $order->order_date, 'rewarder_for' => $reviseoverallarray, 'rewarder_for_frontend' => $reviseoverallarray);
                        $getlogg = get_user_meta($myid, '_my_points_log', true);
                        $mergeds = array_merge((array) $getlogg, $pointspercent);
                        update_user_meta($myid, '_my_points_log', $mergeds);
                    }
                }


                /* Referral Reward Points for Variable Products */
                if (get_post_meta($item['variation_id'], '_enable_reward_points', true) == '1') {
                    $variablereferralrewardpoints = get_post_meta($item['variation_id'], '_referral_reward_points', true);
                    $variationreferralselectrule = get_post_meta($item['variation_id'], '_select_referral_reward_rule', true);
                    $variationreferralrewardpercent = get_post_meta($item['variation_id'], '_referral_reward_percent', true);
                    $variable_products = new WC_Product_Variation($item['variation_id']);
                    if (get_option('rs_set_price_percentage_reward_points') == '1') {
                        $variationregularprice = $variable_products->regular_price;
                    } else {
                        $variationregularprice = $variable_products->price;
                    }


                    do_action_ref_array('rs_redo_points_for_referral_variables', array(&$variationregularprice, &$item));

                    if ($variationreferralselectrule == '1') {
                        $getreferralpoints = get_post_meta($item['variation_id'], '_referral_reward_points', true);
                        $parentvariationid = new WC_Product_Variation($item['variation_id']);
                        $newparentid = $parentvariationid->parent->id;
                        if ($getreferralpoints == '') {
                            $term = get_the_terms($newparentid, 'product_cat');
//var_dump($term);
                            if (is_array($term)) {
//var_dump($term);
                                $rewardpoints = array('0');
                                foreach ($term as $term) {

                                    $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                    $display_type = get_woocommerce_term_meta($term->term_id, 'referral_enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                    if ($enablevalue == 'yes') {
                                        if ($display_type == '1') {
                                            if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true) == '') {
                                                $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                                $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                                if ($global_referral_enable == '1') {
                                                    if ($global_referral_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $variationregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true);
                                            }
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) / 100;
                                            $getaveragepoints = $getaverage * $variationregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) == '') {
                                                $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                                $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                                if ($global_referral_enable == '1') {
                                                    if ($global_referral_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $variationregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        if ($global_referral_enable == '1') {
                                            if ($global_referral_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if ($global_referral_enable == '1') {
                                    if ($global_referral_reward_type == '1') {
                                        $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                    } else {
                                        $pointconversion = get_option('rs_earn_point');
                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                        $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                        $getaveragepoints = $getaverage * $variationregularprice;
                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                    }
                                }
                            }
//var_dump($rewardpoints);
                            $getreferralpoints = max($rewardpoints);

//$rewardpoints = array_search($value, $rewardpoints);
                        }
                        $getvarpoints = RSUserRoleRewardPoints::user_role_based_reward_points($order->user_id, $getreferralpoints) * $item['qty'];

                        $revisereferralproduct = get_option('_rs_log_revise_referral_product_purchase');
                        $revisefromarray = array('{productid}');
                        $revisetoarray = array($item['variation_id']);
                        $reviseoverallarray = str_replace($revisefromarray, $revisetoarray, $revisereferralproduct);

                        $overalllogging[] = array('userid' => $myid, 'totalvalue' => $getvarpoints, 'eventname' => $reviseoverallarray, 'date' => date('Y-m-d H:i:s'));
                        $getoveralllogging = get_option('rsoveralllog');
                        $logmerging = array_merge((array) $getoveralllogging, $overalllogging);
                        update_option('rsoveralllog', $logmerging);
                        $userpoints = get_user_meta($myid, '_my_reward_points', true);
                        $newgetpointing = $userpoints - $getvarpoints;
                        update_user_meta($myid, '_my_reward_points', $newgetpointing);

                        $varpointsfixed[] = array('orderid' => $order_id, 'userid' => $myid, 'points_earned_order' => '', 'points_redeemed' => $getvarpoints, 'points_value' => '', 'before_order_points' => '', 'totalpoints' => get_user_meta($myid, '_my_reward_points', true), 'date' => $order->order_date, 'rewarder_for' => $reviseoverallarray, 'rewarder_for_frontend' => $reviseoverallarray);
                        $getlogge = get_user_meta($myid, '_my_points_log', true);
                        $mergedse = array_merge((array) $getlogge, $varpointsfixed);
                        update_user_meta($myid, '_my_points_log', $mergedse);
                    } else {
                        $pointconversion = get_option('rs_earn_point');
                        $pointconversionvalue = get_option('rs_earn_point_value');
                        $getvaraverage = $variationreferralrewardpercent / 100;
                        $getvaraveragepoints = $getvaraverage * $variationregularprice;
                        $getvarpointsvalue = $getvaraveragepoints * $pointconversion;
                        $varpoints = $getvarpointsvalue / $pointconversionvalue;

                        $parentvariationid = new WC_Product_Variation($item['variation_id']);
                        $newparentid = $parentvariationid->parent->id;
                        if ($variationreferralrewardpercent == '') {
                            $term = get_the_terms($newparentid, 'product_cat');
//var_dump($term);
                            if (is_array($term)) {
//var_dump($term);
                                $rewardpoints = array('0');
                                foreach ($term as $term) {

                                    $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                    $display_type = get_woocommerce_term_meta($term->term_id, 'referral_enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                    if ($enablevalue == 'yes') {
                                        if ($display_type == '1') {
                                            if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true) == '') {
                                                $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                                $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                                if ($global_referral_enable == '1') {
                                                    if ($global_referral_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $variationregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true);
                                            }
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) / 100;
                                            $getaveragepoints = $getaverage * $variationregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) == '') {
                                                $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                                $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                                if ($global_referral_enable == '1') {
                                                    if ($global_referral_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $variationregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        if ($global_referral_enable == '1') {
                                            if ($global_referral_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $variationregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if ($global_referral_enable == '1') {
                                    if ($global_referral_reward_type == '1') {
                                        $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                    } else {
                                        $pointconversion = get_option('rs_earn_point');
                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                        $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                        $getaveragepoints = $getaverage * $variationregularprice;
                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                    }
                                }
                            }
//var_dump($rewardpoints);
                            $varpoints = max($rewardpoints);

//$rewardpoints = array_search($value, $rewardpoints);
                        }

                        $getexactvar = RSUserRoleRewardPoints::user_role_based_reward_points($order->user_id, $varpoints) * $item['qty'];

                        $revisereferralproduct = get_option('_rs_log_revise_referral_product_purchase');
                        $revisefromarray = array('{productid}');
                        $revisetoarray = array($item['variation_id']);
                        $reviseoverallarray = str_replace($revisefromarray, $revisetoarray, $revisereferralproduct);

                        $overallloggered[] = array('userid' => $myid, 'totalvalue' => $getexactvar, 'eventname' => $reviseoverallarray, 'date' => date('Y-m-d H:i:s'));
                        $getoverallloggered = get_option('rsoveralllog');
                        $logmergeder = array_merge((array) $getoverallloggered, $overallloggered);
                        update_option('rsoveralllog', $logmergeder);
                        $userpointser = get_user_meta($myid, '_my_reward_points', true);
                        $newgetpointser = $userpointser - $getexactvar;
                        update_user_meta($myid, '_my_reward_points', $newgetpointser);
                        $varpointspercent[] = array('orderid' => $order_id, 'userid' => $myid, 'points_earned_order' => '', 'points_redeemed' => $getexactvar, 'points_value' => '', 'before_order_points' => '', 'totalpoints' => get_user_meta($myid, '_my_reward_points', true), 'date' => $order->order_date, 'rewarder_for' => $reviseoverallarray, 'rewarder_for_frontend' => $reviseoverallarray);
                        $getlogge = get_user_meta($myid, '_my_points_log', true);
                        $mergedse = array_merge((array) $getlogge, $varpointspercent);
                        update_user_meta($myid, '_my_points_log', $mergedse);
                    }
                }
            }
            if (get_post_meta($item['product_id'], '_rewardsystemcheckboxvalue', true) == 'yes') {

                if (get_post_meta($item['product_id'], '_rewardsystem_options', true) == '1') {
                    $getpoints = get_post_meta($item['product_id'], '_rewardsystempoints', true);
                    if ($getpoints == '') {
                        $term = get_the_terms($item['product_id'], 'product_cat');
// var_dump($term);
                        if (is_array($term)) {
//var_dump($term);
                            $rewardpoints = array('0');
                            foreach ($term as $term) {

                                $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                if ($enablevalue == 'yes') {
                                    if ($display_type == '1') {
                                        if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                            $global_enable = get_option('rs_global_enable_disable_reward');
                                            $global_reward_type = get_option('rs_global_reward_type');
                                            if ($global_enable == '1') {
                                                if ($global_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        } else {
                                            $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                        }
                                    } else {
                                        $pointconversion = get_option('rs_earn_point');
                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                        $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                        $getaveragepoints = $getaverage * $getregularprice;
                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                        if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                            $global_enable = get_option('rs_global_enable_disable_reward');
                                            $global_reward_type = get_option('rs_global_reward_type');
                                            if ($global_enable == '1') {
                                                if ($global_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        } else {
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                } else {
                                    if ($global_enable == '1') {
                                        if ($global_reward_type == '1') {
                                            $rewardpoints[] = get_option('rs_global_reward_points');
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_option('rs_global_reward_percent') / 100;
                                            $getaveragepoints = $getaverage * $getregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($global_enable == '1') {
                                if ($global_reward_type == '1') {
                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                } else {
                                    $pointconversion = get_option('rs_earn_point');
                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                    $getaveragepoints = $getaverage * $getregularprice;
                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                }
                            }
                        }
//var_dump($rewardpoints);
                        $getpoints = max($rewardpoints);

//$rewardpoints = array_search($value, $rewardpoints);
                    }
                    $getpointsmulti = RSUserRoleRewardPoints::user_role_based_reward_points($order->user_id, $getpoints) * $item['qty'];

                    $reviseproductpurchasearray = get_option('_rs_log_revise_product_purchase');
                    $fromreviseproductarray = array('{productid}');
                    $toreviseproductarray = array($item['product_id']);
                    $reviseproductpurchasestringreplace = str_replace($fromreviseproductarray, $toreviseproductarray, $reviseproductpurchasearray);


                    $overalllog[] = array('userid' => $order->user_id, 'totalvalue' => $getpointsmulti, 'eventname' => $reviseproductpurchasestringreplace, 'date' => date('Y-m-d H:i:s'));
                    $getoveralllog = get_option('rsoveralllog');
                    $logmerge = array_merge((array) $getoveralllog, $overalllog);
                    //update_option('rsoveralllog', $logmerge);
                    $userpoints = get_user_meta($order->user_id, '_my_reward_points', true);
                    $newgetpoints = $userpoints - $getpointsmulti;
                    update_user_meta($order->user_id, '_my_reward_points', $newgetpoints);
                } else {
                    $points = get_option('rs_earn_point');
                    $pointsequalto = get_option('rs_earn_point_value');
                    $getpercent = get_post_meta($item['product_id'], '_rewardsystempercent', true);
                    if (get_option('rs_set_price_percentage_reward_points') == '1') {
                        $getregularprice = get_post_meta($item['product_id'], '_regular_price', true);
                    } else {
                        $getregularprice = get_post_meta($item['product_id'], '_price', true);
                    }

                    do_action_ref_array('rs_redo_points_for_simple', array(&$getregularprice, &$item));

                    $percentageproduct = $getpercent / 100;
                    $getpricepercent = $percentageproduct * $getregularprice;
                    $getpointconvert = $getpricepercent * $points;
                    $getexactpoint = $getpointconvert / $pointsequalto;

                    if ($getpercent == '') {
                        $term = get_the_terms($item['product_id'], 'product_cat');
// var_dump($term);
                        if (is_array($term)) {
//var_dump($term);
                            $rewardpoints = array('0');
                            foreach ($term as $term) {

                                $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                if ($enablevalue == 'yes') {
                                    if ($display_type == '1') {
                                        if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                            $global_enable = get_option('rs_global_enable_disable_reward');
                                            $global_reward_type = get_option('rs_global_reward_type');
                                            if ($global_enable == '1') {
                                                if ($global_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        } else {
                                            $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                        }
                                    } else {
                                        $pointconversion = get_option('rs_earn_point');
                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                        $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                        $getaveragepoints = $getaverage * $getregularprice;
                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                        if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                            $global_enable = get_option('rs_global_enable_disable_reward');
                                            $global_reward_type = get_option('rs_global_reward_type');
                                            if ($global_enable == '1') {
                                                if ($global_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        } else {
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                } else {
                                    if ($global_enable == '1') {
                                        if ($global_reward_type == '1') {
                                            $rewardpoints[] = get_option('rs_global_reward_points');
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_option('rs_global_reward_percent') / 100;
                                            $getaveragepoints = $getaverage * $getregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($global_enable == '1') {
                                if ($global_reward_type == '1') {
                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                } else {
                                    $pointconversion = get_option('rs_earn_point');
                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                    $getaveragepoints = $getaverage * $getregularprice;
                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                }
                            }
                        }
//var_dump($rewardpoints);
                        $getexactpoint = max($rewardpoints);

//$rewardpoints = array_search($value, $rewardpoints);
                    }


                    $getpointsmulti = RSUserRoleRewardPoints::user_role_based_reward_points($order->user_id, $getexactpoint) * $item['qty'];
                    $reviseproductpurchasearray = get_option('_rs_log_revise_product_purchase');
                    $fromreviseproductarray = array('{productid}');
                    $toreviseproductarray = array($item['product_id']);
                    $reviseproductpurchasestringreplace = str_replace($fromreviseproductarray, $toreviseproductarray, $reviseproductpurchasearray);

                    $overalllog[] = array('userid' => $order->user_id, 'totalvalue' => $getpointsmulti, 'eventname' => $reviseproductpurchasestringreplace, 'date' => date('Y-m-d H:i:s'));
                    $getoveralllog = get_option('rsoveralllog');
                    $logmerge = array_merge((array) $getoveralllog, $overalllog);
                   // update_option('rsoveralllog', $logmerge);
                    $userpoints = get_user_meta($order->user_id, '_my_reward_points', true);
                    $updatedpoints = $userpoints - $getpointsmulti;
                    update_user_meta($order->user_id, '_my_reward_points', $updatedpoints);
                }
            }
            if (get_post_meta($item['variation_id'], '_enable_reward_points', true) == '1') {
                $checkenablevariation = get_post_meta($item['variation_id'], '_enable_reward_points', true);
                $variablerewardpoints = get_post_meta($item['variation_id'], '_reward_points', true);
                $variationselectrule = get_post_meta($item['variation_id'], '_select_reward_rule', true);
                $variationrewardpercent = get_post_meta($item['variation_id'], '_reward_percent', true);
                $variable_product1 = new WC_Product_Variation($item['variation_id']);
#Step 4: You have the data. Have fun :)
                if (get_option('rs_set_price_percentage_reward_points') == '1') {
                    $variationregularprice = $variable_product1->regular_price;
                } else {
                    $variationregularprice = $variable_product1->price;
                }

                do_action_ref_array('rs_redo_points_for_variable', array(&$variationregularprice, &$item));

                if ($variationselectrule == '1') {
                    $getpoints = get_post_meta($item['variation_id'], '_reward_points', true);
                    $parentvariationid = new WC_Product_Variation($item['variation_id']);
                    $newparentid = $parentvariationid->parent->id;
                    if ($getpoints == '') {
                        $term = get_the_terms($newparentid, 'product_cat');
//var_dump($term);
                        if (is_array($term)) {
//var_dump($term);
                            $rewardpoints = array('0');
                            foreach ($term as $term) {

                                $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                if ($enablevalue == 'yes') {
                                    if ($display_type == '1') {
                                        if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                            $global_enable = get_option('rs_global_enable_disable_reward');
                                            $global_reward_type = get_option('rs_global_reward_type');
                                            if ($global_enable == '1') {
                                                if ($global_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $variationregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        } else {
                                            $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                        }
                                    } else {
                                        $pointconversion = get_option('rs_earn_point');
                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                        $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                        $getaveragepoints = $getaverage * $variationregularprice;
                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                        if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                            $global_enable = get_option('rs_global_enable_disable_reward');
                                            $global_reward_type = get_option('rs_global_reward_type');
                                            if ($global_enable == '1') {
                                                if ($global_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $variationregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        } else {
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                } else {
                                    if ($global_enable == '1') {
                                        if ($global_reward_type == '1') {
                                            $rewardpoints[] = get_option('rs_global_reward_points');
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_option('rs_global_reward_percent') / 100;
                                            $getaveragepoints = $getaverage * $variationregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($global_enable == '1') {
                                if ($global_reward_type == '1') {
                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                } else {
                                    $pointconversion = get_option('rs_earn_point');
                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                    $getaveragepoints = $getaverage * $variationregularprice;
                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                }
                            }
                        }
//var_dump($rewardpoints);
                        $getpoints = max($rewardpoints);

//$rewardpoints = array_search($value, $rewardpoints);
                    }

                    $getpointsmulti = RSUserRoleRewardPoints::user_role_based_reward_points($order->user_id, $getpoints) * $item['qty'];


                    $reviseproductpurchasearray = get_option('_rs_log_revise_product_purchase');
                    $fromreviseproductarray = array('{productid}');
                    $toreviseproductarray = array($item['variation_id']);
                    $reviseproductpurchasestringreplace = str_replace($fromreviseproductarray, $toreviseproductarray, $reviseproductpurchasearray);

                    $overalllog[] = array('userid' => $order->user_id, 'totalvalue' => $getpointsmulti, 'eventname' => $reviseproductpurchasestringreplace, 'date' => date('Y-m-d H:i:s'));
                    $getoveralllog = get_option('rsoveralllog');
                    $logmerge = array_merge((array) $getoveralllog, $overalllog);
                   // update_option('rsoveralllog', $logmerge);
                    $userpoints = get_user_meta($order->user_id, '_my_reward_points', true);
                    $newgetpoints = $userpoints - $getpointsmulti;
                    update_user_meta($order->user_id, '_my_reward_points', $newgetpoints);
                } else {
                    $pointconversion = get_option('rs_earn_point');
                    $pointconversionvalue = get_option('rs_earn_point_value');
                    $getaverage = $variationrewardpercent / 100;
                    $getaveragepoints = $getaverage * $variationregularprice;
                    $getpointsvalue = $getaveragepoints * $pointconversion;
                    $points = $getpointsvalue / $pointconversionvalue;

                    $parentvariationid = new WC_Product_Variation($item['variation_id']);
                    $newparentid = $parentvariationid->parent->id;
                    if ($variationrewardpercent == '') {
                        $term = get_the_terms($newparentid, 'product_cat');
//var_dump($term);
                        if (is_array($term)) {
//var_dump($term);
                            $rewardpoints = array('0');
                            foreach ($term as $term) {

                                $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                if ($enablevalue == 'yes') {
                                    if ($display_type == '1') {
                                        if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                            $global_enable = get_option('rs_global_enable_disable_reward');
                                            $global_reward_type = get_option('rs_global_reward_type');
                                            if ($global_enable == '1') {
                                                if ($global_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $variationregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        } else {
                                            $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                        }
                                    } else {
                                        $pointconversion = get_option('rs_earn_point');
                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                        $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                        $getaveragepoints = $getaverage * $variationregularprice;
                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                        if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                            $global_enable = get_option('rs_global_enable_disable_reward');
                                            $global_reward_type = get_option('rs_global_reward_type');
                                            if ($global_enable == '1') {
                                                if ($global_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $variationregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        } else {
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                } else {
                                    if ($global_enable == '1') {
                                        if ($global_reward_type == '1') {
                                            $rewardpoints[] = get_option('rs_global_reward_points');
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_option('rs_global_reward_percent') / 100;
                                            $getaveragepoints = $getaverage * $variationregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($global_enable == '1') {
                                if ($global_reward_type == '1') {
                                    $rewardpoints[] = get_option('rs_global_reward_points');
                                } else {
                                    $pointconversion = get_option('rs_earn_point');
                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                    $getaverage = get_option('rs_global_reward_percent') / 100;
                                    $getaveragepoints = $getaverage * $variationregularprice;
                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                }
                            }
                        }
//var_dump($rewardpoints);
                        $points = max($rewardpoints);

//$rewardpoints = array_search($value, $rewardpoints);
                    }

                    $getpointsmulti = RSUserRoleRewardPoints::user_role_based_reward_points($order->user_id, $points) * $item['qty'];


                    $reviseproductpurchasearray = get_option('_rs_log_revise_product_purchase');
                    $fromreviseproductarray = array('{productid}');
                    $toreviseproductarray = array($item['variation_id']);
                    $reviseproductpurchasestringreplace = str_replace($fromreviseproductarray, $toreviseproductarray, $reviseproductpurchasearray);

                    $overalllog[] = array('userid' => $order->user_id, 'totalvalue' => $getpointsmulti, 'eventname' => $reviseproductpurchasestringreplace, 'date' => date('Y-m-d H:i:s'));
                    $getoveralllog = get_option('rsoveralllog');
                    $logmerge = array_merge((array) $getoveralllog, $overalllog);
                    //update_option('rsoveralllog', $logmerge);
                    $userpoints = get_user_meta($order->user_id, '_my_reward_points', true);
                    $newgetpoints = $userpoints - $getpointsmulti;
                    update_user_meta($order->user_id, '_my_reward_points', $newgetpoints);
                }
            }


            $rewardpointscoupons = $order->get_items(array('coupon'));
            $getuserdatabyid = get_user_by('id', $order->user_id);
            $getusernickname = $getuserdatabyid->user_login;
            $maincouponchecker = 'sumo_' . strtolower($getusernickname);
            foreach ($rewardpointscoupons as $couponcode => $value) {
                if ($maincouponchecker == $value['name']) {
                    if (get_option('rs_redeem_reward_points_revised' . $order_id) != '1') {

                        $getuserdatabyid = get_user_by('id', $order->user_id);
                        $getusernickname = $getuserdatabyid->user_login;
                        $getcouponid = get_user_meta($order->user_id, 'redeemcouponids', true);
                        $currentamount = get_post_meta($getcouponid, 'coupon_amount', true);
                        //update_option('testings', $order->get_total_discount());
                        if ($currentamount >= $value['discount_amount']) {
                            $current_conversion = get_option('rs_redeem_point');
                            $point_amount = get_option('rs_redeem_point_value');
                            $redeemedamount = $value['discount_amount'] * $current_conversion;
                            $redeemedpoints = $redeemedamount / $point_amount;
                            //$overalllog[] = array('userid' => $order->user_id, 'totalvalue' => $redeemedpoints, 'eventname' => 'Points Redeemed Towards Purchase', 'date' => $order->order_date);
                            //$getoveralllog = get_option('rsoveralllog');
                            //$logmerge = array_merge((array) $getoveralllog, $overalllog);
                            //update_option('rsoveralllog', $logmerge);
                            update_user_meta($order->user_id, '_redeemed_points', $redeemedpoints);
                            update_user_meta($order->user_id, '_redeemed_amount', $value['discount_amount']);
                            $yourpoints = get_user_meta($order->user_id, '_my_reward_points', true);
                            update_user_meta($order->user_id, '_my_reward_points', $yourpoints + $redeemedpoints);
                        }

                        update_option('rs_redeem_reward_points_revised' . $order_id, '1');
                    }
                }
            }


            $totalpoints = get_user_meta($order->user_id, '_my_reward_points', true);
            if(get_option('rs_redeem_reward_points_reviseds'.$order_id) == '1'){
                $getredeemedpoints = '0';
            }else{
               $getredeemedpoints = get_user_meta($order->user_id, '_redeemed_points', true); 
            }
            
            
            $getpointsvalue = get_user_meta($order->user_id, '_redeemed_amount', true);
            $post_url = admin_url('post.php?post=' . $order_id) . '&action=edit';

            $myaccountlink = get_permalink(get_option('woocommerce_myaccount_page_id'));
            $vieworderlink = add_query_arg('view-order', $order_id, $myaccountlink);

            $frontendreviseorderdata = get_option('_rs_log_revise_product_purchase_main');
            $fromfrontendorder = array('{currentorderid}');
            $frontendorderlink = '<a href="' . $vieworderlink . '">#' . $order_id . '</a>';
            $tofrontendorder = array($frontendorderlink);
            $reviseorderstrreplace = str_replace($fromfrontendorder, $tofrontendorder, $frontendreviseorderdata);

            $frombackendorder = array('{currentorderid}');
            $backendorderlink = '<a href="' . $post_url . '">#' . $order_id . '</a>';
            $tobackendorder = array($backendorderlink);
            $revisedbackendorderreplace = str_replace($frombackendorder, $tobackendorder, $frontendreviseorderdata);
            $pointslog[] = array('orderid' => $order_id, 'userid' => $order->user_id, 'points_earned_order' => $getredeemedpoints, 'points_redeemed' => $getpointsmulti, 'points_value' => $getpointsvalue, 'before_order_points' => $userpoints, 'totalpoints' => $totalpoints, 'date' => $order->order_date, 'rewarder_for' => $revisedbackendorderreplace, 'rewarder_for_frontend' => $reviseorderstrreplace);
            add_option('rs_redeem_reward_points_reviseds'.$order_id,'1');
        }
        $rewardpointscoupons = $order->get_items(array('coupon'));
        $getuserdatabyid = get_user_by('id', $order->user_id);
        $getusernickname = $getuserdatabyid->user_login;
        $maincouponchecker = 'sumo_' . strtolower($getusernickname);
        foreach ($rewardpointscoupons as $couponcodeser => $value) {
            if ($maincouponchecker == $value['name']) {
                $getuserdatabyid = get_user_by('id', $order->user_id);
                $getusernickname = $getuserdatabyid->user_login;
                $getcouponid = get_user_meta($order->user_id, 'redeemcouponids', true);
                $currentamount = get_post_meta($getcouponid, 'coupon_amount', true);
                //update_option('testings', $order->get_total_discount());
                if ($currentamount >= $value['discount_amount']) {
                    $current_conversion = get_option('rs_redeem_point');
                    $point_amount = get_option('rs_redeem_point_value');
                    $redeemedamount = $value['discount_amount'] * $current_conversion;
                    $redeemedpoints = $redeemedamount / $point_amount;

                    $revisepointsredeemedpurchase = get_option('_rs_log_revise_points_redeemed_towards_purchase');

                    $overalllog[] = array('userid' => $order->user_id, 'totalvalue' => $redeemedpoints, 'eventname' => $revisepointsredeemedpurchase, 'date' => date('Y-m-d H:i:s'));
                    $getoveralllog = get_option('rsoveralllog');
                    $logmerge = array_merge((array) $getoveralllog, $overalllog);
                    update_option('rsoveralllog', $logmerge);
                    $yourpoints = get_user_meta($order->user_id, '_my_reward_points', true);
                    update_user_meta($order->user_id, '_redeemed_points', $redeemedpoints);
                    update_user_meta($order->user_id, '_redeemed_amount', $value['discount_amount']);
                    $yourpoints = get_user_meta($order->user_id, '_my_reward_points', true);
                    //update_user_meta($order->user_id, '_my_reward_points', $yourpoints - $redeemedpoints);
                }
            }
        }



        $getmypoints = get_user_meta($order->user_id, '_my_points_log', true);
        $merged = array_merge((array) $getmypoints, $pointslog);
        update_user_meta($order->user_id, '_my_points_log', $merged);
    }

    public static function update_reward_points_to_user($order_id) {

        $rs_totalearnedpoints = array();
        $rs_totalredeempoints = array();
        do_action('rs_perform_action_for_order', $order_id);

        $restrictuserpoints = get_option('rs_maximum_earning_points_for_user');
        $enabledisablemaxpoints = get_option('rs_enable_maximum_earning_points');
        $order = new WC_Order($order_id);
        $fp_earned_points_sms = false;


//        $banned_user_list = get_option('rs_banned-users_list');
//        if (!in_array($order->user_id, (array) $banned_user_list)) {
//
//            $getarrayofuserdata = get_userdata($order->user_id);
//            $banninguserrole = get_option('rs_banning_user_role');
//            if (!in_array(isset($getarrayofuserdata->roles[0]) ? $getarrayofuserdata->roles[0] : '0', (array) $banninguserrole)) {

        $banning_type = FPRewardSystem::check_banning_type($order->user_id);
        if ($banning_type != 'earningonly' && $banning_type != 'both') {
            /* Payment Rewards for WooCommerce Order */
            $getpaymentgatewayused = RSUserRoleRewardPoints::user_role_based_reward_points($order->user_id, get_option('rs_reward_payment_gateways_' . $order->payment_method));
            FPRewardSystem::save_total_earned_points($order->user_id, $getpaymentgatewayused);
            if ($getpaymentgatewayused != '') {
                $checkmypoints_payment = get_user_meta($order->user_id, '_my_reward_points', true);
                $totalpoints_payment = $checkmypoints_payment + $getpaymentgatewayused;
                update_user_meta($order->user_id, '_my_reward_points', $totalpoints_payment);
                $mycurrentpoints_payment = get_user_meta($order->user_id, '_my_reward_points', true);

                $localizemessage = str_replace('{payment_title}', $order->payment_method_title, get_option('_rs_localize_reward_for_payment_gateway_message'));

                $pointslogs_payment[] = array('orderid' => $order_id, 'userid' => $order->user_id, 'points_earned_order' => $getpaymentgatewayused, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $mycurrentpoints_payment, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => $localizemessage, 'rewarder_for_frontend' => $localizemessage);
                $overalllogs_payment[] = array('userid' => $order->user_id, 'totalvalue' => $getpaymentgatewayused, 'eventname' => $localizemessage, 'date' => date('Y-m-d H:i:s'));
                $getoveralllogs_payment = get_option('rsoveralllog');
                $logmerges_payment = array_merge((array) $getoveralllogs_payment, $overalllogs_payment);
                update_option('rsoveralllog', $logmerges_payment);
                $getmypointss_payment = get_user_meta($order->user_id, '_my_points_log', true);
                $mergeds_payment = array_merge((array) $getmypointss_payment, $pointslogs_payment);
                update_user_meta($order->user_id, '_my_points_log', $mergeds_payment);
            }
        }

        if (get_post_meta($order_id, '_sumo_points_awarded', true) != 'yes') {
            $pointslog = array();
            $usernickname = get_user_meta($order->user_id, 'nickname', true);
            $checkmypoints = get_user_meta($order->user_id, '_my_reward_points', true);
            delete_user_meta($order->user_id, '_redeemed_points');
            delete_user_meta($order->user_id, '_redeemed_amount');
            $global_referral_enable = get_option('rs_global_enable_disable_reward');
            $global_referral_reward_type = get_option('rs_global_referral_reward_type');
            $global_enable = get_option('rs_global_enable_disable_reward');
            $global_reward_type = get_option('rs_global_reward_type');
            foreach ($order->get_items() as $item) {
                //$userid = get_current_user_id();
                $banning_type = FPRewardSystem::check_banning_type($order->user_id);
                if ($banning_type != 'earningonly' && $banning_type != 'both') {
                    include "inc/rs_update_points_for_earning_points.php";
                

//                        $rewardpointscoupons = $order->get_items(array('coupon'));
//                        $getuserdatabyid = get_user_by('id', $order->user_id);
//                        $getusernickname = $getuserdatabyid->user_login;
//                        $maincouponchecker = 'sumo_' . strtolower($getusernickname);
//                        foreach ($rewardpointscoupons as $couponcode => $value) {
//                            if ($maincouponchecker == $value['name']) {
//                                if (get_option('rewardsystem_looped_over_coupon' . $order_id) != '1') {
//                                    $getuserdatabyid = get_user_by('id', $order->user_id);
//                                    $getusernickname = $getuserdatabyid->user_login;
//                                    $getcouponid = get_user_meta($order->user_id, 'redeemcouponids', true);
//                                    $currentamount = get_post_meta($getcouponid, 'coupon_amount', true);
//                                    //update_option('testings', $order->get_total_discount());
//                                    if ($currentamount >= $value['discount_amount']) {
//                                        $current_conversion = get_option('rs_redeem_point');
//                                        $point_amount = get_option('rs_redeem_point_value');
//                                        $redeemedamount = $value['discount_amount'] * $current_conversion;
//                                        $redeemedpoints = $redeemedamount / $point_amount;
//                                        $overalllog[] = array('userid' => $order->user_id, 'totalvalue' => $redeemedpoints, 'eventname' => 'Points Redeemed Towards Purchase', 'date' => $order->order_date);
//                                        $getoveralllog = get_option('rsoveralllog');
//                                        $logmerge = array_merge((array) $getoveralllog, $overalllog);
//                                        update_option('rsoveralllog', $logmerge);
//                                        update_user_meta($order->user_id, '_redeemed_points', $redeemedpoints);
//                                        update_user_meta($order->user_id, '_redeemed_amount', $value['discount_amount']);
//                                        $yourpoints = get_user_meta($order->user_id, '_my_reward_points', true);
//                                        update_user_meta($order->user_id, '_my_reward_points', $yourpoints - $redeemedpoints);
//                                        $fp_earned_points_sms = true;
//                                        if($fp_earned_points_sms == true){
//                                            if(get_option('rs_enable_send_sms_to_user') == 'yes'){
//                                                if(get_option('rs_send_sms_redeeming_points') == 'yes'){
//                                       if(get_option('rs_sms_sending_api_option') == '1'){
//                        FPRewardSystemSms::send_sms_twilio_api($order_id);
//                    }else{
//                        FPRewardSystemSms::send_sms_nexmo_api($order_id);
//                    }
//                                                }
//                                            }
//                                        }
//
//                                    }
//
//                                    update_option('rewardsystem_looped_over_coupon' . $order_id, '1');
//                                }
//                            }
//                        }


                include 'inc/rs_update_points_for_redeeming.php';


                $totalpoints = get_user_meta($order->user_id, '_my_reward_points', true);

                if (get_option('rewardsystem_looped_over_coupons' . $order_id) == '1') {
                    $getredeemedpoints = "0";
                } else {
                    $getredeemedpoints = get_user_meta($order->user_id, '_redeemed_points', true);
                }


                $getpointsvalue = get_user_meta($order->user_id, '_redeemed_amount', true);
                $post_url = admin_url('post.php?post=' . $order_id) . '&action=edit';
                $myaccountlink = get_permalink(get_option('woocommerce_myaccount_page_id'));
                $vieworderlink = add_query_arg('view-order', $order_id, $myaccountlink);


                $overallmaingetoption = get_option('_rs_localize_points_earned_for_purchase_main');
                $fromstartarray = array('{currentorderid}');

                $frontendorderlink = '<a href="' . $vieworderlink . '">#' . $order_id . '</a>';
                $backendorderlink = '<a href="' . $post_url . '">#' . $order_id . '</a>';
                $toendarrayfrontend = array($frontendorderlink);
                $toendarraybackend = array($backendorderlink);
                $frontendstringreplace = str_replace($fromstartarray, $toendarrayfrontend, $overallmaingetoption);

                if ($getpointsmulti != 0 && $getpointsmulti != '') {
                    if (get_option('rs_send_sms_earning_points') == 'yes') {
                        $fp_earned_points_sms = true;
                    }
                }

                $backendstringreplace = str_replace($fromstartarray, $toendarraybackend, $overallmaingetoption);

                @$pointslog[] = array('orderid' => $order_id, 'userid' => $order->user_id, 'points_earned_order' => $getpointsmulti, 'points_redeemed' => $getredeemedpoints, 'points_value' => $getpointsvalue, 'before_order_points' => $userpoints, 'totalpoints' => $totalpoints, 'date' => $order->order_date, 'rewarder_for' => $backendstringreplace, 'rewarder_for_frontend' => $frontendstringreplace);
                add_option('rewardsystem_looped_over_coupons' . $order_id, '1');

//                    $rewardpointscoupons = $order->get_items(array('coupon'));
//                    $getuserdatabyid = get_user_by('id', $order->user_id);
//                    $getusernickname = $getuserdatabyid->user_login;
//                    $maincouponchecker = 'sumo_' . strtolower($getusernickname);
//                    foreach ($rewardpointscoupons as $couponcodeser => $value) {
//                        if ($maincouponchecker == $value['name']) {
//                            update_user_meta($order->user_id, 'rsfirsttime_redeemed', 1);
//                            $getuserdatabyid = get_user_by('id', $order->user_id);
//                            $getusernickname = $getuserdatabyid->user_login;
//                            $getcouponid = get_user_meta($order->user_id, 'redeemcouponids', true);
//                            $currentamount = get_post_meta($getcouponid, 'coupon_amount', true);
//                            //update_option('testings', $order->get_total_discount());
//                            if ($currentamount >= $value['discount_amount']) {
//                                $current_conversion = get_option('rs_redeem_point');
//                                $point_amount = get_option('rs_redeem_point_value');
//                                $redeemedamount = $value['discount_amount'] * $current_conversion;
//                                $redeemedpoints = $redeemedamount / $point_amount;
//
//                                $redeemloginformation = get_option('_rs_localize_points_redeemed_towards_purchase');
//                                $startarray = array('{currentorderid}');
//                                $endarray = array($order_id);
//
//                                $mainupdatedvaluesinarray = str_replace($startarray, $endarray, $redeemloginformation);
//                                $overalllog[] = array('userid' => $order->user_id, 'totalvalue' => $redeemedpoints, 'eventname' => $mainupdatedvaluesinarray, 'date' => $order->order_date);
//                                $getoveralllog = get_option('rsoveralllog');
//                                $logmerge = array_merge((array) $getoveralllog, $overalllog);
//                                update_option('rsoveralllog', $logmerge);
//                                update_user_meta($order->user_id, '_redeemed_points', $redeemedpoints);
//                                update_user_meta($order->user_id, '_redeemed_amount', $value['discount_amount']);
//                                $yourpoints = get_user_meta($order->user_id, '_my_reward_points', true);
//                                //update_user_meta($order->user_id, '_my_reward_points', $yourpoints - $redeemedpoints);
//                            }
//                        }
//                    }

                



                //Mail Sending for Earning Points and Redeeming Points

                FPRewardSystem::rsmail_sending_on_custom_rule();
                add_post_meta($order_id, '_sumo_points_awarded', 'yes');
                FPRewardSystemCouponPoints::apply_coupon_code_reward_points_user($order_id);

                if ($fp_earned_points_sms == true) {
                    if (get_option('rs_enable_send_sms_to_user') == 'yes') {
                        if (get_option('rs_send_sms_earning_points') == 'yes') {
                            if (get_option('rs_sms_sending_api_option') == '1') {
                                FPRewardSystemSms::send_sms_twilio_api($order_id);
                            } else {
                                FPRewardSystemSms::send_sms_nexmo_api($order_id);
                            }
                        }
                    }
                }
                }
            }
            include 'inc/rs_update_points_for_redeeming2.php';
            update_option('rsoveralllog', $logmerge);
            update_post_meta($order_id, 'rs_total_earned_points', $rs_totalearnedpoints);
            update_post_meta($order_id, 'rs_total_redeem_points', $rs_totalredeempoints);
            $getmypoints = get_user_meta($order->user_id, '_my_points_log', true);
            $merged = array_merge((array) $getmypoints, $pointslog);
            update_user_meta($order->user_id, '_my_points_log', $merged);
        }
//            }
//        }
    }

    public static function test_get_order_discount() {
        global $woocommerce;
        $order = new WC_Order(260);

        var_dump($order->get_used_coupons());
        var_dump($order->get_total_discount());
        $usernickname = get_user_meta($order->user_id, 'nickname', true);
        $getcouponid = get_option($usernickname . 'coupon');
        var_dump(get_post_meta($getcouponid, 'coupon_amount', true));
//
//
//        var_dump($coupons);
//
//        foreach ($rewardpointscoupons as $couponcode => $value) {
//            echo $value['discount_amount'];
//        }
    }

    public static function rsmail_sending_on_custom_rule() {
        global $wpdb;
        global $woocommerce;
        $emailtemplate_table_name = $wpdb->prefix . 'rs_templates_email';
        $email_templates = $wpdb->get_results("SELECT * FROM $emailtemplate_table_name"); //all email templates
        if (is_array($email_templates)) {
            foreach ($email_templates as $emails) {
                if ($emails->mailsendingoptions == '1') {
                    if ($emails->rsmailsendingoptions == '1') {
                        if (get_option('rsearningtemplates' . $emails->id) != '1') {
                            if ($emails->sendmail_options == '1') {
                                include'inc/rsmailsendingearning.php';
                            } else {
                                include'inc/rsmailsendingearning2.php';
                            }
                            update_option('rsearningtemplates' . $emails->id, '1');
                        }
                    }

                    if ($emails->rsmailsendingoptions == '2') {
                        if (get_option('rsredeemingtemplates' . $emails->id) != '1') {
                            if ($emails->sendmail_options == '1') {
                                include'inc/rsmailsendingredeeming.php';
                            } else {
                                include'inc/rsmailsendingredeeming2.php';
                            }
                            update_option('rsredeemingtemplates' . $emails->id, '1');
                        }
                    }
                } else {
                    if ($emails->rsmailsendingoptions == '1') {

                        if ($emails->sendmail_options == '1') {
                            include'inc/rsmailsendingearning.php';
                        } else {
                            include'inc/rsmailsendingearning2.php';
                        }
                    }
                    if ($emails->rsmailsendingoptions == '2') {
                        if ($emails->sendmail_options == '1') {
                            include 'inc/rsmailsendingredeeming.php';
                        } else {
                            include 'inc/rsmailsendingredeeming2.php';
                        }
                    }
                }
            }
        }
    }

    public static function get_variation_id_from_order($order_id) {
        //$order = new WC_Order(195);
        foreach ($order->get_items() as $item) {
            var_dump($item['qty']);
        }
    }

    public static function get_cookies_in_admin() {
        if (isset($_COOKIE['rsreferredusername'])) {
//echo $_COOKIE['rsreferredusername'];
            $refuser = get_user_by('login', $_COOKIE['rsreferredusername']);
            if ($refuser != false) {
                $myid = $refuser->ID;
            } else {
                $myid = $_COOKIE['rsreferredusername'];
            }
            echo $previouscount = get_user_meta($myid, 'rsreferredusernameorder', true) . "<br>";
            echo $previouscounts = get_user_meta($myid, 'rsreferredusernameorders', true);
        }
    }

    public static function myrewardpoints_total_shortcode($content) {
        ob_start();
        if (get_user_meta(get_current_user_id(), '_my_reward_points', true) != '') {
            echo get_option('rs_my_rewards_total') . " " . number_format((float) get_user_meta(get_current_user_id(), '_my_reward_points', true), 2, '.', '') . "<br>";
        } else {
            echo get_option('rs_my_rewards_total') . " 0<br>";
        }
        $content = ob_get_clean();
        return $content;
    }

    public static function viewchangelog_shortcode($content) {
        ob_start();
        ?>
        <style type="text/css">
        <?php echo get_option('rs_myaccount_custom_css'); ?>
        </style>
        <?php
        echo "<h2>" . get_option('rs_my_rewards_title') . "</h2>";
        if (get_user_meta(get_current_user_id(), '_my_reward_points', true) != '') {
            echo "<h4> " . get_option('rs_my_rewards_total') . " " . round(number_format((float) get_user_meta(get_current_user_id(), '_my_reward_points', true), 2, '.', '')) . "</h4><br>";
        } else {
            echo "<h4> " . get_option('rs_my_rewards_total') . " 0</h4><br>";
        }
        $outputtablefields = '<p> ';
        if (get_option('rs_show_hide_search_box_in_my_rewards_table') == '1') {
            $outputtablefields .= __('Search:', 'rewardsystem') . '<input id="filters" type="text"/> ';
        }
        if (get_option('rs_show_hide_page_size_my_rewards') == '1') {
            $outputtablefields .= __('Page Size:', 'rewardsystem') . '<select id="change-page-sizes"><option value="5">5</option><option value="10">10</option><option value="50">50</option>
                    <option value="100">100</option>
                </select>';
        }
        $outputtablefields .= '</p>';
        echo $outputtablefields;
        ?>

        <table class = "examples demo shop_table my_account_orders table-bordered" data-filter = "#filters" data-page-size="5" data-page-previous-text = "prev" data-filter-text-only = "true" data-page-next-text = "next">

            <thead><tr><th data-toggle="true" data-sort-initial = "true"><?php echo get_option('rs_my_rewards_sno_label'); ?></th>
                    <!--<th><?php echo get_option('rs_my_rewards_orderid_label'); ?></th>--><th><?php echo get_option('rs_my_rewards_userid_label'); ?></th>
                    <th><?php echo get_option('rs_my_rewards_rewarder_label'); ?></th>
                    <th><?php echo get_option('rs_my_rewards_points_earned_label'); ?></th>
                    <!--<th data-hide="phone"><?php echo get_option('rs_my_rewards_before_points_label'); ?></th>-->
                    <th data-hide='phone,tablet'><?php echo get_option('rs_my_rewards_redeem_points_label'); ?></th>
                    <th data-hide="phone,tablet"><?php echo get_option('rs_my_rewards_total_points_label'); ?></th><th data-hide="phone,tablet"><?php echo get_option('rs_my_rewards_date_label'); ?></th></tr></thead>
            <tbody>
                <?php
                $user_ID = get_current_user_id();
                //var_dump(get_user_meta($user_ID, '_my_points_log', true));
                $fetcharray = get_user_meta($user_ID, '_my_points_log', true);
                if (is_array($fetcharray)) {
                    if (get_option('rs_points_log_sorting') == '1') {
                        krsort($fetcharray, SORT_NUMERIC);
                    }
                }
                $i = 1;
                if (is_array($fetcharray)) {
                    foreach ($fetcharray as $newarray) {
                        if (is_array($newarray)) {
                            if (!empty($newarray['points_earned_order'])) {
                                if (is_float($newarray['points_earned_order'])) {
                                    $pointsearned = round(number_format($newarray['points_earned_order'], 2));
                                } else {
                                    $pointsearned = round(number_format($newarray['points_earned_order']));
                                }
                            } else {
                                $pointsearned = 0;
                            }

                            if (!empty($newarray['before_order_points'])) {
                                if (is_float($newarray['before_order_points'])) {
                                    $beforepoints = number_format($newarray['before_order_points'], 2);
                                } else {
                                    $beforepoints = number_format($newarray['before_order_points']);
                                }
                            } else {
                                $beforepoints = 0;
                            }

                            if (!empty($newarray['points_redeemed'])) {
                                if (is_float($newarray['points_redeemed'])) {
                                    $redeemedpoints = number_format($newarray['points_redeemed'], 2);
                                } else {
                                    $redeemedpoints = number_format((int) $newarray['points_redeemed']);
                                }
                            } else {
                                $redeemedpoints = 0;
                            }

                            if (!empty($newarray['totalpoints'])) {
                                if (is_float($newarray['totalpoints'])) {
                                    $totalpoints = number_format($newarray['totalpoints'], 2);
                                } else {
                                    $totalpoints = number_format($newarray['totalpoints']);
                                }
                            } else {
                                $totalpoints = 0;
                            }
                            $usernickname = get_user_meta($newarray['userid'], 'nickname', true);

                            if (!empty($newarray['rewarder_for_frontend'])) {
                                $rewarderforfrontend = $newarray['rewarder_for_frontend'];
                            } else {
                                $rewarderforfrontend = '';
                            }
                            ?>
                            <tr>
                                <td data-value="<?php echo $i; ?>"><?php echo $i; ?></td>
                                <!--<td><?php echo $newarray['orderid']; ?> </td>-->
                                <td><?php echo $usernickname; ?> </td>
                                <td><?php echo $rewarderforfrontend; ?></td>
                                <td><?php echo $pointsearned; ?> </td>
                                <!--<td><?php echo $beforepoints; ?></td>-->
                                <td><?php echo $redeemedpoints; ?></td>
                                <td><?php echo $totalpoints; ?> </td>
                                <td><?php echo $newarray['date']; ?></td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                }
                ?>
            </tbody>
            <tfoot>
                <tr style="clear:both;">
                    <td colspan="7">
                        <div class="pagination pagination-centered"></div>
                    </td>
                </tr>
            </tfoot>
        </table>
        <?php
        $content = ob_get_clean();
        return $content;
    }

    public static function viewchangelog() {
        ?>
        <style type="text/css">
        <?php echo get_option('rs_myaccount_custom_css'); ?>
        </style>
        <?php
        echo "<h2>" . get_option('rs_my_rewards_title') . "</h2>";
        $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
        if (get_user_meta(get_current_user_id(), '_my_reward_points', true) != '') {
            echo "<h4> " . get_option('rs_my_rewards_total') . " " . round(number_format((float) get_user_meta(get_current_user_id(), '_my_reward_points', true), 2, '.', ''), $roundofftype) . "</h4><br>";
        } else {
            echo "<h4> " . get_option('rs_my_rewards_total') . " 0</h4><br>";
        }

        $outputtablefields = '<p> ';
        if (get_option('rs_show_hide_search_box_in_my_rewards_table') == '1') {
            $outputtablefields .= __('Search:', 'rewardsystem') . '<input id="filters" type="text"/> ';
        }
        if (get_option('rs_show_hide_page_size_my_rewards') == '1') {
            $outputtablefields .= __('Page Size:', 'rewardsystem') . '<select id="change-page-sizes"><option value="5">5</option><option value="10">10</option><option value="50">50</option>
                    <option value="100">100</option>
                </select>';
        }
        $outputtablefields .= '</p>';
        echo $outputtablefields;
        ?>

        <table class = "examples demo shop_table my_account_orders table-bordered" data-filter = "#filters" data-page-size="5" data-page-previous-text = "prev" data-filter-text-only = "true" data-page-next-text = "next">

            <thead><tr><th data-toggle="true" data-sort-initial = "true"><?php echo get_option('rs_my_rewards_sno_label'); ?></th>
                    <!--<th><?php echo get_option('rs_my_rewards_orderid_label'); ?></th>--><th><?php echo get_option('rs_my_rewards_userid_label'); ?></th>
                    <th><?php echo get_option('rs_my_rewards_rewarder_label'); ?></th>
                    <th><?php echo get_option('rs_my_rewards_points_earned_label'); ?></th>
                    <!--<th data-hide="phone"><?php echo get_option('rs_my_rewards_before_points_label'); ?></th>-->
                    <th data-hide='phone,tablet'><?php echo get_option('rs_my_rewards_redeem_points_label'); ?></th>
                    <th data-hide="phone,tablet"><?php echo get_option('rs_my_rewards_total_points_label'); ?></th><th data-hide="phone,tablet"><?php echo get_option('rs_my_rewards_date_label'); ?></th></tr></thead>
            <tbody>
                <?php
                $user_ID = get_current_user_id();
                //var_dump(get_user_meta($user_ID, '_my_points_log', true));
                $fetcharray = get_user_meta($user_ID, '_my_points_log', true);
                if (is_array($fetcharray)) {
                    if (get_option('rs_points_log_sorting') == '1') {
                        krsort($fetcharray, SORT_NUMERIC);
                    }
                }
                $i = 1;
                if (is_array($fetcharray)) {
                    foreach ($fetcharray as $newarray) {
                        if (is_array($newarray)) {
                            if (!empty($newarray['points_earned_order'])) {
                                if (is_float($newarray['points_earned_order'])) {
                                    $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
                                    $pointsearned = round(number_format($newarray['points_earned_order'], 2), $roundofftype);
                                } else {
                                    $pointsearned = number_format($newarray['points_earned_order']);
                                }
                            } else {
                                $pointsearned = 0;
                            }

                            if (!empty($newarray['before_order_points'])) {
                                if (is_float($newarray['before_order_points'])) {
                                    $beforepoints = number_format($newarray['before_order_points'], 2);
                                } else {
                                    $beforepoints = number_format($newarray['before_order_points']);
                                }
                            } else {
                                $beforepoints = 0;
                            }

                            if (!empty($newarray['points_redeemed'])) {
                                if (is_float($newarray['points_redeemed'])) {
                                    $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
                                    $redeemedpoints = round(number_format($newarray['points_redeemed'], 2), $roundofftype);
                                } else {
                                    $redeemedpoints = number_format((int) $newarray['points_redeemed']);
                                }
                            } else {
                                $redeemedpoints = 0;
                            }

                            if (!empty($newarray['totalpoints'])) {
                                if (get_option('rs_round_off_type') == '1') {
                                    $totalpoints = $newarray['totalpoints'];
                                } else {
                                    $totalpoints = number_format($newarray['totalpoints']);
                                }
                            } else {
                                $totalpoints = 0;
                            }
                            $usernickname = get_user_meta($newarray['userid'], 'nickname', true);

                            if (!empty($newarray['rewarder_for_frontend'])) {
                                $rewarderforfrontend = $newarray['rewarder_for_frontend'];
                            } else {
                                $rewarderforfrontend = '';
                            }
                            ?>
                            <tr>
                                <td data-value="<?php echo $i; ?>"><?php echo $i; ?></td>
                                <!--<td><?php echo $newarray['orderid']; ?> </td>-->
                                <td><?php echo $usernickname; ?> </td>
                                <td><?php echo $rewarderforfrontend; ?></td>
                                <td><?php echo $pointsearned; ?> </td>
                                <!--<td><?php echo $beforepoints; ?></td>-->
                                <td><?php echo $redeemedpoints; ?></td>
                                <td><?php echo $totalpoints; ?> </td>
                                <td><?php echo $newarray['date']; ?></td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                }
                ?>
            </tbody>
            <tfoot>
                <tr style="clear:both;">
                    <td colspan="7">
                        <div class="pagination pagination-centered"></div>
                    </td>
                </tr>
            </tfoot>
        </table>
        <?php
    }

    public static function add_field_to_variation_reward_points($loop, $variation_data, $variations) {
        global $post;


        global $woocommerce;

        $variation_data = get_post_meta($variations->ID);

        if ((float) $woocommerce->version <= (float) ('2.2.0')) {

            // Select
            if (isset($variation_data['_enable_reward_points'][0])) {
                $new_datas = $variation_data['_enable_reward_points'][0];
            } else {
                $new_datas = '';
            }
            $maindata_enable = array('1' => 'Enable', '2' => 'Disable');
            ?>

            <tr>
                <td>
                    <label><?php _e('Enable SUMO Reward Points', 'rewardsystem'); ?></label>
                    <select name="_enable_reward_points[<?php echo $loop; ?>]">
                        <?php
                        foreach ($maindata_enable as $keydata => $valuedata) {
                            echo '<option value="' . esc_attr($keydata === $new_datas ? '' : $keydata ) . '" ' . selected($new_datas, $keydata, false) . '>' . esc_html($valuedata) . '</option>';
                        }
                        ?>
                    </select>
                </td>
            </tr>


            <?php
            // Select
            if (isset($variation_data['_select_reward_rule'][0])) {
                $new_datas = $variation_data['_select_reward_rule'][0];
            } else {
                $new_datas = '';
            }
            $maindata = array('1' => 'By Fixed Reward Points', '2' => 'By Percentage of Product Price');
            ?>

            <tr>
                <td>
                    <label><?php _e('Reward Type', 'rewardsystem'); ?></label>
                    <select name="_select_reward_rule[<?php echo $loop; ?>]">
                        <?php
                        foreach ($maindata as $key => $value) {
                            echo '<option value="' . esc_attr($key === $new_datas ? '' : $key ) . '" ' . selected($new_datas, $key, false) . '>' . esc_html($value) . '</option>';
                        }
                        ?>
                    </select>
                </td>
            </tr>


            <tr>
                <td>
                    <label><?php _e('Reward Points', 'rewardsystem'); ?></label>
                    <input type="text" size="5" name="_reward_points[<?php echo $loop; ?>]" value="<?php if (isset($variation_data['_reward_points'][0])) echo $variation_data['_reward_points'][0]; ?>" class=" " placeholder=" " />
                </td>
            </tr>

            <tr>
                <td>
                    <label><?php _e('Reward Percent', 'rewardsystem'); ?></label>
                    <input type="text" size="5" name="_reward_percent[<?php echo $loop; ?>]" value="<?php if (isset($variation_data['_reward_percent'][0])) echo $variation_data['_reward_percent'][0]; ?>" class=" " placeholder=" " />
                </td>
            </tr>


            <?php
            // Select
            if (isset($variation_data['_select_referral_reward_rule'][0])) {
                $new_datas = $variation_data['_select_referral_reward_rule'][0];
            } else {
                $new_datas = '';
            }
            $maindata_referral = array('1' => 'By Fixed Reward Points', '2' => 'By Percentage of Product Price');
            ?>

            <tr>
                <td>
                    <label><?php _e('Referral Reward Type', 'rewardsystem'); ?></label>
                    <select name="_select_referral_reward_rule[<?php echo $loop; ?>]">
                        <?php
                        foreach ($maindata_referral as $keys => $values) {
                            echo '<option value="' . esc_attr($keys === $new_datas ? '' : $keys ) . '" ' . selected($new_datas, $keys, false) . '>' . esc_html($values) . '</option>';
                        }
                        ?>
                    </select>
                </td>
            </tr>


            <tr>
                <td>
                    <label><?php _e('Referral Reward Points', 'rewardsystem'); ?></label>
                    <input type="text" size="5" name="_referral_reward_points[<?php echo $loop; ?>]" value="<?php if (isset($variation_data['_referral_reward_points'][0])) echo $variation_data['_referral_reward_points'][0]; ?>" class=" " placeholder=" " />
                </td>
            </tr>


            <tr>
                <td>
                    <label><?php _e('Referral Reward Percent', 'rewardsystem'); ?></label>
                    <input type="text" size="5" name="_referral_reward_percent[<?php echo $loop; ?>]" value="<?php if (isset($variation_data['_referral_reward_percent'][0])) echo $variation_data['_referral_reward_percent'][0]; ?>" class=" " placeholder=" " />
                </td>
            </tr>

            <?php
        }else {

            // Select
            extract($variation_data);
            if (isset($variation_data['_enable_reward_points'][0])) {
                $new_datas = $variation_data['_enable_reward_points'][0];
            } else {
                $new_datas = '';
            }
            $maindata_enable = array('1' => 'Enable', '2' => 'Disable');
            ?>
            <div>
                <tr>
                    <td>
                        <label><?php _e('Enable SUMO Reward Points', 'rewardsystem'); ?></label>
                        <select name="_enable_reward_points[<?php echo $loop; ?>]">
                            <?php
                            foreach ($maindata_enable as $keydata => $valuedata) {
                                echo '<option value="' . esc_attr($keydata === $new_datas ? '' : $keydata ) . '" ' . selected($new_datas, $keydata, false) . '>' . esc_html($valuedata) . '</option>';
                            }
                            ?>
                        </select>
                    </td>
                </tr>
            </div>

            <?php
            // Select
            if (isset($variation_data['_select_reward_rule'][0])) {
                $new_datas = $variation_data['_select_reward_rule'][0];
            } else {
                $new_datas = '';
            }
            $maindata = array('1' => 'By Fixed Reward Points', '2' => 'By Percentage of Product Price');
            ?>
            <div>
                <tr>
                    <td>
                        <label><?php _e('Reward Type', 'rewardsystem'); ?></label>
                        <select name="_select_reward_rule[<?php echo $loop; ?>]">
                            <?php
                            foreach ($maindata as $key => $value) {
                                echo '<option value="' . esc_attr($key === $new_datas ? '' : $key ) . '" ' . selected($new_datas, $key, false) . '>' . esc_html($value) . '</option>';
                            }
                            ?>
                        </select>
                    </td>
                </tr>
            </div>

            <div>
                <tr>
                    <td>
                        <label><?php _e('Reward Points', 'rewardsystem'); ?></label>
                        <input type="text" size="5" name="_reward_points[<?php echo $loop; ?>]" value="<?php if (isset($variation_data['_reward_points'][0])) echo $variation_data['_reward_points'][0]; ?>" class=" " placeholder=" " />
                    </td>
                </tr>
            </div>

            <div>
                <tr>
                    <td>
                        <label><?php _e('Reward Percent', 'rewardsystem'); ?></label>
                        <input type="text" size="5" name="_reward_percent[<?php echo $loop; ?>]" value="<?php if (isset($variation_data['_reward_percent'][0])) echo $variation_data['_reward_percent'][0]; ?>" class=" " placeholder=" " />
                    </td>
                </tr>
            </div>

            <?php
            // Select
            if (isset($variation_data['_select_referral_reward_rule'][0])) {
                $new_datas = $variation_data['_select_referral_reward_rule'][0];
            } else {
                $new_datas = '';
            }
            $maindata_referral = array('1' => 'By Fixed Reward Points', '2' => 'By Percentage of Product Price');
            ?>
            <div>
                <tr>
                    <td>
                        <label><?php _e('Referral Reward Type', 'rewardsystem'); ?></label>
                        <select name="_select_referral_reward_rule[<?php echo $loop; ?>]">
                            <?php
                            foreach ($maindata_referral as $keys => $values) {
                                echo '<option value="' . esc_attr($keys === $new_datas ? '' : $keys ) . '" ' . selected($new_datas, $keys, false) . '>' . esc_html($values) . '</option>';
                            }
                            ?>
                        </select>
                    </td>
                </tr>
            </div>


            <div>
                <tr>
                    <td>
                        <label><?php _e('Referral Reward Points', 'rewardsystem'); ?></label>
                        <input type="text" size="5" name="_referral_reward_points[<?php echo $loop; ?>]" value="<?php if (isset($variation_data['_referral_reward_points'][0])) echo $variation_data['_referral_reward_points'][0]; ?>" class=" " placeholder=" " />
                    </td>
                </tr>
            </div>

            <div>
                <tr>
                    <td>
                        <label><?php _e('Referral Reward Percent', 'rewardsystem'); ?></label>
                        <input type="text" size="5" name="_referral_reward_percent[<?php echo $loop; ?>]" value="<?php if (isset($variation_data['_referral_reward_percent'][0])) echo $variation_data['_referral_reward_percent'][0]; ?>" class=" " placeholder=" " />
                    </td>
                </tr>
            </div>


            <?php
        }
    }

    public static function add_field_to_variation_in_js() {
        ?>
        <tr>
            <td>
                <?php
                // Select
                woocommerce_wp_select(
                        array(
                            'id' => '_enable_reward_points[ + loop + ]',
                            'label' => __('Enable SUMO Reward Points', 'rewardsystem'),
                            'description' => __('Choose an Option.', 'rewardsystem'),
                            'value' => $variation_data['_enable_reward_points'][0],
                            'options' => array(
                                '1' => __('Enable', 'rewardsystem'),
                                '2' => __('Disable', 'rewardsystem'),
                            )
                        )
                );
                ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php
                // Select

                woocommerce_wp_select(
                        array(
                            'id' => '_select_reward_rule[ + loop + ]',
                            'label' => __('Reward Type', 'rewardsystem'),
                            'class' => '_select_reward_rule',
                            'description' => __('Select Reward Rule', 'rewardsystem'),
                            'value' => '',
                            'options' => array(
                                '1' => __('By Fixed Reward Points', 'rewardsystem'),
                                '2' => __('By Percentage of Product Price', 'rewardsystem'),
                            )
                        )
                );
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                // Text Field
                woocommerce_wp_text_input(
                        array(
                            'id' => '_reward_points[ + loop + ]',
                            'label' => __('Reward Points', 'rewardsystem'),
                            'placeholder' => '',
                            'desc_tip' => 'true',
                            'description' => __('This Value is applicable for "By Fixed Reward Points" Reward Type', 'rewardsystem'),
                            'value' => ''
                        )
                );
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                woocommerce_wp_text_input(
                        array(
                            'id' => '_reward_percent[ + loop + ]',
                            'label' => __('Reward Percent', 'rewardsystem'),
                            'placeholder' => '',
                            'desc_tip' => 'true',
                            'description' => __('This Value is applicable for "By Percentage of Product Price" Reward Type', 'rewardsystem'),
                            'value' => ''
                        )
                );
                ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php
                // Select

                woocommerce_wp_select(
                        array(
                            'id' => '_select_referral_reward_rule[ + loop + ]',
                            'label' => __('Referral Reward Type', 'rewardsystem'),
                            'class' => '_select_referral_reward_rule',
                            'description' => __('Select Referral Reward Rule', 'rewardsystem'),
                            'value' => '',
                            'options' => array(
                                '1' => __('By Fixed Reward Points', 'rewardsystem'),
                                '2' => __('By Percentage of Product Price', 'rewardsystem'),
                            )
                        )
                );
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                // Text Field
                woocommerce_wp_text_input(
                        array(
                            'id' => '_referral_reward_points[ + loop + ]',
                            'label' => __('Referral Reward Points', 'rewardsystem'),
                            'placeholder' => '',
                            'desc_tip' => 'true',
                            'description' => __('This Value is applicable for "By Fixed Reward Points" Referral Referral Reward Type', 'rewardsystem'),
                            'value' => ''
                        )
                );
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                woocommerce_wp_text_input(
                        array(
                            'id' => '_referral_reward_percent[ + loop + ]',
                            'label' => __('Referral Reward Percent', 'rewardsystem'),
                            'placeholder' => '',
                            'desc_tip' => 'true',
                            'description' => __('This Value is applicable for "By Percentage of Product Price" Referral Reward Type', 'rewardsystem'),
                            'value' => ''
                        )
                );
                ?>
            </td>
        </tr>

        <?php
    }

    public static function save_variable_product_fields($post_id) {
        if (isset($_POST['variable_sku'])) :
            $variable_sku = $_POST['variable_sku'];
            $variable_post_id = $_POST['variable_post_id'];

// Text Field
            $_text_field = $_POST['_reward_points'];
            for ($i = 0; $i < sizeof($variable_sku); $i++) :
                $variation_id = (int) $variable_post_id[$i];
                if (isset($_text_field[$i])) {
                    update_post_meta($variation_id, '_reward_points', stripslashes($_text_field[$i]));
                }
            endfor;

            $percent_text_field = $_POST['_reward_percent'];
            for ($i = 0; $i < sizeof($variable_sku); $i++):
                $variation_id = (int) $variable_post_id[$i];
                if (isset($percent_text_field[$i])) {
                    update_post_meta($variation_id, '_reward_percent', stripslashes($percent_text_field[$i]));
                }
            endfor;
//select
            $new_select = $_POST['_select_reward_rule'];
            for ($i = 0; $i < sizeof($variable_sku); $i++):
                $variation_id = (int) $variable_post_id[$i];
                if (isset($new_select[$i])) {
                    update_post_meta($variation_id, '_select_reward_rule', stripslashes($new_select[$i]));
                }
            endfor;


            $_text_fields = $_POST['_referral_reward_points'];
            for ($i = 0; $i < sizeof($variable_sku); $i++) :
                $variation_id = (int) $variable_post_id[$i];
                if (isset($_text_field[$i])) {
                    update_post_meta($variation_id, '_referral_reward_points', stripslashes($_text_fields[$i]));
                }
            endfor;

            $percent_text_fields = $_POST['_referral_reward_percent'];
            for ($i = 0; $i < sizeof($variable_sku); $i++):
                $variation_id = (int) $variable_post_id[$i];
                if (isset($percent_text_field[$i])) {
                    update_post_meta($variation_id, '_referral_reward_percent', stripslashes($percent_text_fields[$i]));
                }
            endfor;
//select
            $new_selects = $_POST['_select_referral_reward_rule'];
            for ($i = 0; $i < sizeof($variable_sku); $i++):
                $variation_id = (int) $variable_post_id[$i];
                if (isset($new_select[$i])) {
                    update_post_meta($variation_id, '_select_referral_reward_rule', stripslashes($new_selects[$i]));
                }
            endfor;


// Select
            $_select = $_POST['_enable_reward_points'];
            for ($i = 0; $i < sizeof($variable_sku); $i++) :
                $variation_id = (int) $variable_post_id[$i];
                if (isset($_select[$i])) {
                    update_post_meta($variation_id, '_enable_reward_points', stripslashes($_select[$i]));
                }
            endfor;
        endif;
    }

    public static function add_registration_rewards_points($user_id) {
        if (get_option('timezone_string') != '') {
            $timezonedate = date_default_timezone_set(get_option('timezone_string'));
        } else {
            $timezonedate = date_default_timezone_set('UTC');
        }
//        $banned_user_list = get_option('rs_banned-users_list');
//        if (!in_array($user_id, (array) $banned_user_list)) {
//            $getarrayofuserdata = get_userdata($user_id);
//            $banninguserrole = get_option('rs_banning_user_role');
//            if (!in_array(isset($getarrayofuserdata->roles[0]) ? $getarrayofuserdata->roles[0] : '0', (array) $banninguserrole)) {
        $banning_type = FPRewardSystem::check_banning_type($user_id);
        if ($banning_type != 'earningonly' && $banning_type != 'both') {
            $registration_points = RSUserRoleRewardPoints::user_role_based_reward_points($user_id, get_option('rs_reward_signup'));
            FPRewardSystem::save_total_earned_points($user_id, $registration_points);
            $referral_registration_points = get_option('rs_referral_reward_signup');
            $restrictuserpoints = get_option('rs_maximum_earning_points_for_user');
            $enabledisablepoints = get_option('rs_enable_maximum_earning_points');
            if (isset($_COOKIE['rsreferredusername'])) {
                /*
                 * Update the Referred Person Registration Count
                 */

                $user_info = new WP_User($user_id);
                $registered_date = $user_info->user_registered;
                $limitation = false;
                $modified_registered_date = date('Y-m-d h:i:sa', strtotime($registered_date));
                $delay_days = get_option('_rs_select_referral_points_referee_time_content');
                $checking_date = date('Y-m-d h:i:sa', strtotime($modified_registered_date . ' + ' . $delay_days . ' days '));
                $modified_checking_date = strtotime($checking_date);
                $current_date = date('Y-m-d h:i:sa');
                $modified_current_date = strtotime($current_date);
                //Is for Immediatly
                if (get_option('_rs_select_referral_points_referee_time') == '1') {
                    $limitation = true;
                } else {
                    // Is for Limited Time with Number of Days
                    if ($modified_current_date > $modified_checking_date) {
                        $limitation = true;
                    } else {
                        $limitation = false;
                    }
                }
                if ($limitation == true) {
                    $referreduser = get_user_by('login', $_COOKIE['rsreferredusername']);
                    if ($referreduser != false) {
                        $refuserid = $referreduser->ID;
                    } else {
                        $refuserid = $_COOKIE['rsreferredusername'];
                    }
//                        $banned_user_list = get_option('rs_banned-users_list');
//                        if (!in_array($refuserid, (array) $banned_user_list)) {
//                            $getarrayofuserdata = get_userdata($refuserid);
//                            $banninguserrole = get_option('rs_banning_user_role');
//                            if (!in_array(isset($getarrayofuserdata->roles[0]) ? $getarrayofuserdata->roles[0] : '0', (array) $banninguserrole)) {
                    //$userid = get_current_user_id();
                    $banning_type = FPRewardSystem::check_banning_type($refuserid);
                    if ($banning_type != 'earningonly' && $banning_type != 'both') {
                        $getregisteredcount = get_user_meta($refuserid, 'rsreferreduserregisteredcount', true);
                        $currentregistration = $getregisteredcount + 1;
                        update_user_meta($refuserid, 'rsreferreduserregisteredcount', $currentregistration);
                        /*
                         * Update the Referred Person Registration Count End
                         */
                        $oldpointss = get_user_meta($refuserid, '_my_reward_points', true);

                        $referral_registration_points = RSUserRoleRewardPoints::user_role_based_reward_points($refuserid, $referral_registration_points);
                        $previouslog = get_option('rs_referral_log');
                        RS_Referral_Log::main_referral_log_function($refuserid, $user_id, $referral_registration_points, array_filter((array) $previouslog));
                        FPRewardSystem::save_total_earned_points($refuserid, $referral_registration_points);
                        $currentregistrationpointss = $oldpointss + $referral_registration_points;
                        if ($enabledisablepoints == '1') {
                            if (($currentregistrationpointss <= $restrictuserpoints) || ($restrictuserpoints == '')) {
                                $currentregistrationpointss = $currentregistrationpointss;
                            } else {
                                $currentregistrationpointss = $restrictuserpoints;
                            }
                        }



                        update_user_meta($refuserid, '_my_reward_points', $currentregistrationpointss);


                        $referralregisteredusermessage = get_option('_rs_localize_points_earned_for_referral_registration');
                        $fromreferralregistereduser = array('{registereduser}');
                        $toreferralregistereduser = array(get_user_meta($user_id, 'nickname', true));
                        $updatedreferralregistereduser = str_replace($fromreferralregistereduser, $toreferralregistereduser, $referralregisteredusermessage);


                        $myrewards = get_user_meta($refuserid, '_my_reward_points', true);
                        $pointslogs[] = array('orderid' => '', 'userid' => $refuserid, 'points_earned_order' => $referral_registration_points, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $myrewards, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => $updatedreferralregistereduser, 'rewarder_for_frontend' => $updatedreferralregistereduser);
                        $overalllogs[] = array('userid' => $refuserid, 'totalvalue' => $myrewards, 'eventname' => $updatedreferralregistereduser, 'date' => date('Y-m-d H:i:s'));
                        $getoveralllogs = get_option('rsoveralllog');
                        $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
                        update_option('rsoveralllog', $logmerges);
                        $getmypointss = get_user_meta($refuserid, '_my_points_log', true);
                        $mergeds = array_merge((array) $getmypointss, $pointslogs);
                        update_user_meta($refuserid, '_my_points_log', $mergeds);
                        update_user_meta($user_id, '_rs_i_referred_by', $refuserid);
//                            }
//                        }
                    }
                }
            }
            $oldpoints = get_user_meta($user_id, '_my_reward_points', true);
            $currentregistrationpoints = $oldpoints + $registration_points;

            if ($enabledisablepoints == '1') {
                if (($currentregistrationpoints <= $restrictuserpoints) || ($restrictuserpoints != '')) {
                    $currentregistrationpoints = $currentregistrationpoints;
                } else {
                    $currentregistrationpoints = $restrictuserpoints;
                }
            }


            $registeredusermessage = get_option('_rs_localize_points_earned_for_registration');
            $fromregistereduser = array('{registereduser}');
            $toregistereduser = array(get_user_meta($user_id, 'nickname', true));
            $updatedregistereduser = str_replace($fromregistereduser, $toregistereduser, $registeredusermessage);

            update_user_meta($user_id, '_my_reward_points', $currentregistrationpoints);
            $myreward = get_user_meta($user_id, '_my_reward_points', true);
            $pointslog[] = array('orderid' => '', 'userid' => $user_id, 'points_earned_order' => $currentregistrationpoints, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $myreward, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => $updatedregistereduser, 'rewarder_for_frontend' => $updatedregistereduser);
            $overalllog[] = array('userid' => $user_id, 'totalvalue' => $myreward, 'eventname' => $updatedregistereduser, 'date' => date('Y-m-d H:i:s'));
            $getoveralllog = get_option('rsoveralllog');
            $logmerge = array_merge((array) $getoveralllog, $overalllog);
            update_option('rsoveralllog', $logmerge);
            $getmypoints = get_user_meta($user_id, '_my_points_log', true);
            $merged = array_merge((array) $getmypoints, $pointslog);
            update_user_meta($user_id, '_my_points_log', $merged);
//            }
//        }
        }
    }

    public static function delete_referral_registered_people($user_id) {
        if (get_option('timezone_string') != '') {
            $timezonedate = date_default_timezone_set(get_option('timezone_string'));
        } else {
            $timezonedate = date_default_timezone_set('UTC');
        }
        $registration_points = get_option('rs_reward_signup');

        $referral_registration_points = RSUserRoleRewardPoints::user_role_based_reward_points($user_id, get_option('rs_referral_reward_signup'));
        $getreferredusermeta = get_user_meta($user_id, '_rs_i_referred_by', true);
        $refuserid = $getreferredusermeta;
        $getregisteredcount = get_user_meta($refuserid, 'rsreferreduserregisteredcount', true);
        $currentregistration = $getregisteredcount - 1;
        update_user_meta($refuserid, 'rsreferreduserregisteredcount', $currentregistration);
        /*
         * Update the Referred Person Registration Count End
         */

        /* Below Code is for Removing Referral Point Registration when Deleting User */
        if ($getreferredusermeta != '') {
            $oldpointss = get_user_meta($refuserid, '_my_reward_points', true);
            $currentregistrationpointss = $oldpointss - $referral_registration_points;
            update_user_meta($refuserid, '_my_reward_points', $currentregistrationpointss);
            $myrewards = get_user_meta($refuserid, '_my_reward_points', true);

            $revisereferralregistrationpoints = get_option('_rs_localize_referral_account_signup_points_revised');
            $fromreviseregistrationpointsarray = array('{usernickname}');
            $toreviseregistrationpointsarray = array(get_user_meta($user_id, 'nickname', true));
            $reviseregistrationpointsreplace = str_replace($fromreviseregistrationpointsarray, $toreviseregistrationpointsarray, $revisereferralregistrationpoints);

            $pointslogs[] = array('orderid' => '', 'userid' => $refuserid, 'points_earned_order' => '', 'points_redeemed' => $referral_registration_points, 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $myrewards, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => $reviseregistrationpointsreplace, 'rewarder_for_frontend' => $reviseregistrationpointsreplace);
            $overalllogs[] = array('userid' => $refuserid, 'totalvalue' => $referral_registration_points, 'eventname' => $reviseregistrationpointsreplace, 'date' => date('Y-m-d H:i:s'));
            $getoveralllogs = get_option('rsoveralllog');
            $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
            update_option('rsoveralllog', $logmerges);
            $getmypointss = get_user_meta($refuserid, '_my_points_log', true);
            $mergeds = array_merge((array) $getmypointss, $pointslogs);
            update_user_meta($refuserid, '_my_points_log', $mergeds);
            update_user_meta($user_id, '_rs_i_referred_by', $refuserid);
        }
        $getlistoforder = get_user_meta($user_id, '_update_user_order', true);
        if (is_array($getlistoforder)) {
            foreach ($getlistoforder as $order_id) {
                $order = new WC_Order($order_id);
                if ($order->status == 'completed') {
                    $pointslog = array();
                    $usernickname = get_user_meta($order->user_id, 'nickname', true);
                    delete_user_meta($order->user_id, '_redeemed_points');
                    delete_user_meta($order->user_id, '_redeemed_amount');
                    $global_referral_enable = get_option('rs_global_enable_disable_reward');
                    $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                    $global_enable = get_option('rs_global_enable_disable_reward');
                    $global_reward_type = get_option('rs_global_reward_type');
                    foreach ($order->get_items() as $item) {
                        if (get_option('rs_set_price_percentage_reward_points') == '1') {
                            $getregularprice = get_post_meta($item['product_id'], '_regular_price', true);
                        } else {
                            $getregularprice = get_post_meta($item['product_id'], '_price', true);
                        }

                        do_action_ref_array('rs_delete_points_for_referral_simple', array(&$getregularprice, &$item));


                        $getpercent = get_post_meta($item['product_id'], '_rewardsystempercent', true);
                        $item['qty'];
                        $order->user_id;
                        $item['product_id'];
                        $referreduser = get_post_meta($order_id, '_referrer_name', true);
                        if ($referreduser != '') {
                            $refuser = get_user_by('login', $referreduser);
                            $myid = $refuser->ID;
                            if (get_post_meta($item['product_id'], '_rewardsystemcheckboxvalue', true) == 'yes') {
                                $getreferraltype = get_post_meta($item['product_id'], '_referral_rewardsystem_options', true);
                                if ($getreferraltype == '1') {
                                    $getreferralpoints = get_post_meta($item['product_id'], '_referralrewardsystempoints', true);
                                    if ($getreferralpoints == '') {
                                        $term = get_the_terms($item['product_id'], 'product_cat');
                                        if (is_array($term)) {
                                            $rewardpointer = array();
                                            foreach ($term as $term) {
                                                $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                                $display_type = get_woocommerce_term_meta($term->term_id, 'referral_enable_rs_rule', true);
                                                if ($enablevalue == 'yes') {
                                                    if ($display_type == '1') {
                                                        if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true) == '') {

                                                            if ($global_referral_enable == '1') {
                                                                if ($global_referral_reward_type == '1') {
                                                                    $rewardpointer[] = get_option('rs_global_referral_reward_point');
                                                                } else {
                                                                    $pointconversion = get_option('rs_earn_point');
                                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                                    $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                    $rewardpointer[] = $pointswithvalue / $pointconversionvalue;
                                                                }
                                                            }
                                                        } else {
                                                            $rewardpointer[] = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true);
                                                        }
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) == '') {
                                                            $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                                            $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                                            if ($global_referral_enable == '1') {
                                                                if ($global_referral_reward_type == '1') {
                                                                    $rewardpointer[] = get_option('rs_global_referral_reward_point');
                                                                } else {
                                                                    $pointconversion = get_option('rs_earn_point');
                                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                                    $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                    $rewardpointer[] = $pointswithvalue / $pointconversionvalue;
                                                                }
                                                            }
                                                        } else {
                                                            $rewardpointer[] = $pointswithvalue / $pointconversionvalue;
                                                        }
                                                    }
                                                } else {
                                                    if ($global_referral_enable == '1') {
                                                        if ($global_referral_reward_type == '1') {
                                                            $rewardpointer[] = get_option('rs_global_referral_reward_point');
                                                        } else {
                                                            $pointconversion = get_option('rs_earn_point');
                                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                                            $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                            $getaveragepoints = $getaverage * $getregularprice;
                                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                                            $rewardpointer[] = $pointswithvalue / $pointconversionvalue;
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            if ($global_referral_enable == '1') {
                                                if ($global_referral_reward_type == '1') {
                                                    $rewardpointer[] = get_option('rs_global_referral_reward_point');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpointer[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        }
                                        $getreferralpoints = max($rewardpointer);
                                    }
                                    $exactpointer = RSUserRoleRewardPoints::user_role_based_reward_points($user_id, $getreferralpoints) * $item['qty'];


                                    $revisepointsreferralpurchasedata = get_option('_rs_localize_revise_points_for_referral_purchase');
                                    $fromarrayreferralpurchase = array('{productid}', '{usernickname}');
                                    $toarrayreferralpurchase = array($item['product_id'], get_user_meta($order->user_id, 'nickname', true));

                                    $mainreferralpurchasedata = str_replace($fromarrayreferralpurchase, $toarrayreferralpurchase, $revisepointsreferralpurchasedata);



                                    $overalllogs[] = array('userid' => $myid, 'totalvalue' => $exactpointer, 'eventname' => $mainreferralpurchasedata, 'date' => date('Y-m-d H:i:s'));

                                    $getoveralllogs = get_option('rsoveralllog');
                                    $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
                                    update_option('rsoveralllog', $logmerges);

                                    $userpoints = get_user_meta($myid, '_my_reward_points', true);
                                    $newgetpoints = $userpoints - $exactpointer;
                                    update_user_meta($myid, '_my_reward_points', $newgetpoints);

                                    $pointsfixed[] = array('orderid' => $order_id, 'userid' => $myid, 'points_earned_order' => '', 'points_redeemed' => $exactpointer, 'points_value' => '', 'before_order_points' => '', 'totalpoints' => get_user_meta($myid, '_my_reward_points', true), 'date' => $order->order_date, 'rewarder_for' => $mainreferralpurchasedata, 'rewarder_for_frontend' => $mainreferralpurchasedata);
                                    $getlogger = get_user_meta($myid, '_my_points_log', true);
                                    $merged = array_merge((array) $getlogger, $pointsfixed);
                                    update_user_meta($myid, '_my_points_log', $merged);
                                } else {
                                    $points = get_option('rs_earn_point');
                                    $pointsequalto = get_option('rs_earn_point_value');
                                    $getreferralpercent = get_post_meta($item['product_id'], '_referralrewardsystempercent', true);
                                    if (get_option('rs_set_price_percentage_reward_points') == '1') {
                                        $getregularprices = get_post_meta($item['product_id'], '_regular_price', true);
                                    } else {
                                        $getregularprices = get_post_meta($item['product_id'], '_price', true);
                                    }

                                    do_action_ref_array('rs_delete_points_for_referral_simples', array(&$getregularprices, $item));


                                    $referralpercentageproduct = $getreferralpercent / 100;
                                    $getreferralpricepercent = $referralpercentageproduct * $getregularprices;
                                    $getpointconversion = $getreferralpricepercent * $points;
                                    $getreferpoints = $getpointconversion / $pointsequalto;

                                    if ($getreferralpercent == '') {
                                        $term = get_the_terms($item['product_id'], 'product_cat');
// var_dump($term);
                                        if (is_array($term)) {
//var_dump($term);
                                            $rewardpoints = array('0');
                                            foreach ($term as $term) {

                                                $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                                $display_type = get_woocommerce_term_meta($term->term_id, 'referral_enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                                if ($enablevalue == 'yes') {
                                                    if ($display_type == '1') {
                                                        if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true) == '') {
                                                            $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                                            $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                                            if ($global_referral_enable == '1') {
                                                                if ($global_referral_reward_type == '1') {
                                                                    $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                                } else {
                                                                    $pointconversion = get_option('rs_earn_point');
                                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                                    $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                }
                                                            }
                                                        } else {
                                                            $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true);
                                                        }
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) == '') {
                                                            $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                                            $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                                            if ($global_referral_enable == '1') {
                                                                if ($global_referral_reward_type == '1') {
                                                                    $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                                } else {
                                                                    $pointconversion = get_option('rs_earn_point');
                                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                                    $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                }
                                                            }
                                                        } else {
                                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                        }
                                                    }
                                                } else {
                                                    if ($global_referral_enable == '1') {
                                                        if ($global_referral_reward_type == '1') {
                                                            $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                        } else {
                                                            $pointconversion = get_option('rs_earn_point');
                                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                                            $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                            $getaveragepoints = $getaverage * $getregularprice;
                                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            if ($global_referral_enable == '1') {
                                                if ($global_referral_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $getregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        }
                                        $getreferpoints = max($rewardpoints);
                                    }
                                    $exactpointer = RSUserRoleRewardPoints::user_role_based_reward_points($user_id, $getreferpoints) * $item['qty'];

                                    $revisepointsreferralpurchasedata = get_option('_rs_localize_revise_points_for_referral_purchase');
                                    $fromarrayreferralpurchase = array('{productid}', '{usernickname}');
                                    $toarrayreferralpurchase = array($item['product_id'], get_user_meta($order->user_id, 'nickname', true));

                                    $mainreferralpurchasedata = str_replace($fromarrayreferralpurchase, $toarrayreferralpurchase, $revisepointsreferralpurchasedata);


                                    $overalllogs[] = array('userid' => $myid, 'totalvalue' => $exactpointer, 'eventname' => $mainreferralpurchasedata, 'date' => date('Y-m-d H:i:s'));
                                    $getoveralllogs = get_option('rsoveralllog');
                                    $logmerges = array_merge((array) $getoveralllogs, $overalllogs);
                                    update_option('rsoveralllog', $logmerges);
                                    $userpoints = get_user_meta($myid, '_my_reward_points', true);
                                    $updatedpoints = $userpoints - $exactpointer;
                                    update_user_meta($myid, '_my_reward_points', $updatedpoints);

                                    $pointspercent[] = array('orderid' => $order_id, 'userid' => $myid, 'points_earned_order' => '', 'points_redeemed' => $exactpointer, 'points_value' => '', 'before_order_points' => '', 'totalpoints' => get_user_meta($myid, '_my_reward_points', true), 'date' => $order->order_date, 'rewarder_for' => 'Referral Product Purchase is Revised due to User Deleted' . $item['product_id'], 'rewarder_for_frontend' => 'Referral Product Purchase is Revised' . $item['product_id']);
                                    $getlogg = get_user_meta($myid, '_my_points_log', true);
                                    $mergeds = array_merge((array) $getlogg, $pointspercent);
                                    update_user_meta($myid, '_my_points_log', $mergeds);
                                }
                            }


                            /* Referral Reward Points for Variable Products */
                            if (get_post_meta($item['variation_id'], '_enable_reward_points', true) == '1') {
                                $variablereferralrewardpoints = get_post_meta($item['variation_id'], '_referral_reward_points', true);
                                $variationreferralselectrule = get_post_meta($item['variation_id'], '_select_referral_reward_rule', true);
                                $variationreferralrewardpercent = get_post_meta($item['variation_id'], '_referral_reward_percent', true);
                                $variable_products = new WC_Product_Variation($item['variation_id']);
                                if (get_option('rs_set_price_percentage_reward_points') == '1') {
                                    $variationregularprice = $variable_products->regular_price;
                                } else {
                                    $variationregularprice = $variable_products->price;
                                }
                                if ($variationreferralselectrule == '1') {
                                    $getreferralpoints = get_post_meta($item['variation_id'], '_referral_reward_points', true);
                                    $parentvariationid = new WC_Product_Variation($item['variation_id']);
                                    $newparentid = $parentvariationid->parent->id;
                                    if ($getreferralpoints == '') {
                                        $term = get_the_terms($newparentid, 'product_cat');
//var_dump($term);
                                        if (is_array($term)) {
//var_dump($term);
                                            $rewardpoints = array('0');
                                            foreach ($term as $term) {

                                                $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                                $display_type = get_woocommerce_term_meta($term->term_id, 'referral_enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                                if ($enablevalue == 'yes') {
                                                    if ($display_type == '1') {
                                                        if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true) == '') {
                                                            $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                                            $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                                            if ($global_referral_enable == '1') {
                                                                if ($global_referral_reward_type == '1') {
                                                                    $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                                } else {
                                                                    $pointconversion = get_option('rs_earn_point');
                                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                                    $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                    $getaveragepoints = $getaverage * $variationregularprice;
                                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                }
                                                            }
                                                        } else {
                                                            $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true);
                                                        }
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) / 100;
                                                        $getaveragepoints = $getaverage * $variationregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) == '') {
                                                            $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                                            $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                                            if ($global_referral_enable == '1') {
                                                                if ($global_referral_reward_type == '1') {
                                                                    $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                                } else {
                                                                    $pointconversion = get_option('rs_earn_point');
                                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                                    $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                    $getaveragepoints = $getaverage * $variationregularprice;
                                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                }
                                                            }
                                                        } else {
                                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                        }
                                                    }
                                                } else {
                                                    if ($global_referral_enable == '1') {
                                                        if ($global_referral_reward_type == '1') {
                                                            $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                        } else {
                                                            $pointconversion = get_option('rs_earn_point');
                                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                                            $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                            $getaveragepoints = $getaverage * $variationregularprice;
                                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            if ($global_referral_enable == '1') {
                                                if ($global_referral_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $variationregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        }
//var_dump($rewardpoints);
                                        $getreferralpoints = max($rewardpoints);

//$rewardpoints = array_search($value, $rewardpoints);
                                    }
                                    $getvarpoints = $getreferralpoints * $item['qty'];

                                    $revisepointsreferralpurchasedata = get_option('_rs_localize_revise_points_for_referral_purchase');
                                    $fromarrayreferralpurchase = array('{productid}', '{usernickname}');
                                    $toarrayreferralpurchase = array($item['variation_id'], get_user_meta($order->user_id, 'nickname', true));

                                    $mainreferralpurchasedata = str_replace($fromarrayreferralpurchase, $toarrayreferralpurchase, $revisepointsreferralpurchasedata);



                                    $overalllogging[] = array('userid' => $myid, 'totalvalue' => $getvarpoints, 'eventname' => $mainreferralpurchasedata, 'date' => date('Y-m-d H:i:s'));
                                    $getoveralllogging = get_option('rsoveralllog');
                                    $logmerging = array_merge((array) $getoveralllogging, $overalllogging);
                                    update_option('rsoveralllog', $logmerging);
                                    $userpoints = get_user_meta($myid, '_my_reward_points', true);
                                    $newgetpointing = $userpoints - $getvarpoints;
                                    update_user_meta($myid, '_my_reward_points', $newgetpointing);
                                    $varpointsfixed[] = array('orderid' => $order_id, 'userid' => $myid, 'points_earned_order' => '', 'points_redeemed' => $getvarpoints, 'points_value' => '', 'before_order_points' => '', 'totalpoints' => get_user_meta($myid, '_my_reward_points', true), 'date' => $order->order_date, 'rewarder_for' => $mainreferralpurchasedata, 'rewarder_for_frontend' => $mainreferralpurchasedata);
                                    $getlogge = get_user_meta($myid, '_my_points_log', true);
                                    $mergedse = array_merge((array) $getlogge, $varpointsfixed);
                                    update_user_meta($myid, '_my_points_log', $mergedse);
                                } else {
                                    $pointconversion = get_option('rs_earn_point');
                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                    $getvaraverage = $variationreferralrewardpercent / 100;
                                    $getvaraveragepoints = $getvaraverage * $variationregularprice;
                                    $getvarpointsvalue = $getvaraveragepoints * $pointconversion;
                                    $varpoints = $getvarpointsvalue / $pointconversionvalue;

                                    $parentvariationid = new WC_Product_Variation($item['variation_id']);
                                    $newparentid = $parentvariationid->parent->id;
                                    if ($variationreferralrewardpercent == '') {
                                        $term = get_the_terms($newparentid, 'product_cat');
//var_dump($term);
                                        if (is_array($term)) {
//var_dump($term);
                                            $rewardpoints = array('0');
                                            foreach ($term as $term) {

                                                $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                                $display_type = get_woocommerce_term_meta($term->term_id, 'referral_enable_rs_rule', true);
//                                            echo $rewardpoints = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) . "<br>";
//                                            echo $rewardpercent = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) . "<br>";
                                                if ($enablevalue == 'yes') {
                                                    if ($display_type == '1') {
                                                        if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true) == '') {
                                                            $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                                            $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                                            if ($global_referral_enable == '1') {
                                                                if ($global_referral_reward_type == '1') {
                                                                    $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                                } else {
                                                                    $pointconversion = get_option('rs_earn_point');
                                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                                    $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                    $getaveragepoints = $getaverage * $variationregularprice;
                                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                }
                                                            }
                                                        } else {
                                                            $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_points', true);
                                                        }
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) / 100;
                                                        $getaveragepoints = $getaverage * $variationregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        if (get_woocommerce_term_meta($term->term_id, 'referral_rs_category_percent', true) == '') {
                                                            $global_referral_enable = get_option('rs_global_enable_disable_reward');
                                                            $global_referral_reward_type = get_option('rs_global_referral_reward_type');
                                                            if ($global_referral_enable == '1') {
                                                                if ($global_referral_reward_type == '1') {
                                                                    $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                                } else {
                                                                    $pointconversion = get_option('rs_earn_point');
                                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                                    $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                                    $getaveragepoints = $getaverage * $variationregularprice;
                                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                                }
                                                            }
                                                        } else {
                                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                        }
                                                    }
                                                } else {
                                                    if ($global_referral_enable == '1') {
                                                        if ($global_referral_reward_type == '1') {
                                                            $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                        } else {
                                                            $pointconversion = get_option('rs_earn_point');
                                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                                            $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                            $getaveragepoints = $getaverage * $variationregularprice;
                                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                                            $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            if ($global_referral_enable == '1') {
                                                if ($global_referral_reward_type == '1') {
                                                    $rewardpoints[] = get_option('rs_global_referral_reward_point');
                                                } else {
                                                    $pointconversion = get_option('rs_earn_point');
                                                    $pointconversionvalue = get_option('rs_earn_point_value');
                                                    $getaverage = get_option('rs_global_referral_reward_percent') / 100;
                                                    $getaveragepoints = $getaverage * $variationregularprice;
                                                    $pointswithvalue = $getaveragepoints * $pointconversion;
                                                    $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                }
                                            }
                                        }
//var_dump($rewardpoints);
                                        $varpoints = max($rewardpoints);

//$rewardpoints = array_search($value, $rewardpoints);
                                    }

                                    $getexactvar = $varpoints * $item['qty'];

                                    $revisepointsreferralpurchasedata = get_option('_rs_localize_revise_points_for_referral_purchase');
                                    $fromarrayreferralpurchase = array('{productid}', '{usernickname}');
                                    $toarrayreferralpurchase = array($item['variation_id'], get_user_meta($order->user_id, 'nickname', true));
                                    $mainreferralpurchasedata = str_replace($fromarrayreferralpurchase, $toarrayreferralpurchase, $revisepointsreferralpurchasedata);

                                    $overallloggered[] = array('userid' => $myid, 'totalvalue' => $getexactvar, 'eventname' => $mainreferralpurchasedata, 'date' => date('Y-m-d H:i:s'));
                                    $getoverallloggered = get_option('rsoveralllog');
                                    $logmergeder = array_merge((array) $getoverallloggered, $overallloggered);
                                    update_option('rsoveralllog', $logmergeder);
                                    $userpointser = get_user_meta($myid, '_my_reward_points', true);
                                    $newgetpointser = $userpointser - $getexactvar;
                                    update_user_meta($myid, '_my_reward_points', $newgetpointser);
                                    $varpointspercent[] = array('orderid' => $order_id, 'userid' => $myid, 'points_earned_order' => '', 'points_redeemed' => $getexactvar, 'points_value' => '', 'before_order_points' => '', 'totalpoints' => get_user_meta($myid, '_my_reward_points', true), 'date' => $order->order_date, 'rewarder_for' => $mainreferralpurchasedata, 'rewarder_for_frontend' => $mainreferralpurchasedata);
                                    $getlogge = get_user_meta($myid, '_my_points_log', true);
                                    $mergedse = array_merge((array) $getlogge, $varpointspercent);
                                    update_user_meta($myid, '_my_points_log', $mergedse);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static function get_reviewed_user_list($commentid, $approved) {
        if (get_option('timezone_string') != '') {
            $timezonedate = date_default_timezone_set(get_option('timezone_string'));
        } else {
            $timezonedate = date_default_timezone_set('UTC');
        }
        global $post;
        $mycomment = get_comment($commentid);
        //$mycomment->comment_post_ID
        //        $checkitisproduct = get_product($mycomment->comment_post_ID);
        $get_comment_post_type = get_post_type($mycomment->comment_post_ID);
        if ($get_comment_post_type == 'product') {

            if (get_option('rs_restrict_reward_product_review') == 'yes') {
                if (get_user_meta($mycomment->user_id, 'userreviewed' . $mycomment->comment_post_ID, true) != '1') {
                    if (($approved == true)) {
                        $getreviewpoints = RSUserRoleRewardPoints::user_role_based_reward_points($mycomment->user_id, get_option("rs_reward_product_review"));
                        FPRewardSystem::save_total_earned_points($mycomment->user_id, $getreviewpoints);
                        $oldpoints = get_user_meta($mycomment->user_id, '_my_reward_points', true);
                        $restrictuserpoints = get_option('rs_maximum_earning_points_for_user');
                        $enabledisablemaximumpoints = get_option('rs_enable_maximum_earning_points');
                        $currentregistrationpoints = $oldpoints + $getreviewpoints;
                        if ($enabledisablemaximumpoints == '1') {
                            if (($currentregistrationpoints <= $restrictuserpoints) || ($restrictuserpoints != '')) {
                                $currentregistrationpoints = $currentregistrationpoints;
                            } else {
                                $currentregistrationpoints = $restrictuserpoints;
                            }
                        }
                        update_user_meta($mycomment->user_id, '_my_reward_points', $currentregistrationpoints);
                        $myreward = get_user_meta($mycomment->user_id, '_my_reward_points', true);

                        $productreviewmessage = get_option('_rs_localize_points_earned_for_product_review');
                        $fromproductarray = array('{reviewproductid}');

                        $whichproductarray = "<a href='" . get_permalink($mycomment->comment_post_ID) . "'>#" . $mycomment->comment_post_ID . "</a>";

                        $toproductarray = array($whichproductarray);

                        $mainreviewmessage = str_replace($fromproductarray, $toproductarray, $productreviewmessage);


                        $pointslog[] = array('orderid' => '', 'userid' => $mycomment->user_id, 'points_earned_order' => $getreviewpoints, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $myreward, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => $mainreviewmessage, 'rewarder_for_frontend' => $mainreviewmessage);
                        $overalllog[] = array('userid' => $mycomment->user_id, 'totalvalue' => $getreviewpoints, 'eventname' => $mainreviewmessage, 'date' => date('Y-m-d H:i:s'));
                        $getoveralllog = get_option('rsoveralllog');
                        $logmerge = array_merge((array) $getoveralllog, $overalllog);
                        update_option('rsoveralllog', $logmerge);
                        $getmypoints = get_user_meta($mycomment->user_id, '_my_points_log', true);
                        $merged = array_merge((array) $getmypoints, $pointslog);
                        update_user_meta($mycomment->user_id, '_my_points_log', $merged);
                        update_user_meta($mycomment->user_id, 'userreviewed' . $mycomment->comment_post_ID, '1');
                        //   }
                        // }
                    }
                }
            } else {
                if (($approved == true)) {
                    $getreviewpoints = RSUserRoleRewardPoints::user_role_based_reward_points($mycomment->user_id, get_option("rs_reward_product_review"));
                    FPRewardSystem::save_total_earned_points($mycomment->user_id, $getreviewpoints);
                    $oldpoints = get_user_meta($mycomment->user_id, '_my_reward_points', true);
                    $restrictuserpoints = get_option('rs_maximum_earning_points_for_user');
                    $enabledisablemaximumpoints = get_option('rs_enable_maximum_earning_points');
                    $currentregistrationpoints = $oldpoints + $getreviewpoints;
                    if ($enabledisablemaximumpoints == '1') {
                        if (($currentregistrationpoints <= $restrictuserpoints) || ($restrictuserpoints != '')) {
                            $currentregistrationpoints = $currentregistrationpoints;
                        } else {
                            $currentregistrationpoints = $restrictuserpoints;
                        }
                    }
                    update_user_meta($mycomment->user_id, '_my_reward_points', $currentregistrationpoints);
                    $myreward = get_user_meta($mycomment->user_id, '_my_reward_points', true);

                    $productreviewmessage = get_option('_rs_localize_points_earned_for_product_review');
                    $fromproductarray = array('{reviewproductid}');

                    $whichproductarray = "<a href='" . get_permalink($mycomment->comment_post_ID) . "'>#" . $mycomment->comment_post_ID . "</a>";

                    $toproductarray = array($whichproductarray);

                    $mainreviewmessage = str_replace($fromproductarray, $toproductarray, $productreviewmessage);

                    $pointslog[] = array('orderid' => '', 'userid' => $mycomment->user_id, 'points_earned_order' => $getreviewpoints, 'points_redeemed' => '', 'points_value' => '', 'before_order_points' => '', 'totalpoints' => $myreward, 'date' => date('Y-m-d H:i:s'), 'rewarder_for' => $mainreviewmessage, 'rewarder_for_frontend' => $mainreviewmessage);
                    $overalllog[] = array('userid' => $mycomment->user_id, 'totalvalue' => $getreviewpoints, 'eventname' => $mainreviewmessage, 'date' => date('Y-m-d H:i:s'));
                    $getoveralllog = get_option('rsoveralllog');
                    $logmerge = array_merge((array) $getoveralllog, $overalllog);
                    update_option('rsoveralllog', $logmerge);
                    $getmypoints = get_user_meta($mycomment->user_id, '_my_points_log', true);
                    $merged = array_merge((array) $getmypoints, $pointslog);
                    update_user_meta($mycomment->user_id, '_my_points_log', $merged);
                }
            }
        }
    }

    public static function alert_on_user_list() {
        date('Y-m-d H:i:s');
        ?>
        <script type='text/javascript'>
            jQuery(document).ready(function () {
                alert("<?php echo date('Y-m-d H:i:s'); ?>");
            });</script>
        <?php
    }

    public static function getcommentstatus($id) {
        if (get_option('rs_review_reward_status') == '1') {
            FPRewardSystem::get_reviewed_user_list($id, true);
        } else {
            FPRewardSystem::get_reviewed_user_list($id, false);
        }
    }

    public static function add_message_to_single_product() {
        global $post;
        $order = '';
        //$banned_user_list = get_option('rs_banned-users_list');
        if (is_user_logged_in()) {
//            if (!in_array(get_current_user_id(), (array) $banned_user_list)) {
//                $getarrayofuserdata = get_userdata(get_current_user_id());
//                $banninguserrole = get_option('rs_banning_user_role');
//                if (!in_array(isset($getarrayofuserdata->roles[0]) ? $getarrayofuserdata->roles[0] : '0', (array) $banninguserrole)) {
            $userid = get_current_user_id();
            $banning_type = FPRewardSystem::check_banning_type($userid);
            if ($banning_type != 'earningonly' && $banning_type != 'both') {
                $checkproducttype = get_product($post->ID);
                if (get_option('rs_show_hide_message_for_single_product') == '1') {
                    if ($checkproducttype->is_type('simple') || ($checkproducttype->is_type('subscription'))) {
                        if (get_post_meta($post->ID, '_rewardsystemcheckboxvalue', true) == 'yes') {
                            if (get_post_meta($post->ID, '_rewardsystem_options', true) == '1') {
                                $rewardpoints = do_shortcode('[rewardpoints]');
                                if ($rewardpoints > 0) {
                                    ?>
                                    <div class="woocommerce-info"><?php echo do_shortcode(get_option('rs_message_for_single_product_point_rule')); ?></div>
                                    <?php
                                }
                            } else {
                                $rewardpoints = do_shortcode('[rewardpoints]');
                                if ($rewardpoints > 0) {
                                    ?>
                                    <div class="woocommerce-info"><?php echo do_shortcode(get_option('rs_message_for_single_product_point_rule')); ?></div>
                                    <?php
                                }
                            }
                        }
                    } else {
                        global $woocommerce;
                        if (isset($woocommerce->cart->cart_contents)) {
                            ?>
                            <div class="header_cart">
                                <div class="cart_contents">
                                    <a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title=""><?php sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'rewardsystem'), $woocommerce->cart->cart_contents_count); ?> <?php //echo $woocommerce->cart->cart_contents->product_id;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ?></a>
                                </div>
                            </div>
                            <?php
                        }
                    }
                }
//                }
//            }
            }
        } else {
            if (get_option('rs_show_hide_message_for_single_product') == '1') {
                $checkproducttype = get_product($post->ID);
                if ($checkproducttype->is_type('simple') || ($checkproducttype->is_type('subscription'))) {
                    if (get_post_meta($post->ID, '_rewardsystemcheckboxvalue', true) == 'yes') {
                        if (get_post_meta($post->ID, '_rewardsystem_options', true) == '1') {
                            $rewardpoints = do_shortcode('[rewardpoints]');
                            if ($rewardpoints > 0) {
                                ?>
                                <div class="woocommerce-info"><?php echo do_shortcode(get_option('rs_message_for_single_product_point_rule')); ?></div>
                                <?php
                            }
                        } else {
                            $rewardpoints = do_shortcode('[rewardpoints]');
                            if ($rewardpoints > 0) {
                                ?>
                                <div class="woocommerce-info"><?php echo do_shortcode(get_option('rs_message_for_single_product_point_rule')); ?></div>
                                <?php
                            }
                        }
                    }
                } else {
                    global $woocommerce;
                    if (isset($woocommerce->cart->cart_contents)) {
                        ?>
                        <div class="header_cart">
                            <div class="cart_contents">
                                <a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title=""><?php sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'rewardsystem'), $woocommerce->cart->cart_contents_count); ?> <?php //echo $woocommerce->cart->cart_contents->product_id;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ?></a>
                            </div>
                        </div>
                        <?php
                    }
                }
            }
        }
    }

    public static function get_redeem_conversion_value() {
        if (get_current_user_id() > 0) {
            $singleproductvalue = do_shortcode('[rewardpoints]');
        } else {
            $singleproductvalue = do_shortcode('[rewardpoints]');
        }
        $newvalue = $singleproductvalue / get_option('rs_redeem_point');
        $updatedvalue = $newvalue * get_option('rs_redeem_point_value');
        $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
        return get_woocommerce_currency_symbol() . round($updatedvalue, $roundofftype);
    }

    public static function get_minimum_redeeming_points_value() {
        return get_option('rs_minimum_redeeming_points');
    }

    public static function add_shortcode_function_to_single_product() {
        if (get_option('timezone_string') != '') {
            $timezonedate = date_default_timezone_set(get_option('timezone_string'));
        } else {
            $timezonedate = date_default_timezone_set('UTC');
        }
        global $post;
        $checkproducttype = get_product($post->ID);
        $global_enable = get_option('rs_global_enable_disable_reward');
        $global_reward_type = get_option('rs_global_reward_type');
        if (is_shop() || is_product()) {
            if ($checkproducttype->is_type('simple') || ($checkproducttype->is_type('subscription'))) {
                $enablerewards = get_post_meta($post->ID, '_rewardsystemcheckboxvalue', true);
                $getaction = get_post_meta($post->ID, '_rewardsystem_options', true);
                $getpoints = get_post_meta($post->ID, '_rewardsystempoints', true);
                $getpercent = get_post_meta($post->ID, '_rewardsystempercent', true);
                if (get_option('rs_set_price_percentage_reward_points') == '1') {
                    $getregularprice = get_post_meta($post->ID, '_regular_price', true);
                } else {
                    $getregularprice = get_post_meta($post->ID, '_price', true);
                }

                $rewardpoints = array('0');
                if ($enablerewards == 'yes') {
                    if ($getaction == '1') {
                        if ($getpoints == '') {
                            $term = get_the_terms($post->ID, 'product_cat');
// var_dump($term);
                            if (is_array($term)) {
//var_dump($term);
// $rewardpoints = array();
                                foreach ($term as $term) {
                                    $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                    $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
                                    if ($enablevalue == 'yes') {
                                        if ($display_type == '1') {
                                            if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                                $global_enable = get_option('rs_global_enable_disable_reward');
                                                $global_reward_type = get_option('rs_global_reward_type');
                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                            }
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                            $getaveragepoints = $getaverage * $getregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {

                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        if ($global_enable == '1') {
                                            if ($global_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if ($global_enable == '1') {
                                    if ($global_reward_type == '1') {
                                        $rewardpoints[] = get_option('rs_global_reward_points');
                                    } else {
                                        $pointconversion = get_option('rs_earn_point');
                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                        $getaverage = get_option('rs_global_reward_percent') / 100;
                                        $getaveragepoints = $getaverage * $getregularprice;
                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                    }
                                }
                            }
//var_dump($rewardpoints);
                            if (!empty($rewardpoints)) {
                                $getpoints = max($rewardpoints);
                            }
                        }
                        $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
                        if (get_current_user_id() > 0) {
                            return round(RSUserRoleRewardPoints::user_role_based_reward_points(get_current_user_id(), $getpoints), $roundofftype);
                        } else {
                            return round($getpoints, $roundofftype);
                        }
                    } else {
                        $points = get_option('rs_earn_point');
                        $pointsequalto = get_option('rs_earn_point_value');
                        $takeaverage = $getpercent / 100;
                        $mainaveragevalue = $takeaverage * $getregularprice;
                        $addinpoint = $mainaveragevalue * $points;
                        $totalpoint = $addinpoint / $pointsequalto;
                        if ($getpercent === '') {
                            $term = get_the_terms($post->ID, 'product_cat');
// var_dump($term);
                            if (is_array($term)) {
//var_dump($term);
//                            $rewardpoints = array();
                                foreach ($term as $term) {
                                    $enablevalue = get_woocommerce_term_meta($term->term_id, 'enable_reward_system_category', true);
                                    $display_type = get_woocommerce_term_meta($term->term_id, 'enable_rs_rule', true);
                                    if ($enablevalue == 'yes') {
                                        if ($display_type == '1') {
                                            if (get_woocommerce_term_meta($term->term_id, 'rs_category_points', true) == '') {
                                                $global_enable = get_option('rs_global_enable_disable_reward');
                                                $global_reward_type = get_option('rs_global_reward_type');
                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpoints[] = get_woocommerce_term_meta($term->term_id, 'rs_category_points', true);
                                            }
                                        } else {
                                            $pointconversion = get_option('rs_earn_point');
                                            $pointconversionvalue = get_option('rs_earn_point_value');
                                            $getaverage = get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) / 100;
                                            $getaveragepoints = $getaverage * $getregularprice;
                                            $pointswithvalue = $getaveragepoints * $pointconversion;
                                            if (get_woocommerce_term_meta($term->term_id, 'rs_category_percent', true) == '') {
                                                $global_enable = get_option('rs_global_enable_disable_reward');
                                                $global_reward_type = get_option('rs_global_reward_type');
                                                if ($global_enable == '1') {
                                                    if ($global_reward_type == '1') {
                                                        $rewardpoints[] = get_option('rs_global_reward_points');
                                                    } else {
                                                        $pointconversion = get_option('rs_earn_point');
                                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                                        $getaverage = get_option('rs_global_reward_percent') / 100;
                                                        $getaveragepoints = $getaverage * $getregularprice;
                                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                                    }
                                                }
                                            } else {
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    } else {
                                        if ($global_enable == '1') {
                                            if ($global_reward_type == '1') {
                                                $rewardpoints[] = get_option('rs_global_reward_points');
                                            } else {
                                                $pointconversion = get_option('rs_earn_point');
                                                $pointconversionvalue = get_option('rs_earn_point_value');
                                                $getaverage = get_option('rs_global_reward_percent') / 100;
                                                $getaveragepoints = $getaverage * $getregularprice;
                                                $pointswithvalue = $getaveragepoints * $pointconversion;
                                                $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if ($global_enable == '1') {
                                    if ($global_reward_type == '1') {
                                        $rewardpoints[] = get_option('rs_global_reward_points');
                                    } else {
                                        $pointconversion = get_option('rs_earn_point');
                                        $pointconversionvalue = get_option('rs_earn_point_value');
                                        $getaverage = get_option('rs_global_reward_percent') / 100;
                                        $getaveragepoints = $getaverage * $getregularprice;
                                        $pointswithvalue = $getaveragepoints * $pointconversion;
                                        $rewardpoints[] = $pointswithvalue / $pointconversionvalue;
                                    }
                                }
                            }
//var_dump($rewardpoints);
                            if (!empty($rewardpoints)) {
                                $totalpoint = max($rewardpoints);
                            } else {
                                $totalpoint = 0;
                            }
                        }
                        $roundofftype = get_option('rs_round_off_type') == '1' ? '2' : '0';
                        if (get_current_user_id() > 0) {
                            return round(RSUserRoleRewardPoints::user_role_based_reward_points(get_current_user_id(), $totalpoint), $roundofftype);
                        } else {
                            return round($totalpoint, $roundofftype);
                        }
                    }
                }
            } else {

            }
        }
    }

    public static function add_shortcode_get_percent() {
        global $post;
        $checkproducttype = get_product($post->ID);
        if ($checkproducttype->is_type('simple') || ($checkproducttype->is_type('subscription'))) {
            $enablerewards = get_post_meta($post->ID, '_rewardsystemcheckboxvalue', true);
            $getaction = get_post_meta($post->ID, '_rewardsystem_options', true);
            $getpoints = get_post_meta($post->ID, '_rewardsystempoints', true);
            $getpercent = get_post_meta($post->ID, '_rewardsystempercent', true);
            if (get_option('rs_set_price_percentage_reward_points') == '1') {
                $getregularprice = get_post_meta($post->ID, '_regular_price', true);
            } else {
                $getregularprice = get_post_meta($post->ID, '_price', true);
            }
            if ($getaction == '2') {
                $getpercentage = $getpercent;
                return $getpercentage . "%";
            }
        }
    }

    public static function rs_create_table() {
//Email Template Table
        global $wpdb;
        $table_name_email = $wpdb->prefix . 'rs_templates_email';
        $sql = "CREATE TABLE $table_name_email (
  id mediumint(9) NOT NULL AUTO_INCREMENT,
  template_name LONGTEXT NOT NULL,
  sender_opt VARCHAR(10) NOT NULL DEFAULT 'woo',
  from_name LONGTEXT NOT NULL,
  from_email LONGTEXT NOT NULL,
  subject LONGTEXT NOT NULL,
  message LONGTEXT NOT NULL,
  earningpoints LONGTEXT NOT NULL,
  redeemingpoints LONGTEXT NOT NULL,
  mailsendingoptions LONGTEXT NOT NULL,
  rsmailsendingoptions LONGTEXT NOT NULL,
  minimum_userpoints LONGTEXT NOT NULL,
  sendmail_options VARCHAR(10) NOT NULL DEFAULT '1',
  sendmail_to LONGTEXT NOT NULL,
  sending_type VARCHAR(20) NOT NULL,
  UNIQUE KEY id (id)
)DEFAULT CHARACTER SET utf8;";
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta($sql);
        $email_temp_check = $wpdb->get_results("SELECT * FROM $table_name_email", OBJECT);
        if (empty($email_temp_check)) {
            $wpdb->insert($table_name_email, array('template_name' => 'Default',
                'sender_opt' => 'woo',
                'from_name' => 'Admin',
                'from_email' => get_option('admin_email'),
                'subject' => 'SUMO Rewards',
                'message' => 'Hi {rsfirstname} {rslastname}, <br><br> You have Earned Reward Points: {rspoints} on {rssitelink}  <br><br> You can use this Reward Points to make discounted purchases on {rssitelink} <br><br> Thanks',
                'minimum_userpoints' => '0',
                'mailsendingoptions' => '2',
                'rsmailsendingoptions' => '3',
            ));
        }
    }

    public static function get_my_account_url_link() {
        global $woocommerce;
        $myaccountlink = get_permalink(get_option('woocommerce_myaccount_page_id'));
        $myaccounttitle = get_the_title(get_option('woocommerce_myaccount_page_id'));
        return '<a href=' . $myaccountlink . '>' . $myaccounttitle . '</a>';
    }

    public static function rewardsystem_enqueue_script() {
        wp_register_script('wp_reward_footable', plugins_url('rewardsystem/js/footable.js'));
        //wp_register_script('wp_reward_jquery_ui', plugins_url('rewardsystem/js/jquery-ui.js'));
        wp_register_script('wp_reward_footable_sort', plugins_url('rewardsystem/js/footable.sort.js'));
        wp_register_script('wp_reward_footable_paging', plugins_url('rewardsystem/js/footable.paginate.js'));
        wp_register_script('wp_reward_footable_filter', plugins_url('rewardsystem/js/footable.filter.js'));
        wp_register_style('wp_reward_footable_css', plugins_url('rewardsystem/css/footable.core.css'));
        wp_register_style('wp_reward_bootstrap_css', plugins_url('rewardsystem/css/bootstrap.css'));
        //wp_register_style('wp_reward_jquery_ui_css', plugins_url('rewardsystem/css/jquery-ui.css'));
        wp_enqueue_script('jquery');

        wp_enqueue_script('wp_reward_footable');
        wp_enqueue_script('wp_reward_footable_sort');
        wp_enqueue_script('wp_reward_footable_paging');
        wp_enqueue_script('wp_reward_footable_filter');
        wp_enqueue_style('wp_reward_footable_css');
        wp_enqueue_style('wp_reward_bootstrap_css');
    }

    public static function admin_enqueue_rs_script() {
        wp_register_style('wp_reward_jquery_ui_css', plugins_url('rewardsystem/css/jquery-ui.css'));
        wp_register_script('wp_reward_jquery_ui', plugins_url('rewardsystem/js/jquery-ui.js'));

        if (function_exists('wp_enqueue_media')) {
            wp_enqueue_media();
        } else {
            wp_enqueue_style('thickbox');
            wp_enqueue_script('media-upload');
            wp_enqueue_script('thickbox');
        }

        if (isset($_GET['tab'])) {
            if ($_GET['tab'] == 'rewardsystem_point_vouchers') {
                wp_enqueue_script('wp_reward_jquery_ui');
                wp_enqueue_style('wp_reward_jquery_ui_css');
            }
            if ($_GET['tab'] == 'rewardsystem_updates') {
                wp_enqueue_script('wp_reward_jquery_ui');
                wp_enqueue_style('wp_reward_jquery_ui_css');
            }
            do_action_ref_array('enqueuescriptforadmin', array(&$enqueuescript));
        }
    }

    public static function rs_translate_file() {
        load_plugin_textdomain('rewardsystem', false, dirname(plugin_basename(__FILE__)) . '/languages');
    }

    public static function add_script_to_my_account() {
        if (!is_product()) {
            ?>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    jQuery('.examples').footable().bind('footable_filtering', function (e) {
                        var selected = jQuery('.filter-status').find(':selected').text();
                        if (selected && selected.length > 0) {
                            e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
                            e.clear = !e.filter;
                        }
                    });
                    jQuery('#change-page-sizes').change(function (e) {
                        e.preventDefault();
                        var pageSize = jQuery(this).val();
                        jQuery('.footable').data('page-size', pageSize);
                        jQuery('.footable').trigger('footable_initialized');
                    });
                });</script>
            <?php
        }
    }

    public static function add_script_to_dashboard() {
        ?>
        <script type='text/javascript'>
            jQuery(document).ready(function () {
                jQuery('#user_logs').footable().bind('footable_filtering', function (e) {
                    var selected = jQuery('.filter-status').find(':selected').text();
                    if (selected && selected.length > 0) {
                        e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
                        e.clear = !e.filter;
                    }
                });
                jQuery('#changepagesize').change(function (e) {
                    e.preventDefault();
                    var pageSize = jQuery(this).val();
                    jQuery('.footable').data('page-size', pageSize);
                    jQuery('.footable').trigger('footable_initialized');
                });
                jQuery('#individual_log').footable().bind('footable_filtering', function (e) {
                    var selected = jQuery('.filter-status').find(':selected').text();
                    if (selected && selected.length > 0) {
                        e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
                        e.clear = !e.filter;
                    }
                });
                jQuery('#changepagesizes').change(function (e) {
                    e.preventDefault();
                    var pageSize = jQuery(this).val();
                    jQuery('.footable').data('page-size', pageSize);
                    jQuery('.footable').trigger('footable_initialized');
                });
                jQuery('#overall_points').footable().bind('footable_filtering', function (e) {
                    var selected = jQuery('.filter-status').find(':selected').text();
                    if (selected && selected.length > 0) {
                        e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
                        e.clear = !e.filter;
                    }
                });
                jQuery('#changepagesizer').change(function (e) {
                    e.preventDefault();
                    var pageSize = jQuery(this).val();
                    jQuery('.footable').data('page-size', pageSize);
                    jQuery('.footable').trigger('footable_initialized');
                });
                jQuery('#changepagesizertemplates').change(function (e) {
                    e.preventDefault();
                    var pageSize = jQuery(this).val();
                    jQuery('.footable').data('page-size', pageSize);
                    jQuery('.footable').trigger('footable_initialized');
                });
            });</script>
        <?php
    }

    public static function add_enqueue_for_social_messages() {

        wp_register_script('wp_reward_tooltip', plugins_url('rewardsystem/js/jquery.tipsy.js'));
        wp_register_style('wp_reward_tooltip_style', plugins_url('rewardsystem/css/tipsy.css'));
        wp_register_script('wp_jscolor_rewards', plugins_url('rewardsystem/jscolor/jscolor.js'));
        if (get_option('rs_reward_point_enable_tipsy_social_rewards') == '1') {
            wp_enqueue_script('wp_reward_tooltip');
        }
        wp_enqueue_script('wp_jscolor_rewards');
        wp_enqueue_style('wp_reward_tooltip_style');
    }

    public static function add_enqueue_jscolor_for_social_messages() {
        wp_register_script('wp_jscolor_rewards', plugins_url('rewardsystem/jscolor/jscolor.js'));
        wp_enqueue_script('wp_jscolor_rewards');
    }

    public static function add_custom_message_to_payment_gateway_on_checkout() {
        if (get_option('rs_show_hide_message_payment_gateway_reward_points') == '1') {
            ?>
            <script type="text/javascript">
                jQuery(document).ready(function () {

                    jQuery('.subinfogateway').parent().hide();
                    jQuery('#order_review').on('click', '.payment_methods input.input-radio', function () {
                        var orderpaymentgateway = jQuery(this).val();
                        var paymentgatewaytitle = jQuery('.payment_method_' + orderpaymentgateway).find('label').html();
                        var dataparam = ({
                            action: 'rs_order_payment_gateway_reward',
                            getpaymentgatewayid: orderpaymentgateway,
                            getpaymenttitle: paymentgatewaytitle,
                            userid: "<?php echo get_current_user_id(); ?>",
                        });
                        jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                                function (response) {
                                    jQuery('.subinfogateway').parent().show();
                                    // alert(response);
                                    jQuery('.rspgpoints').html(response.rewardpoints);
                                    var responsepoints = jQuery('.rspgpoints').html(response.rewardpoints);
                                    if ((response.rewardpoints == null) || (response.rewardpoints == '')) {
                                        jQuery('.rspgpoints').parent().hide();
                                    } else {
                                        jQuery('.rspgpoints').parent().show();
                                    }
                                    if (response.title !== null) {

                                        jQuery('.subinfogateway').html(response.title.replace(/\\/g, ''));
                                    }
                                }, 'json');
                    });
                });</script>
            <?php
            $getmessage = get_option('rs_message_payment_gateway_reward_points');
            $findarray = array('[paymentgatewaytitle]', '[paymentgatewaypoints]');
            $replacearray = array('<label class="subinfogateway">  </label>', '<span class="rspgpoints"></span>');
            $output = str_replace($findarray, $replacearray, $getmessage);
            ?>

            <div class="woocommerce-info"><?php echo $output; ?></div>


            <?php
        }
    }

    public static function payment_gateway_reward_points_process_ajax_request() {
        if (isset($_POST['getpaymentgatewayid'])) {
            $gatewayid = $_POST['getpaymentgatewayid'];
            $getthevalue = RSUserRoleRewardPoints::user_role_based_reward_points($_POST['userid'], get_option('rs_reward_payment_gateways_' . $gatewayid));
            $getthetitle = $_POST['getpaymenttitle'];
            echo json_encode(array('rewardpoints' => $getthevalue, 'title' => $getthetitle));
        }
        exit();
    }

    public static function checkout_page_custom_css_reward_system() {
        ?>
        <style type="text/css">
        <?php echo htmlspecialchars_decode(get_option('rs_checkout_page_custom_css')); ?>
        </style>
        <?php
    }

    public static function save_total_earned_points($userid, $earningpoints) {
        $rs_previous_points_data = get_user_meta($userid, 'rs_user_total_earned_points', true);
        $rs_total_earned_points = $rs_previous_points_data + $earningpoints;
        //var_dump($rs_previous_points_data);
        return update_user_meta($userid, 'rs_user_total_earned_points', $rs_total_earned_points);
    }

    public static function trigger_total_earned_points() {
        if (is_user_logged_in()) {
            $userid = get_current_user_id();
            //$rs_bool = get_option("rs_boolean_set");
            $rs_bool = get_user_meta($userid, "rs_boolean_set", true);
            if ($rs_bool == "") {
                $user_id = get_current_user_id();
                $rs_previous_earned_points = get_user_meta($user_id, '_my_reward_points', true);
                //delete_user_meta($user_id,'rs_user_total_earned_points');
                $rs_total_earned_points = get_user_meta($user_id, 'rs_user_total_earned_points', true);
                $rs_total_earned_points = $rs_total_earned_points != "" ? $rs_total_earned_points : $rs_previous_earned_points;

                update_user_meta($user_id, 'rs_user_total_earned_points', $rs_total_earned_points);
                //update_option('rs_boolean_set',1);
                update_user_meta($user_id, "rs_boolean_set", 1);
            }
        }
    }

    public static function check_banning_type($userid) {
        $earning = get_option('rs_enable_banning_users_earning_points');
        $redeeming = get_option('rs_enable_banning_users_redeeming_points');
        $banned_user_list = get_option('rs_banned-users_list');
        if (in_array($userid, (array) $banned_user_list)) {
            if ($earning == 'no' && $redeeming == 'no') {
                return "no_banning";
            }

            if ($earning == 'no' && $redeeming == 'yes') {

                return 'redeemingonly';
            }
            if ($earning == 'yes' && $redeeming == 'no') {
                //$banned_user_list = get_option('rs_banned-users_list');
                return 'earningonly';
            }
            if ($earning == 'yes' && $redeeming == 'yes') {
                //$banned_user_list = get_option('rs_banned-users_list');
                return 'both';
            }
        } else {
            $getarrayofuserdata = get_userdata(get_current_user_id());
            $banninguserrole = get_option('rs_banning_user_role');
            if (in_array(isset($getarrayofuserdata->roles[0]) ? $getarrayofuserdata->roles[0] : '0', (array) $banninguserrole)) {
                if ($earning == 'no' && $redeeming == 'no') {
                    return "no_banning";
                }

                if ($earning == 'no' && $redeeming == 'yes') {

                    return 'redeemingonly';
                }
                if ($earning == 'yes' && $redeeming == 'no') {
                    $banned_user_list = get_option('rs_banned-users_list');
                    return 'earningonly';
                }
                if ($earning == 'yes' && $redeeming == 'yes') {
                    $banned_user_list = get_option('rs_banned-users_list');
                    return 'both';
                }
            }
        }
    }

    /* To get the total earned for order */

    public static function get_the_total_earned_points_for_order($order) {
        $status = get_option('rs_order_status_control');
        $orderstatus = $order->post_status;
        if (is_array($status)) {
            foreach ($status as $statuses) {
                $statusstr = $statuses;
            }
        }
        $replacestatus = str_replace('wc-completed', $statusstr, $orderstatus);
        if (get_option('rs_enable_msg_for_earned_points') == 'yes') {
            if (in_array($replacestatus, $status)) {
                $totalearnedvalue = "";
                $earned_total = get_post_meta($order->id, 'rs_total_earned_points', true);
                if (is_array($earned_total)) {
                    foreach ($earned_total as $key => $value) {
                        $totalearnedvalue+=$value;
                    }
                    $msgforearnedpoints = get_option('rs_msg_for_earned_points');
                    $replacemsgforearnedpoints = str_replace('[earnedpoints]', $totalearnedvalue != "" ? $totalearnedvalue : "0", $msgforearnedpoints);
                    echo '<br><br>' . '<b>' . $replacemsgforearnedpoints . '<b>' . '<br><br>';
                }
            }
        }
    }

    /* To get the total redeem for order */

    public static function get_the_total_redeem_points_for_order($order) {
        $status = get_option('rs_order_status_control');
        $orderstatus = $order->post_status;
        if (is_array($status)) {
            foreach ($status as $statuses) {
                $statusstr = $statuses;
            }
        }
        $replacestatus = str_replace('wc-completed', $statusstr, $orderstatus);
        if (get_option('rs_enable_msg_for_redeem_points') == 'yes') {
            if (in_array($replacestatus, $status)) {
                $totalredeemvalue = "";
                $redeem_total = get_post_meta($order->id, 'rs_total_redeem_points', true);
                if (is_array($redeem_total)) {
                    foreach ($redeem_total as $key => $value) {
                        $totalredeemvalue+=$value;
                    }
                    $msgforredeempoints = get_option('rs_msg_for_redeem_points');
                    $replacemsgforredeempoints = str_replace('[redeempoints]', $totalredeemvalue != "" ? $totalredeemvalue : "0", $msgforredeempoints);
                    echo '<b>' . $replacemsgforredeempoints . '</b>';
                }
            }
        }
    }

}

function rs_fp_save_product_variations($variation_id, $i) {
    global $woocommerce;
    if ((float) $woocommerce->version > (float) ('2.2.0')) {
        update_option('variationids', $variation_id);


        $variable_sku = $_POST['variable_sku'];
        $variable_post_id = $_POST['variable_post_id'];

// Text Field
        $_text_field = $_POST['_reward_points'];

        // $variation_id = (int) $variable_post_id[$i];
        if (isset($_text_field[$i])) {
            update_post_meta($variation_id, '_reward_points', stripslashes($_text_field[$i]));
        }


        $percent_text_field = $_POST['_reward_percent'];

        // $variation_id = (int) $variable_post_id[$i];
        if (isset($percent_text_field[$i])) {
            update_post_meta($variation_id, '_reward_percent', stripslashes($percent_text_field[$i]));
        }

//select
        $new_select = $_POST['_select_reward_rule'];

        //$variation_id = (int) $variable_post_id[$i];
        if (isset($new_select[$i])) {
            update_post_meta($variation_id, '_select_reward_rule', stripslashes($new_select[$i]));
        }



        $_text_fields = $_POST['_referral_reward_points'];

        //$variation_id = (int) $variable_post_id[$i];
        if (isset($_text_field[$i])) {
            update_post_meta($variation_id, '_referral_reward_points', stripslashes($_text_fields[$i]));
        }


        $percent_text_fields = $_POST['_referral_reward_percent'];

        //$variation_id = (int) $variable_post_id[$i];
        if (isset($percent_text_field[$i])) {
            update_post_meta($variation_id, '_referral_reward_percent', stripslashes($percent_text_fields[$i]));
        }

//select
        $new_selects = $_POST['_select_referral_reward_rule'];

        //$variation_id = (int) $variable_post_id[$i];
        if (isset($new_select[$i])) {
            update_post_meta($variation_id, '_select_referral_reward_rule', stripslashes($new_selects[$i]));
        }



// Select
        $_select = $_POST['_enable_reward_points'];

        //$variation_id = (int) $variable_post_id[$i];
        if (isset($_select[$i])) {
            update_post_meta($variation_id, '_enable_reward_points', stripslashes($_select[$i]));
        }
    }
}

function load_product_variations() {
    var_dump(get_option('variationids'));
}

//add_action('admin_head', 'load_product_variations');


add_action('woocommerce_save_product_variation', 'rs_fp_save_product_variations', 10, 2);

add_action('wp_ajax_rs_order_payment_gateway_reward', array('FPRewardSystem', 'payment_gateway_reward_points_process_ajax_request'));
//add_action('wp_ajax_no_priv_rs_order_payment_gateway_reward', array('FPRewardSystem', 'payment_gateway_reward_points_process_ajax_request'));


add_action('woocommerce_after_checkout_form', array('FPRewardSystem', 'add_custom_message_to_payment_gateway_on_checkout'));
add_action('wp_head', array('FPRewardSystem', 'trigger_total_earned_points'));
add_action('admin_head', array('FPRewardSystem', 'trigger_total_earned_points'));

add_action('wp_enqueue_scripts', array('FPRewardSystem', 'rewardsystem_enqueue_script'));
add_action('wp_enqueue_scripts', array('FPRewardSystem', 'add_enqueue_for_social_messages'));
add_action('admin_enqueue_scripts', array('FPRewardSystem', 'rewardsystem_enqueue_script'));
add_action('admin_enqueue_scripts', array('FPRewardSystem', 'admin_enqueue_rs_script'));
add_action('admin_enqueue_scripts', array('FPRewardSystem', 'add_enqueue_jscolor_for_social_messages'));


add_action('wp_head', array('FPRewardSystem', 'add_script_to_my_account'));
add_action('admin_head', array('FPRewardSystem', 'add_script_to_dashboard'));
add_action('admin_head', array('FPRewardSystem', 'reward_point_delay_for_referee'));


add_shortcode('loginlink', array('FPRewardSystem', 'get_my_account_url_link'));
add_shortcode('rewardpoints', array('FPRewardSystem', 'add_shortcode_function_to_single_product'));
add_shortcode('equalamount', array('FPRewardSystem', 'get_redeem_conversion_value'));
add_shortcode('rewardpercent', array('FPRewardSystem', 'add_shortcode_get_percent'));
add_action('woocommerce_before_single_product', array('FPRewardSystem', 'add_message_to_single_product'));
add_action('comment_post', array('FPRewardSystem', 'get_reviewed_user_list'), 10, 2);
add_action('woocommerce_email_after_order_table', array('FPRewardSystem', 'get_the_total_earned_points_for_order'));
add_action('woocommerce_email_after_order_table', array('FPRewardSystem', 'get_the_total_redeem_points_for_order'));



if (get_option('rs_review_reward_status') == '1') {
    add_action('comment_unapproved_to_approved', array('FPRewardSystem', 'getcommentstatus'), 10, 1);
}
if (get_option('rs_review_reward_status') == '2') {
    add_action('comment_unapproved', array('FPRewardSystem', 'getcommentstatus'), 10, 1);
}
//add_action('transition_post_status', array('FPRewardSystem', 'get_status_control_reviews'));
//add_action('wp_head', array('FPRewardSystem', 'get_reviewed_user_list'));

/* * *********************************************************************
 * **Support for Variation with adding the input field and saving value*
 * *********************************************************************
 */
//add_action('admin_head', array('FPRewardSystem', 'get_variation_id_from_order'));
//add_action('woocommerce_created_customer', array('FPRewardSystem', 'add_registration_rewards_points'), 10, 1);
add_action('user_register', array('FPRewardSystem', 'add_registration_rewards_points'), 10, 1);

add_action('woocommerce_product_after_variable_attributes', array('FPRewardSystem', 'add_field_to_variation_reward_points'), 10, 3);
add_action('woocommerce_product_after_variable_attributes_js', array('FPRewardSystem', 'add_field_to_variation_in_js'));
add_action('woocommerce_process_product_meta_variable', array('FPRewardSystem', 'save_variable_product_fields'), 10, 1);



add_action('woocommerce_process_product_meta_variable-subscription', array('FPRewardSystem', 'save_variable_product_fields'), 10, 1);
if (get_option('rs_my_reward_table') == '1') {
    add_action('woocommerce_after_my_account', array('FPRewardSystem', 'viewchangelog'));
}

add_shortcode('rs_my_rewards_log', array('FPRewardSystem', 'viewchangelog_shortcode'));

add_shortcode('rs_my_reward_points', array('FPRewardSystem', 'myrewardpoints_total_shortcode'));


/**
 * Checkout Form Custom CSS Start
 */
add_action('woocommerce_before_checkout_form', array('FPRewardSystem', 'checkout_page_custom_css_reward_system'));
/*
 * Checkout Form Custom CSS Option End
 */


add_action('woocommerce_before_checkout_form', array('FPRewardSystem', 'reward_system_checkout_page_redeeming'));
add_action('wp_head', 'apply_matched_coupons');

$orderstatuslist = get_option('rs_order_status_control');
if (is_array($orderstatuslist)) {
    foreach ($orderstatuslist as $value) {
        add_action('woocommerce_order_status_' . $value, array('FPRewardSystem', 'update_reward_points_to_user'));
    }
}

function rewards_rs_order_status_control() {
    global $woocommerce;
    $orderslugs = array();

    if (function_exists('wc_get_order_statuses')) {
        $orderslugss = str_replace('wc-', '', array_keys(wc_get_order_statuses()));
        foreach ($orderslugss as $value) {
            if (is_array(get_option('rs_order_status_control'))) {
                if (!in_array($value, get_option('rs_order_status_control'))) {
                    $orderslugs[] = $value;
                }
            }
        }
//var_dump($orderslugs);
    } else {
        $taxonomy = 'shop_order_status';
        $orderstatus = '';
        $term_args = array(
            'hide_empty' => false,
            'orderby' => 'date',
        );
        $tax_terms = get_terms($taxonomy, $term_args);
        foreach ($tax_terms as $getterms) {
            if (is_array(get_option('rs_order_status_control'))) {
                if (!in_array($getterms->slug, get_option('rs_order_status_control'))) {
                    $orderslugs[] = $getterms->slug;
                }
            }
        }
    }
    update_option('rs_list_other_status', $orderslugs);
}

add_action('woocommerce_update_options_rewardsystem_statustab', 'rewards_rs_order_status_control', 99);
add_action('init', 'rewards_rs_order_status_control', 9999);
$order_status_control = get_option('rs_list_other_status');
//var_dump($order_status_control);
if (get_option('rs_list_other_status') != '') {
    foreach ($order_status_control as $order_status) {
        $orderstatuslist = get_option('rs_order_status_control');
        if (is_array($orderstatuslist)) {
            foreach ($orderstatuslist as $value) {
                add_action('woocommerce_order_status_' . $value . '_to_' . $order_status, array('FPRewardSystem', 'add_reward_points_to_header'));
            }
        }
    }
}

//var_dump(rewards_rs_order_status_control());
//add_action('admin_head', 'rewards_rs_order_status_control');

function get_attached_points() {
    $getcurrentuserid = get_current_user_id();
//var_dump(get_user_meta($getcurrentuserid, '_update_user_order', true));
    $userlistorder = get_user_meta($getcurrentuserid, '_update_user_order', true);
    foreach ($userlistorder as $eachorder) {
        echo $eachorder . "<br>";
    }
}

function woocommerce_reward_system_redeem_points_in_slider() {
    session_start();

    $myuserid = get_current_user_id();
    global $woocommerce;
    global $point_to_redeem;
    ?>

    <script type="text/javascript" id="rsjsscript">
        jQuery(document).ready(function () {
            jQuery("#master").slider({
                value: '0',
                orientation: "horizontal",
                range: "min",
                max: '<?php echo get_user_meta($myuserid, '_my_reward_points', true); ?>',
                animate: true,
                change: function (event, ui) {
                    //alert(ui.value);
                    var values = ui.value;
                    var dataparam = ({
                        action: 'rewardsystem_redeempoints',
                        redeempoints: values,
                    });
                    jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                            function (response) {
                                var newresponse = response.replace(/\s/g, '');
                                if (newresponse === 'success') {
                                    //jQuery(".woocommerce").load('<?php $woocommerce->cart->get_cart_url(); ?> ' + " .woocommerce");
                                    jQuery('.woocommerce').load('<?php echo $woocommerce->cart->get_cart_url(); ?>' + ' .woocommerce', function (data) {
                                        var scripts = jQuery(data).find('#rsjsscript');
                                        console.log(scripts);
                                        var length = jQuery(data).find('#rsjsscript').length;
                                        for (var i = 0; i < length; i++) {
                                            var scriptincluder = scripts[0];
                                            jQuery('.woocommerce').append(scriptincluder);
                                        }
                                    });
                                }
                            });
                    return false;
                }

            });
        });</script>
    <div id="master" style="width:260px; margin:15px;"></div>
    <?php
}

//if (get_option('rs_redeem_point_apply_option') == '2') {
//    add_action('woocommerce_after_cart_table', 'woocommerce_reward_system_redeem_points_in_slider');
//    add_action('wp_ajax_nopriv_rewardsystem_redeempoints', 'apply_ajax_matched_coupons');
//    add_action('wp_ajax_rewardsystem_redeempoints', 'apply_ajax_matched_coupons');
//    add_action('wp_head', 'newapply_matched_coupons');
//}

function newapply_matched_coupons() {
    global $woocommerce;
    if (isset($_POST['apply_coupon'])) {
        $user_ID = get_current_user_id();
        $coupon_code = get_user_meta($user_ID, 'nickname', true); // Code
        // $woocommerce->cart->remove_coupon(strtolower($coupon_code));
    }
}

function rs_remove_coupon_code_after_place_order($order_id, $order_posted) {
    $order = new WC_Order($order_id);
    $getuserdatabyid = get_user_by('id', $order->user_id);
    $getusernickname = $getuserdatabyid->user_login;
    $maincouponchecker = 'sumo_' . strtolower($getusernickname);

    foreach ($order->get_used_coupons() as $newcoupon) {
        if ($maincouponchecker == $newcoupon) {
            $getcouponid = get_user_meta($order->user_id, 'redeemcouponids', true);
            wp_trash_post($getcouponid);
        }
    }
}

add_action('woocommerce_checkout_update_order_meta', 'rs_remove_coupon_code_after_place_order', 10, 2);

function apply_ajax_matched_coupons() {
    global $woocommerce;
    global $point_to_redeem;
    session_start();
// var_dump($woocommerce->cart->cart_contents_total);
    if ($_POST['redeempoints']) {
        foreach ($woocommerce->cart->applied_coupons as $code) {
            // $woocommerce->cart->remove_coupon(strtolower($code));
        }
//        if (!empty($woocommerce->cart->applied_coupons)) {
//            return false;
//        }
    }
    if (isset($_POST['redeempoints'])) {
        $user_ID = get_current_user_id();
        $coupon_code = get_user_meta($user_ID, 'nickname', true); // Code
//$amount = '10'; // Amount
        $discount_type = 'fixed_cart';
        $coupon = array(
            'post_title' => str_replace(' ', '', strtolower($coupon_code)),
            'post_content' => '',
            'post_status' => 'publish',
            'post_author' => get_current_user_id(),
            'post_type' => 'shop_coupon'
        );
        $newusercouponexists = get_option(get_user_meta($user_ID, 'nickname', true) . "coupon");

        if (!isset($newusercouponexists)) {
            $new_coupon_id = wp_insert_post($coupon);
            update_option(get_user_meta($user_ID, 'nickname', true) . "coupon", $new_coupon_id);
        } else {
            if ('publish' == get_post_status(get_option(get_user_meta($user_ID, 'nickname', true) . "coupon"))) {
                $coupon_args = array(
                    'ID' => get_option(get_user_meta($user_ID, 'nickname', true) . "coupon"),
                    'post_title' => str_replace(' ', '', strtolower($coupon_code)),
                    'post_content' => '',
                    'post_status' => 'publish',
                    'post_author' => get_current_user_id(),
                    'post_type' => 'shop_coupon'
                );
                $new_coupon_id = wp_update_post($coupon_args);
            } else {
                $new_coupon_id = wp_insert_post($coupon);
                update_option(get_user_meta($user_ID, 'nickname', true) . "coupon", $new_coupon_id);
            }
        }
        update_post_meta($new_coupon_id, 'discount_type', $discount_type);
        $point_to_redeem = $_POST['redeempoints'];
        $_SESSION['redeempoints'] = $_POST['redeempoints'];
        $point_control = get_option('rs_redeem_point');
        $point_control_price = get_option('rs_redeem_point_value'); //i.e., 100 Points is equal to $1
        $revised_amount = $point_to_redeem / $point_control;
        $newamount = $revised_amount * $point_control_price;
        $getmaxruleoption = get_option('rs_max_redeem_discount');
        $getfixedmaxoption = get_option('rs_fixed_max_redeem_discount');
        $getpercentmaxoption = get_option('rs_percent_max_redeem_discount');
        $errpercentagemsg = get_option('rs_errmsg_for_max_discount_type');
        $errpercentagemsg1 = str_replace('[percentage]', $getfixedmaxoption, $errpercentagemsg);
        if ($getmaxruleoption == '1') {
            if ($getfixedmaxoption != '') {
                if ($newamount > $getfixedmaxoption) {
                    $newamount = $getfixedmaxoption;
                    wc_add_notice(__($errpercentagemsg1), 'error');
                }
            }
        } else {
            if ($getpercentmaxoption != '') {
                $getpercent = $getpercentmaxoption;
                $gettotalprice = $woocommerce->cart->cart_contents_total;
                $percentageproduct = $getpercent / 100;
                $getpricepercent = $percentageproduct * $gettotalprice;
                $getpointconvert = $getpricepercent * $point_control;
                $getexactpoint = $getpointconvert / $point_control_price;
                $errpercentagemsg = get_option('rs_errmsg_for_max_discount_type');
                $errpercentagemsg1 = str_replace('[percentage]', $getpercent, $errpercentagemsg);
                if ($point_to_redeem > $getexactpoint) {
                    $revised_amount = $getexactpoint / $point_control;
                    $newamount = $revised_amount * $point_control_price;
                    wc_add_notice(__($errpercentagemsg1), 'error');
                }
// if($newamount)
            }
        }

        update_post_meta($new_coupon_id, 'coupon_amount', $newamount);
    }
//$woocommerce->cart->add_fee('Charges delivery', '-20');
    update_post_meta($new_coupon_id, 'individual_use', 'no');
    update_post_meta($new_coupon_id, 'product_ids', '');
    update_post_meta($new_coupon_id, 'exclude_product_ids', '');
    update_post_meta($new_coupon_id, 'usage_limit', '1');
    update_post_meta($new_coupon_id, 'expiry_date', '');
    if (get_option('rs_apply_redeem_before_tax') == '1') {
        update_post_meta($new_coupon_id, 'apply_before_tax', 'yes');
    } else {
        update_post_meta($new_coupon_id, 'apply_before_tax', 'no');
    }
    if (get_option('rs_apply_shipping_tax') == '1') {
        update_post_meta($new_coupon_id, 'free_shipping', 'yes');
    } else {
        update_post_meta($new_coupon_id, 'free_shipping', 'no');
    }

//    if ($woocommerce->cart->has_discount(str_replace(' ', '', strtolower($coupon_code))))
//        return;
//    if (!$woocommerce->cart->has_discount(str_replace(' ', '', strtolower($coupon_code)))) {
//        $woocommerce->cart->add_discount(str_replace(' ', '', strtolower($coupon_code)));
//    }
    echo "success";
    exit();
}

//add_action('wp_head', 'get_attached_points');
//add_action('admin_head', array('FPRewardSystem', 'update_reward_points_to_user'));

function apply_matched_coupons() {
    global $woocommerce;

// var_dump($woocommerce->cart->cart_contents_total);
    if (isset($_POST['apply_coupon'])) {
        $user_ID = get_current_user_id();
        $coupon_code = get_user_meta($user_ID, 'nickname', true); // Code
        // $woocommerce->cart->remove_coupon(str_replace(' ', '', strtolower($coupon_code)));
    }
    if (isset($_POST['rs_apply_coupon_code'])) {
        foreach ($woocommerce->cart->applied_coupons as $code) {
            //  $woocommerce->cart->remove_coupon(str_replace(' ', '', strtolower($code)));
        }
//        if (!empty($woocommerce->cart->applied_coupons)) {
//            return false;
//        }
        if (isset($_POST['rs_apply_coupon_code_field'])) {
            $user_ID = get_current_user_id();

            // $coupon_code = get_user_meta($user_ID, 'nickname', true); // Code
            $getinfousernickname = get_user_by('id', $user_ID);
            $couponcodeuserlogin = $getinfousernickname->user_login;

//$amount = '10'; // Amount
            $discount_type = 'fixed_cart';

            if (!is_array(get_option('rs_select_products_to_enable_redeeming'))) {
                $allowproducts = explode(',', get_option('rs_select_products_to_enable_redeeming'));
            } else {
                $allowproducts = get_option('rs_select_products_to_enable_redeeming');
            }

            if (!is_array(get_option('rs_exclude_products_to_enable_redeeming'))) {
                $excludeproducts = explode(',', get_option('rs_exclude_products_to_enable_redeeming'));
            } else {
                $excludeproducts = get_option('rs_exclude_products_to_enable_redeeming');
            }
            //  $excludeproducts = get_option('rs_exclude_products_to_enable_redeeming');
            $allowcategory = get_option('rs_select_category_to_enable_redeeming');
            $excludecategory = get_option('rs_exclude_category_to_enable_redeeming');
            $coupon = array(
                'post_title' => 'sumo_' . strtolower($couponcodeuserlogin),
                'post_content' => '',
                'post_status' => 'publish',
                'post_author' => get_current_user_id(),
                'post_type' => 'shop_coupon'
            );


            $getuserdataby = get_user_by('id', $user_ID);
            $getloginnickname = $getuserdataby->user_login;


            //var_dump($newcouponexistsuser);
            //if (!isset($newcouponexistsuser)) {
            $oldcouponid = get_user_meta($user_ID, 'redeemcouponids', true);

            //for deletion
            wp_delete_post($oldcouponid, true);

            $getuserdataby = get_user_by('id', $user_ID);
            $getloginnickname = $getuserdataby->user_login;
            $new_coupon_id = wp_insert_post($coupon);
            update_user_meta($user_ID, 'redeemcouponids', $new_coupon_id);
            update_post_meta($new_coupon_id, 'carttotal', $woocommerce->cart->cart_contents_total);
            update_post_meta($new_coupon_id, 'cartcontenttotal', $woocommerce->cart->cart_contents_count);

            //update_option($getloginnickname . "coupon", $new_coupon_id);
            // } else {
            /*  $getuserdataby = get_user_by('id',$user_ID);
              $getloginnickname = $getuserdataby->user_login;
              if('publish'==get_post_status(get_option($getloginnickname . "coupon"))) {
              $coupon_args = array(
              'ID' => get_option($getloginnickname . "coupon"),
              'post_title' => 'sumo_'.strtolower($couponcodeuserlogin),
              'post_content' => '',
              'post_status' => 'publish',
              'post_author' => get_current_user_id(),
              'post_type' => 'shop_coupon'
              );
              $new_coupon_id = wp_update_post($coupon_args);
              } else {
              $getuserdataby = get_user_by('id',$user_ID);
              $getloginnickname = $getuserdataby->user_login;
              $new_coupon_id = wp_insert_post($coupon);
              update_option($getloginnickname . "coupon", $new_coupon_id);
              } */
            // }
            update_post_meta($new_coupon_id, 'discount_type', $discount_type);
            $point_to_redeem = $_POST['rs_apply_coupon_code_field'];
            $point_control = get_option('rs_redeem_point');
            $point_control_price = get_option('rs_redeem_point_value'); //i.e., 100 Points is equal to $1
            $revised_amount = $point_to_redeem / $point_control;
            $newamount = $revised_amount * $point_control_price;
            $getmaxruleoption = get_option('rs_max_redeem_discount');
            $getfixedmaxoption = get_option('rs_fixed_max_redeem_discount');
            $getpercentmaxoption = get_option('rs_percent_max_redeem_discount');
            $errpercentagemsg = get_option('rs_errmsg_for_max_discount_type');
            $errpercentagemsg1 = str_replace('[percentage]', $getfixedmaxoption, $errpercentagemsg);
            if ($getmaxruleoption == '1') {
                if ($getfixedmaxoption != '') {
                    if ($newamount > $getfixedmaxoption) {
                        $newamount = $getfixedmaxoption;
                        update_post_meta($new_coupon_id, 'rsmaximumdiscountcart', 1);
                        wc_add_notice(__($errpercentagemsg1), 'error');
                    }
                }
            } else {
                if ($getpercentmaxoption != '') {
                    $getpercent = $getpercentmaxoption;
                    $gettotalprice = $woocommerce->cart->cart_contents_total;
                    $percentageproduct = $getpercent / 100;
                    $getpricepercent = $percentageproduct * $gettotalprice;
                    $getpointconvert = $getpricepercent * $point_control;
                    $getexactpoint = $getpointconvert / $point_control_price;
                    $errpercentagemsg = get_option('rs_errmsg_for_max_discount_type');
                    $errpercentagemsg1 = str_replace('[percentage]', $getpercent, $errpercentagemsg);
                    if ($point_to_redeem > $getexactpoint) {
                        $revised_amount = $getexactpoint / $point_control;
                        $newamount = $revised_amount * $point_control_price;
                        update_post_meta($new_coupon_id, 'rsmaximumdiscountcart', 1);
                        wc_add_notice(__($errpercentagemsg1), 'error');
                    }
// if($newamount)
                }
            }

            update_post_meta($new_coupon_id, 'coupon_amount', $newamount);
        }
//$woocommerce->cart->add_fee('Charges delivery', '-20');
        update_post_meta($new_coupon_id, 'individual_use', 'no');

        //Redeeming only for Selected Products option start
        $enableproductredeeming = get_option('rs_enable_redeem_for_selected_products');
//        var_dump($enableproductredeeming);
        if ($enableproductredeeming == 'yes') {
            update_post_meta($new_coupon_id, 'product_ids', implode(',', array_filter(array_map('intval', $allowproducts))));
        }
        $excludeproductredeeming = get_option('rs_exclude_products_for_redeeming');
        if ($excludeproductredeeming == 'yes') {
            update_post_meta($new_coupon_id, 'exclude_product_ids', implode(',', array_filter(array_map('intval', $excludeproducts))));
        }
        $enablecategoryredeeming = get_option('rs_enable_redeem_for_selected_category');
        if ($enablecategoryredeeming == 'yes') {
            update_post_meta($new_coupon_id, 'product_categories', implode(',', array_filter(array_map('intval', $allowcategory))));
        }
        $excludecategoryredeeming = get_option('rs_exclude_category_for_redeeming');
        if ($excludecategoryredeeming == 'yes') {
            update_post_meta($new_coupon_id, 'exclude_product_categories', implode(',', array_filter(array_map('intval', $excludecategory))));
        }


        //Redeeming only for Selected Products option End



        update_post_meta($new_coupon_id, 'usage_limit', '1');
        update_post_meta($new_coupon_id, 'expiry_date', '');
        if (get_option('rs_apply_redeem_before_tax') == '1') {
            update_post_meta($new_coupon_id, 'apply_before_tax', 'yes');
        } else {
            update_post_meta($new_coupon_id, 'apply_before_tax', 'no');
        }
        if (get_option('rs_apply_shipping_tax') == '1') {
            update_post_meta($new_coupon_id, 'free_shipping', 'yes');
        } else {
            update_post_meta($new_coupon_id, 'free_shipping', 'no');
        }

//var_dump($woocommerce->cart->get_applied_coupons());

        if ($woocommerce->cart->has_discount('sumo_' . strtolower($couponcodeuserlogin)))
            return;
        if (!$woocommerce->cart->has_discount('sumo_' . strtolower($couponcodeuserlogin))) {
            $woocommerce->cart->add_discount('sumo_' . strtolower($couponcodeuserlogin));
        }
    }
}

function change_coupon_label($link, $coupon) {
//    $banned_user_list = get_option('rs_banned-users_list');
//    if (!in_array(get_current_user_id(), (array) $banned_user_list)) {
//        $getarrayofuserdata = get_userdata(get_current_user_id());
//        $banninguserrole = get_option('rs_banning_user_role');
//        if (!in_array(isset($getarrayofuserdata->roles[0]) ? $getarrayofuserdata->roles[0] : '0', (array) $banninguserrole)) {
    // update_option('testcoupon', $coupon);
    $userid = get_current_user_id();
    $banning_type = FPRewardSystem::check_banning_type($userid);
    if ($banning_type != 'earningonly' && $banning_type != 'both') {
        $couponcode = $coupon->code;
        if (is_string($coupon))

        // $vaers = var_dump($coupon);
            $coupon = new WC_Coupon($coupon);
        $user_ID = get_current_user_id();

        // $coupon_code = get_user_meta($user_ID, 'nickname', true); // Code
        $getinfousernickname = get_user_by('id', $user_ID);
        $couponcodeuserlogin = $getinfousernickname->user_login;
        if (strtolower($couponcode) == 'sumo_' . strtolower($couponcodeuserlogin)) {
            $newcoupon = get_option('rs_coupon_label_message');
            $link = ' ' . $newcoupon;
        }
//        }
//    }
    }
    return $link;
}

function get_coupon_code_data($msg, $msg_code) {
//var_dump($msg_code);
    switch ($msg_code) {
        case 200 :
            if (isset($_POST['rs_apply_coupon_code'])) {
                $msg = __(get_option('rs_success_coupon_message'), 'rewardsystem');
                ?>
                <?php
                if (get_option('rs_redeem_field_type_option') == '2') {
                    ?>
                    <script type="text/javascript">
                        jQuery(document).ready(function () {
                            jQuery("#mainsubmi").parent().hide();
                        });</script>
                    <?php
                }
            }
            //wp_safe_redirect(get_permalink());
            break;
        default:
            $msg = '';
            break;
    }
    return $msg;
}

function rs_add_script_admin() {
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('#rs_fixed_max_redeem_discount').parent().parent().hide();
            jQuery('#rs_percent_max_redeem_discount').parent().parent().hide();
            jQuery('#rs_referral_cookies_expiry_in_min').parent().parent().hide();
            jQuery('#rs_referral_cookies_expiry_in_hours').parent().parent().hide();
            jQuery('#rs_referral_cookies_expiry_in_days').parent().parent().hide();
            //            jQuery('#rs_category_points').parent().parent().hide();
            //            jQuery('#rs_category_percent').parent().parent().hide();
            //            jQuery('#enable_rs_rule').change(function() {
            //            });

            if ((jQuery('#rs_max_redeem_discount').val()) === '1') {
                jQuery('#rs_fixed_max_redeem_discount').parent().parent().show();
                jQuery('#rs_percent_max_redeem_discount').parent().parent().hide();
            } else {
                jQuery('#rs_fixed_max_redeem_discount').parent().parent().hide();
                jQuery('#rs_percent_max_redeem_discount').parent().parent().show();
            }

            if ((jQuery('#rs_referral_cookies_expiry').val()) === '1') {
                jQuery('#rs_referral_cookies_expiry_in_min').parent().parent().show();
                jQuery('#rs_referral_cookies_expiry_in_hours').parent().parent().hide();
                jQuery('#rs_referral_cookies_expiry_in_days').parent().parent().hide();
            } else if ((jQuery('#rs_referral_cookies_expiry').val()) === '2') {
                jQuery('#rs_referral_cookies_expiry_in_min').parent().parent().hide();
                jQuery('#rs_referral_cookies_expiry_in_hours').parent().parent().show();
                jQuery('#rs_referral_cookies_expiry_in_days').parent().parent().hide();
            } else {
                jQuery('#rs_referral_cookies_expiry_in_min').parent().parent().hide();
                jQuery('#rs_referral_cookies_expiry_in_hours').parent().parent().hide();
                jQuery('#rs_referral_cookies_expiry_in_days').parent().parent().show();
            }
            jQuery('#rs_max_redeem_discount').change(function () {
                if ((jQuery(this).val()) === '1') {
                    jQuery('#rs_fixed_max_redeem_discount').parent().parent().show();
                    jQuery('#rs_percent_max_redeem_discount').parent().parent().hide();
                } else {
                    jQuery('#rs_fixed_max_redeem_discount').parent().parent().hide();
                    jQuery('#rs_percent_max_redeem_discount').parent().parent().show();
                }
                return false;
            });
            jQuery('#rs_referral_cookies_expiry').change(function () {
                if ((jQuery(this).val()) === '1') {
                    jQuery('#rs_referral_cookies_expiry_in_min').parent().parent().show();
                    jQuery('#rs_referral_cookies_expiry_in_hours').parent().parent().hide();
                    jQuery('#rs_referral_cookies_expiry_in_days').parent().parent().hide();
                } else if ((jQuery(this).val()) === '2') {
                    jQuery('#rs_referral_cookies_expiry_in_min').parent().parent().hide();
                    jQuery('#rs_referral_cookies_expiry_in_hours').parent().parent().show();
                    jQuery('#rs_referral_cookies_expiry_in_days').parent().parent().hide();
                } else {
                    jQuery('#rs_referral_cookies_expiry_in_min').parent().parent().hide();
                    jQuery('#rs_referral_cookies_expiry_in_hours').parent().parent().hide();
                    jQuery('#rs_referral_cookies_expiry_in_days').parent().parent().show();
                }

                return false;
            });
            if (jQuery('#rs_global_reward_type').val() === '1') {
                jQuery('#rs_global_reward_points').parent().parent().show();
                jQuery('#rs_global_reward_percent').parent().parent().hide();
            } else {
                jQuery('#rs_global_reward_points').parent().parent().hide();
                jQuery('#rs_global_reward_percent').parent().parent().show();
            }

            if (jQuery('#rs_local_reward_type').val() === '1') {
                jQuery('#rs_local_reward_points').parent().parent().show();
                jQuery('#rs_local_reward_percent').parent().parent().hide();
            } else {
                jQuery('#rs_local_reward_points').parent().parent().hide();
                jQuery('#rs_local_reward_percent').parent().parent().show();
            }


            if (jQuery('#rs_local_referral_reward_type').val() === '1') {
                jQuery('#rs_local_referral_reward_point').parent().parent().show();
                jQuery('#rs_local_referral_reward_percent').parent().parent().hide();
            } else {
                jQuery('#rs_local_referral_reward_point').parent().parent().hide();
                jQuery('#rs_local_referral_reward_percent').parent().parent().show();
            }

            if (jQuery('#rs_local_reward_type_for_facebook').val() === '1') {
                jQuery('#rs_local_reward_points_facebook').parent().parent().show();
                jQuery('#rs_local_reward_percent_facebook').parent().parent().hide();
            } else {
                jQuery('#rs_local_reward_points_facebook').parent().parent().hide();
                jQuery('#rs_local_reward_percent_facebook').parent().parent().show();
            }

            jQuery('#rs_local_reward_type_for_facebook').change(function () {
                if ((jQuery(this).val()) === '1') {
                    jQuery('#rs_local_reward_points_facebook').parent().parent().show();
                    jQuery('#rs_local_reward_percent_facebook').parent().parent().hide();
                } else {
                    jQuery('#rs_local_reward_points_facebook').parent().parent().hide();
                    jQuery('#rs_local_reward_percent_facebook').parent().parent().show();
                }
            });
            if (jQuery('#rs_local_reward_type_for_twitter').val() === '1') {
                jQuery('#rs_local_reward_points_twitter').parent().parent().show();
                jQuery('#rs_local_reward_percent_twitter').parent().parent().hide();
            } else {
                jQuery('#rs_local_reward_points_twitter').parent().parent().hide();
                jQuery('#rs_local_reward_percent_twitter').parent().parent().show();
            }

            jQuery('#rs_local_reward_type_for_twitter').change(function () {
                if ((jQuery(this).val()) === '1') {
                    jQuery('#rs_local_reward_points_twitter').parent().parent().show();
                    jQuery('#rs_local_reward_percent_twitter').parent().parent().hide();
                } else {
                    jQuery('#rs_local_reward_points_twitter').parent().parent().hide();
                    jQuery('#rs_local_reward_percent_twitter').parent().parent().show();
                }
            });
            if (jQuery('#rs_local_reward_type_for_google').val() === '1') {
                jQuery('#rs_local_reward_points_google').parent().parent().show();
                jQuery('#rs_local_reward_percent_google').parent().parent().hide();
            } else {
                jQuery('#rs_local_reward_points_google').parent().parent().hide();
                jQuery('#rs_local_reward_percent_google').parent().parent().show();
            }

            jQuery('#rs_local_reward_type_for_google').change(function () {
                if ((jQuery(this).val()) === '1') {
                    jQuery('#rs_local_reward_points_google').parent().parent().show();
                    jQuery('#rs_local_reward_percent_google').parent().parent().hide();
                } else {
                    jQuery('#rs_local_reward_points_google').parent().parent().hide();
                    jQuery('#rs_local_reward_percent_google').parent().parent().show();
                }
            });
            jQuery('#rs_local_reward_type').change(function () {
                if ((jQuery(this).val()) === '1') {
                    jQuery('#rs_local_reward_points').parent().parent().show();
                    jQuery('#rs_local_reward_percent').parent().parent().hide();
                } else {
                    jQuery('#rs_local_reward_points').parent().parent().hide();
                    jQuery('#rs_local_reward_percent').parent().parent().show();
                }
            });
            jQuery('#rs_local_referral_reward_type').change(function () {
                if ((jQuery(this).val()) === '1') {
                    jQuery('#rs_local_referral_reward_point').parent().parent().show();
                    jQuery('#rs_local_referral_reward_percent').parent().parent().hide();
                } else {
                    jQuery('#rs_local_referral_reward_point').parent().parent().hide();
                    jQuery('#rs_local_referral_reward_percent').parent().parent().show();
                }
            });
            jQuery('#rs_global_reward_type').change(function () {
                if ((jQuery(this).val()) === '1') {
                    jQuery('#rs_global_reward_points').parent().parent().show();
                    jQuery('#rs_global_reward_percent').parent().parent().hide();
                } else {
                    jQuery('#rs_global_reward_points').parent().parent().hide();
                    jQuery('#rs_global_reward_percent').parent().parent().show();
                }
            });
            if ((jQuery('#rs_global_referral_reward_type').val()) === '1') {
                jQuery('#rs_global_referral_reward_point').parent().parent().show();
                jQuery('#rs_global_referral_reward_percent').parent().parent().hide();
            } else {
                jQuery('#rs_global_referral_reward_point').parent().parent().hide();
                jQuery('#rs_global_referral_reward_percent').parent().parent().show();
            }
            jQuery('#rs_global_referral_reward_type').change(function () {
                if ((jQuery(this).val()) === '1') {
                    jQuery('#rs_global_referral_reward_point').parent().parent().show();
                    jQuery('#rs_global_referral_reward_percent').parent().parent().hide();
                } else {
                    jQuery('#rs_global_referral_reward_point').parent().parent().hide();
                    jQuery('#rs_global_referral_reward_percent').parent().parent().show();
                }
            });
        });</script>
    <?php
}

//register_activation_hook(__FILE__, array('FPRewardSystemEmailTemplatesTab', 'rs_create_table'));
add_action('admin_head', 'rs_add_script_admin');
add_action('woocommerce_coupon_message', 'get_coupon_code_data', 150, 2);

add_action('wp_head', array('FPRewardSystem', 'reward_system_variable_jquery'));

add_action('wp_ajax_nopriv_getvariationid', array('FPRewardSystem', 'wp_product_variation'));
add_action('wp_ajax_getvariationid', array('FPRewardSystem', 'wp_product_variation'));

add_filter('woocommerce_cart_totals_coupon_label', 'change_coupon_label', 1, 2);
//Hook to get returned message on add to cart
//add_action('woocommerce_before_cart', array('FPRewardSystem', 'add_message_box_on_above_cart'));
// Hook for the Cart Page with custom input field to redeem the points
//if (get_option('rs_redeem_point_apply_option') == '1') {
//add_action('woocommerce_after_cart_table', array('FPRewardSystem', 'reward_system_add_message_after_cart_table'));

if (get_option('rs_show_hide_redeem_field') == '1') {
    if (get_option('rs_reward_point_troubleshoot_after_cart') == '1') {
        add_action('woocommerce_after_cart_table', array('FPRewardSystem', 'reward_system_add_message_after_cart_table'));
    } else {
        add_action('woocommerce_cart_coupon', array('FPRewardSystem', 'reward_system_add_message_after_cart_table'));
    }
}



//}
//add_action('woocommerce_cart_collaterals', array('FPRewardSystem', 'reward_system_add_message_after_cart_table'));
//Hook to register the general options in a admin settings
add_action('woocommerce_product_options_general_product_data', array('FPRewardSystem', 'reward_system_admin_option_simple_product'), 1);

add_action('woocommerce_product_options_general_product_data', array('FPRewardSystem', 'reward_system_social_input_field'));


// Hook to Save the Metabox value using woocommerce hook (woocommerce_process_product_meta)
add_action('woocommerce_process_product_meta', array('FPRewardSystem', 'save_rewardpoint_meta'));

// Reward System Single Product pricing
add_filter('woocommerce_get_price_html', array('FPRewardSystem', 'reward_system_after_single_product_summary'), 99, 2);


add_action('woocommerce_before_single_product', array('FPRewardSystem', 'reward_system_variable_product'));

// Reward System Fragment Add to Cart
//add_filter('add_to_cart_fragments', array('FPRewardSystem', 'reward_system_fragment_for_add_to_cart'));
// Show the Reward Point Details before the Cart Table
//if (get_option('rs_reward_point_troubleshoot_before_cart') == '1') {
//    add_action('woocommerce_before_cart', array('FPRewardSystem', 'update_reward_points'));
//} else {
//    add_action('woocommerce_before_cart_table', array('FPRewardSystem', 'update_reward_points'));
//}
//add_action('woocommerce_before_checkout_form', array('FPRewardSystem', 'update_reward_points'));

add_action('admin_head', array('FPRewardSystem', 'add_script_to_head'));


/* * ***************************************************************************************
 * ***************Hooks for Admin Settings in a WooCommerce Menu Page***********************
 * *****************************************************************************************
 */
// Register a New Tab in a WooCommerce Reward System Settings
add_filter('woocommerce_rs_settings_tabs_array', array('FPRewardSystem', 'reward_system_tab_settings'), 100);

// Register Reward System Menu in a Admin Settings of WooCommerce
add_action('admin_menu', array('FPRewardSystem', 'register_reward_system_sub_menu'));

// Reset Reward System Admin Settings
add_action('init', array('FPRewardSystem', 'reset_reward_system_admin_settings'));

//add_action('init', array('FPRewardSystem', 'reward_system_load_default_enqueues'));
if (isset($_GET['page'])) {
    if (($_GET['page'] == 'rewardsystem_callback')) {
        add_filter('woocommerce_screen_ids', array('FPRewardSystem', 'reward_system_load_default_enqueues'), 1);
    }
}

/* * ***************************************************************************************
 * ***************Essential Stuff to Register Tabs in the WooCommerce***********************
 * *****************************************************************************************
 */

// call the woocommerce_update_options_{slugname} to update the reward system
add_action('woocommerce_update_options_rewardsystem', array('FPRewardSystem', 'reward_system_update_settings'));

// call the init function to update the default settings on page load
add_action('init', array('FPRewardSystem', 'reward_system_default_settings'));

// Call to register the admin settings in the Reward System Submenu with general Settings tab
add_action('woocommerce_rs_settings_tabs_rewardsystem', array('FPRewardSystem', 'reward_system_register_admin_settings'));

// Prevent Header Already Sent Problem and Unexpected Problem Facing
add_action('init', array('FPRewardSystem', 'reward_system_get_output_buffer'));
if (get_option('rs_reward_point_troubleshoot_before_cart') == '1') {
    add_action('woocommerce_before_cart', array('FPRewardSystem', 'reward_points_in_top_of_content'));
} else {
    add_action('woocommerce_before_cart_table', array('FPRewardSystem', 'reward_points_in_top_of_content'));
}
add_action('woocommerce_before_checkout_form', array('FPRewardSystem', 'reward_points_in_top_of_content'));


if (get_option('rs_reward_point_troubleshoot_before_cart') == '1') {
    add_action('woocommerce_before_cart', array('FPRewardSystem', 'rewardmessage_in_cart_page'));
} else {
    add_action('woocommerce_before_cart_table', array('FPRewardSystem', 'rewardmessage_in_cart_page'));
}
add_action('woocommerce_before_checkout_form', array('FPRewardSystem', 'rewardmessage_in_checkout_page'));


if (get_option('rs_reward_point_troubleshoot_before_cart') == '1') {
    add_action('woocommerce_before_cart', array('FPRewardSystem', 'show_message_for_guest_cart_page'));
} else {
    add_action('woocommerce_before_cart_table', array('FPRewardSystem', 'show_message_for_guest_cart_page'));
}
add_action('woocommerce_before_checkout_form', array('FPRewardSystem', 'show_message_for_guest_checkout_page'));

if (get_option('rs_reward_point_troubleshoot_before_cart') == '1') {
    add_action('woocommerce_before_cart', array('FPRewardSystem', 'totalrewardpoints_cart_page'));
} else {
    add_action('woocommerce_before_cart_table', array('FPRewardSystem', 'totalrewardpoints_cart_page'));
}
add_action('woocommerce_before_checkout_form', array('FPRewardSystem', 'totalrewardpoints_checkout_page'));

if (get_option('rs_reward_point_troubleshoot_before_cart') == '1') {
    add_action('woocommerce_before_cart', array('FPRewardSystem', 'your_current_points_cart_page'));
} else {
    add_action('woocommerce_before_cart_table', array('FPRewardSystem', 'your_current_points_cart_page'));
}
add_action('woocommerce_before_checkout_form', array('FPRewardSystem', 'your_current_points_checkout_page'));
if (get_option('rs_reward_point_troubleshoot_before_cart') == '1') {
    add_action('woocommerce_before_cart', array('FPRewardSystem', 'add_redeem_header_message_cart_page'));
} else {
    add_action('woocommerce_before_cart_table', array('FPRewardSystem', 'add_redeem_header_message_cart_page'));
}
add_action('woocommerce_before_checkout_form', array('FPRewardSystem', 'add_redeem_header_message_checkout_page'));

add_action('wp_head', array('FPRewardSystem', 'validation_in_my_cart'));

add_shortcode('titleofproduct', array('FPRewardSystem', 'add_shortcode_cart_id'));
add_shortcode('rspoint', array('FPRewardSystem', 'get_each_product_price'));
add_shortcode('carteachvalue', array('FPRewardSystem', 'cart_each_product_points_value'));
add_shortcode('totalrewards', array("FPRewardSystem", 'getshortcodetotal_rewards'));
add_shortcode('totalrewardsvalue', array('FPRewardSystem', 'getvalueshortcodetotal_rewards'));
add_shortcode('userpoints', array('FPRewardSystem', 'add_shortcode_for_user_points'));
add_shortcode('userpoints_value', array('FPRewardSystem', 'add_shortcode_for_user_points_value'));
add_shortcode('redeempoints', array('FPRewardSystem', 'add_redeem_point'));
add_shortcode('redeemeduserpoints', array('FPRewardSystem', 'redeem_points_user_control'));


add_shortcode('rsminimumpoints', array('FPRewardSystem', 'get_minimum_redeeming_points_value'));


add_action('product_cat_add_form_fields', array('FPRewardSystem', 'add_category_field'));
add_action('product_cat_edit_form_fields', array('FPRewardSystem', 'edit_category_field'), 10, 2);
add_action('created_term', array('FPRewardSystem', 'save_category_fields'), 10, 3);
add_action('edit_term', array('FPRewardSystem', 'save_category_fields'), 10, 3);

add_shortcode('variationrewardpoints', array('FPRewardSystem', 'add_variation_shortcode_div'));
add_shortcode('variationpointsvalue', array('FPRewardSystem', 'add_variation_point_values_shortcode'));

add_action('plugins_loaded', array('FPRewardSystem', 'rs_translate_file'));

add_action('admin_head', array('FPRewardSystem', 'add_chosen_to_general_tab'));
//add_action('admin_head',array('FPRewardSystem','ban_users_from_reward_points'));
//add_action('delete_user', array('FPRewardSystem', 'delete_referral_registered_people'));
//add_action('wp_head', array('FPRewardSystem', 'get_term_meta_info'));
//add_action('wp_head', array('FPRewardSystem', 'alert_on_user_list'));

/* * ***************************************************************************************
 * ***************Include the Class files in a main file of woocommerce*********************
 * *****************************************************************************************
 */

// WPML Support for mail
include_once 'inc/class_wpml_support.php';

include_once 'inc/class_points_rule_reward_system.php';
// Adding User Role Reward Points
include_once 'inc/class_user_role_reward_points.php';
include_once 'inc/class_user_points_reward_system.php';
include_once 'inc/class_messages_reward_system.php';
include_once 'inc/class_cart_reward_system.php';
include_once 'inc/class_checkout_reward_system.php';
include_once 'inc/class_points_log_reward_system.php';
//include_once 'inc/class_info_display_reward_system.php';
include_once 'inc/class_my_account_reward_system.php';
include_once 'inc/class_referral_reward_system.php';
include_once 'inc/class_add_remove_reward_system.php';
include_once 'inc/class_updates_reward_system.php';
include_once 'inc/class_status_tab_reward_system.php';
include_once 'inc/class_refer_a_friend_reward_system.php';
include_once 'inc/class_social_reward_system.php';
include_once 'inc/class_email_templates_reward_system.php';
include_once 'inc/class_mail_notification_reward_system.php';
include_once 'inc/class_sms_notificattions_rewards.php';
include_once 'inc/class_order_tab_reward_system.php';
include_once 'inc/class_points_vouchers_reward_system.php';
include_once 'inc/class_import_export_reward_system.php';
include_once 'inc/class_reward_points_encash.php';
include_once 'inc/class_reward_points_encash_application.php';
include_once 'inc/class_reward_points_coupon.php';
include_once 'inc/class_manual_linking_referer_referral.php';
include_once 'inc/wc_class_encashing_wplist.php';
include_once 'inc/class_points_report.php';
include_once 'inc/class_referral_reward_log.php';
include_once 'inc/class_reset_tab_reward_system.php';
include_once 'inc/class_troubleshoot_reward_system.php';
include_once 'inc/class_localization_reward_system.php';
include_once 'inc/class_rewardgateway.php';
include_once 'inc/class_cart_removal_trigger.php';
include_once 'inc/rs_referral_log_count.php';
include_once 'inc/class_free_product_main_function.php';
// Adding Compatibility For Dynamic Pricing
include_once 'inc/rs_price_rule_checker_for_variant.php';
//adding woocommerce backward compatibility for settings
include_once 'inc/wc_settings_backward_compatibility.php';

// Adding Compatibility for Booking Compatibility
include_once 'inc/wc_booking_compatibility.php';

// Adding Buying Reward Points
include_once 'inc/class_buying_reward_points.php';




register_activation_hook(__FILE__, array('FPRewardSystem', 'rs_create_table'));
register_activation_hook(__FILE__, array('FPRewardSystemMailTab', 'rs_cron_job_setting'));

add_filter('cron_schedules', array('FPRewardSystemMailTab', 'rs_add_x_hourly'));



add_action('rscronjob', array('FPRewardSystemMailTab', 'main_function_for_mail_sending'));

function add_button_to_rewardsystem() {
    ?>
    <tr valign="top">
        <th class="titledesc" scope="row">
            <label for="rs_sumo_reward_button"><?php _e('', 'rewardsystem'); ?></label>
        </th>
        <td class="forminp forminp-select">
            <input type="submit" class="rs_sumo_reward_button button-primary" value="Save and Update"/>
            <img class="gif_rs_sumo_reward_button" src="<?php echo WP_PLUGIN_URL; ?>/rewardsystem/img/update.gif" style="width:32px;height:32px;position:absolute"/>
     <!--    <input type="submit" class="rs_sumo_undo_reward button-secondary" value="Disable Reward Points for All Existing Products"/> -->
            <div class='rs_sumo_rewards' style='margin-bottom:10px; margin-top:10px; color:green;'></div>
        </td>
    </tr>
    <?php
}

function add_additional_button_to_rewardsystem() {
    ?>
    <tr valign="top">
        <th class="titledesc" scope="row">
            <label for="rs_sumo_reward_button_social"><?php _e('', 'rewardsystem'); ?></label>
        </th>
        <td class="forminp forminp-select">
            <input type="submit" class="rs_sumo_reward_button_social button-primary" value="Save and Update"/>
            <img class="gif_rs_sumo_reward_button_social" src="<?php echo WP_PLUGIN_URL; ?>/rewardsystem/img/update.gif" style="width:32px;height:32px;position:absolute"/>
        <!--    <input type="submit" class="rs_sumo_undo_reward button-secondary" value="Disable Reward Points for All Existing Products"/> -->
            <div class='rs_sumo_rewards_social' style='margin-bottom:10px; margin-top:10px; color:green;'></div>
        </td>
    </tr>
    <?php
}

add_action('woocommerce_admin_field_button_social', 'add_additional_button_to_rewardsystem');

add_action('woocommerce_admin_field_button', 'add_button_to_rewardsystem');

function add_previous_order_button_to_rewardsystem() {
    ?>
    <tr valign="top">
        <th class="titledesc" scope="row">
            <label for="rs_sumo_rewards_for_previous_order"><?php _e('Apply Reward Points to Previous Orders', 'rewardsystem'); ?></label>
        </th>
        <td class="forminp forminp-select">
            <input type="submit" class="rs_sumo_rewards_for_previous_order button-primary" value="Apply Points for Previous Orders"/>
            <div class="rs_sumo_rewards_previous_order" style="margin-bottom:10px;margin-top:10px; color:green;"></div>
        </td>
    </tr>
    <?php
}

add_action('woocommerce_admin_field_previous_order_button', 'add_previous_order_button_to_rewardsystem');

function rs_add_from_to_date_picker() {
    ?>
    <script type="text/javascript">
        jQuery(function () {
            jQuery("#rs_from_date").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                dateFormat: 'yy-mm-dd',
                numberOfMonths: 1,
                onClose: function (selectedDate) {
                    jQuery("#to").datepicker("option", "minDate", selectedDate);
                }
            });
            jQuery('#rs_from_date').datepicker('setDate', '-1');
            jQuery("#rs_to_date").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                dateFormat: 'yy-mm-dd',
                numberOfMonths: 1,
                onClose: function (selectedDate) {
                    jQuery("#from").datepicker("option", "maxDate", selectedDate);
                }

            });
            jQuery("#rs_to_date").datepicker('setDate', new Date());
        });</script>
    <tr valign="top">
        <th class="titledesc" scope="row">
            <label for="rs_sumo_rewards_for_selecting_particular_date"><?php _e('Select From Specific Date', 'rewardsystem'); ?></label>
        </th>
        <td class="forminp forminp-select">
            From <input type="text" id="rs_from_date" value=""/> To <input type="text" id="rs_to_date" value=""/>
        </td>
    </tr>
    <?php
}

add_action('woocommerce_admin_field_previous_order_button_range', 'rs_add_from_to_date_picker');

function rs_send_ajax_points_to_previous_orders() {
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('.rs_sumo_rewards_for_previous_order').click(function () {
                jQuery(this).attr('data-clicked', '1');
                var dataclicked = jQuery(this).attr('data-clicked');
                var fromdate = jQuery('#rs_from_date').val();
                var todate = jQuery('#rs_to_date').val();
                if (jQuery('#rs_sumo_select_order_range') === '1') {
                    var dataparam = ({
                        action: 'previousorderpoints',
                        proceedanyway: dataclicked,
                        //                    fromdate: fromdate,
                        //                    todate: todate,
                    });
                } else {
                    var dataparam = ({
                        action: 'previousorderpoints',
                        proceedanyway: dataclicked,
                        fromdate: fromdate,
                        todate: todate,
                    });
                }
                jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                        function (response) {
                            var newresponse = response.replace(/\s/g, '');
                            if (newresponse === 'success') {
                                jQuery('.rs_sumo_rewards_previous_order').fadeIn();
                                jQuery('.rs_sumo_rewards_previous_order').html('Points Successfully Added to Previous Order');
                                jQuery('.rs_sumo_rewards_previous_order').fadeOut(5000);
                                return false;
                            }
                        });
                return false;
            });
        });</script>
    <?php
}

add_action('admin_head', 'rs_send_ajax_points_to_previous_orders');

function rs_process_ajax_points_to_previous_order() {
    if (isset($_POST['proceedanyway'])) {
        if ($_POST['proceedanyway'] == '1') {
            foreach (get_posts('post_type=shop_order&numberposts=-1&post_status=completed') as $orders) {
                $rewardpoints = array('0');
                $order = new WC_Order($orders->ID);
                //var_dump($orders->ID);
                //if ($order->status == 'completed') {
//$modified_date = $order->modified_date;
                $modified_date = get_the_time('Y-m-d', $orders->ID);
                if (isset($_POST['fromdate']) && ($_POST['todate'])) {
                    if (($_POST['fromdate'] <= $modified_date) && $modified_date <= $_POST['todate']) {
// echo $modified_date;
//echo $orders->ID;
                        if (get_post_meta($orders->ID, '_sumo_points_awarded', true) != 'yes') {
                            include_once('inc/ajax_main_function.php');
                            add_post_meta($orders->ID, '_sumo_points_awarded', 'yes');
                        }
                    }
                } else {
                    if (get_post_meta($orders->ID, '_sumo_points_awarded', true) != 'yes') {
                        include_once('inc/ajax_main_function.php');
                        add_post_meta($orders->ID, '_sumo_points_awarded', 'yes');
                    }
                }
                //}
            }
            echo "success";
        }
    }
    exit();
}

add_action('wp_ajax_nopriv_previousorderpoints', 'rs_process_ajax_points_to_previous_order');
add_action('wp_ajax_previousorderpoints', 'rs_process_ajax_points_to_previous_order');

function add_upload_button_to_rewardsystem() {
    ?>
    <tr valign="top">
        <th class="titledesc" scope="row">
            <label for="rs_image_url_upload"><?php _e('Upload your Own Gift Icon', 'rewardsystem'); ?></label>
        </th>
        <td class="forminp forminp-select">
            <input type="text" id="rs_image_url_upload" name="rs_image_url_upload" value="<?php echo get_option('rs_image_url_upload'); ?>"/>
            <input type="submit" id="rs_image_upload_button" name="rs_image_upload_button" value="Upload Image"/>
        </td>
    </tr>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            var rs_custom_uploader;
            jQuery('#rs_image_upload_button').click(function (e) {
                e.preventDefault();
                if (rs_custom_uploader) {
                    rs_custom_uploader.open();
                    return;
                }
                rs_custom_uploader = wp.media.frames.file_frame = wp.media({
                    title: 'Choose Image',
                    button: {text: 'Choose Image'
                    },
                    multiple: false
                });
                //When a file is selected, grab the URL and set it as the text field's value
                rs_custom_uploader.on('select', function () {
                    attachment = rs_custom_uploader.state().get('selection').first().toJSON();
                    jQuery('#rs_image_url_upload').val(attachment.url);
                });
                //Open the uploader dialog
                rs_custom_uploader.open();
            });
        });</script>
    <?php
}

add_action('woocommerce_admin_field_uploader', 'add_upload_button_to_rewardsystem');


add_action('wp_ajax_rssplitajaxoptimization', 'process_chunk_ajax_request_in_rewardsystem');

function process_chunk_ajax_request_in_rewardsystem() {
    //var_dump($_POST['ids']);
    if (isset($_POST['ids'])) {
        $products = $_POST['ids'];
        foreach ($products as $product) {
            $checkproduct = get_product($product);
            if ($checkproduct->is_type('simple') || ($checkproduct->is_type('subscription'))) {
                if ($_POST['enabledisablereward'] == '1') {
                    update_post_meta($product, '_rewardsystemcheckboxvalue', 'yes');
                } else {
                    update_post_meta($product, '_rewardsystemcheckboxvalue', 'no');
                }
                update_post_meta($product, '_rewardsystem_options', $_POST['rewardtype']);
                update_post_meta($product, '_rewardsystempoints', $_POST['rewardpoints']);
                update_post_meta($product, '_rewardsystempercent', $_POST['rewardpercent']);
                update_post_meta($product, '_referral_rewardsystem_options', $_POST['referralrewardtype']);
                update_post_meta($product, '_referralrewardsystempoints', $_POST['referralrewardpoint']);
                update_post_meta($product, '_referralrewardsystempercent', $_POST['referralrewardpercent']);
            } else {
                if ($checkproduct->is_type('variable') || ($checkproduct->is_type('variable-subscription'))) {
                    if (is_array($checkproduct->get_available_variations())) {
                        foreach ($checkproduct->get_available_variations() as $getvariation) {
                            update_post_meta($getvariation['variation_id'], '_enable_reward_points', $_POST['enabledisablereward']);
                            update_post_meta($getvariation['variation_id'], '_select_reward_rule', $_POST['rewardtype']);
                            update_post_meta($getvariation['variation_id'], '_reward_points', $_POST['rewardpoints']);
                            update_post_meta($getvariation['variation_id'], '_reward_percent', $_POST['rewardpercent']);
                            update_post_meta($getvariation['variation_id'], '_select_referral_reward_rule', $_POST['referralrewardtype']);
                            update_post_meta($getvariation['variation_id'], '_referral_reward_points', $_POST['referralrewardpoint']);
                            update_post_meta($getvariation['variation_id'], '_referral_reward_percent', $_POST['referralrewardpercent']);
                        }
                    }
                }
            }
        }
    }

    exit();
}

add_action('wp_ajax_rssplitajaxoptimizationsocial', 'process_chunk_ajax_request_in_social_rewardsystem');

function process_chunk_ajax_request_in_social_rewardsystem() {
    if (isset($_POST['ids'])) {
        $products = $_POST['ids'];
        foreach ($products as $product) {

//  if ($checkproduct->is_type('simple') || ($checkproduct->is_type('subscription'))) {
            if ($_POST['enabledisablereward'] == '1') {
                update_post_meta($product, '_socialrewardsystemcheckboxvalue', 'yes');
            } else {
                update_post_meta($product, '_socialrewardsystemcheckboxvalue', 'no');
            }

            update_post_meta($product, '_social_rewardsystem_options_facebook', $_POST['rewardtypefacebook']);
            update_post_meta($product, '_socialrewardsystempoints_facebook', $_POST['facebookrewardpoints']);
            update_post_meta($product, '_socialrewardsystempercent_facebook', $_POST['facebookrewardpercent']);

            update_post_meta($product, '_social_rewardsystem_options_twitter', $_POST['rewardtypetwitter']);
            update_post_meta($product, '_socialrewardsystempoints_twitter', $_POST['twitterrewardpoints']);
            update_post_meta($product, '_socialrewardsystempercent_twitter', $_POST['twitterrewardpercent']);

            update_post_meta($product, '_social_rewardsystem_options_google', $_POST['rewardtypegoogle']);
            update_post_meta($product, '_socialrewardsystempoints_google', $_POST['googlerewardpoints']);
            update_post_meta($product, '_socialrewardsystempercent_google', $_POST['googlerewardpercent']);

// }
        }
    }
    exit();
}

function check_trigger_button_rewardsystem() {
    ?>
    <script type='text/javascript'>
        jQuery(document).ready(function () {
            jQuery('.rs_sumo_reward_button').click(function () {
                jQuery('.gif_rs_sumo_reward_button').css('display', 'inline-block');
                var whichproduct = jQuery('#rs_which_product_selection').val();
                var enabledisablereward = jQuery('#rs_local_enable_disable_reward').val();
                var selectparticularproducts = jQuery('#rs_select_particular_products').val();
                var selectedcategories = jQuery('#rs_select_particular_categories').val();
                var rewardtype = jQuery('#rs_local_reward_type').val();
                var rewardpoints = jQuery('#rs_local_reward_points').val();
                var rewardpercent = jQuery('#rs_local_reward_percent').val();
                var referralrewardtype = jQuery('#rs_local_referral_reward_type').val();
                var referralrewardpoint = jQuery('#rs_local_referral_reward_point').val();
                var referralrewardpercent = jQuery('#rs_local_referral_reward_percent').val();
                jQuery(this).attr('data-clicked', '1');
                var dataclicked = jQuery(this).attr('data-clicked');
                var dataparam = ({
                    action: 'previousproductvalue',
                    proceedanyway: dataclicked,
                    whichproduct: whichproduct,
                    enabledisablereward: enabledisablereward,
                    selectedproducts: selectparticularproducts,
                    selectedcategories: selectedcategories,
                    rewardtype: rewardtype,
                    rewardpoints: rewardpoints,
                    rewardpercent: rewardpercent,
                    referralrewardtype: referralrewardtype,
                    referralrewardpoint: referralrewardpoint,
                    referralrewardpercent: referralrewardpercent,
                });
                function getData(id) {
                    return jQuery.ajax({
                        type: 'POST',
                        url: "<?php echo admin_url('admin-ajax.php'); ?>",
                        data: ({action: 'rssplitajaxoptimization', ids: id, enabledisablereward: enabledisablereward,
                            selectedproducts: selectparticularproducts,
                            selectedcategories: selectedcategories,
                            rewardtype: rewardtype,
                            rewardpoints: rewardpoints,
                            rewardpercent: rewardpercent,
                            referralrewardtype: referralrewardtype,
                            referralrewardpoint: referralrewardpoint,
                            referralrewardpercent: referralrewardpercent, }),
                        success: function (response) {
                            console.log(response);
                        },
                        dataType: 'json',
                        async: false
                    });
                }
                jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                        function (response) {
                            console.log(response);
                            if (response !== 'success') {
                                var j = 1;
                                var i, j, temparray, chunk = 10;
                                for (i = 0, j = response.length; i < j; i += chunk) {
                                    temparray = response.slice(i, i + chunk);
                                    //console.log(temparray.length);
                                    getData(temparray);


                                    //
                                }
                                jQuery.when(getData()).done(function (a1) {
                                    console.log('Ajax Done Successfully');
                                    jQuery('.submit .button-primary').trigger('click');
                                });
                            } else {
                                var newresponse = response.replace(/\s/g, '');
                                if (newresponse === 'success') {
                                    jQuery('.submit .button-primary').trigger('click');
                                }
                            }
                        }, 'json');
                return false;
                //                jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                //                        function (response) {
                //                            var newresponse = response.replace(/\s/g, '');
                //                            if (newresponse === 'success') {
                //                                jQuery('.submit .button-primary').trigger('click');
                //                            }
                //                        });
                //                return false;
            });
            jQuery('.rs_sumo_reward_button_social').click(function () {
                jQuery('.gif_rs_sumo_reward_button_social').css('display', 'inline-block');
                var whichproduct = jQuery('#rs_which_social_product_selection').val();
                var enabledisablereward = jQuery('#rs_local_enable_disable_social_reward').val();
                var selectparticularproducts = jQuery('#rs_select_particular_social_products').val();
                var selectedcategories = jQuery('#rs_select_particular_social_categories').val();
                var rewardtypefacebook = jQuery('#rs_local_reward_type_for_facebook').val();
                var facebookrewardpoints = jQuery('#rs_local_reward_points_facebook').val();
                var facebookrewardpercent = jQuery('#rs_local_reward_percent_facebook').val();
                var rewardtypetwitter = jQuery('#rs_local_reward_type_for_twitter').val();
                var twitterrewardpoints = jQuery('#rs_local_reward_points_twitter').val();
                var twitterrewardpercent = jQuery('#rs_local_reward_percent_twitter').val();
                var rewardtypegoogle = jQuery('#rs_local_reward_type_for_google').val();
                var googlerewardpoints = jQuery('#rs_local_reward_points_google').val();
                var googlerewardpercent = jQuery('#rs_local_reward_percent_google').val();
                jQuery(this).attr('data-clicked', '1');
                var dataclicked = jQuery(this).attr('data-clicked');
                var dataparam = ({
                    action: 'previoussocialproductvalue',
                    proceedanyway: dataclicked,
                    whichproduct: whichproduct,
                    enabledisablereward: enabledisablereward,
                    selectedproducts: selectparticularproducts,
                    selectedcategories: selectedcategories,
                    rewardtypefacebook: rewardtypefacebook,
                    facebookrewardpoints: facebookrewardpoints,
                    facebookrewardpercent: facebookrewardpercent,
                    rewardtypetwitter: rewardtypetwitter,
                    twitterrewardpoints: twitterrewardpoints,
                    twitterrewardpercent: twitterrewardpercent,
                    rewardtypegoogle: rewardtypegoogle,
                    googlerewardpoints: googlerewardpoints,
                    googlerewardpercent: googlerewardpercent,
                });
                function getDataSocial(id) {
                    return jQuery.ajax({
                        type: 'POST',
                        url: "<?php echo admin_url('admin-ajax.php'); ?>",
                        data: ({action: 'rssplitajaxoptimizationsocial', ids: id, enabledisablereward: enabledisablereward,
                            selectedproducts: selectparticularproducts,
                            selectedcategories: selectedcategories,
                            rewardtypefacebook: rewardtypefacebook,
                            facebookrewardpoints: facebookrewardpoints,
                            facebookrewardpercent: facebookrewardpercent,
                            rewardtypetwitter: rewardtypetwitter,
                            twitterrewardpoints: twitterrewardpoints,
                            twitterrewardpercent: twitterrewardpercent,
                            rewardtypegoogle: rewardtypegoogle,
                            googlerewardpoints: googlerewardpoints,
                            googlerewardpercent: googlerewardpercent, }),
                        success: function (response) {
                            console.log(response);
                        },
                        dataType: 'json',
                        async: false
                    });
                }
                jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                        function (response) {
                            console.log(response);
                            if (response !== 'success') {
                                var j = 1;
                                var i, j, temparray, chunk = 10;
                                for (i = 0, j = response.length; i < j; i += chunk) {
                                    temparray = response.slice(i, i + chunk);
                                    //console.log(temparray.length);
                                    getDataSocial(temparray);
                                }
                                jQuery.when(getDataSocial()).done(function (a1) {
                                    console.log('Ajax Done Successfully');
                                    jQuery('.submit .button-primary').trigger('click');
                                });
                            } else {
                                var newresponse = response.replace(/\s/g, '');
                                if (newresponse === 'success') {
                                    jQuery('.submit .button-primary').trigger('click');
                                }
                            }
                        }, 'json');
                return false;
            });
            jQuery('.rs_sumo_undo_reward').click(function () {
                jQuery(this).attr('data-clicked', '0');
                var dataclicked = jQuery(this).attr('data-clicked');
                var dataparam = ({
                    action: 'previousproductvalue',
                    proceedanyway: dataclicked,
                });
                jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                        function (response) {
                            var newresponse = response.replace(/\s/g, '');
                            if (newresponse === 'success') {
                                jQuery('.rs_sumo_rewards').fadeIn();
                                jQuery('.rs_sumo_rewards').html('Successfully Disabled from Existing Products');
                                jQuery('.rs_sumo_rewards').fadeOut(5000);
                            }
                        });
                return false;
            });
        });
    </script>
    <?php
}

add_action('admin_head', 'check_trigger_button_rewardsystem');

function get_ajax_request_for_previous_product() {
    global $woocommerce;
    global $post;
    if (isset($_POST['proceedanyway'])) {
        if ($_POST['proceedanyway'] == '1') {
            if ($_POST['whichproduct'] == '1') {
                $args = array('post_type' => 'product', 'posts_per_page' => '-1', 'post_status' => 'publish', 'fields' => 'ids', 'cache_results' => false);
                $products = get_posts($args);
                //var_dump($products);
                echo json_encode($products);
            } elseif ($_POST['whichproduct'] == '2') {
                if (!is_array($_POST['selectedproducts'])) {
                    $_POST['selectedproducts'] = explode(',', $_POST['selectedproducts']);
                }
                if (is_array($_POST['selectedproducts'])) {

                    foreach ($_POST['selectedproducts']as $particularpost) {
                        $checkprod = get_product($particularpost);
                        if ($checkprod->is_type('simple') || ($checkprod->is_type('subscription'))) {
                            if ($_POST['enabledisablereward'] == '1') {
                                update_post_meta($particularpost, '_rewardsystemcheckboxvalue', 'yes');
                            } else {
                                update_post_meta($particularpost, '_rewardsystemcheckboxvalue', 'no');
                            }
                            update_post_meta($particularpost, '_rewardsystem_options', $_POST['rewardtype']);
                            update_post_meta($particularpost, '_rewardsystempoints', $_POST['rewardpoints']);
                            update_post_meta($particularpost, '_rewardsystempercent', $_POST['rewardpercent']);
                            update_post_meta($particularpost, '_referral_rewardsystem_options', $_POST['referralrewardtype']);
                            update_post_meta($particularpost, '_referralrewardsystempoints', $_POST['referralrewardpoint']);
                            update_post_meta($particularpost, '_referralrewardsystempercent', $_POST['referralrewardpercent']);
                        } else {
                            update_post_meta($particularpost, '_enable_reward_points', $_POST['enabledisablereward']);
                            update_post_meta($particularpost, '_select_reward_rule', $_POST['rewardtype']);
                            update_post_meta($particularpost, '_reward_points', $_POST['rewardpoints']);
                            update_post_meta($particularpost, '_reward_percent', $_POST['rewardpercent']);
                            update_post_meta($particularpost, '_select_referral_reward_rule', $_POST['referralrewardtype']);
                            update_post_meta($particularpost, '_referral_reward_points', $_POST['referralrewardpoint']);
                            update_post_meta($particularpost, '_referral_reward_percent', $_POST['referralrewardpercent']);
                        }
                    }
                }
                echo json_encode("success");
            } elseif ($_POST['whichproduct'] == '3') {
                $allcategories = get_terms('product_cat');
                if (is_array($allcategories)) {
                    foreach ($allcategories as $mycategory) {
                        if ($_POST['enabledisablereward'] == '1') {
                            update_woocommerce_term_meta($mycategory->term_id, 'enable_reward_system_category', 'yes');
                        } else {
                            update_woocommerce_term_meta($mycategory->term_id, 'enable_reward_system_category', 'no');
                        }
                        update_woocommerce_term_meta($mycategory->term_id, 'enable_rs_rule', $_POST['rewardtype']);
                        update_woocommerce_term_meta($mycategory->term_id, 'rs_category_points', $_POST['rewardpoints']);
                        update_woocommerce_term_meta($mycategory->term_id, 'rs_category_percent', $_POST['rewardpercent']);

                        update_woocommerce_term_meta($mycategory->term_id, 'referral_enable_rs_rule', $_POST['referralrewardtype']);
                        update_woocommerce_term_meta($mycategory->term_id, 'referral_rs_category_points', $_POST['referralrewardpoint']);
                        update_woocommerce_term_meta($mycategory->term_id, 'referral_rs_category_percent', $_POST['referralrewardpercent']);
                    }
                }
                echo json_encode("success");
            } else {
                $mycategorylist = $_POST['selectedcategories'];
                if (is_array($mycategorylist)) {
                    foreach ($mycategorylist as $eachlist) {
                        if ($_POST['enabledisablereward'] == '1') {
                            update_woocommerce_term_meta($eachlist, 'enable_reward_system_category', 'yes');
                        } else {
                            update_woocommerce_term_meta($eachlist, 'enable_reward_system_category', 'no');
                        }
                        update_woocommerce_term_meta($eachlist, 'enable_rs_rule', $_POST['rewardtype']);
                        update_woocommerce_term_meta($eachlist, 'rs_category_points', $_POST['rewardpoints']);
                        update_woocommerce_term_meta($eachlist, 'rs_category_percent', $_POST['rewardpercent']);

                        update_woocommerce_term_meta($eachlist, 'referral_enable_rs_rule', $_POST['referralrewardtype']);
                        update_woocommerce_term_meta($eachlist, 'referral_rs_category_points', $_POST['referralrewardpoint']);
                        update_woocommerce_term_meta($eachlist, 'referral_rs_category_percent', $_POST['referralrewardpercent']);
                    }
                }
                echo json_encode("success");
            }
        }
        if ($_POST['proceedanyway'] == '0') {
            $args = array('post_type' => 'product', 'posts_per_page' => '-1', 'post_status' => 'publish', 'fields' => 'ids', 'cache_results' => false);
            $products = get_posts($args);
            foreach ($products as $product) {
                $checkproducts = get_product($product);
                if ($checkproducts->is_type('simple') || ($checkproducts->is_type('subscription'))) {
                    update_post_meta($product, '_rewardsystemcheckboxvalue', 'no');
                } elseif ($checkproducts->is_type('variable') || ($checkproducts->is_type('variable-subscription'))) {
                    if (is_array($checkproducts->get_available_variations())) {
                        foreach ($checkproducts->get_available_variations() as $getvariation) {
                            update_post_meta($getvariation['variation_id'], '_enable_reward_points', '2');
                        }
                    }
                }
            }
            echo json_encode("success");
        }
        exit();
    }
}

function get_ajax_request_for_previous_social_product() {
    global $woocommerce;
    global $post;
    if (isset($_POST['proceedanyway'])) {
        if ($_POST['proceedanyway'] == '1') {
            if ($_POST['whichproduct'] == '1') {
                $args = array('post_type' => 'product', 'posts_per_page' => '-1', 'post_status' => 'publish', 'fields' => 'ids', 'cache_results' => false);
                $products = get_posts($args);
                //var_dump($products);
                echo json_encode($products);
            } elseif ($_POST['whichproduct'] == '2') {
                if (!is_array($_POST['selectedproducts'])) {
                    $_POST['selectedproducts'] = explode(',', $_POST['selectedproducts']);
                }
                if (is_array($_POST['selectedproducts'])) {
                    foreach ($_POST['selectedproducts']as $particularpost) {
                        $checkprod = get_product($particularpost);
//if ($checkprod->is_type('simple') || ($checkprod->is_type('subscription'))) {
                        if ($_POST['enabledisablereward'] == '1') {
                            update_post_meta($particularpost, '_socialrewardsystemcheckboxvalue', 'yes');
                        } else {
                            update_post_meta($particularpost, '_socialrewardsystemcheckboxvalue', 'no');
                        }
                        update_post_meta($particularpost, '_social_rewardsystem_options_facebook', $_POST['rewardtypefacebook']);
                        update_post_meta($particularpost, '_socialrewardsystempoints_facebook', $_POST['facebookrewardpoints']);
                        update_post_meta($particularpost, '_socialrewardsystempercent_facebook', $_POST['facebookrewardpercent']);

                        update_post_meta($particularpost, '_social_rewardsystem_options_twitter', $_POST['rewardtypetwitter']);
                        update_post_meta($particularpost, '_socialrewardsystempoints_twitter', $_POST['twitterrewardpoints']);
                        update_post_meta($particularpost, '_socialrewardsystempercent_twitter', $_POST['twitterrewardpercent']);

                        update_post_meta($particularpost, '_social_rewardsystem_options_google', $_POST['rewardtypegoogle']);
                        update_post_meta($particularpost, '_socialrewardsystempoints_google', $_POST['googlerewardpoints']);
                        update_post_meta($particularpost, '_socialrewardsystempercent_google', $_POST['googlerewardpercent']);
//}
                    }
                }
                echo json_encode("success");
            } elseif ($_POST['whichproduct'] == '3') {
                $allcategories = get_terms('product_cat');
                if (is_array($allcategories)) {
                    foreach ($allcategories as $mycategory) {
                        if ($_POST['enabledisablereward'] == '1') {
                            update_woocommerce_term_meta($mycategory->term_id, 'enable_social_reward_system_category', 'yes');
                        } else {
                            update_woocommerce_term_meta($mycategory->term_id, 'enable_social_reward_system_category', 'no');
                        }
                        update_woocommerce_term_meta($mycategory->term_id, 'social_facebook_enable_rs_rule', $_POST['rewardtypefacebook']);
                        update_woocommerce_term_meta($mycategory->term_id, 'social_facebook_rs_category_points', $_POST['facebookrewardpoints']);
                        update_woocommerce_term_meta($mycategory->term_id, 'social_facebook_rs_category_percent', $_POST['facebookrewardpercent']);

                        update_woocommerce_term_meta($mycategory->term_id, 'social_twitter_enable_rs_rule', $_POST['rewardtypetwitter']);
                        update_woocommerce_term_meta($mycategory->term_id, 'social_twitter_rs_category_points', $_POST['twitterrewardpoints']);
                        update_woocommerce_term_meta($mycategory->term_id, 'social_twitter_rs_category_percent', $_POST['twitterrewardpercent']);


                        update_woocommerce_term_meta($mycategory->term_id, 'social_google_enable_rs_rule', $_POST['rewardtypegoogle']);
                        update_woocommerce_term_meta($mycategory->term_id, 'social_google_rs_category_points', $_POST['googlerewardpoints']);
                        update_woocommerce_term_meta($mycategory->term_id, 'social_google_rs_category_percent', $_POST['googlerewardpercent']);
                    }
                }
                echo json_encode("success");
            } else {
                $mycategorylist = $_POST['selectedcategories'];
                if (is_array($mycategorylist)) {
                    foreach ($mycategorylist as $eachlist) {
                        if ($_POST['enabledisablereward'] == '1') {
                            update_woocommerce_term_meta($eachlist, 'enable_social_reward_system_category', 'yes');
                        } else {
                            update_woocommerce_term_meta($eachlist, 'enable_social_reward_system_category', 'no');
                        }
                        update_woocommerce_term_meta($eachlist, 'social_facebook_enable_rs_rule', $_POST['rewardtypefacebook']);
                        update_woocommerce_term_meta($eachlist, 'social_facebook_rs_category_points', $_POST['facebookrewardpoints']);
                        update_woocommerce_term_meta($eachlist, 'social_facebook_rs_category_percent', $_POST['facebookrewardpercent']);

                        update_woocommerce_term_meta($eachlist, 'social_twitter_enable_rs_rule', $_POST['rewardtypetwitter']);
                        update_woocommerce_term_meta($eachlist, 'social_twitter_rs_category_points', $_POST['twitterrewardpoints']);
                        update_woocommerce_term_meta($eachlist, 'social_twitter_rs_category_percent', $_POST['twitterrewardpercent']);


                        update_woocommerce_term_meta($eachlist, 'social_google_enable_rs_rule', $_POST['rewardtypegoogle']);
                        update_woocommerce_term_meta($eachlist, 'social_google_rs_category_points', $_POST['googlerewardpoints']);
                        update_woocommerce_term_meta($eachlist, 'social_google_rs_category_percent', $_POST['googlerewardpercent']);
                    }
                }
                echo json_encode("success");
            }
        }
        exit();
    }
}

add_action('wp_ajax_nopriv_previousproductvalue', 'get_ajax_request_for_previous_product');
add_action('wp_ajax_previousproductvalue', 'get_ajax_request_for_previous_product');

add_action('wp_ajax_nopriv_previoussocialproductvalue', 'get_ajax_request_for_previous_social_product');
add_action('wp_ajax_previoussocialproductvalue', 'get_ajax_request_for_previous_social_product');

function get_woocommerce_product_attributes() {
    global $product, $post, $woocommerce;
    $product = get_product($post->ID);
    if ($product->is_type('variable') || ($product->is_type('variable-subscription'))) {
        echo "<pre>";
        print_r($product->get_available_variations());
        echo "</pre>";
        foreach ($product->get_available_variations() as $getvariation) {
            echo $getvariation['variation_id'];
        }
    }
}

function get_woocommerce_upload_field() {
    if (isset($_REQUEST['rs_image_url_upload'])) {
        update_option('rs_image_url_upload', $_POST['rs_image_url_upload']);
    }
}

add_action('admin_head', 'get_woocommerce_upload_field');
if (class_exists('FPReferralSystem')) {

    class FPRewardSystemShortcode extends FPReferralSystem {

        public static function rs_fp_rewardsystem($atts) {
            ob_start();
            extract(shortcode_atts(array(
                'referralbutton' => 'show',
                'referraltable' => 'show',
                            ), $atts));
            if ($referralbutton == 'show') {
                FPReferralSystem::generate_referral_key();
            }
            if ($referraltable == 'show') {
                FPReferralSystem::list_table_array();
            }
            $maincontent = ob_get_clean();
            return $maincontent;
        }

    }

    new FPRewardSystemShortcode();
    add_shortcode('rs_generate_referral', array('FPRewardSystemShortcode', 'rs_fp_rewardsystem'));
}

function checkthisproductorpost() {
    var_dump(get_option('rs_restrict_reward_product_review'));
    var_dump(get_user_meta('2', 'userreviewed' . '505', true));
}

//add_action('admin_head', array('FPRewardSystem', 'test_get_order_discount'));
//Backward Compatibility
function rs_change_single_status_to_multiple() {
    $old_status = get_option("rs_order_status_control");
    //var_dump($old_status);
    if ($old_status != false) {
        if (!is_array($old_status)) {
            $new_status = array($old_status);
            update_option('rs_order_status_control', $new_status);
        }
    }
}

function get_sub_value() {
    if ($_POST['getcurrentuser'] && $_POST['subscribe'] == 'no') {
        update_user_meta($_POST['getcurrentuser'], 'unsub_value', 'no');
        echo "1";
    } else {
        update_user_meta($_POST['getcurrentuser'], 'unsub_value', 'yes');
        echo "2";
    }

    exit();
}

/* For Unsubscribe option in My account Page */

function sub_option_in_my_account_page() {
    if ((get_option('rs_show_hide_your_subscribe_link')) == '1') {
        ?>
        <br><h3><input type="checkbox" name="subscribeoption" id="subscribeoption" value="no" <?php checked("no", get_user_meta(get_current_user_id(), 'unsub_value', true)); ?>/>    Subscribe Here To Receive Email</h3>
        <?php
    }
}

function default_sub_option() {
    foreach (get_users() as $myuser) {

        add_user_meta($myuser->ID, 'unsub_value', 'no');
    }
}

function default_sub_option_for_newuser($user_id) {
    add_user_meta($user_id, 'unsub_value', 'no');
}

function get_the_checkboxvalue_from_myaccount_page() {
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('#subscribeoption').click(function () {
                var subscribe = jQuery('#subscribeoption').is(':checked') ? 'no' : 'yes';
                var getcurrentuser =<?php echo get_current_user_id(); ?>
                //alert(getcurrentuser);
                var data = {
                    action: 'subscribevalue',
                    subscribe: subscribe,
                    getcurrentuser: getcurrentuser,
                    //dataclicked:dataclicked,
                };
                jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", data,
                        function (response) {
                            //var newresponse = response.replace(/\s/g, '');
                            if (response === '2') {
                                alert("Successfully Unsubscribed...");
                            } else {
                                alert("Successfully Subscribed...");
                            }
                        });
                //  return false;
            });
        });
    </script>

    <?php
}

function add_meta_box_for_earned() {
    add_meta_box('order_earned_points', 'Earned Point and Redeem Points For Current Order', 'add_meta_box_to_earned_points', 'shop_order', 'normal', 'low');
}

function add_meta_box_to_earned_points($order) {

    $order = $_GET['post'];
    $totalearnedvalue = "";
    $earned_total = get_post_meta($order, 'rs_total_earned_points', true);

    if (get_option('rs_enable_msg_for_earned_points') == 'yes') {
        if (is_array($earned_total)) {
            foreach ($earned_total as $key => $value) {
                $totalearnedvalue+=$value;
            }
        }
        $msgforearnedpoints = get_option('rs_msg_for_earned_points');
        $replacemsgforearnedpoints = str_replace('[earnedpoints]', $totalearnedvalue != "" ? $totalearnedvalue : "0", $msgforearnedpoints);
    } else {
        $msgforearnedpoints = get_option('rs_msg_for_earned_points');
        $replacemsgforearnedpoints = str_replace('[earnedpoints]', $totalearnedvalue != "" ? $totalearnedvalue : "0", $msgforearnedpoints);
    }

    $totalredeemvalue = "";
    $redeem_total = get_post_meta($order, 'rs_total_redeem_points', true);

    if (get_option('rs_enable_msg_for_redeem_points') == 'yes') {
        if (is_array($redeem_total)) {
            foreach ($redeem_total as $key => $value) {
                $totalredeemvalue+=$value;
            }
        }
        $msgforredeempoints = get_option('rs_msg_for_redeem_points');
        $replacemsgforredeempoints = str_replace('[redeempoints]', $totalredeemvalue != "" ? $totalredeemvalue : "0", $msgforredeempoints);
    } else {
        $msgforredeempoints = get_option('rs_msg_for_redeem_points');
        $replacemsgforredeempoints = str_replace('[redeempoints]', $totalredeemvalue != "" ? $totalredeemvalue : "0", $msgforredeempoints);
    }
    ?>
    <table width="100%" style=" border-radius: 10px; border-style: solid; border-color: #dfdfdf;">
        <tr><td style="text-align:center; background-color:#F1F1F1"><h3>Earned Points</h3></td><td style="text-align:center;background-color:#F1F1F1"><h3>Redeem Points</h3></td></tr>
        <tr><td style="text-align:center"><?php echo $replacemsgforearnedpoints; ?></td><td style="text-align:center"><?php echo $replacemsgforredeempoints; ?></td></tr>
    </table>


    <?php
}

add_action('add_meta_boxes', 'add_meta_box_for_earned');

add_action('user_register', 'default_sub_option_for_newuser');
add_action('wp_ajax_subscribevalue', 'get_sub_value');
add_action('wp_head', 'get_the_checkboxvalue_from_myaccount_page');
add_action('woocommerce_before_my_account', 'sub_option_in_my_account_page');


register_activation_hook(__FILE__, 'rs_change_single_status_to_multiple');
register_activation_hook(__FILE__, 'default_sub_option');
register_activation_hook(__FILE__, array('FPRewardSystemEncashTab', 'encash_reward_points_submitted_data'));
