<?php
/*
Plugin Name: Shipping Rewards
Plugin URI: 
Description: It enables to use rewards towards shipping cost as well
Author: Agile Solutions PK
Version: 2.2
Author URI: http://agilesolutionspk.com
*/

if ( !class_exists( 'Shipping_Rewards' )){
	class Shipping_Rewards{
		
		function __construct() {
			add_action( 'wp_enqueue_scripts', array(&$this, 'init'), 99 );
			add_action( 'admin_init', array(&$this, 'admin_init'), 99 );
			add_action( 'admin_enqueue_scripts', array(&$this, 'admin_init'), 99 );
			add_action('admin_enqueue_scripts', array(&$this, 'scripts_method') );
			add_action('wp_enqueue_scripts', array(&$this, 'frontend_scripts') );
			add_filter('woocommerce_calculated_total', array(&$this, 'wc_calc_tot'),33,2);
			add_action('admin_menu', array(&$this, 'register_reward_system_sub_menu2'));
		}
		
		function register_reward_system_sub_menu2(){
			add_submenu_page('woocommerce', 'Export User Reward Points', 'Export User Reward Points', 'manage_options', 'aspk_reward_points', array(&$this, 'aspk_reward_points'));
		}
		
		function get_current_month_orders($month_year){
			global $wpdb;
			
			$st_dt = $month_year.'-01 00:00:00';
			$date = new DateTime($st_dt);
			$st_dt = $date->format('Y-m-d H:i:s');
			$end_dt = $month_year.'-31 23:59:59';
			$date = new DateTime($end_dt);
			$end_dt = $date->format('Y-m-d H:i:s');
			$query = "SELECT ID FROM {$wpdb->prefix}posts where post_date >= '{$st_dt}' AND post_date <='{$end_dt}' AND post_type = 'shop_order' AND post_status = 'wc-completed'"; 
			$results = $wpdb->get_col($query);
			return $results;
		}
		
		function get_user_ids_via_oid($order_ids){
			$uids = array();
			foreach($order_ids as $order_id){
				$order = new WC_Order( $order_id ); 
				$user_id = $order->user_id;
				$uids[] = $user_id;
			}
			return $uids;
		}
		
		function genterate_redeem_poinst_csv($uids,$delimater,$file_name){
			ob_end_clean();
			header("Content-type: application/octet-stream");
			header("Content-disposition: attachment; filename=".$file_name);
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: no-cache');
			header('Pragma: public');
			echo "Username,Points Issued,Points Redeemed,Current Points\r\n";
			if($uids){
				$uids = array_unique($uids);
				foreach($uids as $uid){
					$logs = get_user_meta($uid, '_my_points_log', true);
					$nickname = get_user_meta($uid, 'nickname', true);
					$points_redeemed = 0;
					foreach($logs as $log){
						if($log){
							if(isset($log['points_earned_order'])){
								$points_issued = $log['points_earned_order'];
							}
							$points_redeemed += $log['points_redeemed'];
							$totalpoints = $points_issued - $points_redeemed;
						}
					}
					echo $nickname.' '.$delimater.$points_issued.' '.$delimater.$points_redeemed.' '.$delimater.$totalpoints.PHP_EOL;
				}
			}
			exit;
		}
		
		function aspk_reward_points(){
			if(isset($_POST['export_redeem_points'])){
				$month_year = $_POST['month_year'];
				$order_ids = $this->get_current_month_orders($month_year);
				if($order_ids){
					$uids = $this->get_user_ids_via_oid($order_ids);
					$delimater = ",";
					$file_name = 'Redeem_Points_'.date('Y-m-d-H:i:s').'.csv';
					$this->genterate_redeem_poinst_csv($uids,$delimater,$file_name);
				}else{
					$no_report = "No Report Available";
				}
			} ?>
			<div style="float:left;clear:left;padding:2em;background-color:#FFFFFF;margin-top: 1em;"> 
				<?php
					if(isset($no_report)){
						?>
						<div id="error_message" class="error" style="float:left;clear:left;">
							<?php echo $no_report; ?>
						</div>
						<?php
					}
					?>
				<div style="float:left;clear:left;">
					<h3> Export User Reward Points </h3>
				</div>
				<div style="float:left;clear:left;">
					<form action="" method="post">
						<div style="clear:left;">
							<div style="float:left;width: 7em;">Select Month</div>
							<div style="margin-left:1em;float:left;">
								<select required style="width: 20em;" name="month_year">
									<option value="">--select--</option><?php
									for($dt = date('m'); $dt > 0; $dt--){ 
										$st_dt = date('Y').'-'.$dt;
										$date = new DateTime($st_dt);?>
										<option value="<?php echo $date->format('Y-m');?>"><?php echo $date->format('Y-M'); ?></option>
																														
									<?php } 
									$end = 12 - date('m');
									for($dt = 12; $dt >= $end; $dt--){ 
										$st_dt = date('Y') - 1 .'-'.$dt;
										$date = new DateTime($st_dt);?>
										<option value="<?php echo $date->format('Y-m');?>"><?php echo $date->format('Y-M'); ?></option>																			
									<?php } ?>
									
								</select>
							</div>
						</div>
						<div style="clear:left;">
							<input type="submit" name="export_redeem_points" value="Generate" />
						</div>
					</form>
				</div>
			</div> 
			<script>
				setTimeout(function(){ 
					jQuery('#error_message').hide();
				}, 8000);
			</script>
			<?php
		}
		

		function wc_calc_tot($tot, $ct){
			
			$this->show_reward_points_redeemed();
			$requested_amt = $this->get_coupon_value();
			
			$available_for_shipping = $requested_amt - $this->get_applied_amount();
			
			$new_shipping = $ct->shipping_total;
			
			if($available_for_shipping > 0){
				if($available_for_shipping > $ct->shipping_total){
					$new_shipping = 0;
				}else{
					$new_shipping = $ct->shipping_total - $available_for_shipping;
				}
			}
			
			$applied_amount = $this->get_applied_amount();
			if($applied_amount == 0){
				//$newamount = $applied_amount + ($ct->shipping_total - $new_shipping);
				$new_shipping = $ct->shipping_total;
			}

			$newtot = round( $ct->subtotal + $ct->tax_total + $ct->shipping_tax_total + $new_shipping + $ct->fee_total - $applied_amount , $ct->dp );
			
			return $newtot;
		}
		
		
		function show_reward_points_redeemed(){
			global $woocommerce;
			
			$requested_amt = $this->get_coupon_value();
			
			$available_for_shipping = $requested_amt - $this->get_applied_amount();
			
			$new_shipping = $woocommerce->cart->shipping_total;
			
			if($available_for_shipping > 0){
				if($available_for_shipping > $woocommerce->cart->shipping_total){
					$new_shipping = 0;
				}else{
					$new_shipping = $woocommerce->cart->shipping_total - $available_for_shipping;
				}
			}
			
			$applied_amount = $this->get_applied_amount();
			$newamount = $applied_amount + ($woocommerce->cart->shipping_total - $new_shipping);
			$this->set_applied_amount($newamount);

		}
		
		
		function get_coupon_id(){
			$user_ID = get_current_user_id();
			$getinfousernickname = get_user_by('id', $user_ID);
			$couponcodeuserlogin = $getinfousernickname->user_login;

			$usernickname = 'sumo_' . strtolower("$couponcodeuserlogin");
			
			return $usernickname;
		}
		
		function get_coupon_pid(){
			$user_ID = get_current_user_id();
			$getcouponid = get_user_meta($user_ID, 'redeemcouponids', true);
			
			return $getcouponid;
		}
		
		function get_applied_amount(){
			global $woocommerce;
			
			$usernickname = $this->get_coupon_id();
			
			$total = 0;
			if (isset($woocommerce->cart->coupon_discount_amounts["$usernickname"])) {
				$total = $woocommerce->cart->coupon_discount_amounts[$usernickname];
			}
			
			return $total;
		}
		
		function set_applied_amount($amount){
			global $woocommerce;
			
			$usernickname = $this->get_coupon_id();
			
			if (isset($woocommerce->cart->coupon_discount_amounts["$usernickname"])) {
				$woocommerce->cart->coupon_discount_amounts[$usernickname] = $amount;
				
			}

		}
		
		function get_balance(){
			
		}
		
		
		function get_coupon_value(){
			
			$cid = $this->get_coupon_pid();
			$currentamount = get_post_meta($cid, 'coupon_amount', true);

			return $currentamount;
		}
		
		
		function scripts_method(){
	
		}
		
		function frontend_scripts(){

		}
		
		function init(){
			global $woocommerce;
			
			$requested_amt = $this->get_coupon_value();
			
			$available_for_shipping = $requested_amt - $this->get_applied_amount();
			
			$new_shipping = $woocommerce->cart->shipping_total;
			
			if($available_for_shipping > 0){
				if($available_for_shipping > $woocommerce->cart->shipping_total){
					$new_shipping = 0;
				}else{
					$new_shipping = $woocommerce->cart->shipping_total - $available_for_shipping;
				}
			}
			
			$applied_amount = $this->get_applied_amount();
			$newamount = $applied_amount + ($woocommerce->cart->shipping_total - $new_shipping);
			$this->set_applied_amount($newamount);
			
		}
		
		function admin_init(){
			$this->init();
		}
		
	} //class ends
} //class exists ends
new Shipping_Rewards();

?>
